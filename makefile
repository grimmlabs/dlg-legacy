Obs = LinkLib Library RexxLib Exec Handler Fido Install Install-Debug

.PHONY: $(Obs) Build

Build: $(Obs)

LinkLib:
	@echo "Link Libs"
	@ImportEnv
	@make -$(MAKEFLAGS) -Csource/LinkLibs

Library:
	@echo "DLG.LIBRARY"
	@echo "  Generic version"
	@make -$(MAKEFLAGS) DLGCPU=ANY -Csource/library
	@echo "  030 Version"
	@make -$(MAKEFLAGS) DLGCPU=68030 -C source/library

RexxLib:
	@echo "DLGREXX.LIBRARY"
	@echo "  Generic version"
	@make -$(MAKEFLAGS) DLGCPU=ANY -Csource/DLGRexxLib
	@echo "  030 Version"
	@make -$(MAKEFLAGS) DLGCPU=68030 -Csource/DLGRexxLib

Exec:
	@echo "Executables"
	@ImportEnv
	@make -$(MAKEFLAGS) -Csource -fmakefile.exec

Handler:
	@echo "Handler"
	@echo "  Normal"
	@make -$(MAKEFLAGS) MAKE_DEBUG_CODE=0 -Csource/handler
	@echo "  Debug"
	@make -$(MAKEFLAGS) MAKE_DEBUG_CODE=1 -Csource/handler

Fido:
	@echo "Fido"
	@ImportEnv
	@make -$(MAKEFLAGS) -Csource/Fido OPT=NoOpt

Install:
	@echo "Installer"
	@ImportEnv
	@make -$(MAKEFLAGS) -Csource/Install

Install-Debug:
	@echo "Debug Installer"
	@ImportEnv
	@make -$(MAKEFLAGS) -Csource/Install-Debug

Force:
	@touch dlgsrc:include/private/Touch.h

