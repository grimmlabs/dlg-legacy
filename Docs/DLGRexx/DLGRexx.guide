@database 0753fe60-0
@master dlgsrc:Docs/DLGRexx/DLGRexx.guide
@$VER: 1.0
@remark Created with Heddley v1.1 (c) Edd Dumbill 1994

@node "Main" "DLGRexx.library User Guide"

 dlgrexx.library @{"�1999" link "Copyright" 0} by Digerati Dreams
 ========================================

DLGRexx.library is a companion library to DLG.library -- it does not
duplicate the functions of the main library, but rather provides the Arexx
programmer an interface to the main library's functions.

Documentation for functions is provided in AmigaGuide format, arranged by
functional area. For example, resource management functions are grouped
together, port functions are grouped together, and so on.

A number of functions are not supported. @{"Read�here" link "Unsupported" 0} for more information.

 dlgrexx.library
  |
  +-- Resource Management
       |
       +- @{"Port�Control�Functions" link "DLGRexx-Port.guide/main" 0}
       |
       +- @{"File�and�Message�Area�Control�Functions" link "DLGRexx-Area.guide/main" 0}
@endnode

@node "Unsupported" "Unsupported Functions"
 Unsupported Functions
 =====================

Exists()

   This function already exists in the standard Arexx library code. No
   need to duplicate it.
@endnode

@node "Copyright" "Copyright Information"
 DLG, which includes dlgrexx.library, is copyright �1995-1999 by Digerati
Dreams. You may not distribute this program without direct permission of
Digerati Dreams. Permission is granted to distribute this upgrade package
via electronic means as long as it remains unaltered.

Official information and updates can be found at the main DLG support web
site: http://www.ald.net/dlg.  An official support echo, DLG_SUPPORT, is
maintained on the FidoNet web site, and the author can generally be
contacted there.

Until August 1999, you can email dlg@cts.com as well. After that date, we're
moving, but when we have our new accounts set up we will ensure that
information is disseminated.
@endnode

