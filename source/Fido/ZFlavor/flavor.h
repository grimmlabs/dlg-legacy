/* flavor.h */

#define flavor_CRASH	(1<<0)
#define flavor_NORMAL	(1<<1)
#define flavor_DIRECT	(1<<2)
#define flavor_HOLD	(1<<3)
#define flavor_NONE	(1<<4)
#define flavor_ANY	(1<<5)

#define flavor_PKTFILE	(1<<6)

#define flavor_FLOWFILE	(1<<7)

#define flavor_SUNDAY	(1<<8)
#define flavor_MONDAY	(1<<9)
#define flavor_TUESDAY	(1<<10)
#define flavor_WEDNESDAY (1<<11)
#define flavor_THURSDAY	(1<<12)
#define flavor_FRIDAY	(1<<13)
#define flavor_SATURDAY	(1<<14)

#define flavor_BUNDLE	(1<<15)


#define	flavor_CLO	(flavor_FLOWFILE | flavor_CRASH)
#define	flavor_FLO	(flavor_FLOWFILE | flavor_NORMAL)
#define	flavor_DLO	(flavor_FLOWFILE | flavor_DIRECT)
#define	flavor_HLO	(flavor_FLOWFILE | flavor_HOLD)
#define	flavor_NLO	(flavor_FLOWFILE | flavor_NONE)

#define	flavor_CUT	(flavor_PKTFILE | flavor_CRASH)
#define	flavor_OUT	(flavor_PKTFILE | flavor_NORMAL)
#define	flavor_DUT	(flavor_PKTFILE | flavor_DIRECT)
#define	flavor_HUT	(flavor_PKTFILE | flavor_HOLD)
#define	flavor_NUT	(flavor_PKTFILE | flavor_NONE)

#define flavor_SU	(flavor_BUNDLE | flavor_SUNDAY)
#define flavor_MO	(flavor_BUNDLE | flavor_MONDAY)
#define flavor_TU	(flavor_BUNDLE | flavor_TUESDAY)
#define flavor_WE	(flavor_BUNDLE | flavor_WEDNESDAY)
#define flavor_TH	(flavor_BUNDLE | flavor_THURSDAY)
#define flavor_FR	(flavor_BUNDLE | flavor_FRIDAY)
#define flavor_SA	(flavor_BUNDLE | flavor_SATURDAY)

