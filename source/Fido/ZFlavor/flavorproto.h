/* FILE: flavor.c */

extern int EvaluateFilename(char *s,char *p,int *zone,int *net,int *node,int *point,ULONG *type);
extern int AddToWild(char *in,char *out,ULONG type);
extern int mystrsfn(char *in, char *a, char *b, char *c, char *d, char *e);
extern int CreateNewName(char *newname, int zone, int net, int node, int point, ULONG type);
extern int RenameOutbound(char *old, char *new);
extern int CreatePKTName(char *name);
extern ULONG UserType(char *spec, char *pktorflow);
extern int ShowFlags(ULONG flag);
extern int FidoPatternMatch(char *s_in, char *s_pat);
extern int NFComponents(char *in, char *A, char *B, char *C, char *D, char *E);
