#include <exec/types.h>

#include <exec/memory.h>

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <dos.h>


#define DLGMV2 1


#include <proto/dos.h>
#include <proto/exec.h>
#include <proto/dlg.h>

#include <dlg/globalconfig.h>

#include "bundleprotos.h"

#include <libraries/dos.h>

#include <dos/dostags.h>

#include "/zflavor/flavor.h"
#include "/zflavor/flavorproto.h"

#include "/zap_h/zap.h"

#include <pragmas/dlg.h>


#define PROGRAMREVISION "v2.63"
#define PROGRAMNAME  "DLGBundle"
#define _LOGFILE   "LOGS:DLGBundle.LOG"
#define MLSLENGTH 2000
#define MyDEBUG (GCFG->ZBUNDLE_DB)
#define MAXPKTS 1000

#define DEBUG 0

/* ==== zbundle.c ==== */

extern struct PKTS     *packets;
extern unsigned long    numpkts;

extern BPTR fl1;
extern BPTR fl3;

extern BPTR fh2;
extern BPTR fh3;

extern BPTR nilfh;
extern BPTR sout;

extern char logstring[1000];

extern struct Library *DLGBase;


struct PKTS
{
   char *filename;
   char *dest;
   char *fullname;
};
