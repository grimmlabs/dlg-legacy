/* FILE: proc.c */
extern int ExportTheArea(void);
extern int FetchHW(char *path, long areanum);
extern int StoreHW(char *path, long areanum, int hiwater);
/* FILE: r_seenby.c */
extern int PackMsgs(int from, int to);
extern int handlemsg(char *name, char *tagname);
extern int readinmsg(char *name,char **start,int *length);
extern int writeoutmsg(char *name,char *start,int length);
/* FILE: aloop.c */
extern int CheckDLGMAILSTOP(void);
extern int AreaLoop(void);
extern int DeletePassMsgs(char *path);
extern int ExportReport(void);
extern int CheckToExport(char *tagname,long number);
/* FILE: log.c */
extern void Log(char *);
/* FILE: pdqexp.c */
extern int TallyMsgs(int z, int N, int n, int p, int how);
extern int TallyThreshold(int size);
extern int GetPKTPw(char *address, char *password, char *use);
extern int AddressToZone(char *input);
extern int AddressToNet(char *input);
extern int AddressToNode(char *input);
extern int AddressToPoint(char *input);
extern void QuietExit(void);
extern int MyBorrowArea(long number);
extern int UnBorrowArea(long number);
extern int main(int argc, char **argv);
extern int FileToPKT(int partialonly);
extern int SetZTaskPri(void);
extern int LogImported(int imported,int exported,int totnodes,char *path,char *tagname,int classno);
extern int ClearPKTStruct(char *s,int size);
extern int AppendToPKTStruct(char *buffer,int offset,char *string,int length);
