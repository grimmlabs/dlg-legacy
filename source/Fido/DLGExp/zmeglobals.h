
#define LOGFILENAME "LOGS:DLGExp.LOG"
#define PKTSIZELIMIT 250000
#define PKTHEADLENGTH 500
#define MLSLENGTH 2500

extern BPTR PKTfile;

extern char *PKTHEADSTRUCT;

extern char DLGSerial[8];

/*============== from ZME.c ====================*/

extern BOOL WEEXPORTED;

extern BOOL EXPORTLIMITFLAG;

extern struct areas *a;

extern BPTR ZMAILEXPfile;  /*this is opened when execution starts and must be closed anytime the program exits...*/

extern char logstring[3000];


/*==================== arealoop.c ===================*/

extern int EX_THIS_AREA;
extern int EX_NODES_THIS_AREA;

extern char MSGAREAPATH[255];    /* these lines will hold the equivalent */
extern int  MSGAREANUMBER;
extern char MSGAREATAG[255];     /* information in a line of areas.bbs   */
extern BOOL    MSGAREAPASSTHRU;

extern int AREASPROCESSED;
extern int AREASSKIPPED;
extern int MSGSPROCESSED;
extern int OUTBOUNDSCREATED;
extern int PKTSCREATED;

extern struct area *thisarea;

/*============== from packemup.c ==============*/

extern char LOGHELPER[20];

/*============== from buildpackets.c ===========*/

