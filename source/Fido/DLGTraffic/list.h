
struct LIST 
{
  struct LIST *next,*previous;
};

typedef struct LIST List;

struct LISTHEADER 
{
  List *first,*last;
};

typedef struct LISTHEADER ListHdr;

/* FILE: list.c */
void initlist(List *l);
void initlisthdr(ListHdr *lh);
void add_item_first(void *list,void *header);
void add_item_last(void *list,void *header);
void *get_first(void *listhdr);
void *get_last(void *listhdr);
void *get_next(void *list);
void *get_previous(void *list);
void init_list_hdr(void *listhdr);
void init_list(void *list);
