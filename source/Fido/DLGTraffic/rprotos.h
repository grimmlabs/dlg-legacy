/* FILE: rstat.c */
void calcdate(void);
void print_single_header(void);
void print_single_stats(struct echostat *stat);
void print_multiple_stats(void);
void print_multiple_stat(struct echostat *stat,int d,int m, int y, int days);
void print_multiple_header(void);
char *getnamefromnum(struct areas *aa,int num);
struct echostat *get_single_stats(struct areas *aa,char *echoname);
struct echostat *get_multiple_stats(struct area *arearray[],int areanum);
char *mergepath(char *path1,char *path2);
int readstats(struct echostat *e,char *file,char *echoname,int echonum);
void decrement(int *day,int *month,int *year);
void increment(int *day,int *month,int *year);
void sort_activity(struct echostat *stat,int d,int m,int y,int num);
void sort_idle(struct echostat *stat,int d,int m,int y,int num);
void sort_export(struct echostat *stat,int d,int m,int y,int num);
void sort_import(struct echostat *stat,int d,int m,int y,int num);
void sort_local(struct echostat *stat,int d,int m,int y,int num);
void sort_dud(struct echostat *stat,int d,int m,int y,int num);
void sort_erratic(struct echostat *stat,int d,int m,int y,int num);
void sort_nodes(struct echostat *stat,int d,int m,int y,int num);
void swap(struct echostat *s1,struct echostat *s2);
void sort_stats(void);
void print_activity (struct echostat *s,int d,int m,int y,int num);
void print_idle (struct echostat *s,int d,int m,int y,int num);
void print_export (struct echostat *s,int d,int m,int y,int num);
void print_import (struct echostat *s,int d,int m,int y,int num);
void print_local (struct echostat *s,int d,int m,int y,int num);
void print_dud (struct echostat *s,int d,int m,int y,int num);
void print_erratic (struct echostat *s,int d,int m,int y,int num);
void print_nodes (struct echostat *s,int d,int m,int y,int num);
void cleanup(int errornum);
void print_usage(void);
double calc_activity(struct echostat *s,int d,int m,int y,int num);
double calc_idle(struct echostat *s,int d,int m,int y,int num);
double calc_export(struct echostat *s,int d,int m,int y,int num);
double calc_import(struct echostat *s,int d,int m,int y,int num);
double calc_local(struct echostat *s,int d,int m,int y,int num);
double calc_dud(struct echostat *s,int d,int m,int y,int num);
double calc_erratic(struct echostat *s,int d,int m,int y,int num);
double calc_nodes(struct echostat *s,int d,int m,int y,int num);

void print_single_footer(void);
void print_multiple_footer(void);

void print_single_activity_header1(void);
void print_single_idle_header1(void);
void print_single_export_header1(void);
void print_single_import_header1(void);
void print_single_local_header1(void);
void print_single_dud_header1(void);
void print_single_erratic_header1(void);
void print_single_nodes_header1(void);

void print_single_activity_header2(void);
void print_single_idle_header2(void);
void print_single_export_header2(void);
void print_single_import_header2(void);
void print_single_local_header2(void);
void print_single_dud_header2(void);
void print_single_erratic_header2(void);
void print_single_nodes_header2(void);


void print_multiple_activity_header1(void);
void print_multiple_idle_header1(void);
void print_multiple_export_header1(void);
void print_multiple_import_header1(void);
void print_multiple_local_header1(void);
void print_multiple_dud_header1(void);
void print_multiple_erratic_header1(void);
void print_multiple_nodes_header1(void);

void print_multiple_activity_header2(void);
void print_multiple_idle_header2(void);
void print_multiple_export_header2(void);
void print_multiple_import_header2(void);
void print_multiple_local_header2(void);
void print_multiple_dud_header2(void);
void print_multiple_erratic_header2(void);
void print_multiple_nodes_header2(void);

void printdate(int day,int month,int year);
void unformatted_printdate(int day,int month,int year);
int equaldates(int d1,int m1,int y1,int d2,int m2,int y2);
int dates_in_order(int d1, int m1, int y1, int d2, int m2, int y2);

void print_echo_name(char *s);

int breakcleanup(void);

void sort_alpha(void);

int arraymatch(int num,int *numarray,int numnum);
int stringmatch(char *string,char *template);
int thisstringmatch(char *string,char *template);

int charmatch(int c1,int c2);
