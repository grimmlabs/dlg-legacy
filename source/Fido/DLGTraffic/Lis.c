#include "list.h"

#define NULL 0

/*
 * Initialize list structure.
 */
void initlist(List *l)
{
  l->next=0;
  l->previous=0;
}

/*
 * Initialize list header structure.
 */
void initlisthdr(ListHdr *lh)
{
  lh->first=0;
  lh->last=0;
}

/*
 * Add item at start of list.
 */
void add_item_first(void *list,void *header)
{
  ((List *)list)->next=((ListHdr *)header)->first;
  ((List *)list)->previous=NULL;

  if(((ListHdr *)header)->first)
    ((ListHdr *)header)->first->previous=(List *)list;
  else
    ((ListHdr *)header)->last=(List *)list;

  ((ListHdr *)header)->first=(List *)list;
}

/*
 * Add item to end of list.
 */
void add_item_last(void *list,void *header)
{
  ((List *)list)->next=NULL;
  ((List *)list)->previous=((ListHdr *)header)->last;

  if(((ListHdr *)header)->last)
    ((ListHdr *)header)->last->next=(List *)list;
  else
    ((ListHdr *)header)->first=(List *)list;

  ((ListHdr *)header)->last=(List *)list;
}

/*
 * Get first list item.
 */
void *get_first(void *listhdr)
{
  return ((ListHdr *)listhdr)->first;
}

/*
 * Get last list item.
 */
void *get_last(void *listhdr)
{
  return ((ListHdr *)listhdr)->last;
}

/*
 * Get Next List Item
 */
void *get_next(void *list)
{
  if(list)
    return ((List *)list)->next;
  else
    return 0;
}

/*
 * Get previous list item.
 */
void *get_previous(void *list)
{
  if(list)
    return ((List *)list)->previous;
  else
    return 0;
}

/*
 * Initialize list header.  Need to put zeros in the proper places
 * after allocation.
 */
void init_list_hdr(void *listhdr)
{
  ((ListHdr *)listhdr)->first=0;
  ((ListHdr *)listhdr)->last=0;
}

/*
 * Initialize list header.  Need to put zeros in the proper places
 * after allocation.
 */
void init_list(void *list)
{
  ((List *)list)->next=0;
  ((List *)list)->previous=0;
}



