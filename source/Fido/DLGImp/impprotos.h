
/* FILE: zlink.c */
extern int EXPtoLink(void);
extern int LinkReplies(char *path, int lownum);
/* FILE: dupchk.c */
extern void read_dup_table(char *filename,unsigned long *buffer);
extern void write_dup_table(char *filename,unsigned long *buffer,BOOL force);
extern int CRC_dup_check(unsigned long CRC,unsigned long *buffer);
extern void add_CRC_to_buffer(unsigned long CRC,unsigned long *buffer);
extern BOOL DupCheck(char *areapathname,char *messagestart, int messagelength, char *echoname, char *whybad,char *datestring,char *tostring,char *fromstring,char *subjectstring);
extern int Reasoning(char *name, char *why , char *reason);
extern int EndOfMsg(char *mes, int k);
/* FILE: pktd.c */
extern int CheckDLGMAILSTOP(void);
extern int TossPackets(void);
extern int FindOldestBundle(char *bundle);
/* FILE: log.c */
extern int Log(char *);



/* FILE: tz.c */
extern char *MyTime(char *timestring,int FidoTime);
extern char * SkipPackedMsgHeader(char *d, char *bufend);
extern char * FetchArea(char *name);
extern BOOL FetchPassthru(char *name);
extern BOOL FetchLink(char *name);
extern void AreasClose(void);
extern USHORT FixNumbers(USHORT number);
extern int PreScanForLength(char *c);
extern int GetEchoArea(char *search, char *echo_area);
extern int AddToEXP(char *tagname,int num);
extern int WriteEXP(void);
extern int NotateTosses(void);
extern int tzmain(char *pktname, int pktnum, int pktof);
extern int get_packet_header(FILE *fp, char *newname, char *filenotestring);
extern int IsPKTToMe(struct Packet_Header *ph, char *whoitwasto,char *hexnet, char*hexnode);
extern int SaveCorruptFile(char *oldname);
extern int AFTag(int num);
extern int StowIt(char *where, char *what, int length, BOOL passthru, char *whybad);
extern long HowMany(long areanum);
extern int DeleteMsg(char *deleteme);
extern int toss_message(unsigned char *mp,int positionage);
extern int OneOfOurs(USHORT tnet, USHORT tnode);
extern int IsItAnARCMailName(char *name);
extern int ToAPoint(char *msg,int bodysize);
extern int MyMainCopy(char *src, char *destination, char *comment,BOOL del);
extern int MovePointFileAttaches(char *subject,char *comment);
extern int SecurityCheck(char *EchoArea,ULONG onet,ULONG onode);







/* FILE: DLGimp.c */
extern int AddressToZone(char *input);
extern int AddressToNet(char *input);
extern int AddressToNode(char *input);
extern int AddressToPoint(char *input);
extern int main(int argc, char **argv);
extern int NormalProcess(void);
extern int Extract(char *what,char *where,char *resultname);
extern int IncrementalRename(char *old, char *new);
extern int LoadDialogArea(void);
extern int LogEvent(char *to, char *from, char *fidoname,int msgnum,int areanumber,char *subject);
extern int InUserMsgFile(long areanumber, char *name);
extern void QuietExit(void);
extern int MyBorrowArea(long number);
extern int UnBorrowArea(long number);
extern int NewMyBorrowArea(long areanum);
extern int NewUnBorrowArea(long areanum);
extern int EnterAreaLoop(void);
extern int LeaveAreaLoop(void);
extern int ztime(void);
extern int LoadUserMem(void);
extern int SearchUsers(char *searchfor);
extern int SetZTaskPri(void);
extern int UnBundle(void);
extern int LogImported(int imported,int exported,int totnodes,char *path,char *tagname);
extern int MyExecute(char *string,BPTR a,BPTR b);
extern int MyFork(char *cmdline,int priority, int stack, BPTR outfh);
extern int ParseArgs(char **argv,char *argstring);
