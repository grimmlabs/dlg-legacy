#include <exec/types.h>

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <dos.h>
#include <time.h>
#include <math.h>

#include <exec/memory.h>
#include <exec/io.h>

#include <dos/dosextens.h>

#include <libraries/dos.h>

#include <proto/dos.h>
#include <proto/exec.h>
#include <proto/dlg.h>

#include <clib/alib_protos.h>

#include <dlg/resman.h>
#include <dlg/msg.h>
#include <dlg/user.h>
#include <dlg/broadcast.h>
#include <dlg/globalconfig.h>

#include <pragmas/dlg.h>

#include "/zap_h/zap.h"
#include "/ross/protos.h"
#include "/Ross/Header.h"
#include "impprotos.h"
#include "/zactive/active.h"
#include "/expobj/bpkt.h"
#include "/zflavor/flavorproto.h"

/* ===== global DEFINES ===== */
#define IDLEN   12
#define FROMLEN 36
#define SUBLEN  72
#define AREALEN 34

#define DLGMV2 1

#define  MyDEBUG  (GCFG->ZIMP_DB)
#define LOGFILENAME "LOGS:DLGImp.LOG"

#define BUFLEN 32000

#define MAXEXPAREAS 1000

#define ARC 1
#define ZOO 2
#define LZH 3
#define ZIP 4

extern int NULLMSGS;

/* dupe checking items */

extern unsigned long *the_dup_table;
extern char last_dup_table[100];
extern BOOL last_dup_table_written;
extern struct Library *DLGBase;
extern BPTR sout;

#define TABLELENGTH 1000
#define BAD TRUE
#define GOOD FALSE


/* ==== zlink.c =============== */

/* no globals */

/* ============================ */


/* ==== tossmsgs.c ============ */

/* no globals defined here - see globals in tz.c */

/* ============================ */



/* ==== tz.c ================== */

extern char far REMEMBERAREA[MAXEXPAREAS][30];
extern int far REMEMBERAREALOW[MAXEXPAREAS];
extern int far REMEMBEREXPTALLY[MAXEXPAREAS];
extern int far totREMEMBEREXP;


/* ==== dupchk.c ============== */

extern char prob[20];

/* ============================ */



/* ==== zimp.c ================ */

extern BOOL DLGMAILV2;

extern BOOL GOTNETMAIL;
extern BOOL GOTECHOMAIL;
extern BOOL GOTTIC;

extern struct NameStruct *UserMem;
extern int TotalUsers;

extern int  BORROWED_AREA;
extern long MSGSIMPORTED;

extern char localareaname[50];
extern char CurrentARCmail[50];
extern char ARCpat[7][13];

extern struct FileInfoBlock *FIB;
extern struct areas *a;

extern BPTR DirectoryLock;

extern struct Msg_Area *AllAreas;
extern long TotalAreas;

/* formerly from ztime */

extern struct tm *tp;
extern char timenow[30];
extern long tt;

/* ============================ */

/* ==== pktdir.c ============== */

extern BPTR LockFile;
extern char packets[100][50];

/* ============================ */

/* ==== s_log.c =============== */
extern char logstring[2000];
/* ============================ */




extern int LOG;
extern int LOGUSERMESSAGE;

#define MAXMSGLEN 32000

extern struct Library *DLGBase;

