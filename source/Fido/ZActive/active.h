/* structures for importing/exporting records */

#define ECHOLOGNAME "Activity.DAT"

struct OurDate
	{
		UWORD  od_year;
		UBYTE  od_month;
		UBYTE  od_date;
	};

struct Daily
	{
		struct OurDate d_recorddate;
		UWORD  d_imported;		/* number of msgs imported from all sources on this date */
		UWORD  d_exported;		/* number of msgs exported from all sources (including local, which won't be reflected in the imported), on this date */
		UWORD  d_totnodes;		/* total # of nodes that received any mail from this echo */
		UWORD  d_totexported;		/* the total number of messages created and mailed (roughly the nodesXexported number) */
	};

struct EchoLog
	{
		char   el_tagname[40];
		struct OurDate el_lastimported;	/* date messages last imported */
		struct OurDate el_lastexported; /* date messages last exported */
		struct OurDate el_created;	/* date the activity.dat file was created */
		struct OurDate el_lastreport;	/* LAST REPORT - update this whenever a full report is run */
		UWORD el_class;			/* this is not necessarily valid */
		char   fill[38];
		struct Daily   el_daily[416];	/* somewhat wasteful, but it works - see formula for offsetting into this array below */
	};

/* offset into the stucture is as follows:

	32 * month + date

	1=january
	2=february
	  etc...

Yes, there is a lot of wasted space, but it makes
access SO easy and straightforward...             */

