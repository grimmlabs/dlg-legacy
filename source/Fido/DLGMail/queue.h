/* queue.h -- included in globals.h */

#define LISTTYPE	255;
#define ZQ_ARGLENGTH 	256
#define ZQ_CMDLENGTH 	30

struct ZQueue
{
	struct Node zq_node;
	char 	zq_command[ZQ_CMDLENGTH];	/* original queued command */
	char	zq_args[ZQ_ARGLENGTH];		/* whitespace separated */
	int	zq_commandtoken;		/* what the token number is */
	ULONG	zq_flags;			/* flags */
};
