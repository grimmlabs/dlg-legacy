// Check these and check off with "ok"

#define DLGMAIL         0xFFFF     
#define DLGMAILREXXPORT "DLGM_ARexx"
#define MAX_ARGS        30
#define COPYBUFSIZE     10000

// debugging -- these have to go!

#define IF_INFORMATIVE  if(MyDEBUG>=INFORMATIVE)
#define IF_TALKATIVE    if(MyDEBUG>=TALKATIVE)
#define IF_VERBOSE      if(MyDEBUG>=VERBOSE)
#define IF_ANNOYING     if(MyDEBUG>=ANNOYING)
#define IF_OBNOXIOUS    if(MyDEBUG>=OBNOXIOUS)

// Filenames

#define USEREXPFILE     "FIDO:USE_EXP"
#define MACROFILENAME   "FIDO:DLGMail.MAC"
#define CONFIGFILENAME  "FIDO:DLGMail.CFG"
#define LOGFILENAME     "LOGS:DLGMail.LOG"
#define REPORTFILENAME  "LOGS:DLGMailCFG.RPT"
#define CFGERRFILE      "LOGS:DLGMailCFGErr.RPT"
#define INFILE    "LOGS:__IN"


// top.c

#define CLASS_TOTALS 4  /* the total number of queues POSITIVE queues*/

#define CLASS_NOW    -1
#define CLASS_IMMED  0
#define CLASS_BKGND  1
#define CLASS_CALL   2
#define CLASS_PROC   3


// gc.c

#define LS_NO_ERROR        0
#define LS_NO_FILE        -1
#define LS_MISSING_ENTRY  -2
#define LS_DICKHEAD       -3

// llg.c

#define  STARTLOG    1
#define  ENDLOG      2
#define  LEVELDOWN   3
#define  LEVELUP     4
#define  SPECIAL     5


