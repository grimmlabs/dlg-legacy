#include "Includes.h"
#include "Defines.h"
#include "Protos.h"
#include "Version.h"
#include "Vars.h"

#include "/zflavor/flavor.h"
#include "/Ross/Header.h"

#include <link/io.h>

#define DEBUGPRINT 0


/* this module contains the support goodies for DLGMail */

/* REMOVE */

#ifndef extstring
char *extstring;
#endif

/// Log()
void  __stdargs Log(char *format, ...)
{
   BPTR  fh       =  NULL;
   char  time[32];

   fh = Open(LOGFILENAME, MODE_READWRITE);

   if(fh)
   {
      Seek(fh,0,OFFSET_END);

      MDate(time);
      AFPrintf(NULL, fh, "%s: ", time);
      XAFPrintf(NULL, fh, format, (char *)(&format+1) );
      AFPrintf(NULL, fh, "\n");
      Close(fh); fh = NULL;
   }

   return;
}
//-

/*
/// Log
void Log(char *logthis)
{
   AppendFile(LOGFILENAME,logthis);
}
//-
*/
/// XMatch
/* PUBLIC
 *
 * Some peculiar string comparison function
 *
 *
 */

BOOL XMatch(char *what,char *string)
{
   if(!Strnicmp(what,string,strlen(string)))
      return TRUE;
   else
      return FALSE;
}
//-

/// GetEnv
/* PUBLIC
 *
 * Get an environment variable... The contents won't be returned, just a 1 if
 * the variable is set. The contents can be passed back in the string contents
 * if desired
 *
 */

int GetEnv(char *name, char *contents)
{
   BPTR fp;

   int i;

   char buffer[100];
   char pathname[100];
   char *p;

   ASPrintf(NULL,pathname,"ENV:%s",name);

   for(i=0;i<10;i++)
   {
      fp=Open(pathname,MODE_OLDFILE);
      if(fp) break;
      Delay(10);
   }
   if(!fp)
      return(0);

   p=FGets(fp,buffer,80);
   buffer[80]=NULL;
   Close(fp);
   if(!p)
      return(0);
   if(contents) strcpy(contents,buffer);
   return(1);
}
//-

/// SetEnv
/* PUBLIC
 *
 * Set an environment variable
 *
 *
 */

int SetEnv(char *name, char *setting)
{
   BPTR fp=NULL;
   int i;

   char pathname[100];

   ASPrintf(NULL,pathname,"ENV:%s",name);

   for(i=0;i<10;i++)
   {
      fp=Open(pathname,MODE_NEWFILE);
      if(fp) break;
      Delay(10);
   }
   if(fp==NULL)
   {        
      return(0);
   }
   AFPrintf(NULL,fp,"%s",setting);
   Close(fp);

   return(1);
}
//-

/// WaitForEnv
/* PUBLIC
 *
 * WaitForEnv() waits until an environment variable clears
 *
 *
 */

int WaitForEnv(char *waitforwhat)
{
   while(1)
   {
      if(0==GetEnv(waitforwhat,NULL))
         break;
      Delay(55);
   }
   return(0);
}
//-

/// DoAScript
/* PUBLIC
 *
 * do a FIDO:Batch/*.DMB script... Returns 300 if no script, or the rc
 * that MyFork() actually returns
 *
 */

int DoAScript(char *scriptname,BPTR fh)
{
   char buf1[300];
   int rc=300;

   ASPrintf(NULL,buf1,"FIDO:batch/%s",scriptname);

   if(Exists(buf1))
   {
      Log(". Executing [%s]",buf1);
      rc = Spawn(NULL, fh, "Execute >NIL: <NIL: %s >NIL: <NIL:",buf1);
      Log(". Script execution [%s] returned [%d]", buf1, rc);
   }

   return rc;
}
//-

/// ToNil
/* PUBLIC
 *
 * ToNil() inserts a ">NULL:" after the command.
 *
 *
 */

int ToNil(char *commandline,int flag)
{
   char buffer[250];
   char tok[50];
   char *s;

   if(flag) return(1);

   strcpy(buffer,commandline);

   s=buffer;
   s=stpblk(s);
   s=stptok(s,tok,sizeof(tok)," \t");
   s=stpblk(s);

   ASPrintf(NULL,commandline,"%s >NULL: %s",tok,s);
   return(1);
}
//-

/// TrapTell
/* PUBLIC
 * 
 * Functions to talk to Trapdoor. Please note that the string for "returnstring"
 * should be allocated to a minimum of 100 bytes.
 * 
 * TrapTell() provides conversion for ` into " characters
 * 
 * SendRexx() privides for specifying a trapdoor port name
 * 
 * --- both of these above functions will return if TrapDoor is busy
 * 
 * SendRexx2() same as SendRexx() except that commands to TrapDoor will pend
 *   until TrapDoor can handle them unless TrapDoor isn't loaded, then the
 *   function will return immediately.
 * 
 */


int TrapTell(char *what,char *returnstring)
{
   char buffer[100];

   int i;

   strcpy(buffer,what);

   for(i=0;buffer[i]!=NULL;i++)
   {
      if(buffer[i]=='`') buffer[i]=34;
   }

   Log(". Sending command [%s] to Trapdoor",buffer);
   return(SendRexx(_TDPORT,buffer,returnstring));

}
//-

/// IsTrapdoorThere
/* PUBLIC
 *
 * Checks to see if TrapDoor present (according to the resource mgr)
 *
 *
 */

int IsTrapdoorThere(char *bbsport)
{
   struct PortInfo istruct;
   char port[20];
   char password[100];
   int retval;

   strcpy(port,bbsport);
   Upper(port);
   istruct.port=port;

   strcpy(password,"");

   retval=GetPortInfo(&istruct);

   if(retval==RMNOERR)
      strcpy(password,istruct.passwd);

   FreePortInfo(&istruct);

   if((retval==RMNOERR) && (!Stricmp(password,_TDLOCKNAME)))
   {
      return(1);
   }

   return(0);
}
//-

/// IsTrapdoorIdle
/* PUBLIC
 *
 * checks to see if TrapDoor is idle
 *
 *
 */

int IsTrapdoorIdle(char *tdport)
{
   char string[100];

   strcpy(string,"");

   SendRexx2(tdport,"@STATUS S",string);
   if(!Stricmp(string,"IDLE")) return 1;
   return 0;
}
//-

/// SendRexx
int SendRexx(char *portname,TEXT *string,char *returnstring)
{
   if(returnstring) strcpy(returnstring,"");

   if(IsTrapdoorThere(_BBSPORT))
   {
      if(IsTrapdoorIdle(_TDPORT))
      {
         return(SendRexx2(portname,string,returnstring));
      }
      else
      {
         if(returnstring)strcpy(returnstring,"TrapDoor not idle");
      }
   }
   else
   {
      if(returnstring)strcpy(returnstring,"TrapDoor not present");
   }
   return(-1);
}
//-

/// SendRexx2
int SendRexx2(char *portname,TEXT *string, char *returnstring)
{
   struct RexxMsg *RexxMsg;
   struct MsgPort *RexxPort;
   struct MsgPort *ReplyPort;
   LONG  retval = -1;

   if(returnstring)
      strcpy(returnstring,"");

   if (ReplyPort=(void *)CreatePort(0,0))
   {
      if (RexxMsg=(void *)AllocMem(sizeof(struct RexxMsg), MEMF_CLEAR|MEMF_PUBLIC))
      {
         /* following line is experimental */

         RexxMsg->rm_Node.mn_Node.ln_Name="REXX";

         RexxMsg->rm_Node.mn_ReplyPort=ReplyPort;
         RexxMsg->rm_Node.mn_Length=sizeof(struct RexxMsg);
         RexxMsg->rm_Action=RXCOMM | RXFF_RESULT;
         RexxMsg->rm_Args[0]=string;

         Forbid();
         RexxPort=(void *)FindPort(portname);

         if (RexxPort)
            PutMsg(RexxPort, (struct Message *) RexxMsg);
         Permit();

         if (RexxPort)
         {
            WaitPort(ReplyPort);
            GetMsg(ReplyPort);
            if(!(retval=RexxMsg->rm_Result1))
            {
               struct RexxArg *ra;

               if(RexxMsg->rm_Result2)
               {
               if(returnstring) stccpy(returnstring,(char *)RexxMsg->rm_Result2,100);

                  ra=(struct RexxArg *)((char *)RexxMsg->rm_Result2 - &((struct RexxArg *)NULL)->ra_Buff[0]);
                  FreeMem(ra, ra->ra_Size);
               }
            }
         }

         FreeMem(RexxMsg,sizeof(struct RexxMsg));
      }
      DeletePort(ReplyPort);
   }
   return retval;
}
//-

/// RMWaitPort
/* PUBLIC
 *
 * Wait for a DLG port to be locked with a certain password, returns 1 on success
 *
 *
 */

int RMWaitPort(char *portname, char *passwd)
{
   struct PortInfo istruct;
   int retval;
   char port[20];

   strcpy(port,portname);
   Upper(port);

   istruct.port=port;

   for(;;)
   {
         retval=GetPortInfo(&istruct);
         if((retval==RMNOERR)||(retval==NOTLOCKED))
         {
               if(retval==RMNOERR)
               {
                  if(!Stricmp(istruct.passwd,passwd))
                  {
                     FreePortInfo(&istruct);
                     break;
                  }
               
                  FreePortInfo(&istruct);
               }

               Delay(52);
               continue;
         }
         else if(retval==NORM)
      {
         return(0);
      }
         else if(retval==BADPORT)
      {
         return(0);
      }
         else
      {
         return(0);
      }
   }
   return(1);
}
//-

/// PatMatch
/* PUBLIC
 *
 * Weird little anchored pattern match, returns 1 or 2 on a match, 0 if no
 * match.
 *
 */

int PatMatch(char *s, char *pat)
{
   int i=-1;
   int size;
   int rc=0;
   char string[100],pattern[100];

   strcpy(string,s);
   strcpy(pattern,pat);

   Upper(string);
   Upper(pattern);

   size=strlen(pat);
   if(size)
      i=stcpma(string,pattern);
   if(i==size) rc=1;
   if(i==strlen(string)) rc=2;
   return rc;
}
//-

/// CheckInbound
/* PUBLIC
 *
 * Detect whatever is in INBOUND:
 *
 *
 */

int CheckInbound(void)
{
   /* will return bits according to what is present */

   char *bundles[]={"????????.SU?","????????.MO?","????????.TU?","????????.WE?","????????.TH?","????????.FR?","????????.SA?",""};

   int i;
   int tt;
   int rc=0;
   BPTR fh;
   char filename[50];

   struct FileInfoBlock *fib;

   fib=(void *)AllocMem(sizeof(struct FileInfoBlock),MEMF_PUBLIC);
   if(fib)
   {
      fh=Lock("INBOUND:",ACCESS_READ);
      if(fh)
      {
         if(Examine(fh,fib))
         {
            while(ExNext(fh,fib))
            {
               tt=0;

               strcpy(filename,fib->fib_FileName);
               Upper(filename);
               /* packets */

               if(!tt)
               {
                  if(PatMatch(filename,"????????.PKT"))
                     tt |= IN_PKT;
               }
               /* tic files */

               if(!tt)
               {
                  if(PatMatch(filename,"????????.TIC"))
                     tt |= IN_TIC;
               }
               /* damaged packets */

               if(!tt)
               {
                  if(PatMatch(filename,"????????.DMG"))
                     tt |= IN_DMG;
               }
               /* trapdoor temporary files */

               if(!tt)
               {
                  if(PatMatch(filename,"! trap"))
                     tt |= IN_TMP;
               }
               /* bundles */
               
               if(!tt)
               {
                  for(i=0;*bundles[i];i++)
                  {
                     if(PatMatch(filename,bundles[i])) 
                     // bug fix - comment out this stuff --> && filename[11]<'Z')
                     {
                        tt |= IN_BUN;
                        break;
                     }
                  }
               }
               if(!tt)
               {
                  tt |= IN_ATTACH;
               }

               rc |= tt;
            }
         }
         UnLock(fh);
      }
      FreeMem(fib,sizeof(struct FileInfoBlock));
      return(rc);
   }

}
//-

/// CalcAreasCRC
#define areas_size (sizeof(struct areas))
#define area_size  (sizeof(struct area))
#define nodenum_size (sizeof(struct nodenum))

unsigned long  CalcAreasCRC(struct areas *areas)
{
   unsigned long crc=0;

   struct area *area;
   struct nodenum *nodenum;

   if(areas==0)
      return 0;
   if(areas->first_area==0)
      return 0;

   crc=CalcCRC((unsigned char *)areas,areas_size,crc);

   area=areas->first_area;
   
   while(area)
   {
      crc=CalcCRC((unsigned char *)area,area_size,crc);

      nodenum=area->first_nodenum;

      while(nodenum)
      {
         crc=CalcCRC((unsigned char *)nodenum,nodenum_size,crc);

         nodenum=nodenum->next_nodenum;
      }
      area=area->next_area;
   }
   return crc;
}
//-

/// OK2Strip
/* PUBLIC
 *
 * Compares passed net address to the total addresses we have 
 *
 *
 */

int OK2Strip(int net)
{
   int i;
   char tmp[100];

   for(i=0;i<_totADDRESS;i++)
   {
      if(i==1) continue;

      strsfn(_ADDRESS[i],NULL,tmp,NULL,NULL);
      if(net==atoi(tmp))
         return(1);
   }
   return(0);
}
//-

/// NumStrsfn
int NumStrsfn(char *s,int *zone, int *net, int *node, int *point)
{
   char add[4][35];

   strsfn(s,add[0],add[1],add[2],add[3]);
   *zone=atoi(add[0]);
   *net=atoi(add[1]);
   *node=atoi(add[2]);
   *point=atoi(add[3]);
   return(1);
}
//-

/// MyClock
int MyClock(int *day,int *hour,int *minute,int *second)
{
   UBYTE clock[10];

   getclk(clock);

   *day=clock[0]+1;
   *hour=clock[4];
   *minute=clock[5];
   *second=clock[6];

   return (int)clock[4];
}
//-

/// __UserType
ULONG __UserType(char *spec, char *pktorflow)
{
   char s[100], p[100];

   strcpy(s,spec);
   strcpy(p,pktorflow);

   Upper(s);
   Upper(p);

   if(*p=='P')
   {
      if(*s=='C') return(flavor_PKTFILE | flavor_CRASH);
      if(*s=='N') return(flavor_PKTFILE | flavor_NORMAL);
      if(*s=='D') return(flavor_PKTFILE | flavor_DIRECT);
      if(*s=='H') return(flavor_PKTFILE | flavor_HOLD);
      if(*s=='!') return(flavor_PKTFILE | flavor_NONE);
      if(*s=='?') return(flavor_PKTFILE | flavor_ANY);
   }

   if(*p=='F')
   {
      if(*s=='C') return(flavor_FLOWFILE | flavor_CRASH);
      if(*s=='N') return(flavor_FLOWFILE | flavor_NORMAL);
      if(*s=='D') return(flavor_FLOWFILE | flavor_DIRECT);
      if(*s=='H') return(flavor_FLOWFILE | flavor_HOLD);
      if(*s=='!') return(flavor_FLOWFILE | flavor_NONE);
      if(*s=='?') return(flavor_FLOWFILE | flavor_ANY);
   }

   // error return, better than 0

   return(flavor_FLOWFILE | flavor_HOLD); 
}
//-

/// AddToWild
/* AddToWild() takes a wildcardable z:n/n.p address and converts it into
   a newstyle flow or packet name with wildcards, suitable for use with
   the lattice pattern matching functions (or possibly Lock()). This
   will not produce a pattern to find oldstyle .pkt's */

int __AddToWild(char *in,char *out,ULONG type)
{
   char a[40],b[40],c[40],d[40],e[40];

   int daynum;

   __mystrsfn(in,a,b,c,d,e);

   strcpy(e,"");



   if(*a=='*')
      strcpy(a,"*");

   if(0==Strnicmp(a,"#?",2))
   {
      strcpy(a,"*");
      strcpy(b,"*");
      strcpy(c,"*");
      strcpy(d,"*");
   }

   if(*b=='*')
      strcpy(b,"*");

   if(0==Strnicmp(b,"#?",2))
   {
      strcpy(b,"*");
      strcpy(c,"*");
      strcpy(d,"*");
   }

   if(*c=='*')
      strcpy(c,"*");

   if(0==Strnicmp(c,"#?",2))
   {
      strcpy(c,"*");
      strcpy(d,"*");

   }

   if(*d=='*' || 0==Strnicmp(d,"#?",2))
   {
      strcpy(d,"*");
   }

   if(type & flavor_PKTFILE)
   {
      if(type & flavor_CRASH) strcpy(e,"CUT");
      if(type & flavor_NORMAL) strcpy(e,"OUT");
      if(type & flavor_DIRECT) strcpy(e,"DUT");
      if(type & flavor_HOLD) strcpy(e,"HUT");
      if(type & flavor_NONE) strcpy(e,"!UT");
      if(type & flavor_ANY) strcpy(e,"?UT");
   }

   if(type & flavor_FLOWFILE)
   {
      if(type & flavor_CRASH) strcpy(e,"CLO");
      if(type & flavor_NORMAL) strcpy(e,"FLO");
      if(type & flavor_DIRECT) strcpy(e,"DLO");
      if(type & flavor_HOLD) strcpy(e,"HLO");
      if(type & flavor_NONE) strcpy(e,"!LO");
      if(type & flavor_ANY) strcpy(e,"?LO");
   }

   if(type & flavor_BUNDLE)
   {
      if(type & flavor_SUNDAY)   strcpy(e,"SU");
      if(type & flavor_MONDAY)   strcpy(e,"MO");
      if(type & flavor_TUESDAY)  strcpy(e,"TU");
      if(type & flavor_WEDNESDAY)   strcpy(e,"WE");
      if(type & flavor_THURSDAY) strcpy(e,"TH");
      if(type & flavor_FRIDAY)   strcpy(e,"FR");
      if(type & flavor_SATURDAY) strcpy(e,"SA");
      if(type & flavor_ANY)   strcpy(e,"??");

      if(!(type & flavor_ANY))
      {
         daynum=type&15;

         if(daynum>9) daynum=0;

         if(daynum==0) strcat(e,"0");
         if(daynum==1) strcat(e,"1");
         if(daynum==2) strcat(e,"2");
         if(daynum==3) strcat(e,"3");
         if(daynum==4) strcat(e,"4");
         if(daynum==5) strcat(e,"5");
         if(daynum==6) strcat(e,"6");
         if(daynum==7) strcat(e,"7");
         if(daynum==8) strcat(e,"8");
         if(daynum==9) strcat(e,"9");
      }

      if(type & flavor_ANY) strcat(e,"?");
   }

   ASPrintf(NULL,out,"%s.%s.%s.%s.%s",a,b,c,d,e);
   Upper(out);
   return(1);
}
//-

/// __mystrsfn
/* mystrsfn is sort of an extension of lattice's version. it picks the
   address apart and allows wildcards in it. in addition, it returns
   the filename extension in the sixth argument */

/* THIS ROUTINE EXPECTS ZONE TO BE PRESENT */

int __mystrsfn(char *in, char *aa, char *bb, char *cc, char *dd, char *ee)
{
   char *s;
   char *a,*b,*c,*d,*e;

   a=aa;
   b=bb;
   c=cc;
   d=dd;
   e=ee;

   *a=NULL;
   *b=NULL;
   *c=NULL;
   *d=NULL;
   *e=NULL;

   s=in;

   while(*s != ':' && *s != NULL)
   {
      *a++=*s++;
      *a=NULL;
   }

   if(*s==NULL) return(1);

   s++; /* skip the ':' */

   while(*s != '/' && *s!=NULL)
   {
      *b++=*s++;
      *b=NULL;
   }

   if(*s==NULL) return(2);

   s++; /* skip the '/' */


   while(*s!='.' && *s!=NULL)
   {
      *c++=*s++;
      *c=NULL;
   }

   if(*s==NULL) return(3);

   s++; /* skip the '.' */

   while(*s!='.' && *s!=NULL)
   {
      *d++=*s++;
      *d=NULL;
   }

   if(*s==NULL) return(4);

   s++; /* skip the '.' */

   while(*s!=NULL)
   {
      *e++=*s++;
      *e=NULL;
   }

   return(5);
}
//-

/// mystrsfn2
/* mystrsfn is sort of an extension of lattice's version. it picks the
   address apart and allows wildcards in it. in addition, it returns
   the filename extension in the sixth argument */

/* THIS ROUTINE SHOULD BE SMART ENOUGH TO FIGURE OUT WHAT'S THERE AND NOT */

int __mystrsfn2(char *in, char *aa, char *bb, char *cc, char *dd, char *ee)
{
   char *s;

   char *a;
   char *b;
   char *c;
   char *d;
   char *e;

   char ONE[20],TWO[20],THREE[20],FOUR[20],FIVE[20];
   int sp1=NULL,sp2=NULL,sp3=NULL,sp4=NULL; //,sp5=NULL;

   a=ONE;
   b=TWO;
   c=THREE;
   d=FOUR;
   e=FIVE;

   *aa=NULL;
   *bb=NULL;
   *cc=NULL;
   *dd=NULL;
   *ee=NULL;

   *a=NULL;
   *b=NULL;
   *c=NULL;
   *d=NULL;
   *e=NULL;

   s=in;

   if(*s)
   {
      while(*s != ':' && *s != '/' && *s!= '.' && *s != NULL)
      {
         *a++=*s++;
         *a=NULL;
      }
      sp1=*s;


      if(*s)
      {
         s++;
         while(*s != '/' && *s != '.' && *s!=NULL)
         {
            *b++=*s++;
            *b=NULL;
         }
         sp2=*s;

         if(*s)
         {
            s++;
            while(*s!='.' && *s!=NULL)
            {
               *c++=*s++;
               *c=NULL;
            }
            sp3=*s;

            if(*s)
            {
               s++;
               while(*s!='.' && *s!=NULL)
               {
                  *d++=*s++;
                  *d=NULL;
               }
               sp4=*s;

               if(*s)
               {
                  s++;
                  while(*s!=NULL)
                  {
                     *e++=*s++;
                     *e=NULL;
                  }

               }
            }
         }
      }
   }

   /* OK, the stuff is in ONE,TWO,THREE,FOUR and FIVE. depending on the
   separators, we have to copy various things various places*/

   switch(sp1)
   {
      case ':':
      strcpy(aa,ONE);
      strcpy(bb,TWO);
      strcpy(cc,THREE);
      strcpy(dd,FOUR);
      strcpy(ee,FIVE);
      break;

      case '/':
      strcpy(bb,ONE);
      strcpy(cc,TWO);
      strcpy(dd,THREE);
      strcpy(ee,FOUR);
      break;

      case '.':
      strcpy(cc,ONE);
      strcpy(dd,TWO);
      strcpy(ee,THREE);
      break;

      default:
      strcpy(cc,ONE);
      break;
   }
   return(5);
}
//-

/// __FidoPatternMatch
int __FidoPatternMatch(char *s_in, char *s_pat)
{
   char sin[6][31];
   char spat[6][31];

   char in[500];
   char pat[500];

   int sinnum;
   int spatnum;

   int counter;

   int cc;

   strcpy(in,s_in);
   strcpy(pat,s_pat);

   Upper(in);
   Upper(pat);

   sinnum=__NFComponents(in,sin[1],sin[2],sin[3],sin[4],sin[5]);
   spatnum=__NFComponents(pat,spat[1],spat[2],spat[3],spat[4],spat[5]);

   if(sinnum!=spatnum) return(0); /* first level of error checking */

   for(counter=1;counter<(spatnum+1);counter++)
   {
      if(spat[counter][0]=='*') continue; /* it doesn't matter what's in the input file in this case */

      if(strlen(spat[counter])!=strlen(sin[counter])) return 0;   /* they can't match */

      for(cc=0;cc<(strlen(spat[counter])+1);cc++)
      {
         if(spat[counter][cc]=='?') continue;
         if(spat[counter][cc]!=sin[counter][cc]) return 0;  /* the character didn't match exactly */
      }
   }
   return(1);
}
//-

/// __NFComponents
int __NFComponents(char *in, char *A, char *B, char *C, char *D, char *E)
{
   char *s,*p;

   s=in;
   p=in;

   p=stptok(s,A,31,".:");
   if(p!=in+strlen(in)) p++;
   s=p;
   p=stptok(s,B,31,"./");
   if(p!=in+strlen(in)) p++;
   s=p;
   p=stptok(s,C,31,".");
   if(p!=in+strlen(in)) p++;
   s=p;
   p=stptok(s,D,31,".");
   if(p!=in+strlen(in)) p++;
   s=p;
   strcpy(E,s);

   if(*E) return 5;
   if(*D) return 4;
   if(*C) return 3;
   if(*B) return 2;
   if(*A) return 1;
   return 0;
}
//-

/// __EvaluateFilename
/* EvaluateFilename() takes any filename found in the newstyle outbound dir
   and returns the address that it is to, along with the type. Supply
   it with both the filename and filenote for evaluative purposes */

int __EvaluateFilename(char *s,char *p,int *zone,int *net,int *node,int *point,ULONG *type)

/* s is either oldstyle .pkt or newstyle any
   if s is oldstyle, p should contain z:n/n/p address */
{
   char string0[40];
   char string1[40];
   char string2[40];
   char string3[40];
   char string4[40];

   char a[40],b[40],c[40],d[40],e[40];

   *zone=0;
   *net=0;
   *node=0;
   *point=0;
   *type=0;

   strcpy(a,"");
   strcpy(b,"");
   strcpy(c,"");
   strcpy(d,"");
   strcpy(e,"");

   __NFComponents(s,a,b,c,d,e);

   if(*c && *d && *e)
   {
      *zone=atoi(a);
      *net=atoi(b);
      *node=atoi(c);
      *point=atoi(d);
   
      if(!Stricmp(e,"CLO")) *type=flavor_CLO;
      if(!Stricmp(e,"FLO")) *type=flavor_FLO;
      if(!Stricmp(e,"DLO")) *type=flavor_DLO;
      if(!Stricmp(e,"HLO")) *type=flavor_HLO;
      if(!Stricmp(e,"NLO")) *type=flavor_NLO;
   
      if(!Stricmp(e,"CUT")) *type=flavor_CUT;
      if(!Stricmp(e,"OUT")) *type=flavor_OUT;
      if(!Stricmp(e,"DUT")) *type=flavor_DUT;
      if(!Stricmp(e,"HUT")) *type=flavor_HUT;
      if(!Stricmp(e,"NUT")) *type=flavor_NUT;
      if(!Stricmp(e,"PKT")) *type=flavor_PKTFILE;

      if(!Strnicmp(e,"SU",2)) *type=flavor_SU;
      if(!Strnicmp(e,"MO",2)) *type=flavor_MO;
      if(!Strnicmp(e,"TU",2)) *type=flavor_TU;
      if(!Strnicmp(e,"WE",2)) *type=flavor_WE;
      if(!Strnicmp(e,"TH",2)) *type=flavor_TH;
      if(!Strnicmp(e,"FR",2)) *type=flavor_FR;
      if(!Strnicmp(e,"SA",2)) *type=flavor_SA;

      if(*type & flavor_BUNDLE)
      {
         *type=*type+atoi(e+2);
      }
   
      if(*type)
         return(1);
      else
         return(0);
   }
   else
   {
      if(strlen(s)!=12) return(0);
      if(s[8] != '.') return(0);
      if(Stricmp(s+9,"PKT")) return(0);

      __mystrsfn(p,string0,string1,string2,string3,string4);

      *zone=atoi(string0);
      *net=atoi(string1);
      *node=atoi(string2);
      *point=atoi(string3);

      if(0==Stricmp("PKT",string4))
      {
         *type=flavor_PKTFILE;
         return(1);
      }
      else
         return(0);

   }
}
//-

/// __CreateNewName
/* this creates a new name based on arguments supplied, including the
   TYPE which adheres to flavor.h convention */

int __CreateNewName(char *newname, int zone, int net, int node, int point, ULONG type)
{
   int tmp;
   char tmpstr[40];

   ASPrintf(NULL,newname,"%d.%d.%d.%d.",zone,net,node,point);

   if(type & flavor_BUNDLE)
   {
      if(type & flavor_SUNDAY) strcat(newname,"SU");
      if(type & flavor_MONDAY) strcat(newname,"MO");
      if(type & flavor_TUESDAY) strcat(newname,"TU");
      if(type & flavor_WEDNESDAY) strcat(newname,"WE");
      if(type & flavor_THURSDAY) strcat(newname,"TH");
      if(type & flavor_FRIDAY) strcat(newname,"FR");
      if(type & flavor_SATURDAY) strcat(newname,"SA");

      tmp=type&15;
      if(tmp>9)tmp-=9;

      ASPrintf(NULL,tmpstr,"%d",tmp);

      strcat(newname,tmpstr);

      return(1);
   }

   if(type & flavor_FLOWFILE)
   {
      if(type & flavor_CRASH) strcat(newname,"CLO");
      if(type & flavor_NORMAL) strcat(newname,"FLO");
      if(type & flavor_DIRECT) strcat(newname,"DLO");
      if(type & flavor_HOLD) strcat(newname,"HLO");
      if(type & flavor_NONE) strcat(newname,"NLO");

      return(1);
   }

   if(type & flavor_PKTFILE)
   {
      if(type & flavor_CRASH) strcat(newname,"CUT");
      if(type & flavor_NORMAL) strcat(newname,"OUT");
      if(type & flavor_DIRECT) strcat(newname,"DUT");
      if(type & flavor_HOLD) strcat(newname,"HUT");
      if(type & flavor_NONE) strcat(newname,"NUT");

      return(1);
   }

   return(0);
}
//-

/// __RenameOutbound
/* pretty straightforward, pass it old and new and it tacks outbound: to the
   front and calls DOS Rename() */

int __RenameOutbound(char *old, char *new)
{
   char old1[100];
   char new1[100];

   ASPrintf(NULL,old1,"OUTBOUND:%s",old);
   ASPrintf(NULL,new1,"OUTBOUND:%s",new);

   return((int)Rename(old1,new1));
}
//-


