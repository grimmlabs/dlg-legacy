// top.c 

extern   struct   List    *CreateZQList(int class);
extern   struct   Window  *GetCurWindow(void);
extern   struct   ZQueue  *CreateQEntry(char *command, char *args, ULONG token, ULONG flags, char pri);
extern   struct   ZQueue  *GetFromQueue(int class, char *command);

extern   BPTR              WinOpen(int x, int y, int wid, int height, char *name, char *scnname);

extern   int               AddToQueue(char *command, char *args, int token, int class, ULONG flags, int pri);
extern   int               ARexxHeart(void);
extern   int   __stdargs   CP_CommandLine(int class, char *string, ...);
extern   int               DeleteDupeQEntries(int class,struct ZQueue *rem);
extern   int               InitQueues(void);
extern   int               KillQEntry(struct ZQueue *q);
extern   int               KillQueues(void);
extern   int               MacroSubstitution(char *tok, char *args, char **macromemory);
extern   int               ParseARexxCommand(struct RexxMsg *rm, BOOL INHIBIT);
extern   int               ParseArgs(char **argv, char *argstring);
extern   int               ShowZQ(int class);
extern   int               Usage(char *outstring, int toknum);
extern   int             __RECONFIG(void);
extern   int             __TD(int c, int log);

extern   ULONG             CreateARexxTextReply(int whichreply, char *macmem, char *spectext);
extern   ULONG           __HELP(char *args);

extern   void              DeleteZQList(int class);

extern   __saveds void     CoProcImmed(void);
extern   __saveds void     CoProcBkgnd(void);
extern   __saveds void     CoProcCall(void);
extern   __saveds void     CoProcProc(void);


// cp0.c 

extern   int             __MSGADDED(int argc, char **argv);
extern   int             __REPORT(int argc, char **argv);
extern   int             __ZMH(int argc, char **argv);

// cp1.c

extern   int             __QUICKATTACH(int argc, char **argv);
extern   int             __QUICKREQUEST(int argc, char **argv);

// cp2.c

extern   int               CallIt(char *add);
extern   int               MasterCallRoutine(char *flavors,char *limzone);
extern   int               Zone2Adjust(int c,int zone,int hour,int minute);
extern   int             __ACCOUNTING(int argc, char **argv);
extern   int             __CALL(int argc, char **argv);
extern   int             __CRASH(int argc, char **argv);
extern   int             __DELAYCALL(int argc, char **argv);
extern   int             __DIRECT(int argc, char **argv);
extern   int             __DIRECTONLY(int argc, char **argv);
extern   int             __NODELIST(int argc, char **argv);
extern   int             __NORMAL(int argc, char **argv);
extern   int             __NO_PEND_TRAPDOOR(int argc, char **argv);
extern   int             __REQUEST(int argc, char **argv);

/* cp3.c */

extern   BPTR              ToPrint(BPTR window,BPTR mynull,int conditional);

extern   int               ChangeIt(int argc, char **argv, char *huh, BPTR window, BPTR mynull);
extern   int               DoAreafix(BPTR window);
extern   int               PerformBundle(BPTR where);
extern   int               PerformExport(BPTR where);
extern   int               PerformImport(BPTR where);
extern   int               PerformNetscan(BPTR where);
extern   int               TruncateFile(char *name, int length);
extern   int               TruncateFiles(char *names,int length,BPTR window);
extern   int               TwoDto4D(char *string);
extern   int             __CHANGEBOTH(int argc, char **argv, BPTR window, BPTR mynull);
extern   int             __CHANGEFLOW(int argc, char **argv, BPTR window, BPTR mynull);
extern   int             __CHANGEPKT(int argc, char **argv, BPTR window, BPTR mynull);
extern   int             __EXPORT(int argc, char **argv, BPTR window, BPTR mynull);
extern   int             __NETSCAN(int argc, char **argv, BPTR window, BPTR mynull);
extern   int             __PROCESS(int argc, char **argv, BPTR window, BPTR mynull);
extern   int             __TRIMLOGS(int argc, char **argv,BPTR window,BPTR mynull);
extern   int             __USEREXPORT(int argc,char **argv,BPTR window, BPTR mynull);


// spt.c

extern   BOOL              XMatch(char *what,char *string);

extern   int               CheckInbound(void);
extern   int               DoAScript(char *scriptname,BPTR fh);
extern   int               GetEnv(char *name, char *contents);
extern   int               IsTrapdoorIdle(char *tdport);
extern   int               IsTrapdoorThere(char *bbsport);
extern   int               MyClock(int *day,int *hour,int *minute,int *second);
extern   int               NumStrsfn(char *s,int *zone, int *net, int *node, int *point);
extern   int               OK2Strip(int net);
extern   int               PatMatch(char *s, char *pat);
extern   int               RMWaitPort(char *portname, char *passwd);
extern   int               SendRexx(char *portname,TEXT *string,char *returnstring);
extern   int               SendRexx2(char *portname,TEXT *string, char *returnstring);
extern   int               SetEnv(char *name, char *setting);
extern   int               ToNil(char *commandline,int flag);
extern   int               TrapTell(char *what,char *returnstring);
extern   int               WaitForEnv(char *waitforwhat);
extern   int             __AddToWild(char *in,char *out,ULONG type);
extern   int             __CreateNewName(char *newname, int zone, int net, int node, int point, ULONG type);
extern   int             __EvaluateFilename(char *s,char *p,int *zone,int *net,int *node,int *point,ULONG *type);
extern   int             __FidoPatternMatch(char *s_in, char *s_pat);
extern   int             __mystrsfn(char *in, char *aa, char *bb, char *cc, char *dd, char *ee);
extern   int             __mystrsfn2(char *in, char *aa, char *bb, char *cc, char *dd, char *ee);
extern   int             __NFComponents(char *in, char *A, char *B, char *C, char *D, char *E);
extern   int             __RenameOutbound(char *old, char *new);

extern   ULONG             CalcAreasCRC(struct areas *areas);
extern   ULONG           __UserType(char *spec, char *pktorflow);

extern   void  __stdargs   Log(char *format, ...);



// DLGmail.c

extern   int               brkfunc(void);
extern   int               CreateMes(struct IntuiText *x, int left,int top,char *mesg);
extern   int               DisplayErrReq(struct Window *window, char *errtext, char *solutiontext);
extern   int               LoadUpAreas(void);
extern   int               main(int argc, char **argv);

// 1con.c

extern   char              ConGetChar(void);

extern   int               ConCheck(void);
extern   int               ConOpen(void);
extern   int               ConPutStr(char *Str);
extern   int               InitGrid(void);
extern   int               InitLines(void);
extern   int               InitWindow(void);
extern   int               PrintConsoleCoProc(char *x);
extern   int               QueueRead(void);
extern   int               RedrawWindow(void);

extern   void              ConClose(void);
extern   void              ConCommandLine(char *string);
extern   void              ConPutChar(char Character);
extern   void              CP0CommandLine(char *string);
extern   void              CP1CommandLine(char *string);
extern   void              CP2CommandLine(char *string);
extern   void              CP3CommandLine(char *string);
extern   void              InstCommandLine(char *string);
extern   void  __stdargs   StatCommandLine(char *string, ...);

// gc.c

extern   int               CheckArchivers(void);
extern   int               ClearStructLoadFlags(void);
extern   int               CurrentConfig(void);
extern   int               LoadConfig(void);
extern   int               LoadStruct(int how);
extern   int               PutCFGNumber(int magic,int offset,int number);
extern   int               PutCFGString(int magic,int offset,char *string);
extern   int               RedFace1(void);
extern   int               StuffStruct(char *token,char *args,int number,char *stufferror);
extern   int             __LoadARE(void);
extern   int             __UnLoadARE(void);

