extern   struct Library   *DiskfontBase;
extern   struct GfxBase   *GfxBase;

// Check out and note if it's OK

extern   int                (*_ONBREAK)();

// top.c

extern   struct   List       *queues[CLASS_TOTALS];
extern   BOOL                 DLGMAILSTOP;
extern   int                  PROCESSINGSUSPENDED;

// gc.c

extern   struct   GConfig    *GCFG;
extern   char                *GCFG_block;
extern   int                  GCFG_size;

// DLGmail.c

extern   struct   Screen     *DLGMailScreen;
extern   struct   Window     *DLGMailWindow;
extern   struct   Library    *DLGBase;

extern   BPTR                 NULLFH;

extern   char                 logstr[1000];

extern   int                  IT_IS_ZMH;
extern   int                  TRAPDOOR_LOADED;
extern   int                  MyDEBUG;

// 1con.c

extern   struct   IOStdReq   *ConRiteMsg;
extern   struct   Window     *Window;
extern   struct   Screen     *Screen;
extern   UBYTE                PUBSCNNAME[50];
extern   int                  ScreenPixelWidth;

// main

extern   BPTR                 sout;
extern   int                  MyDEBUG;
extern   int                  OS_VERSION;
extern   LONG                 i_am_bundling;
extern   LONG                 i_am_processing;
extern   long                 curdir;

