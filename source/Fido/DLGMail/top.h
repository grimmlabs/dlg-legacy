/* top.h -- include this ONLY with top.c */


/* create 4 different queues */

struct Tokenize
{
   char *tk_command;
   int   tk_toknum;
   int   tk_class;
   int   tk_pri;
   ULONG tk_flags;
   char *tk_template;
   char *tk_help;
};

struct PhoneySegList
{
   LONG  psl_size;   /* ours, not Leo's */
   LONG  psl_length;
   BPTR  psl_NextSeg;      /*  BPTR to next element in list  */
   UWORD psl_JMP;    /*  A 68000 JMP abs.l instruction  */
   void  (*func)();  /*  The address of the function  */
};

struct ProcMsg
{                        /* startup message sent to child */
        struct Message msg;
        int (*fp)();                    /* function we're going to call */
        void *global_data;              /* global data reg (A4)         */
        long return_code;               /* return code from process     */
        struct PhoneySegList *seg;       /* pointer to fake seglist so   */
                                        /* can it can be free'd         */
                                        /* user info can go here */
};

struct Tokenize tokens[]=
{
   /* instant - activity returned immediately */

   { "QUIT",   255,  -2,      0, 0, "" ,  "Kills the DLGMail process, allows processing to completion"},
   { "STOP",   254,  -2,      0, 0, "" ,  "Kills the DLGMail process, sets flags to halt processing"},
   { "TRAPDUMP",  1, CLASS_NOW,  0, 0, "<ON | OFF>" , "Enables/disables TrapDoor  WARNING: May tie up DLGMail ARexx port."},
   { "RECONFIG",  2, CLASS_NOW,  0, 0, "" ,  "Reloads the DLGMail.CFG file"},
   { "WHEREIS",   3, CLASS_NOW,  0, 0, "" ,  "Private, do not call"},
   { "PUBSHELL",  4, CLASS_NOW,  0, 0, "" ,  "Opens a new shell on the public screen"},
   { "STATUS", 5, CLASS_NOW,  0, 0, "<CPx | TRAPDOOR>" ,"Reports status of various processes" },
   { "HELP",   6, CLASS_NOW,  0, 0, "<COMMAND> to give help on","Displays help for command specified"},
   { "PROCESSING", 7,      CLASS_NOW,      0,      0,      "<SUSPEND | RESUME>","Suspends or resumes all CP3 activity" },

   /* immediate - no status returned */

   { "REPORT", 11,   CLASS_IMMED,   0, 0, "" ,  "Generates a LOGS:DLGMailCFG.RPT file"},
   { "ZMH", 12,   CLASS_IMMED,   5, 0, "<ON | OFF>" , "Enables/disables Zone Mail Hour"},
   { "MSGADDED",   13,     CLASS_IMMED,    0,      0,      "<TAGNAME | AREA#>" , "Queues an echo area for later use by USEREXPORT"},
   { "DUMMY",  14,   CLASS_IMMED,   0, 0, "a test entry only" ,   "a test entry only" },

   /*bkgnd - status information stored in status variable */

   { "QUICKATTACH",21,  CLASS_BKGND,   0, 0, "<PATH:FILENAME> <FLAVOR> <ADDR> [...]" , "File attach generator"},
   { "QUICKREQUEST",22, CLASS_BKGND,   0, 0, "<ADDR> <FILE> [FILE...]" ,   "File request generator"},

   /*call*/

   { "NODELIST",  31,   CLASS_CALL, 5, 0, "<DIFF | NEW | RECOMPILE>" ,  "Processes the nodelist(s) using .DMB scripts"},
   { "CALL",   32,   CLASS_CALL, 0, 0, "[ADDRESS ADDR] | [ZONE <ZONENUMBER>]" ,  "Scans outbound, places calls of appropriate flavor"},
   { "DELAYCALL", 33,   CLASS_CALL, 0, 0, "[SECONDS] [ZONE <ZONENUMBER>]" ,   "Pauses, scans outbound, places calls of appropriate flavor"},
   { "CRASH",  34,   CLASS_CALL, 1, 0, "[ZONE <ZONENUMBER>]" , "Scans outbound, places only CRASH flavored calls"},
   { "DIRECT", 35,   CLASS_CALL, 0, 0, "[ZONE <ZONENUMBER>]" , "Scans outbound, places CRASH, NORMAL and DIRECT flavored calls"},
   { "DIRECTONLY",   36,   CLASS_CALL, 0, 0, "[ZONE <ZONENUMBER>]" , "Scans outbound, places only DIRECT flavored calls"},
   { "NORMAL", 37,   CLASS_CALL, 0, 0, "[ZONE <ZONENUMBER>]" , "Scans outbound, places CRASH, NORMAL and REQ flavored calls"},
   { "REQUEST",   38,   CLASS_CALL, -1,   0, "[ZONE <ZONENUMBER>]" , "Scans outbound, places only REQ flavored calls"},
   { "TRAPDOOR",  39,   CLASS_CALL, -5,   0, "<ON | OFF | RECONFIG | ANSWER | NOANSWER>" ,   "Enables/disables TrapDoor, alters operation"},
   { "ACCOUNTING",391,  CLASS_CALL, 1, 0, "<CLEAR> [ADDRESS <address>]" , "Clears TrapList call accounting"},

   /*proc*/

   { "NETSCAN",   40,   CLASS_PROC, 2, 0, "" ,  "Scans netmail area only, then bundles"},
   { "PROCESS",   41,   CLASS_PROC, 1, 0, "" ,  "Processes inbound mail and files"},
   { "EXPORT", 42,   CLASS_PROC, 0, 0, "[[AREA# | TAGNAME]...]" , "Scans echo and netmail areas, then bundles"},
   { "CHANGEFLOW",   43,   CLASS_PROC, -1,   0, "<4D_ADDR> <FLAVOR> TO <FLAVOR> [POLL [EXPORT]]" , "Reflavors flow files, generates poll flow files, may EXPORT"},
   { "CHANGEPKT", 44,   CLASS_PROC, -1,   0, "<4D_ADDR> <FLAVOR> TO <FLAVOR>" ,  "Reflavors packets"},
   { "CHANGEBOTH", 47,  CLASS_PROC, -1,   0, "<4D_ADDR> <FLAVOR> TO <FLAVOR>" ,  "Reflavors both packets and flow files,generates poll flow, may EXPORT"},
   { "USEREXPORT", 45,     CLASS_PROC,     -1,      0,     "" ,   "Scans echo areas with user activity plus netmail area, then bundles"},
   { "TRIMLOGS",   46,     CLASS_PROC,     -1,     0, "" ,  "Trims all LOGS:*.LOG files"},
   { "RELOADARE",  48,  CLASS_PROC, -1,   0, "" ,  "Reloads an edited DLGMail.ARE file"},
   /* end of list */
   { "",    -1,   -1,      -127, 0, "" ,  ""}
};
