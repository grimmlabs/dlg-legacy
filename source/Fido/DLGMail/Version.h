#define PROGNAME "DLGMail"
#define PROGVER 2
#define PROGREV 70
#define TESTCHAR "A"
#define COMPILEDATE ""__DATE__" "__TIME__""
#define CYR 1997
#define CMO 8
#define CDA 11

#include <private/Version.h>
//static char yyy_version[]="$VER: DLGMail 2.71 ("__DATE__")";
static char ID_STRING[]="DLGMail " BUILDVER " �" COPYRIGHT " by Digerati Dreams "__AMIGADATE__;
