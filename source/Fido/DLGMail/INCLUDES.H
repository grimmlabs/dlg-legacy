
#include <exec/types.h>

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <dos.h>
#include <dos/dostags.h>

#include <math.h>
#include <ctype.h>

#include <exec/memory.h>
#include <exec/ports.h>
#include <exec/lists.h>

#include <libraries/dosextens.h>

#include <intuition/intuition.h>
#include <intuition/intuitionbase.h>

#include <rexx/storage.h>

#include <clib/exec_protos.h>
#include <clib/graphics_protos.h>
#include <clib/diskfont_protos.h>


#include <proto/dlg.h>
#include <proto/dos.h>
#include <proto/intuition.h>
#include <proto/exec.h>
#include <proto/Graphics.h>
#include <proto/DiskFont.h>

#include <dlg/resman.h>

#include <dlg/globalconfig.h>
#include "queue.h"

#include <pragmas/dlg.h>

