
extern void exportreport(void);
extern int FileToPKT(int partialonly);
extern int TallyMsgs(int z, int N, int n, int p, int how);
extern int TallyThreshold(int size);
extern int ClearPKTStruct(char *s,int size);
extern int AppendToPKTStruct(char *buffer,int offset,char *string,int length);
extern int GetPKTPw(char *address, char *password, char *use);
extern BPTR SearchFO(char *name);
extern BPTR FastOpen(char *name, long how);
extern int FastClose(BPTR fh);
extern int FastCloseAll(void);
extern long FastWrite(BPTR fh,void *bu,LONG size);
extern int FastClosePKT(void);
extern int ClosePKT(void);
extern USHORT FixNumbers(USHORT number);
extern int AddToPackets(char *start,int length,struct nodenum *outbound,int strippedlength, int waslocal,struct area *_thisarea);
extern int AppendToPKT(UBYTE *what, int howmuch);
extern int OpenPKT(int zone, int net, int node, int point,int fromzone,int fromnet,int fromnode,int frompoint);
extern void delete_all_nodenum2(struct nodenum *n);
extern void *new2(int size);
extern struct nodenum *create_nodenum5_2(char *org,short zone,short net,short node,short point);
extern struct nodenum *create_nodenum_on_end5_2(struct nodenum **n,char *org,short zone,short net,short node,short point);
extern struct nodenum *create_nodenum_in_order2(struct nodenum **n,short net,short node);
extern char *memreaduntilterm_SL(char *loc,char *buffer,char *termchars);
extern struct nodenum *my_get_nodenum(struct areas *X,char *areaname);
extern int handlemsg2(char *name, char *tagname, char *start, int *length, struct area *_thisarea);
extern int pointtest(char *originaddress,short zone,short net,short node,short point,int akanet);
extern char *readextendednetlist(char *loc,char *listindstring, struct nodenum **nn,int stripthis,int ifstrip);
extern char *printoutstring(char *loc,char *string,int *linecounter);
extern char *writeextendednetlist(char *loc,char *listindstring,struct nodenum *n);
 

