#include "pdqnet.h"

/* Globals */

char MASTERLOGSTRING[MLSLENGTH];

/*******************************/

int Log(char *map,char *string)
{
	char logme[1000];

	char logbuf[100];

	char NOW[20];		/* holds the time each time MyTime is called */
	char WHEN[20];		/* holds the date each time MyTime is called */

	FILE *fp;

	int i;

	unsigned char clock[8];
	unsigned char time[8];
	unsigned char date[8];

	char p[30];
	char p1[30];

	if(_NLNET != 1)
		return(1);

	getclk(clock);

	time[0]=clock[4];
	time[1]=clock[5];
	time[2]=clock[6];
	time[3]=clock[4];

	stptime(p,2,time);	/*mode 7= hr:mm HH*/

	strcpy(NOW,p);

	date[0]=clock[1];
	date[1]=clock[2];
	date[2]=clock[3];

	stpdate(p,4,date); 	/* will give us MONth */
	stpdate(p1,3,date); 	/* will give us dd-yy */

	strlwr(p+1);

	p[3]=0x00;
	strncat(p,p1+2,6);

	strcpy(WHEN,p);

	strcpy(logbuf,map);
	strcat(logbuf,"        ");
	logbuf[5]=NULL;

	sprintf(logme,"%s%s %s %s\n",logbuf,WHEN,NOW,string);

	for(i=0;i<10;i++)
	{
		fp=fopen(LOGFILENAME,"a");
		if(!fp)
		{
			Delay(25);	/* try again in half a second */
			continue;
		}
		else break;
	}
	if(!fp)
	{
		printf("!LOGGING ERROR!\n");
		return(0);
	}
	if(fp)
	{
		fprintf(fp,"%s",logme);
		fclose(fp);
	}
	
	return(0);
}
