#include "pdqnet.h"



/*    the functions in this module constitute the outter most loop of
   processing the netmail area   */

char *remember=NULL;
int rememberlength;

struct Fido_Msg_Header *FMH;

void ProcessNetArea(void)
{
   BOOL crashpkt=FALSE;

   char theirzone[100];
   char theirnet[100];
   char theirnode[100];
   char theirpoint[100];
   char dummy[100];

   char *p;

   char *fa_p;

   int pta;

   int destinationzone;
   int destinationnet;
   int destinationnode;
   int destinationpoint;

   int pointkludge=0;
   int fromzone;
   int fromnet;
   int fromnode;
   int frompoint;
   int mkg;

   int d_zone;
   int d_net;
   int d_node;
   int d_point;

   int pkt_f_zone;
   int pkt_f_net;
   int pkt_f_node;
   int pkt_f_point;

   int fa_zone, fa_net, fa_node, fa_point, fa_junk;
   char fa_how[20];

   char newintlkludge[100];
   BOOL ik;
   BOOL pk;
   BOOL mk;       /* MSGID kludge flag */

   char msgidtoken[30];
   int msgidzone,msgidnet,msgidnode;

   char quickstring[255];

   int k_z,k_N,k_n,k_p;




   char *pp;

   char kludge[170];
   int kludgelength;

   char intltoken[100];
   char token[100];
   char name[255];
   int ptb;
   int skipkludgebytes;

   BOOL WriteHWFlag=TRUE;

   int lo,hi,hiwater;

   int ii;  /* our overall loop variable */

      char *start;   /* from readinmsg */
      int length;

   int okay;

   /* first, get our working pointers */



   if(!GetPointers())
   {
      printf("ERROR: Can't get current pointers\n");
      QuietExit();
   }

   lo=CURlo;
   hi=CURhi;

   hiwater=GetHiWater();
   if(!hiwater)
   {
      hiwater=lo-1;
      if(hiwater<1) hiwater=1;
      SetHiWater(hiwater);
   }

   sprintf(logstring,"LO=%d, HI=%d, HIWATER=%d",lo,hi,hiwater);
   Log("||   ",logstring);

   if(hiwater>hi)
   {
      printf("WARNING: Peculiar, hiwater marker > hi message.\n");
      hiwater=lo-1;
      SetHiWater(hiwater);
   }

   /* we've already established that we have something to work on in
      that hiwater < hi */

   remember=NULL;

/*
 *
 * OUTTER LOOP, process all the messages within this loop here...
 *
 */

   for(ii=(hiwater+1);ii<(hi+1);ii++)
   {
      pkt_f_zone=0;
      pkt_f_net=0;
      pkt_f_node=0;
      pkt_f_point=0;

      skipkludgebytes=0;
      strcpy(newintlkludge,"");
      ik=FALSE;
      pk=FALSE;
      pointkludge=0;
      if(remember)
      {
         FreeMem(remember,rememberlength);
         remember=NULL;
      }

      sprintf(name,"%s%d.MSG",_NETMAILDIRPATH,ii);

      printf("\n[0m[[33m%16.16s[0m] ",name);

      sprintf(logstring,"Processing %s:",name);
      Log("||*--",logstring);

      /* now we're ready to chug thru messages */

         okay=readinmsg(name,&start,&length);
      if(!okay)
      {
         printf("...Message %d Missing\n",ii);
         Log("||*--","Message Missing");
         continue;
      }

/*
 *
 * WE HAVE A MESSAGE, NOW PARSE THROUGH IT AND FIGURE OUT WHERE IT IS FROM/TO
 *
 */

      FMH=(void *)start;

      destinationzone=_MyZone;   /* <=== AN ASSUMPTION, OF COURSE */
      destinationnet=FMH->DestNet;
      destinationnode=FMH->DestNode;
      destinationpoint=0;     /* <=== TO BE REPLACED LATER */
      d_point=0;

      sprintf(intltoken,"%d:%d/%d",destinationzone,destinationnet,destinationnode);

      fromnet=FMH->OrigNet;
      fromnode=FMH->OrigNode;

      /* put code in here to check for point and international kludges
      and use those addresses instead of that in the header */

      kludgelength=length-sizeof(struct Fido_Msg_Header);
      if(kludgelength>168) kludgelength=168;

/*
 *
 * LOOK FOR KLUDGES...
 *
 */

      if(kludgelength>0)
      {
         kludgelength=stccpy(kludge,(char *)(start+sizeof(struct Fido_Msg_Header)),kludgelength);

         kludge[kludgelength]=NULL;
         p=kludge;
   
         strcpy(logstring,"Looking for kludges... ");

         while(1)
         {
            p=strchr(p,CTRL_A);
            if(p==NULL)
            {
               break;
            }
            p++;  /* get over control A */

            p=stptok(p,token,sizeof(token)," \r\n");

            if(0==stricmp(token,"INTL"))
            {
               ik=TRUE;
               p=stpblk(++p); /* get past known whitespace */
            
               p=stptok(p,intltoken,sizeof(intltoken)," \r\n"); /* get DESTINATION address out of kludge */

               printf("[32m[[33mINTL[32m][0m ");
               strcat(logstring,"[INTL] ");

               NFComponents(intltoken,theirzone,theirnet,theirnode,theirpoint,dummy);
               d_zone=atoi(theirzone);
               d_net=atoi(theirnet);
               d_node=atoi(theirnode);
   
               if(d_net==destinationnet && d_node==destinationnode)
               {
                  destinationzone=d_zone;
               }



/* VERY EXPERIMENTAL PIECE OF CODE HERE TO BYPASS DLG'S ZONE GATING! */

               destinationzone=d_zone;
               destinationnet=d_net;
               destinationnode=d_node;
            
/* END VERY EXPERIMENTAL PIECE OF CODE */

               /* use this as destination */
            }

            if(0==stricmp(token,"TOPT"))
            {
               pk=TRUE;
               p=stpblk(++p);

               p=stptok(p,token,sizeof(token)," \r\n");

               printf("[32m[[33mTOPT[32m][0m ");
               strcat(logstring,"[TOPT] ");
               d_point=atoi(token);

               destinationpoint=d_point;
            }

/*
 *
 * HUNT FOR MSGID KLUDGE
 *
 */


            if(0==stricmp(token,"MSGID:"))
            {
               mk=TRUE;
               p=stpblk(++p);

               p=stptok(p,msgidtoken,sizeof(msgidtoken)," \r\n"); /* get DESTINATION address out of kludge */

               printf("[32m[[33mMSGID[32m][0m ");
               strcat(logstring,"[MSGID] ");

               if(MSGIDComponents(msgidtoken,theirzone,theirnet,theirnode,theirpoint,dummy))
               {
                  msgidzone=atoi(theirzone);
                  msgidnet=atoi(theirnet);
                  msgidnode=atoi(theirnode);
   
                  if(ik==FALSE)
                  {
                     destinationzone=msgidzone;
                  }
               }
            }

/*
 *
 * END MSGID KLUDGE
 *
 */
               





         }

         Log("|||  ",logstring);
      }
      else
      {
         if(kludgelength<0)
         {
            printf("ERROR! Kludgelength < 0 ... Please report to Steve\n");
            Log("|||  ","ERROR! Kludgelength < 0, nofity Steve!");
         }
         else
            Log("|||  ","Message has no body text");
      }

      printf("-> [[33m%d:%d/%d.%d[0m] ",destinationzone,destinationnet,destinationnode,destinationpoint);
      sprintf(logstring,"Destination: [%d:%d/%d.%d] ",destinationzone,destinationnet,destinationnode,destinationpoint);
      Log("|||  ",logstring);

      /* now we need to decide if we need to process the msg */

      if((FMH->Attribute & SENT) && (FMH->Attribute & LOCAL))
      {
         printf("[32m<LOCAL> <SENT>[0m\n");
         Log("|||  ","Skipping: Local,Sent");
         hiwater=ii;
         if(WriteHWFlag) SetHiWater(hiwater);
         continue;   /* go to next msg */
      }

      /* ok, it's local and has been sent so no */

      if(FMH->Attribute & SENT)
      {
         printf("[32m<SENT> ROUTED[0m\n");
         Log("|||  ","Skipping: Routed,Sent");
         hiwater=ii;
         if(WriteHWFlag) SetHiWater(hiwater);
         continue;   /* go to next msg */
      }

      /* ok, it's not from here but has already been sent so no */

      if((FMH->Attribute & FILEREQUEST) && (FMH->Attribute & LOCAL))
      {
         printf("[33m<LOCAL>[0m [[33mProcessing File Request[0m]\n");
         Log("||||*","FILEREQUEST BIT DETECTED...");

         sprintf(quickstring,"FIDO:DMC QuickRequest %d:%d/%d.%d %s",destinationzone,destinationnet,destinationnode,destinationpoint,FMH->Title);
         MyExecute(quickstring,0,0);

         Log("|||||",quickstring);

         hiwater=ii;
         if(WriteHWFlag) SetHiWater(hiwater);
         DeleteFile(name); /* get rid of this thing */
         continue;
      }

      pta=IsMsgToMe(destinationzone,destinationnet,destinationnode,destinationpoint);

      if(pta==1)
      {

/* we need to do
name remapping
just in case a
message comes in
to one of my
points where the
sender doesn't
know the point's
point number */

         ptb=-1;
         if(_POINTENABLE) ptb=IsMsgToAPoint(FMH->To);
         if(ptb<1)
         {
            printf("[32mAT DESTINATION[0m\n");
            Log("|||  ","Skipping: At destination");
            hiwater=ii;
            if(WriteHWFlag) SetHiWater(hiwater);
            continue;   /* go to next msg */
         }
         else
         {
            printf("\nRemapping message for [[33m%s[0m] to point [[33m%d[0m]\n",FMH->To,ptb);
            destinationpoint=ptb;
            destinationzone=_MyZone;
            destinationnet=_MyNet;
            destinationnode=_MyNode;
            pta=3;

/* if there isn't a
TOPT kludge, add one */

            pointkludge=ptb;

         }
      }
      
      /* ok, it's addressed to me (my main address or an AKA) so no */

      if(pta==0 && destinationpoint) /* it's to a point, but not mine */
      {
         destinationpoint=0;
      }


      if((FMH->Attribute & FILEATTACHED) && (FMH->Attribute & LOCAL))
      {
         printf("[33m<LOCAL> <FILEATTACHED> [0m[[33mProcessing Attached File[0m]\n");
         Log("||||*","FILEATTACHED BIT DETECTED...");

         fa_p=FMH->Title;

         if(FMH->Attribute & CRASH)
            strcpy(fa_how,"CRASH");
         else
            strcpy(fa_how,"EXISTING");

         while(1)
         {
            fa_p=stpblk(fa_p);
            if(*fa_p==NULL || *fa_p=='\r' || *fa_p=='\n') break;  /* end of subject line */
            fa_p=stptok(fa_p,token,sizeof(token)," \t\r\n");

            printf("[32mFile: [[33m%s[0m]\n");

            sprintf(quickstring,"FIDO:DMC QuickAttach %s %s %d:%d/%d.%d",token,fa_how,destinationzone,destinationnet,destinationnode,destinationpoint);
            MyExecute(quickstring,0,0);
            Log("|||||",quickstring);
         }
      }



      /* at this point, the message is either in transit or local and needs to be processed */








/* routing of
file attaches
for points */


      if((FMH->Attribute & FILEATTACHED) && !(FMH->Attribute & LOCAL) && destinationpoint)
      {
         printf("[33m<FILEATTACHED> [0m[[33mRouted To Point[0m]\n");
         Log("||||*","FILEATTACHED BIT DETECTED (FOR POINT)...");

         fa_zone=destinationzone;
         fa_net=destinationnet;
         fa_node=destinationnode;
         fa_point=destinationpoint;

         if(NoRouteNetMail(fa_zone,fa_net,fa_node,fa_point,0))
         {
            if(MyDEBUG) printf("point fileattach destination in bundler routing file");
            Log("|||| ","Point file attach destination address in bundler routing file");
         }
         else
         {
            if(RouteNetMail(&fa_zone,&fa_net,&fa_node,&fa_point,0))
               if(MyDEBUG) printf("\tRouting point fileattach though [%d:%d/%d.%d]\n",fa_zone,fa_net,fa_node,fa_point);
         }

         fa_p=FMH->Title;

         while(1)
         {
            fa_p=stpblk(fa_p);
            if(*fa_p==NULL || *fa_p=='\r' || *fa_p=='\n') break;  /* end of subject line */
            fa_p=stptok(fa_p,token,sizeof(token)," \t\r\n");

            printf("[32mFile: [[33m%s[32m][0m\n");

            fa_junk=StripRoutedFileAttach(token);
            if(fa_junk)
            {
               sprintf(quickstring,"FIDO:DMC QuickAttach %s HOLD %d:%d/%d.%d",token,fa_zone,fa_net,fa_node,fa_point);
               MyExecute(quickstring,0,0);
               Log("|||||",quickstring);
            }
         }
      }











      if(FMH->Attribute & LOCAL)
      {
         printf("[33m<LOCAL>[0m\n");
         if((fromnet==0) && (fromnode==0))
         {
            fromnet=_MyNet;
            fromnode=_MyNode;
         }
      }
      else
      {
         if((fromnet!=_MyNet) || (fromnode!=_MyNode))
         {
            if(_NETMAILROUTE==0)
            {
               printf("* Skipping: Routing of netmail not enabled\n");
               Log("|||  ","Skipping: Routing of netmail not enabled");
               hiwater=ii;
               if(WriteHWFlag) SetHiWater(hiwater);
               continue;   /* go to next msg */
            }
            else
               printf("[[33mRouting Net Message[0m]\n");
         }
      }

      if(destinationpoint && (FMH->Attribute & CRASH))
      {
         printf("* Killing <CRASH> bit on message to a point\n");
         FMH->Attribute ^=CRASH;
      }


/* we want to
do some remapping
here to take care
of netmail destined
for other networks
we might belong to
right here */

/*** INTL BUG ***/

      if(FMH->Attribute & LOCAL && destinationpoint==0)
      {
         mkg=-2;

         if(  (destinationzone!=_MyZone)  || _FORCEINTL)
            mkg=MapFromAddress(_MyZone,destinationzone,&fromzone,&fromnet,&fromnode,&frompoint);

            /* mkg is the array index of our address, that
               is to say, _ADDRESS[mkg] */

         if(mkg==-1)
         {
            printf("\n* No remapping done. Origination and Destination both in Fidonet.\n");
         }


         if(mkg>-1)  /* mkg is valid only if 0 or over */
         {
            if(mkg>-1)  /* if mkg is a valid index */
            {
               printf("\n* Remapping/building kludge...\nOrig address was [[33m%s[0m] now [[33m%s[0m]\n",_ADDRESS[0],_ADDRESS[mkg]);
               sprintf(logstring,"Remapping origination address from [%s] to [%s]",_ADDRESS[0],_ADDRESS[mkg]);
               Log("|||  ",logstring);
/* we need to fix the
kludge line in the
outgoing msg pkt... it
now contains our local
fidonet address as an
origin address, and
we'd rather show our
AKA address there */

/* this code assumes DLG
will always build an INTL
kludge for all zones besides
my own */


               /* first, adjust start and length to pass up existing
                     intl and point kludges*/

               pp=start+sizeof(struct Fido_Msg_Header);

               if(ik)
               {
                  if(MyDEBUG)printf("INTL KLUDGE REMAP\n");
                  while(1)
                  {
                     if(MyDEBUG)printf("searching for INTL ^M\n");
                     pp++;
                     skipkludgebytes++;
                     if(*pp=='\r') break;
                  }
                  if(*pp=='\n')
                  {
                     if(MyDEBUG)printf("found a INTL ^J\n");
                     pp++;
                     skipkludgebytes++;;
                  }
               }

               if(pk)
               {
                  if(MyDEBUG)printf("TOPT KLUDGE REMAP\n");
                  while(1)
                  {
                     if(MyDEBUG)printf("searching for TOPT ^M\n");
                     pp++;
                     skipkludgebytes++;            
                     if(*pp=='\r') break;
                  }
                  if(*pp=='\n')
                  {
                     if(MyDEBUG)printf("found a TOPT ^J\n");
                     pp++;
                     skipkludgebytes++;
                  }
               }

               RetzNn(_ADDRESS[mkg],&k_z,&k_N,&k_n,&k_p);
   
               sprintf(newintlkludge,"%s %d:%d/%d",intltoken,k_z,k_N,k_n);

               if(MyDEBUG)printf("found the period, about to pointkludge=d_point\n");
               /* pp is now sitting on the first character (or null)
                  of a "from point" number, suitable for atoi()ing
                  int a from point # address if necessary 
                  WE'RE NOT DOING ANYTHING WITH IT NOW */

               pkt_f_zone=k_z;
               pkt_f_net=k_N;
               pkt_f_node=k_n;
               pkt_f_point=k_p;

               if(d_point) pointkludge=d_point;
               if(skipkludgebytes) skipkludgebytes++;

            }
         }

      }



      if(FMH->Attribute & CRASH)
         crashpkt=TRUE;
      else
         crashpkt=FALSE;

      if(MyDEBUG)printf("Packing message...\n");

      if(MsgToPKT(start,length,destinationzone,destinationnet,destinationnode,destinationpoint,fromnet,fromnode,crashpkt,pointkludge,newintlkludge,skipkludgebytes,pkt_f_zone,pkt_f_net,pkt_f_node,pkt_f_point))  /* expects a true if OK */
      {
         FMH->Attribute |= SENT; /* set the sent bit */
         mywriteoutmsg(name,start,length,(FMH->Attribute & KILLSENT));
         hiwater=ii;
         if(WriteHWFlag) SetHiWater(hiwater);
      }
      else
      {
         WriteHWFlag=FALSE;   /* so we can try again later */
      }
      FreeMem(remember,rememberlength);
      remember=NULL;

      Log("||*--","Done Processing Message");
   }
   return;
}

int StripRoutedFileAttach(char *file)
{
   /* we need to strip off everything but the filename and extension
      then look for the file in ODD: or inbound: and add the path to it */

   char *s;
   char t[100];
   BPTR fh;

   s=file+strlen(file);

   while(s>file)
   {
      if(*s=='/' || *s==':' || *s=='\\')
      {
         s++;
         break;
      }
      s--;
   }

   /* s now sitting on top of file name */



   /*
   if(_MOVEATTACHES)
      sprintf(t,"ODD:%s",s);
   else
      sprintf(t,"INBOUND:%s",s);
   */

   sprintf(t,"OUTBOUND:%s",s);

   if(MyDEBUG)printf("trying to lock for routed file to point [%s]\n",t);


   fh=Lock(t,ACCESS_READ);
   if(fh)
   {
      if(MyDEBUG)printf("locked it!\n");
      UnLock(fh);
      strcpy(file,t);
      return(1);
   }
   strcpy(file,"");
   return(0);
}


int RetzNn(char *string,int *zone,int *net, int *node, int *point)
{
   char z[20],N[20],n[20],p[20];

   strsfn(string,z,N,n,p);
   *zone=atoi(z);
   *net=atoi(N);
   *node=atoi(n);
   *point=atoi(p);
   return(1);
}

/// MapFromAddress
int MapFromAddress(int myzone,int destinationzone,int *fromzone,int *fromnet,int *fromnode,int *frompoint)
{
   char a[10],b[10],c[10],d[10];
   int aa,bb,cc,dd;

   int i,rc=0;

   BOOL MYFIDOFLAG=FALSE;
   BOOL FROMFIDOFLAG=FALSE;
   BOOL TOFIDOFLAG=FALSE;

// undo this later!

   return(-1);

   if(myzone>0 && myzone<8)
      MYFIDOFLAG=TRUE;

   if(destinationzone>0 && destinationzone<8)
      TOFIDOFLAG=TRUE;


   if(!_FORCEINTL)
   {
      if(MYFIDOFLAG && TOFIDOFLAG)
         return(-1); /* we're in fido and the destination is fido */

      for(i=0;i<_totADDRESS;i++)
      {
         strsfn(_ADDRESS[i],a,b,c,d);
         aa=atoi(a);
         bb=atoi(b);
         cc=atoi(c);
         dd=atoi(d);
      
         if(aa==destinationzone)
         {
            rc=i;
            *fromzone=aa;
            *fromnet=bb;
            *fromnode=cc;
            *frompoint=dd;
            break;
         }
      }
   }
   else
   {
      if(MYFIDOFLAG)
      {
         /* we are in fido as a primary address */

         if(TOFIDOFLAG)
            return(0);

         /* the above will force a return of _ADDRESS[0]
            to build the kludge */
      }
      for(i=0;i<_totADDRESS;i++)
      {
         FROMFIDOFLAG=FALSE;

         strsfn(_ADDRESS[i],a,b,c,d);
         aa=atoi(a);
         bb=atoi(b);
         cc=atoi(c);
         dd=atoi(d);
      
         if(aa>0 && aa<8)
            FROMFIDOFLAG=TRUE;

         if(aa==destinationzone || (FROMFIDOFLAG && TOFIDOFLAG))
         {
            rc=i;
            *fromzone=aa;
            *fromnet=bb;
            *fromnode=cc;
            *frompoint=dd;
            break;
         }
      }

   }
   return(rc);
}
//-

int IsMsgToAPoint(char *name)
{
   FILE *fp;
   char buffer[250],buf[250];
   char bufp[250];
   BOOL found=FALSE;
   int arraypointer=0;
   char namearray[5][40];
   int i,point=-1;
   char *s,*p;

   fp=fopen("FIDO:DLGMail.PNT","r");
   if(!fp)
   {
      if(MyDEBUG) printf("no FIDO:DLGMail.PNT routing file available\n");
      return(-1);
   }

   while(1)
   {
      if(NULL==fgets(buffer,248,fp))
         break;

      buffer[strlen(buffer)-1]=NULL;

      if(buffer[0]==';') continue;
      if(strlen(buffer)==0) continue;


      arraypointer=0;
      p=buffer;
      while(1)
      {
         s=p;
         p=stptok(s,buf,sizeof(buf)," \t");
         s=stpblk(p);   /* pass blanks */
         if(p==s) break;   /* hmmm... fuckup... break! */
         p=s;

         if(0==stricmp(buf,"TOPOINT"))
         {
            strcpy(bufp,"");
            for(i=0;i<arraypointer;i++)
            {
               strcat(bufp,namearray[i]);
               if(i!=(arraypointer-1)) strcat(bufp," ");
            }

            if(0==stricmp(name,bufp))
            {
               /* one of our points */
               p=stptok(s,buf,sizeof(buf)," \t");
               point=atoi(buf);
               found=TRUE;
               break;
            }

         }
         else
         {
            strcpy(namearray[arraypointer],buf);
            arraypointer++;
            if(arraypointer==5)
            {
               printf("WARNING: Syntax problem in DLGMail.PNT file\n");
               Log("     ","WARNING: Syntax problem in DLGMail.PNT file\n");
               break;
            }
         }
      }
      if(found==TRUE) break;
   }
   fclose(fp);
   return(point);
}

int IsMsgToMe(int destinationzone, int destinationnet, int destinationnode,int destinationpoint)
{
   int rc=0;

   int i;

   char myzone[10];
   char mynet[10];
   char mynode[10];
   char mypoint[10];

   int mzone;
   int mnet;
   int mnode;
   int mpoint;


   /* this code does not account for messages which need to be
      routed to any points feeding off this system */


   for(i=0;i<_totADDRESS;i++)
   {
      strsfn(_ADDRESS[i],myzone,mynet,mynode,mypoint);
      mzone=atoi(myzone);
      mnet=atoi(mynet);
      mnode=atoi(mynode);
      mpoint=atoi(mypoint);

      if(mzone==destinationzone && mnet==destinationnet && mnode==destinationnode)
      {
         if(MyDEBUG)
            printf("this msg matches my address %s\n",_ADDRESS[i]);

         /* put point matching code in right here */

         if(MyDEBUG) printf("test 1 is true");
         rc |= 1;
         
         if(mpoint!=destinationpoint)
         {
            if(MyDEBUG) printf("test 2 is true\n");
            rc |= 2;
         }
      }
      if(rc) return(rc);

   }

   return(0);

}

int readinmsg(char *name,char **start,int *length)
{
      BPTR fh;

   BorrowNetArea();

      fh=Open(name,MODE_OLDFILE);
      if(!fh)
   {
      if(MyDEBUG) printf("Message missing\n");
      ReturnNetArea();
      return(0);
   }

   Seek(fh,0,OFFSET_END);
      *length=Seek(fh,0,OFFSET_BEGINNING);

   rememberlength=*length+500;

   remember=(char *)AllocMem(rememberlength,MEMF_PUBLIC);

   if(remember==NULL)
   {
      printf("ERROR: Couldn't allocate memory for message\n");
      /*sprintf(logstring,"ERROR: problem allocating memory");
      if(_LOG) Log("||| ",logstring);*/
      Close(fh);
      ReturnNetArea();
      return(0);
   }

      *start=remember;
      Read(fh,(void *)*start,*length);
      Close(fh);
   ReturnNetArea();
      return(1);
}

int mywriteoutmsg(char *name,char *start,int length,BOOL kill)
{
      BPTR fh;

   BorrowNetArea();

   if(kill)
   {
      DeleteFile(name);
      printf("* <KILLSENT>\n",name);
      ReturnNetArea();
      return(1);
   }

      fh=Open(name,MODE_NEWFILE);
      if(!fh)
   {
      printf("ERROR: Can't open output file!\n");
      ReturnNetArea();
      return(0);
   }
      Write(fh,(void *)start,length);
      Close(fh);
   ReturnNetArea();
   return(1);
}




int MSGIDComponents(char *in, char *A, char *B, char *C, char *D, char *E)
{
   char *s,*p;

   p=strchr(in,':');
   if(!p) return 0;
   s=strchr(p,'/');
   if(!s) return 0;

   s=in;
   p=in;

   *A=NULL;
   *B=NULL;
   *C=NULL;
   *D=NULL;
   *E=NULL;

   p=stptok(s,A,31,".:");
   if(p!=in+strlen(in)) p++;
   s=p;
   p=stptok(s,B,31,"./");
   if(p!=in+strlen(in)) p++;
   s=p;
   p=stptok(s,C,31,".");
   if(p!=in+strlen(in)) p++;
   s=p;
   p=stptok(s,D,31,".");
   if(p!=in+strlen(in)) p++;
   s=p;
   strcpy(E,s);

   if(*E) return 5;
   if(*D) return 4;
   if(*C) return 3;
   if(*B) return 2;
   if(*A) return 1;
   return 0;
}
