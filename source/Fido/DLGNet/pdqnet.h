#include <exec/types.h>

#include <stdio.h>
#include <dos.h>
#include <string.h>
#include <time.h>
#include <stdlib.h>

#include <exec/memory.h>

#include <libraries/dos.h>

#include <proto/exec.h>
#include <proto/dos.h>
#include <proto/dlg.h>
#include <pragmas/dlg.h>

#include <dlg/globalconfig.h>
#include <dlg/resman.h>
#include <dlg/msg.h>

#include "/zflavor/flavor.h"
#include "/zflavor/flavorproto.h"
#include "/zap_h/zap.h"

#include "/ross/protos.h"

#define DLGMV2 1

#define MLSLENGTH 3000
#define MyDEBUG   (GCFG->ZNET_DB)
#define LOGFILENAME "LOGS:DLGNet.LOG"
#define CTRL_A 1
#define ZMAIL_NAME_FORMAT 1


/* ===== znet.c ===== */

/* no reference need be made to the GCfg structure, it's done in its own header */

extern char DLGSerial[8];

extern struct HiWater MyHiWater;

extern char logstring[1000];

extern BOOL MADECRASH;
extern BOOL MADENORMAL;
extern BOOL BORROWED;

extern BPTR sout;

extern UBYTE *CURMSG;

extern int CURMSGLENGTH;
extern int CURlo;
extern int CURhi;
extern int CURhiwater;

extern struct Library *DLGBase;

/* ===== procnet.c ===== */

extern char *remember;
extern int rememberlength;

extern struct Fido_Msg_Header *FMH;


/* ===== log.c ========== */

extern char MASTERLOGSTRING[MLSLENGTH];

/* ===== msg2pkt.c ======= */


extern BPTR PKTfile;


// PDQNet.c

int SetZTaskPri(void);
int MyExecute(char *, BPTR, BPTR);
int MyFork(char *, int, int, BPTR);
int ParseArgs(char **, char *);
int GetPKTPw(char *, char *, char *);
void QuietExit(void);
void BorrowNetArea(void);
void ReturnNetArea(void);
void EnterNetArea(void);
void LeaveNetArea(void);
int GetPointers(void);
BOOL SetPointers(int, int);
int GetHiWater(void);
BOOL SetHiWater(int);
USHORT FixNumbers(USHORT);

// Log.c

int Log(char *, char *);

// Msg2Pkt.c

BOOL MsgToPKT(char *, int, int, int, int, int, int, int, BOOL, int, char *, int, int, int, int, int);
int AppendToPKT(UBYTE *, int, BOOL);
OpenPKT(int, int, int, int, BOOL, int, int, int, int);
int ClosePKT(void);
RouteNetMail(int *, int *, int *, int *, int);
int NoRouteNetMail(int, int, int, int, int);
int FileToPKT(void);

// Procnet.c

void ProcessNetArea(void);
int StripRoutedFileAttach(char *);
int RetzNn(char *, int *, int *, int *, int *);
int MapFromAddress(int , int, int *, int *, int *, int *);
int IsMsgToAPoint(char *);
int IsMsgToMe(int, int, int, int);
int readinmsg(char *, char **, int *);
int mywriteoutmsg(char *, char *, int, BOOL);
int MSGIDComponents(char *, char *, char *, char *, char *, char *);
