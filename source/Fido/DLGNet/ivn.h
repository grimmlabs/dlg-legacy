#define PROGNAME "DLGNet"
#define PROGVER 2
#define PROGREV 70
#define TESTCHAR ""
#define COMPILEDATE ""__DATE__""
#define CYR 1997
#define CMO 8
#define CDA 11

#ifndef VERSIONINFO
#define VERSIONINFO

#include <private/Version.h>
#define  ObjRev "2"

static char yyy_version[]="\0$VER: DLGNet " BUILDVER "." ObjRev " �" COPYRIGHT " by Digerati Dreams "__AMIGADATE__;
static char ID_STRING[]="DLGNet " BUILDVER " �" COPYRIGHT " by Digerati Dreams "__AMIGADATE__;
#endif
