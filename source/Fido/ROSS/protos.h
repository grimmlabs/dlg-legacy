/* ross function prototypes */

/* FILE: r_utils.c */
int string1subsetstring2(char *string1,char *string2);
int charisinstring(int c,char *string);
int readuntilterm(FILE *fp,char *buffer,char *termchars);
int skipuntilterm(FILE *fp,char *termchars);
int skipchars(FILE *fp,char *skipchars);
char *memreaduntilterm(char *loc,char *buffer,char *termchars);
char *memskipuntilterm(char *loc,char *termchars);
char *memsearch(char *tof, int length, char *loc,char *searchstring,int direction);
char *memskipchars(char *loc,char *chars);
void error(char *c);

/* FILE: r_areas.c */
struct nodenum *get_nodenum(struct area *a,int i);
struct nodenum *get_next_nodenum(struct nodenum *n);
struct nodenum *get_last_nodenum(struct nodenum *n);
int is_in_nodenum_list(struct nodenum *n,short net,short node);
struct area *get_area(struct areas *a,int i);
struct area *get_area_from_tagname(struct areas *a,char *tag);
struct area *get_next_area(struct area *a);
char *get_name(struct area *a);
char *get_path(struct area *a);
short get_class(struct area *a);
short get_passthru(struct area *a);
short get_link(struct area *a);
long get_areanum(struct area *a);
struct area *find_area_string(struct areas *as,char *(*area_func)(struct area *),char *string);
struct area *find_area_name(struct areas *as,char *string);
char *get_organization(struct nodenum *n);
unsigned short get_zone(struct nodenum *n);
unsigned short get_net(struct nodenum *n);
unsigned short get_node(struct nodenum *n);
unsigned short get_point(struct nodenum *n);
char *get_from_organization(struct nodenum *n);
unsigned short get_from_zone(struct nodenum *n);
unsigned short get_from_net(struct nodenum *n);
unsigned short get_from_node(struct nodenum *n);
unsigned short get_from_point(struct nodenum *n);
struct areas *create_areas(void);
struct area *create_area(char *areastring,char *name);
struct nodenum *create_nodenum(short net,short node);
struct nodenum *create_nodenum5(char *org,short zone,short net,short node,                              short point);
struct nodenum **get_last_nodenum_ptr(struct nodenum **n);
struct nodenum *create_nodenum_on_end(struct nodenum **n,short net,short node);
struct nodenum *create_nodenum_on_end5(struct nodenum **n,char *org,short zone,short net,                           short node,short point);
struct nodenum *create_nodenum_in_order(struct nodenum **n,short net,short node);
void print_areas(struct area *a);
void fprint_netnodes(struct nodenum *n,FILE *fp);
void print_netnodes(struct nodenum *n);
void new_delete_areas(struct areas *a);
void delete_areas(struct areas *a);
void delete_all_area(struct area *a);
void delete_all_nodenum(struct nodenum *n);
struct area *add_area(struct areas *a,char *string,char *name);
struct area **get_last_area_ptr(struct areas *a);
struct area *add_area_on_end(struct areas *a,char *string,char *name);
struct nodenum *add_nodenum(struct area *a,short net,short node);
struct nodenum *add_nodenum5(struct area *a,char *org,short zone,short net,                          short node,short point);
struct nodenum *add_nodenum_on_end5(struct area *a,char *org,short zone,                                    short net,short node,short point);
int translate_line_keyword(char *word);
int translate_entry_keyword(char *word);
int skip_to_line_token(FILE *fp);
int skip_to_entry_token(FILE *fp);
int read_address(FILE *fp,char *org,short *zone,short *net,short *node,short *point);
struct areas *new_read_areas(char *fname,char *err1,char *err2);
struct areas *read_areas(char *fname);
void *new(int size);
void delete(void *v);
void myexit(int i);
void set_error_code(int,char *,char *);
void read_entry_token(FILE *fp,char *token);
short get_alias(struct area *a);
char *get_description(struct area *a);
char *get_area_from_organization(struct area *a);
unsigned short get_area_from_zone(struct area *a);
unsigned short get_area_from_net(struct area *a);
unsigned short get_area_from_node(struct area *a);
unsigned short get_area_from_point(struct area *a);



