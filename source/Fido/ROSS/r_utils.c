#include <exec/types.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <proto/dlg.h>
#include <pragmas/dlg.h>

#include "header.h"
#include "protos.h"

#define MyDEBUG 0

#define MAXSEARCH 10000

extern struct Library *DLGBase;

int string1subsetstring2(char *string1,char *string2)
{
  for(;*string1;string1++)
    if(!charisinstring(*string1,string2))
      return FALSE;
  return TRUE;
}

int charisinstring(int c,char *string)
    {
    while(*string)
   {
   if(c==*string++)
       return TRUE;
   }
    return FALSE;
    }

int readuntilterm(FILE *fp,char *buffer,char *termchars)
     /* returns termination character matched. */
{
  int c;
  for(;;)
    {
      c=fgetc(fp);
      if(c==EOF || charisinstring(c,termchars))
   {
     *buffer=0;
     return c;
   }
      else
   *buffer++=c;
    }
}

int skipuntilterm(FILE *fp,char *termchars)
          /* returns termination character matched. */
{
  int c;
  for(;;)
    {
      c=fgetc(fp);
      if(c==EOF)
   return c;
      if(charisinstring(c,termchars))
   {
     return c;
   }
    }
}

int skipchars(FILE *fp,char *skipchars) /* skips desired characters. */
{
  int c;
  for(;;)
    {
      c=fgetc(fp);
      if(c==EOF)
   return c;
      if(!charisinstring(c,skipchars))
   {
     ungetc(c,fp);
     return c;
   }
    }
}

char *memreaduntilterm(char *loc,char *buffer,char *termchars)
    {
    int i=0;
    char c;
    for(;;)
   {
   if(i++>MAXSEARCH)
      return(NULL);
       /*error("memreaduntilterm: Maximum termination search limit exceeded.\n");
      */
   c=*loc++;
   if(charisinstring(c,termchars))
       {
       *buffer=0;
       loc--;
       return loc;
       }
   else
       *buffer++=c;
   }
    }

char *memskipuntilterm(char *loc,char *termchars)
    {
    int i=0;
    char c;
    for(;;)
   {
   if(i++>MAXSEARCH)
      return(NULL);
       /*error("memskipuntilterm: Maximum termination search limit exceeded.\n");
        */
   c=*loc++;
   if(charisinstring(c,termchars))
       {
       loc--;
       return loc;
       }
   }
    }

char *memsearch(char *tof, int length, char *loc,char *searchstring,int direction)
{        /* direction must be +1 or -1 */
      int len;
      len=strlen(searchstring);
      while(Strnicmp(loc,searchstring,len))
   {
/*    printf ("LOC=%d, TOF=%d, LENGTH=%d, MAX=%d\n",loc,tof,length,tof+length);
*/ 
      /*if(i++>MAXSEARCH)*/

      if(loc<tof || loc>(tof+length-strlen(searchstring)))
      {
/*       printf("maximum termination search limit exceeded\n");
*/
         return(NULL);
      }
         /*error("memsearch: Maximum termination search limit exceeded.\n");*/
      
      loc+=direction;
   }
      return loc;
}

char *memskipchars(char *loc,char *chars)
    {
    while(charisinstring(*loc,chars))
   loc++;

    return loc;
    }

/* OLD CODE
 *
 * void error(char *c)
 *   {
 *   puts(c);
 *   fflush(stdout);
 *   myexit(10);
 *   }
 */
