#include <exec/types.h>
#include <dos.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <exec/memory.h>
#include <proto/intuition.h>

#include <proto/dlg.h>
#include <pragmas/dlg.h>

#include <libraries/dos.h>

#include "header.h"
#include "protos.h"

#include <DLG/globalconfig.h>

#define MyDEBUG 0

char *e1,*e2;

extern struct Library *DLGBase;

/// FixSlashes
void FixSlashes(char *s)
{
   char *r;

   while(r=strchr(s,'/'))
   {
      if(r) *r='!';
   }

   return;
}
//-

/// get_nodenum
struct nodenum *get_nodenum(struct area *a,int i)
{
   struct nodenum *n;

   if(i<1)
      if(MyDEBUG)
         printf("R_AREAS.C<>get_nodenum: invalid nodenum referenced\n");

   n=a->first_nodenum;

   while(--i)
   {
      n=n->next_nodenum;

      if(n == 0)
         return(0);
   }

   return(n);
}
//-

/// get_last_nodenum
struct nodenum *get_last_nodenum(struct nodenum *n)
{
   while(n->next_nodenum)
   {
      n = n->next_nodenum;
   }

   return(n);
}
//-

/// is_in_nodenum_list
int is_in_nodenum_list(struct nodenum *n,short net,short node)
{
   for(;;)
   {
      if(n==0)
         return(FALSE);

      if(net==n->net && node==n->node)
         return(TRUE);

      n=n->next_nodenum;
   }
}
//-

/// get_area
struct area *get_area(struct areas *a,int i)
{
  struct area *aa;

   if(i<1)
   {
      if(MyDEBUG) printf("R_AREAS.C<>get_area: invalid area referenced\n");
   }

   aa=a->first_area;

   while(--i)
   {
      aa=aa->next_area;

      if(aa == 0)
         return(0);
   }

   return(aa);
}
//-

struct area *get_area_from_tagname(struct areas *a,char *tagname)
{
  struct area *aa;

  aa=a->first_area;
  while(aa)
    {
      if(!Stricmp(tagname,aa->name))
   return aa;
      aa=aa->next_area;
    }
  return 0;
}

struct area *get_next_area(struct area *a)
{
  return a->next_area;
}

char *get_name(struct area *a)
{
  return a->name;
}

char *get_path(struct area *a)
{
  return a->path;
}

short get_class(struct area *a)
{
  return a->class;
}

short get_passthru(struct area *a)
{
  return a->passthru;
}

short get_link(struct area *a)
{
  return a->link;
}

short get_alias(struct area *a)
{
  return a->alias;
}

char *get_description(struct area *a)
{
  return a->description;
}

char *get_area_from_organization(struct area *a)
{
  return a->from_organization;
}

unsigned short get_area_from_zone(struct area *a)
{
  return a->from_zone;
}

unsigned short get_area_from_net(struct area *a)
{
  return a->from_net;
}

unsigned short get_area_from_node(struct area *a)
{
  return a->from_node;
}

unsigned short get_area_from_point(struct area *a)
{
  return a->from_point;
}

long get_areanum(struct area *a)
{
  return a->areanum;
}

/***********************************************************************/

struct area *find_area_string(struct areas *as,char *(*area_func)(struct area *),char *string)
{
  struct area *a;
  a=get_area(as,1);
  while( a!=0 && strcmpi( (*area_func)(a),string ) )
    {
      a=get_next_area(a);
    }
  return a;       /* return of zero is error. */
}

struct area *find_area_name(struct areas *as,char *string)
{
  struct area *a;
  a=get_area(as,1);
  while( a!=0 && strcmpi( get_name(a),string ) )
    {
      a=get_next_area(a);
    }
  return a;       /* return of zero is error. */
}

char *get_organization(struct nodenum *n)
{
  return n->organization;
}

unsigned short get_zone(struct nodenum *n)
{
  return n->zone;
}

unsigned short get_net(struct nodenum *n)
{
  return n->net;
}

unsigned short get_node(struct nodenum *n)
{
  return n->node;
}

unsigned short get_point(struct nodenum *n)
{
  return n->point;
}

char *get_from_organization(struct nodenum *n)
{
  return n->organization;
}

unsigned short get_from_zone(struct nodenum *n)
{
  return n->zone;
}

unsigned short get_from_net(struct nodenum *n)
{
  return n->net;
}

unsigned short get_from_node(struct nodenum *n)
{
  return n->node;
}

unsigned short get_from_point(struct nodenum *n)
{
  return n->point;
}

struct areas *create_areas()
{
  struct areas *a;
  a=new(sizeof(struct areas));
  if(a==0)
    return 0;
  a->first_area=0;

/*  OLD STUFF:
 *
 * strncpy(a->bbs_name,bbs,MAX_STR);
 * a->bbs_name[MAX_STR-1]=0;
 * strncpy(a->administrator,admin,MAX_STR);
 * a->administrator[MAX_STR-1]=0;
 *
 */

  /*  printf("Creating areas, bbs= %s admin= %s\n",bbs,admin); */
    return a;
}

struct area *create_area(char *areastring,char *name)
{
  char *p;
  struct area *a;

   char junk[100];

  a=new(sizeof(struct area));
  if(a==0)
    return 0;
  a->first_nodenum=0;
  a->next_area=0;
  a->passthru=default_passthru;
  a->link=default_link;
  a->alias=default_alias;
  a->areanum=atoi(areastring);
  a->class=default_class;


   /*
    * Build the path to the message base here... We won't really
    * concern ourself if it exists or not at this stage, that
    * is better handled in executables that use the path...
    * 
    */




   if(a->passthru)
   {
      p=stpcpy(a->path,"PASS:");
            a->areanum=0;
   }
   else
   {
      p=stpcpy(a->path,"MSG:");
   }


   
   /*
    *
    * We want to detect a special case for passthru areas where the
    * path is created from the area name... look for "[DEF]" and
    * substitute the name for areastring
    *
    *
    */


   if(Stricmp(areastring,"[DEF]"))
   {
      if(strlen(areastring)+(p-(a->path))+1>=MAX_STR)
      {
         set_error_code(STRING_LEN,"DLGMail.ARE: Area Name is too long: ",areastring);
               delete(a);
               return(0); /* Error: path name will be too long. */
      }

      p=stpcpy(p,areastring);
      stpcpy(p,"/");
      a->path[MAX_STR-1]=0; /* Double Insurance that it gets null terminated. */
   }
   else
   {
      /***** [def] area *****/

      if(a->passthru==0)
      {
         set_error_code(STRING_LEN,"DLGMail.ARE: Can't use [DEF] name for local areas: ",name);
         delete(a);
         return(0);
      }
      if(strlen(name)+(p-(a->path))+1>=MAX_STR)
      {
         set_error_code(STRING_LEN,"DLGMail.ARE: Area Name is too long: ",areastring);
               delete(a);
               return(0); /* Error: path name will be too long. */
      }

      strcpy(junk,name);
      FixSlashes(junk);

      p=stpcpy(p,junk);
      stpcpy(p,"/");
   }

   strupr(a->path);

   /*
    * End of Steve's diddling for passthru paths...
    * 
    */



//here
  strncpy(a->name,name,MAX_STR);
  strupr(a->name);    /* STEVE HACK FOR UPPERCASE TAGS */ 
  a->name[MAX_STR-1]=0;


  /*  printf("Creating area, path= %s, name= %s\n",path,name); */
//here
  strncpy(a->from_organization,default_from_organization,MAX_STR);
  a->from_organization[MAX_STR-1]=0;
  a->from_zone=default_from_zone;
  a->from_net=default_from_net;
  a->from_node=default_from_node;
  a->from_point=default_from_point;

  a->description=0;

  return a;
}

struct nodenum *create_nodenum(short net,short node)
{
   return create_nodenum5("",0,net,node,0);
}

struct nodenum *create_nodenum5(char *org,short zone,short net,short node,
            short point)
{
  struct nodenum *n;
  n=new(sizeof(struct nodenum));
  if(n==0)
    return 0;
  n->next_nodenum=0;

  stccpy(n->organization,org,MAX_STR);
  n->zone=zone;
  n->net=net;
  n->node=node;
  n->point=point;

  stccpy(n->from_organization,current_from_organization,MAX_STR);
  n->from_zone=current_from_zone;
  n->from_net=current_from_net;
  n->from_node=current_from_node;
  n->from_point=current_from_point;

  n->strip=current_strip;

  /*  printf("Creating nodenum, net= %d, node= %d\n",net,node); */
  return n;
}

struct nodenum **get_last_nodenum_ptr(struct nodenum **n)
{
  for(;;)
    {
      if(*n==0)
   return n;
      n=&((*n)->next_nodenum);
    }
}

struct nodenum *create_nodenum_on_end(struct nodenum **n,short net,short node)
{
  n=get_last_nodenum_ptr(n);
  *n=create_nodenum(net,node);
  return *n;
}

struct nodenum *create_nodenum_on_end5(struct nodenum **n,char *org,short zone,short net,
             short node,short point)
{
  n=get_last_nodenum_ptr(n);
  *n=create_nodenum5(org,zone,net,node,point);
  return *n;
}

struct nodenum *create_nodenum_in_order(struct nodenum **n,short net,short node)
{
  struct nodenum *nn;
  for(;;)
    {
      if(*n==0)
   {
     *n=create_nodenum(net,node);
     return *n;
   }
      if(net>((*n)->net) || net==((*n)->net) && node>((*n)->node))
   {
     n=&((*n)->next_nodenum);
     continue;
   }
      if(net==((*n)->net) && node==((*n)->node))
   {
     /* printf("Net and Node are already in list!\n"); */
     return *n;
   }
      /* At this point, we are ready to insert... */
      nn=create_nodenum(net,node);
      if(nn)
   {
     nn->next_nodenum=*n;
     *n=nn;
     return *n;
   }
      else
   {
     return 0;
   }
    }
}

void print_areas(struct area *a)
{
  FILE *fp;

  int passthru;
  int link;
  int alias;
  char *passthrustring[]={"NOPASSTHRU","PASSTHRU"};
  char *linkstring[]={"NOLINK","LINK"};
  char *aliasstring[]={"NOALIASES","ALIASES"};

   fp=fopen("logs:zareas.rpt","w");
   if(fp)
   {

   fprintf(fp,"*****  List of Known Areas *****");
   for(;a;a=get_next_area(a))
      {
            passthru=a->passthru;
            link=a->link;
      alias=a->alias;

            if(passthru)
        passthru=1;
            if(link)
        link=1;
      if(alias)
        alias=1;

            fprintf(fp,"\n*\n* Area %s Path %s Area Number %d Class %d %s %s %s\n",
            a->name,a->path,a->areanum,a->class,passthrustring[passthru],linkstring[link],aliasstring[alias]);
      if(a->description)
        fprintf(fp,"Description: %s\n");
      fprintf(fp,"*\n");
            fprint_netnodes(a->first_nodenum,fp);
      }
   fclose(fp);
   }
}

void print_netnodes(struct nodenum *n)
{
  char *stripstring[]={"","SEENBY'S-STRIPPED"};
  int strip;

  for(;n;n=n->next_nodenum)
    {
      strip=n->strip;
      if(strip)
   strip=1;
      printf("* Remote: %s!%d:%d/%d.%d  Me: %s!%d:%d/%d.%d %s\n",
        n->organization,n->zone,n->net,n->node,n->point,
        n->from_organization,n->from_zone,n->from_net,n->from_node,
        n->from_point,stripstring[strip]);
    }
    printf("*\n");
}
void fprint_netnodes(struct nodenum *n,FILE *fp)
{
  char *stripstring[]={"","SEENBY'S-STRIPPED"};
  int strip;

  for(;n;n=n->next_nodenum)
    {
      strip=n->strip;
      if(strip)
   strip=1;
      fprintf(fp,"* Remote: %s!%d:%d/%d.%d  Me: %s!%d:%d/%d.%d %s\n",
        n->organization,n->zone,n->net,n->node,n->point,
        n->from_organization,n->from_zone,n->from_net,n->from_node,
        n->from_point,stripstring[strip]);
    }
    fprintf(fp,"*\n");
}

void delete_areas(struct areas *a)
{
   printf(":-)\n");
}

void new_delete_areas(struct areas *a)
{
  if(a==0 || _MYREMEMBER==0)
    return;
  FreeRemember(&_MYREMEMBER,TRUE);
  a=0;
  _MYREMEMBER=0;
  //delete_all_area(get_area(a,1));
  //delete(a);
}

/****
void delete_all_area(struct area *a)
{
  if(a==0)
    return;
  delete_all_area(get_next_area(a));
  delete_all_nodenum(get_nodenum(a,1));
  if(a->description)
    delete(a->description);
  delete(a);
}
*****/

/****
void delete_all_nodenum(struct nodenum *n)
{
  if(n==0)
    return;
  delete_all_nodenum(get_next_nodenum(n));
  delete(n);
}
*****/

struct area *add_area(struct areas *a,char *string,char *name)
{
  struct area *aa;
  aa=create_area(string,name);
  if(aa==0)
    return 0;
  aa->next_area=a->first_area;
  a->first_area=aa;
  return aa;
}

struct area **get_last_area_ptr(struct areas *a)
{
  struct area **aa;
  aa=&a->first_area;
  while(*aa)
    aa=&((*aa)->next_area);
  return aa;
}

struct area *add_area_on_end(struct areas *a,char *string,char *name)
{
  struct area **aa;
  aa=get_last_area_ptr(a);
  *aa=create_area(string,name);
  return *aa;
}

struct nodenum *add_nodenum(struct area *a,short net,short node)
{
  struct nodenum *n;
  n=create_nodenum(net,node);
  if(n==0)
    return 0;
  n->next_nodenum=a->first_nodenum;
  a->first_nodenum=n;
  return n;
}

struct nodenum *add_nodenum5(struct area *a,char *org,short zone,short net,
              short node,short point)
{
  struct nodenum *n;
  n=create_nodenum5(org,zone,net,node,point);
  if(n==0)
    return 0;
  n->next_nodenum=a->first_nodenum;
  a->first_nodenum=n;
  return n;
}

struct nodenum *add_nodenum_on_end5(struct area *a,char *org,short zone,
                short net,short node,short point)
{
  struct nodenum *n;
  n=create_nodenum_on_end5(&a->first_nodenum,org,zone,net,node,point);
  return n;
}

int translate_line_token(char *word)
{
int i;
for(i=0;;i++)
  {
    if(!linetokenlist[i])
   break;
    if(!strcmpi(linetokenlist[i],word))
      return i;
  }
return -1;
}

int translate_entry_token(char *word)
{
int i;
for(i=0;;i++)
  {
    if(!entrytokenlist[i])
   break;
    if(!strcmpi(entrytokenlist[i],word))
      return i;
  }
return -1;
}

int skip_to_line_token(FILE *fp)
{
  int c;
  for(;;)
    {
      c=skipchars(fp," \t\n\r");
      if(c==EOF)
   return 1;
      if(c!=';')
   {
     return 0;
   }
      skipuntilterm(fp,"\n");
    }
}

int skip_to_entry_token(FILE *fp)
{
  int c;
  for(;;)
    {
      c=skipchars(fp," \t\r");
      if(c==EOF)
   return 1;
      if(c==';')
   {
     c=skipuntilterm(fp,"\n");  /* c will be '\n' on return. */
     ungetc(c,fp);
   }
      return 0;
    }
}

int read_address(FILE *fp,char *org,short *zone,short *net,short *node,
       short *point)
{
  int c;
  char t1[100];
  char *numbers="0123456789";

  skipchars(fp," \t");

  *point=0;
  c=readuntilterm(fp,t1,"!:/. ;\t\n\r");
  if(c=='.' && strlen(t1)==0)
    {
      if(!string1subsetstring2(t1,numbers))
   return 1; /* Fields not Numeric; return error. */
      c=readuntilterm(fp,t1,"!:/. ;\t\n\r");
      if(!string1subsetstring2(t1,numbers))
   return 1; /* Fields not Numeric; return error. */
      if(charisinstring(c," ;\t\n\r"))
   {
     *point=atoi(t1);
     ungetc(c,fp);
     return 0;
   }
      else
   {
     return 1;
   }
    }
  if(c=='!')
    {
      stccpy(org,t1,MAX_STR);
      c=readuntilterm(fp,t1,"!:/. ;\t\n\r");
      if(c!=':')
   {
     return 1;
   }
    }
  if(!string1subsetstring2(t1,numbers))
    return 1; /* Fields not Numeric; return error. */
  if(c==':')
    {
      *zone=atoi(t1);
      c=readuntilterm(fp,t1,"!:/. ;\t\n\r");
      if(!string1subsetstring2(t1,numbers))
   return 1; /* Fields not Numeric; return error. */
      if(c!='/')
   {
     return 1;
   }
    }
  if(c=='/')
    {
      *net=atoi(t1);
      c=readuntilterm(fp,t1,"!:/. ;\t\n\r");
      if(!string1subsetstring2(t1,numbers))
   return 1; /* Fields not Numeric; return error. */
      if(c!='.')
   {
     if(charisinstring(c," ;\t\n\r"))
       {
         *node=atoi(t1);
         ungetc(c,fp);
         return 0;
       }
     else
       {
         return 1;
       }
   }
    }
  if(c=='.')
    {
      *node=atoi(t1);
      c=readuntilterm(fp,t1,"!:/. ;\t\n\r");
      if(!string1subsetstring2(t1,numbers))
   return 1; /* Fields not Numeric; return error. */
      if(charisinstring(c," ;\t\n\r"))
   {
     *point=atoi(t1);
     ungetc(c,fp);
     return 0;
   }
      else
   {
     return 1;
   }
    }
  ungetc(c,fp);
  *node=atoi(t1);
  return 0;
}

void read_entry_token(FILE *fp,char *token)
{
  int c;
  long position;
  position=ftell(fp);
  c=fgetc(fp);
  if(c!='\n')
    {
      fseek(fp,position,0);
      c=readuntilterm(fp,token," ;\n\r\t");
      ungetc(c,fp);
      return;
    }
  if(charisinstring(fgetc(fp),"cC") &&
     charisinstring(fgetc(fp),"oO") &&
     charisinstring(fgetc(fp),"nN") &&
     charisinstring(fgetc(fp),"tT") )
    {
      strcpy(token,"\nCONT");
    }
  else
    {
      fseek(fp,position+1,0);
      strcpy(token,"\n");
    }
  return;
}


#define FBUFSIZE 10000


struct areas *read_areas(char *fname)
{
   printf(":-(\n");
   return 0;
}

struct areas *new_read_areas(char *fname, char *errline1, char *errline2)
{
  struct areas *Areas;
  char t1[100],t2[100];
  char token[100];
  int tokennum;
  struct area *current_area;
  char organization[MAX_STR];
  short zone,net,node,point;
  int c,loop,error,size;
  FILE *fp;
  long position;

   e1=errline1;
   e2=errline2;

   default_class=0;
   default_passthru=FALSE;
   default_link=TRUE;
   default_alias=FALSE;
   *default_from_organization=0;

   set_error_code(NO_ERROR,"DLGMail.ARE: No Error Occurred.","");

   fp=fopen(fname,"r");
   if(!fp)
      {
            set_error_code(FILE_ACCESS,"DLGMail.ARE: Can't open areas file: ",fname);
            return 0;
      }

   skip_to_line_token(fp);
   readuntilterm(fp,token," ;\t\n\r");  /* Read in Token. */
   if(translate_line_token(token)!=MYADDRESS)
      {
            set_error_code(PARSE_ERROR,"DLGMail.ARE: First keyword in file must be",
           linetokenlist[MYADDRESS]);
            fclose(fp);
            return 0;
      }
   position=ftell(fp); /* record position of start of address. */
   error=read_address(fp,default_from_organization,(short *)&default_from_zone,
           (short *)&default_from_net,(short *)&default_from_node,
           (short *)&default_from_point);
   if(error)
      {
            fseek(fp,position,0);
            read_entry_token(fp,token);
            set_error_code(PARSE_ERROR,"DLGMail.ARE: Illegal MYADDRESS address: ",
           token);
            fclose(fp);
            return 0;
      }
   if(!*default_from_organization)
      {
            fseek(fp,position,0);
            skipchars(fp," \t");
            read_entry_token(fp,token);
            set_error_code(PARSE_ERROR,
            "DLGMail.ARE: MYADDRESS must be 5d address:",
            token);
            fclose(fp);
            return 0;
      }

   Areas = create_areas();
   if(Areas==0)
      {
            set_error_code(MEM_ALLOC,
         "DLGMail.ARE: Couldn't allocate memory","for areas structure");
            fclose(fp);
            return 0;
      }

   for(;;)
      {
            /* At this point, we are at the beginning of a line (or the file) */
            error=skip_to_line_token(fp); /* Find start of first line token. */
            if(error)
         break;
            readuntilterm(fp,token," ;\t\n\r");  /* Read in token. */
            tokennum=translate_line_token(token);
            switch(tokennum)
      {
         case MYADDRESS:
         position=ftell(fp); /* record position of start of address. */
         error=read_address(fp,default_from_organization,(short *)&default_from_zone,
              (short *)&default_from_net,(short *)&default_from_node,
              (short *)&default_from_point);
         if(error)
            {
                  fseek(fp,position,0);
                  skipchars(fp," \t");
                  read_entry_token(fp,token);
            set_error_code(PARSE_ERROR,"DLGMail.ARE: Illegal MYADDRESS address:",
               token);
/* ==> */                  new_delete_areas(Areas);
                  fclose(fp);
                  return 0;
            }
         break;
   
         case LINK:
         default_link=TRUE;
         break;
   
         case NOLINK:
         default_link=FALSE;
         break;
   
         case ALIAS:
         default_alias=TRUE;
         break;
   
         case NOALIAS:
         default_alias=FALSE;
         break;
   
         case PASSTHRU:
         default_passthru=TRUE;
         break;
   
         case NOPASSTHRU:
         default_passthru=FALSE;
         break;
   
         case CLASS:
         skipchars(fp," \t");
         readuntilterm(fp,t1," ;\t\n\r");
         if(!string1subsetstring2(t1,"0123456789"))
            {
                  set_error_code(PARSE_ERROR,"DLGMail.ARE: CLASS keyword followed by:",t1);
/* ==> */               new_delete_areas(Areas);
                  fclose(fp);
                  return 0;
            }
         default_class=atoi(t1);
         break;
   
         case TAG:
         case AREA:
         skipchars(fp," \t");
         c=readuntilterm(fp,t1," ;\t\n\r");
         if(charisinstring(c,"\n\r;"))
            {
                  set_error_code(PARSE_ERROR,"DLGMail.ARE: TAG/AREA keyword followed by:",t1);
/* ==> */               new_delete_areas(Areas);
                  fclose(fp);
                  return 0;
            }
         skipchars(fp," \t");
         c=readuntilterm(fp,t2," ;\t\n\r");
         ungetc(c,fp);




         current_area = add_area_on_end(Areas,t2,t1);

         if(current_area==0)
            {
                  set_error_code(MEM_ALLOC,"DLGMail.ARE: Can't allocate enough memory","to handle all the configured areas.");
/* ==> */               new_delete_areas(Areas);
                  fclose(fp);
                  return 0;
         }

         stccpy(current_from_organization,default_from_organization,MAX_STR);
         current_from_zone=default_from_zone;
         current_from_net=default_from_net;
         current_from_node=default_from_node;
         current_from_point=default_from_point;
         current_strip=FALSE;

         stccpy(organization,current_from_organization,MAX_STR);
         zone=current_from_zone;
         net=current_from_net;

         /* Now we are reading entries for an area. */
         loop=TRUE;
         while(loop)
            {     
                  error=skip_to_entry_token(fp);
                  /* Find start of first entry token. */
                  if(error)
            {
               set_error_code(PARSE_ERROR,
                  "DLGMail.ARE: Abnormal End Of File","Check last area for structure and carriage returns");
               fclose(fp);
/* ==> */            new_delete_areas(Areas);
               return 0;
            }
                  position=ftell(fp); /* record position of start of token. */
                  read_entry_token(fp,token);  /* Read in token. */
                  tokennum=translate_entry_token(token);
                  switch(tokennum)
            {
               case CONT:
               break;
               
               case NEWLINE:
               loop=FALSE;
               break;
      
               case USEADDRESS:
               position=ftell(fp); /* record position of start of address. */
               error=read_address(fp,current_from_organization,
                     (short *)&current_from_zone,
                     (short *)&current_from_net,(short *)&current_from_node,
                     (short *)&current_from_point);
               if(error)
                  {
                        fseek(fp,position,0);
                        skipchars(fp," \t");
                        read_entry_token(fp,token);
                  set_error_code(PARSE_ERROR,"DLGMail.ARE: Illegal USEADDRESS address:",
                     token);
/* ==> */                     new_delete_areas(Areas);
                        fclose(fp);
                        return 0;
                  }
               stccpy(organization,current_from_organization,MAX_STR);
               zone=current_from_zone;
               net=current_from_net;
               break;
      
               case STRIP:
               current_strip=TRUE;
               break;
      
               case NOSTRIP:
               current_strip=FALSE;
               break;
      
               case DESC:
               if(current_area->description)
                  {
                        set_error_code(PARSE_ERROR,"DLGMail.ARE: Two area descriptions given for area",current_area->name);
/* ==> */                     new_delete_areas(Areas);
                        fclose(fp);
                        return 0;
                  }
               skipchars(fp," \t");
               c=readuntilterm(fp,token,"\n\r");
               ungetc(c,fp);
               size=strlen(token)+1;
               if(size>DESC_MAX)
                     size=DESC_MAX;
               current_area->description=new(size);
               if(current_area->description==0)
                  {
                        set_error_code(MEM_ALLOC,"DLGMail.ARE: Couldn't allocate memory","for a bitty string!");
/* ==> */                     new_delete_areas(Areas);
                        fclose(fp);
                        return 0;
                  }
//here
               strncpy(current_area->description,token,size);
               current_area->description[size-1]=0;
               break;
      
               default:
               /* Default is a new address to add to area. */
               /* rewind to start of this token. */
               fseek(fp,position,0);
               error=read_address(fp,organization,&zone,&net,&node,&point);
               if(error)
                  {
                  set_error_code(PARSE_ERROR,"DLGMail.ARE: Entry on line not keyword or valid address:",
                     token);
/* ==> */                     new_delete_areas(Areas);
                        fclose(fp);
                        return 0;
                  }
               error=!add_nodenum_on_end5(current_area,organization,zone,
                        net,node,point);
               if(error)
                  {
                  set_error_code(MEM_ALLOC,"DLGMail.ARE: Not enough memory","to parse areas file.");
/* ==> */                     new_delete_areas(Areas);
                        fclose(fp);
                        return 0;
                  }

               break;
            }
            }
         break;
      
         default:
         /* Default line token at line start is an error. */
         set_error_code(PARSE_ERROR,"DLGMail.ARE: Illegal token in input:",
            token);
/* ==> */      new_delete_areas(Areas);
         fclose(fp);
         return 0;
      }
      }
   fclose(fp);
   return Areas;
}

void *new(int size)
{
  void *v;
  v=(void *)AllocRemember(&_MYREMEMBER,size,MEMF_PUBLIC | MEMF_CLEAR);
  return v;
}

void delete(void *v)
{
   printf(":-)");
  //free(v);
}

/*  OLD CODE
 *
 * int Log(char *s1,char *s2);
 * Log(char *s1,char *s2)
 * {
 * return 0;
 * }
 *
 * int QuietExit(void);
 * QuietExit()
 * {
 * return 0;
 * }
 *
 * void myexit(int i)
 * {
 *
 *  Log("*-----","ERROR FOUND BY ROSS' CODE - QUITTING\n");
 *  printf("ERROR FOUND BY ROSS' CODE - QUITTING\n");
 *
 *  QuietExit();
 * }
 *
 */

void set_error_code(int code,char *str1,char *str2)
{
if(code)
{
   //here
   strncpy(e1,str1,50);
   strncpy(e2,str2,50);

 //printf(" [33m## Z_ROSS.C Error\n");
 //printf("        ##      Error code: %d\n",code);
 //printf("        ## Verbose message: %s\n",str1);
 //printf("        ##                  %s\n[0m",str2);
 //Delay(500);
}

global_error_code=code;
stccpy(global_error_string1,str1,MAX_LONG_STR);
stccpy(global_error_string2,str2,MAX_LONG_STR);
}

/* Global Variable Definitions: */

short current_strip;
char  current_from_organization[MAX_STR];
unsigned short current_from_zone;
unsigned short current_from_net;
unsigned short current_from_node;
unsigned short current_from_point;

char  default_from_organization[MAX_STR];
unsigned short default_from_zone;
unsigned short default_from_net;
unsigned short default_from_node;
unsigned short default_from_point;

short default_class;
short default_passthru;
short default_link;
short default_alias;


/* order here must correspond to definitions in header.h */
char *linetokenlist[]=
{
"MYADDRESS",
"PASSTHRU",
"NOPASSTHRU",
"CLASS",
"TAG",
"AREA",
"LINK",
"NOLINK",
"ALIAS",
"NOALIAS",
(char *)0
};

char *entrytokenlist[]=
{
"\nCONT",
"\n",
"USEADDRESS",
"STRIP",
"NOSTRIP",
"DESC",
(char *)0
};

int global_error_code=0;
char global_error_string1[MAX_LONG_STR];
char global_error_string2[MAX_LONG_STR];



SteveAreaTest(char *filename,char *comment,struct areas *x)
{
   int rc=1;
   struct area *as;
   FILE *fp;
   int i=0;

   fp=fopen(filename,"a");
   if(fp)
   {
      fprintf(fp,"========\nDebugging comment: %s\n",comment);

      for(as=x->first_area;;as=as->next_area)
      {
         i++;

         fprintf(fp,"%3d  [%20.20s] [%25.25s] %4d %3d %d %d %d\n",
            i,
            as->name,
            as->path,
            as->areanum,
            as->class,
            as->passthru,
            as->link,
            as->alias);

         if(*as->path==NULL) rc=0;

         if(as->next_area==NULL) break;
      }
      fprintf(fp,"=========\n");
      fclose(fp);
   }
   return rc;
}

