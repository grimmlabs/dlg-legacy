#include <exec/types.h>
#include <exec/memory.h>

#include <proto/dos.h>
#include <proto/dlg.h>
#include <proto/exec.h>

#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include <stdlib.h>

#include <dos.h>
#include <dos/dostags.h>

#include <dlg/resman.h>

#include "proto.h"

#include <dlg/globalconfig.h>

#include <pragmas/dlg.h>

#define MAX_ARGS 20

extern char logstring[1000];

extern int DAY;
extern char DATESTR[20];
extern char TIMESTR[20];
extern char DATETIMESTR[40];

extern struct StoredCommands *CURCMDS;

extern BOOL DO_LOG;
extern BOOL DEBUG;
extern BOOL CHAT;

extern BPTR sout;

extern struct Library *DLGBase;
extern struct Config  *CFG;

extern int  Index;

struct   Config
{
   BOOL     Skip;
   char     Dir[256];
   char     Pat[256];
   struct   List *clist;
   ULONG    Size;
};

