#define EXPORTATTRIB 0
#define PRODUCTCODE 0xBA
#define FIDOMSGTYPE 2
#define FIDOPKTTYPE 2
#define CAPABILITYWORD 1


struct Fido_Msg_Header
	{
	char	From[36];
	char	To[36];
	char 	Title[72];
	char	Date[20];
	unsigned short	TimesRead;
	unsigned short	DestNode;
	unsigned short	OrigNode;
	unsigned short	Cost;
	unsigned short	OrigNet;
	unsigned short	DestNet;

	unsigned short  DestZone;
	unsigned short	OrigZone;
	unsigned short	DestPoint;
	unsigned short  OrigPoint;

	unsigned short	ReplyTo;
	unsigned short 	Attribute;
	unsigned short	NextReply;

	/*char	Text[MAXMSGLEN];*/
	};

/* attributes:*/

#define PRIVATE		1
#define CRASH		2
#define RECD		4
#define SENT		8
#define FILEATTACHED	16
#define INTRANSIT	32
#define ORPHAN		64
#define KILLSENT	128
#define LOCAL		256
#define HOLDFORPICKUP	512
#define ATTRIBUNUSED1	1024
#define FILEREQUEST	2048
#define RRREQUEST	4096
#define ISRR		8192
#define AUDREQ		16384
#define FILEUPREQ	32768


/*	0-	private*/
/*	1-	crash*/
/*	2	recd*/
/*	3	sent*/
/*	4-	fileattached*/
/*	5	intransit*/
/*	6	orphan*/
/*	7	killsent*/
/*	8	local*/
/*	9	hold for pickup*/
/*	10-	unused*/
/*	11	filerequest*/
/*	12-	returnreceiptrequest*/
/*	13-	isreturnreceipt*/
/*	14-	auditrequest*/
/*	15	fileupdatereq*/

struct PackedMsg
	{
	short MsgType;  /* 02H 00H is standard */
	short OrigNode;
	short DestNode;
	short OrigNet;
	short DestNet;
	short Attribute;
	short Cost;
	char Date[20];
	};
/*	char To[36];
	char From[36];
	char Title[72];
	char Text[MAXMSGLEN];*/

struct Packet_Header
{
	/* this is a type 39 header */

	unsigned short 	OrigNode;
	unsigned short 	DestNode;
	unsigned short 	Year;
	unsigned short 	Month;		/* 8*/
	unsigned short 	Day;
	unsigned short 	Hour;
	unsigned short 	Minute;
	unsigned short 	Second;		/*16*/
	unsigned short 	Baud;
	unsigned short 	PacketType;	/*20*/
	unsigned short 	OrigNet;	/*22*/
	unsigned short 	DestNet;	/*24*/
	unsigned char 	ProductCode;	/*25*/
	unsigned char   PV_Version;		/*26*/	/*unused*/
	unsigned char  	Password[8];	/*34*/	/*unused*/

	unsigned short	OrigZone1;    	/*36*/	/* NEW */
	unsigned short	DestZone1;	/*38*/	/* NEW */

/* FSC-0039 char	strange[4];	unused*/

/* FSC-0048 bullshit replaces the strange filler bytes below */

	unsigned short	AuxNet;
	unsigned short  CWValidate;

/* end FSC-0048 */

	unsigned char	ProductCode_HIBYTE;
	unsigned char	PV_Revision;	/*44*/

	unsigned short	CapabilityWord;	/*46*/
	unsigned short  OrigZone2;	/*48*/
	unsigned short	DestZone2;	/*50*/
	unsigned short  OrigPoint;	/*52*/
	unsigned short	DestPoint;	/*54*/
	unsigned long	MySerialNumber;	/*58*/	/* NEW */
	};

struct HiWater
	{
	char	From[36];
	char	To[36];
	char 	Title[72];
	char	Date[20];
	char	Filler[20];
	short	HiWater1;
	short	HiWater2;
	short	HiWater3; /*set to $00*/
	char	Text[100];
	};

