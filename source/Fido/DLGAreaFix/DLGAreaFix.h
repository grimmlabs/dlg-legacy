#include <exec/types.h>

#include <stdio.h>
#include <stdlib.h>
#include <dos.h>
#include <dos/dos.h>
#include <dos/dostags.h>

#include <exec/exec.h>
#include <libraries/dos.h>
#include <string.h>
#include <exec/memory.h>
#include <stdlib.h>

#include <proto/dos.h>
#include <proto/exec.h>
#include <proto/dlg.h>

#include <dlg/resman.h>

#include <DLG/globalconfig.h>

#include "Protos.h"

#include <pragmas/dlg.h>

#include "/ross/header.h"
#include "/ross/protos.h"

#define NEWROSSAREAS 1

#define ACTIVE 1
#define AVAILABLE 2

#define AF_UNKNOWN_ERR 0
#define AF_NO_AFX -1
#define AF_BAD_PASSWORD -2
#define AF_BAD_ADDRESS -3
#define AF_NO_ENTRY -4
#define AF_BAD_AFX_SYNTAX -5
#define AF_OK 1


#define AF_MSGMISSING   -1
#define AF_LOCALMSG  -2
#define AF_ALREADYSENT  -3
#define AF_TOAPOINT  -4
#define AF_NOTTOUS   -5

#define EXPORTATTRIB 0
#define LogFile "Logs:AreaFix.log"

/* attributes:*/
/* 0- private*/
#define PRIVATE   1
/* 1- crash*/
#define CRASH  2
/* 2  recd*/
#define RECD   4
/* 3  sent*/
#define SENT   8
/* 4- fileattached*/
#define FILEATTACHED 16
/* 5  intransit*/
#define INTRANSIT 32
/* 6  orphan*/
#define ORPHAN    64
/* 7  killsent*/
#define KILLSENT  128
/* 8  local*/
#define LOCAL     256
/* 9  hold for pickup*/
#define HOLDFORPICKUP   512
/* 10-   unused*/
#define ATTRIBUNUSED1   1024
/* 11 filerequest*/
#define FILEREQUEST  2048
/* 12-   returnreceiptrequest*/
#define RRREQUEST 4096
/* 13-   isreturnreceipt*/
#define ISRR      8192
/* 14-   auditrequest*/
#define AUDREQ    16384
/* 15 fileupdatereq*/
#define FILEUPREQ 32768

#define MAX_STR 50

extern char logstring[2000];


extern BOOL RAIDFLAG;

extern struct areas *a;    /* ross's reference */

/// TicStruct
struct TicStruct
{
   struct TicStruct *next;
   char *entry;
};
//-

extern struct TicStruct *TS;
extern struct Library *DLGBase;


extern int DO_Q, DO_L, DO_H, DO_U, DO_R, DO_I;

struct Fido_Msg_Header
   {
   char  From[36];
   char  To[36];
   char  Title[72];
   char  Date[20];
   short TimesRead;
   short DestNode;
   short OrigNode;
   short Cost;
   short OrigNet;
   short DestNet;
   char  Fill[8];
   short ReplyTo;
   short    Attribute;
   short NextReply;
   /*char   Text[MAXMSGLEN];*/
   };

struct PackedMsg
   {
   short MsgType;  /* 02H 00H is standard */
   short OrigNode;
   short DestNode;
   short OrigNet;
   short DestNet;
   short Attribute;
   short Cost;
   char Date[20];
   };

struct Packet_Header
{
   short OrigNode;
   short DestNode;
   short Year;
   short Month;
   short Day;
   short Hour;
   short Minute;
   short Second;
   short Baud;
   short PacketType;
   short OrigNet;
   short DestNet;
   UBYTE ProductCode;
   UBYTE fill[33];
   };

struct HiWater
   {
   char  From[36];
   char  To[36];
   char  Title[72];
   char  Date[20];
   char  Filler[20];
   short HiWater1;
   short HiWater2;
   short HiWater3; /*set to $00*/
   char  Text[100];
   };

extern BPTR sout;
