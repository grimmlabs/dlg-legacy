// AreaFix.c

extern   int               AreaFix(int);

// DeadEnds.c

extern   int               DeadEnd(int, char **);

// ProcessMail.c

extern   int               FetchMsg(int, char *, char *, char *, char *, char *, int);

// Raid.c

extern   int               LoadDLGMAILTIC(void);
extern   int               SaveDLGMAILTIC(void);
extern   int               RaidWorkArea(char *, char *, char *, char *);
extern   void              RaidReplyReport(char *, char *, int, char *);

// Upstream.c

extern   int               FindIt(char *, char *, char *);
extern   void              FixSlashes(char *string);
extern   int               FindFeed(BPTR, char *, char *, int *, char *, char *,
                                    char *, char *, char *);


// Support.c

extern   int               parse5dstring(char *, char *, short *, short *, short *,
                                         short *);
extern   int               TestPCAddress(char *);
extern   void              Log(char *);
extern   char             *mystpsym(char *, char *, int);
extern   int               Make5DAddress(char *, char *);
extern   BOOL              CheckClass(int, char *);


// GenerateActiveReport.c

extern   int               GenerateActiveReport(char *, char *);
extern   int               CheckActive(struct nodenum *, char *);

