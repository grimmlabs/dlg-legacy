#include <exec/types.h>

#define DLGMV2 1

#include <stdio.h>
#include <exec/memory.h>
#include <ctype.h>

#include <dos.h>
#include <dos/dostags.h>
#include <dos/exall.h>

#include <libraries/dos.h>
#include <string.h>
#include <time.h>
#include <stdlib.h>

#include <proto/dlg.h>
#include <proto/dos.h>
#include <proto/exec.h>
#include <pragmas/dlg.h>

#include <dlg/globalconfig.h>
#include <dlg/resman.h>
#include <dlg/msg.h>
#include <dlg/file.h>

/* globals start here */

#define MASTER    "T:Master.TIC"
#define TICKREPLY "T:Tick.Reply"

extern struct  Library *DLGBase;

extern char logstring[1000];

extern BPTR sout;

extern int              Areas;
extern int              AIndex;

extern struct  TICAREA *TicArea;


// pdqtick.c

int   CheckDLGMAILSTOP(void);

// Tick.c

BOOL  Tick(char *);
void  MoveToBad(char *, char *, char *, char *, char *, char *);

// Hatch.c

int   Hatch(int, char **);

// Maint.c

BOOL  TickMaint(void);

// misc.c

int   AppendToTIC(char *, char *);
int   NotateExp(int);
int   SendMsg(char *);
int   Log(char *);
BOOL  CompareFidoAddress(char *, char *);
void  StripQuote(char *);

// CRC.c

int   FileCRC(char *, ULONG *);

// TCRC.c

unsigned long CalcRunningCRC(unsigned char *, int, unsigned long);

// Local.c

BOOL  DLGFile(char *, char *, char *, char *, ULONG, BOOL, char *, BOOL, BOOL);

// Config.c

int   LoadConfig(void);
void  PrintConfig(void);
int   GetEcho(char *);
struct TicAddr *GetAddr(char *, char *);

// SendLoop.c
BOOL  SendLoop(char *, char *, char *, char *, BOOL, char *, struct TicAddr *);


// ------------------ // Struct Definitions // --------------- //

///   Struct TICAREA
struct TICAREA
{
   char           AreaName[100];
   int            Class;
   char           Desc[500];
   BOOL           Purge;
   BOOL           Replace;
   BOOL           Loc;
   BOOL           Pass;
   unsigned long  DLGArea;
   BOOL           Free;
   BOOL           Ignore;
   char           PreList[500];
   char           PostList[500];
   char           Uploader[128];
   BOOL           ShowArea;
   char           Execute[128];
   char           UseAddress[64];
};
//-

///   struct TicAddr
struct TicAddr
{
   char  Address[50];
   char  Flavor;
   BOOL  Reverse;
   BOOL  Hatch;
};
//-

