#include "DLGTick.h"

#include <link/io.h>

#define _LogFile "Logs:DLGTick.log"
#define BODYFILE "T:DLGSND.BODY"

/// AppendToTIC
AppendToTIC(char *filename, char *string)
{
   BPTR fp;

   fp = Open(filename,MODE_READWRITE);

   if(fp)
   {
      Seek(fp,0,OFFSET_END);
      AFPrintf(NULL,fp,"%s\r\n",string);
      Close(fp);
      return(1);
   }
   return(0);
}
//-

/// NotateExp()
NotateExp(int number)
{
   BPTR fp;

   fp = Open("T:USE_EXP",MODE_READWRITE);

   if(fp)
   {
      Seek(fp,0,OFFSET_END);
      
      AFPrintf(NULL,fp,"%d\n",number);
      Close(fp);
   }

   fp = Open(EXPFILE,MODE_READWRITE);

   if(fp)
   {
      Seek(fp,0,OFFSET_END);
      
      AFPrintf(NULL,fp,"%d\n",number);
      Close(fp);
   }
   _RETURNCODE |=ACT_DLGEXP;
   return 0;
}
//-

/// SendMsg()
SendMsg(char *infile)
{
   BPTR in;
   BPTR out;
   int rc;

   char *s;
   char buffer[600];
   char token[50];
   char from[50];
   char address[50];
   char subject[80];
   char to[50];
   char area[50];
   BOOL inbody=FALSE;

   strcpy(from,"DLGTick");
   strcpy(to,"All");
   strcpy(subject,"Files processed by DLGTick");
   *area=NULL;
   *address=NULL;

   if(in = Open(infile,MODE_OLDFILE))
   {
      if(out = Open(BODYFILE,MODE_NEWFILE))
      {
         while(FGets(in,buffer,sizeof(buffer)))
         {
            if(inbody)
            {
               AFPrintf(NULL,out,"%s",buffer);
               continue;
            }
            s=buffer;
            s=stpblk(s);
            s=stptok(s,token,sizeof(token)," \t\n");
            s=stpblk(s);

            s[strlen(s)-1]=NULL;

            if(!Stricmp(token,"FROM:"))
            {
               strcpy(from,s);
               continue;
            }

            if(!Stricmp(token,"TO:"))
            {
               strcpy(to,s);
               continue;
            }

            if(!Stricmp(token,"SUBJECT:"))
            {
               strcpy(subject,s);
               continue;
            }

            if(!Stricmp(token,"AREA:"))
            {
               strcpy(area,s);
               continue;
            }

            if(!Stricmp(token,"FIDOADDRESS:"))
            {
               strcpy(address,s);
               continue;
            }

            if(!Stricmp(token,"BODY:"))
            {
               inbody=TRUE;
               continue;
            }
         }
         Close(out);

         ASPrintf(NULL,buffer,"DLG:SendMsg -n -q -f \"%s\" -s \"%s\" -b \"%s\" -r \"",from,subject,BODYFILE);
         
         if(*address)
         {
            strcat(buffer,"NET ");
            strcat(buffer,address);
            strcat(buffer," ");
         }
         else
         {
            if(*area)
            {
               strcat(buffer,"AREA ");
               strcat(buffer,area);
               strcat(buffer," ");
            }
         }           
         strcat(buffer,to);
         strcat(buffer,"\"");

         rc = Spawn(NULL,sout,buffer);
         DeleteFile(BODYFILE);
      }
      Close(in);
   }
   return 1;
}
//-

/// Log()
int Log(char *Event)
{
   return(AppendFile(_LogFile,Event));
}
//-

/// StripQuote    ** New 2.36.2.35 **
void StripQuote(char *string)
{
   char *s;
   long  len;

   // strip leading quote
   for(s = string; *s == '"'; s++);
   len = strlen(s);

   // Offset the string ONLY if a quote was found

   if(s != string)
      movmem(s, string, len+1);

   // Now trim off the trailing quote

   if (len)
   {
      for(s = string + len - 1; *s == '"'; s--);
      s++;
      *s = 0;
   }

   return;
}
//-

/// CompareFidoAddress     ** New 2.36.2.39 **
BOOL CompareFidoAddress(char *Addr1, char *Addr2)
{
   char A1[256];
   char A2[256];

   if(!Addr1 || !Addr2) return(FALSE);

   strcpy(A1,Addr1);
   strcpy(A2,Addr2);

   if(!strstr(Addr1,".0")) strcat(A1,".0");
   if(!strstr(Addr2,".0")) strcat(A2,".0");

   if(!Stricmp(A1,A2))
      return(TRUE);
   else
      return(FALSE);

   return(FALSE);
}
//-


