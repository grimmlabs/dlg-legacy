#include <clib/dos_protos.h>

/****************************************************************************/

   /* The sample library base. */

struct DLGRexxBase
{
   struct Library          drx_Library;

   struct SignalSemaphore  drx_Semaphore;
   BPTR                    drx_Segment;

   struct Library         *drx_SysBase;
   struct Library         *drx_DOSBase;
   struct Library         *drx_UtilityBase;
   struct Library         *drx_RexxSysBase;
   struct Library         *drx_DLGBase;
};

#define SysBase      DLGRexxBase->drx_SysBase
#define DOSBase      DLGRexxBase->drx_DOSBase
#define UtilityBase  DLGRexxBase->drx_UtilityBase
#define RexxSysBase  DLGRexxBase->drx_RexxSysBase
#define DLGBase      DLGRexxBase->drx_DLGBase

/****************************************************************************/

   /* Function table entry. */

typedef LONG (* REXXCMD)(STRPTR *, LONG *, LONG *, struct DLGRexxBase *);

struct RexxCmd
{
   STRPTR   Name;    /* Function name. */
   LONG     MinArgs; /* Minimum number of arguments required. */
   LONG     MaxArgs; /* Maximum number of arguments required. */
   LONG     Type;    // 0 = return number
                     // 1 = return text
   REXXCMD  Cmd;     /* Handler function. */
};

/****************************************************************************/

   /* Handy macros. */

#define ASM    __asm
#define REG(x) register __ ## x
