#include <string.h>

#include "DLGRexxBase.h"

#include <clib/utility_protos.h>

#include <pragmas/utility_pragmas.h>
#include <pragmas/dos_pragmas.h>

/// StringToNumber()
/****************************************************************************/

   /* StringToNumber(STRPTR String,struct RexxSampleBase *RexxSampleBase):
    *
    * Convert a string into a number.
    */

LONG
StringToNumber(
   STRPTR                  String,
   struct   DLGRexxBase   *DLGRexxBase)
{
   LONG Result;

      /* Uses the dos.library code. */

   StrToLong(String,&Result);

   return(Result);
}
//-
/// NumberToString()
   /* NumberToString(LONG Value,STRPTR String,struct RexxSampleBase *RexxSampleBase):
    *
    * Convert a number into a string. This is done using the
    * utility.library integer math routines and thus looks a
    * bit bizarre...
    */

VOID
NumberToString(
   LONG                 Value,
   STRPTR               String,
   struct DLGRexxBase  *DLGRexxBase)
{
   LONG Sign;
   LONG Next;
   LONG Len;
   LONG i;
   UBYTE c;

   if(Value < 0)
   {
      Sign = -1;
      Value = (-Value);
   }
   else
   {
      Sign = 1;
   }

   Len = 0;

   while(Value > 0 && Len < 15)
   {
      Next = SDivMod32(Value,10);

      String[Len++] = (Value - SMult32(Next,10)) + '0';

      Value = Next;
   }

   if(Sign < 0)
      String[Len++] = '-';

   String[Len] = 0;

   if(Len > 1)
   {
      for(i = 0 ; i < Len / 2 ; i++)
      {
         c                 = String[i];
         String[i]            = String[Len - (i + 1)];
         String[Len - (i + 1)]   = c;
      }
   }
   else
   {
      strcpy(String,"0");
   }

}
//-

