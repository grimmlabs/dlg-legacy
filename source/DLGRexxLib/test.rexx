/* Test of DLGrexx.library functions */

Options Results

address command "copy dlgrexx.library_any libs:dlgrexx.library"
address command "avail flush"

/*say show(l)
*/

if ~show(l, "dlgrexx.library") then do
   if ~addlib("dlgrexx.library", 0, -30, 0) then do
      say "Could not load dlgrexx.library."
      exit(20)
   end
end


call GetAreaInfo(10,1)
say RC
say RESULT

