#include <stdlib.h>
#include <string.h>

#include "DLGRexxBase.h"

#include <dlg/resman.h>

#include <rexx/errors.h> // Leave this in for error reporting

#include <pragmas/dlg.h>

#include <proto/dlg.h>

#include "Protos.h"

extern   char *TextRC;

/****************************************************************************/

/// BorrowArea
LONG  Cmd_BorrowArea(STRPTR *Args, LONG *Error, LONG *NumArgs, struct DLGRexxBase *DLGRexxBase)
{
   LONG     pri   =  0;
   UBYTE    flags =  0;
   USHORT   area  =  0;

   area = (USHORT) StringToNumber(Args[0],DLGRexxBase);  // Must have area

   if(*NumArgs >= 2)
      flags = (UBYTE) StringToNumber(Args[1],DLGRexxBase);

   if( (area <= 0) || (area > 9999)) { (*Error) = ERR10_018; return(0); }

   if(*NumArgs == 5)
   {
      pri = (char) StringToNumber(Args[4],DLGRexxBase);

      if( (pri < -127) || (pri > 128)) { (*Error) = ERR10_018; return(0); }
   }

   return(BorrowArea(  area,
                     (*NumArgs > 2) ? (char *)Args[2] : "DIALOG",
                     (*NumArgs > 3) ? (char *)Args[3] : "No Reason Given",
                     pri, flags
                  ));
}
//-
/// LockArea
LONG  Cmd_LockArea(STRPTR *Args, LONG *Error, LONG *NumArgs, struct DLGRexxBase *DLGRexxBase)
{
   LONG     pri   =  0;
   UBYTE    flags =  0;
   USHORT   area  =  0;

   area = (USHORT) StringToNumber(Args[0],DLGRexxBase);  // Must have area

   if(*NumArgs >= 2)
   {
      flags = (UBYTE) StringToNumber(Args[1],DLGRexxBase);

      if(flags & FILELOCK)
         flags |= PENDLOCK | WRITELOCK;
      else
         flags = PENDLOCK | WRITELOCK;
   }
   else
      flags = PENDLOCK | WRITELOCK;


   if( (area <= 0) || (area > 9999)) { (*Error) = ERR10_018; return(0); }

   if(*NumArgs == 5)
   {
      pri = (char) StringToNumber(Args[4],DLGRexxBase);

      if( (pri < -127) || (pri > 128)) { (*Error) = ERR10_018; return(0); }
   }

   return(LockArea(  area,
                     (*NumArgs > 2) ? (char *)Args[2] : "DIALOG",
                     (*NumArgs > 3) ? (char *)Args[3] : "No Reason Given",
                     pri, flags
                  ));
}
//-
/// FreeArea
LONG  Cmd_FreeArea(STRPTR *Args, LONG *Error, LONG *NumArgs, struct DLGRexxBase *DLGRexxBase)
{
   UBYTE flags =  MSGLOCK;
   USHORT   area  =  0;

   area = (USHORT) StringToNumber(Args[0],DLGRexxBase);  // Must have area


   if(*NumArgs >= 2)
      flags = (UBYTE) StringToNumber(Args[1],DLGRexxBase);
   else
      flags = MSGLOCK;

   if(*NumArgs == 3)
      return(FreeArea(area,Args[2],flags));
   else
      return(FreeArea(area,"DIALOG", flags));
}
//-
/// EnterArea
LONG  Cmd_EnterArea(STRPTR *Args, LONG *Error, LONG *NumArgs, struct DLGRexxBase *DLGRexxBase)
{
   UBYTE    flags =  0;
   USHORT   area  =  0;

   area = (USHORT) StringToNumber(Args[0],DLGRexxBase);  // Must have area

   if(*NumArgs == 2)
      flags = (UBYTE) StringToNumber(Args[1],DLGRexxBase);


   if( (area <= 0) || (area > 9999)) { (*Error) = ERR10_018; return(0); }

   return(EnterArea(  area, flags ));
}
//-
/// LeaveArea
LONG  Cmd_LeaveArea(STRPTR *Args, LONG *Error, LONG *NumArgs, struct DLGRexxBase *DLGRexxBase)
{
   UBYTE    flags =  0;
   USHORT   area  =  0;

   area = (USHORT) StringToNumber(Args[0],DLGRexxBase);  // Must have area

   if(*NumArgs == 2)
      flags = (UBYTE) StringToNumber(Args[1],DLGRexxBase);


   if( (area <= 0) || (area > 9999)) { (*Error) = ERR10_018; return(0); }

   return(LeaveArea(  area, flags ));
}
//-
/// GetAreaInfo
LONG  Cmd_GetAreaInfo(STRPTR *Args, LONG *Error, LONG *NumArgs, struct DLGRexxBase *DLGRexxBase)
{
   char                 t[20];
   LONG                 myRC  =  RMNOERR;
   struct   DLGAreaInfo ai;
   UBYTE                flags =  0;

   if(TextRC)  { free(TextRC); TextRC = NULL; }

   ai.area = (USHORT) StringToNumber(Args[0],DLGRexxBase);

   if(*NumArgs == 2)
      flags = (UBYTE) StringToNumber(Args[1],DLGRexxBase);

   myRC = GetAreaInfo(&ai, flags);

   if(myRC == RMNOERR)
   {
      LONG MaxLen = 0;

      MaxLen = strlen(ai.passwd) + strlen(ai.reason) + 4 + 4;
      MaxLen = MaxLen * 3;

      TextRC = calloc(1,MaxLen);

      sprintf(TextRC,"PASS \"%s\" REASON \"%s\" PRI %ld USERS %ld",  ai.passwd,     ai.reason,
                                                                     ai.priority,   ai.users);
      FreeAreaInfo(&ai);
   }  // myRC == RMNOERR
   else
   {
      NumberToString(myRC,t,DLGRexxBase);
      TextRC = strdup(t);
   }

   return(0);
}
//-

