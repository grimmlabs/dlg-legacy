// Support.c

extern LONG StringToNumber(STRPTR String, struct DLGRexxBase *DLGRexxBase);
extern VOID NumberToString(LONG Value, STRPTR String, struct DLGRexxBase  *DLGRexxBase);

// Port.c

extern LONG  Cmd_ActivatePort(STRPTR *Args, LONG *Error, LONG *NumArgs, struct DLGRexxBase *DLGRexxBase);
extern LONG  Cmd_DeactivatePort(STRPTR *Args, LONG *Error, LONG *NumArgs, struct DLGRexxBase *DLGRexxBase);
extern LONG  Cmd_FreePort(STRPTR *Args, LONG *Error, LONG *NumArgs, struct DLGRexxBase *DLGRexxBase);
extern LONG  Cmd_ImmedLockPort(STRPTR *Args, LONG *Error, LONG *NumArgs, struct DLGRexxBase *DLGRexxBase);
extern LONG  Cmd_LockPort(STRPTR *Args, LONG *Error, LONG *NumArgs, struct DLGRexxBase *DLGRexxBase);
extern LONG  Cmd_TransferPortLock(STRPTR *Args, LONG *Error, LONG *NumArgs, struct DLGRexxBase *DLGRexxBase);
extern LONG  Cmd_GetPortInfo(STRPTR *Args, LONG *Error, LONG *NumArgs, struct DLGRexxBase *DLGRexxBase);
extern LONG  Cmd_ListPorts(STRPTR *Args, LONG *Error, LONG *NumArgs, struct DLGRexxBase *DLGRexxBase);

// Area.c
extern LONG  Cmd_LockArea(STRPTR *Args, LONG *Error, LONG *NumArgs, struct DLGRexxBase *DLGRexxBase);
extern LONG  Cmd_FreeArea(STRPTR *Args, LONG *Error, LONG *NumArgs, struct DLGRexxBase *DLGRexxBase);
extern LONG  Cmd_BorrowArea(STRPTR *Args, LONG *Error, LONG *NumArgs, struct DLGRexxBase *DLGRexxBase);
extern LONG  Cmd_EnterArea(STRPTR *Args, LONG *Error, LONG *NumArgs, struct DLGRexxBase *DLGRexxBase);
extern LONG  Cmd_LeaveArea(STRPTR *Args, LONG *Error, LONG *NumArgs, struct DLGRexxBase *DLGRexxBase);
extern LONG  Cmd_GetAreaInfo(STRPTR *Args, LONG *Error, LONG *NumArgs, struct DLGRexxBase *DLGRexxBase);

