#include <exec/types.h>
#include <stdlib.h>
#include <string.h>

#include <dlg/misc.h>
#include <dlg/menu.h>
#include <dlg/msg.h>
#include <dlg/resman.h>
#include <dlg/user.h>

#include <link/io.h>
#include <link/msg.h>
#include <link/util.h>

#include <pragmas/dlg.h>

#include <proto/dlg.h>

#include "Mess.h"
#include "Action.h"

extern   BOOL                       shortmenuflag;
extern   BOOL                       contread;
extern   BOOL                       TagMode;

extern   char                       search[];
extern   char                       WheelOn;
extern   char                       threadmode;       // Threading mode user chose
extern   char                      *transarray;
extern   char                       tagfile[];        // Name of tags file
extern   char                       highestmsg;

extern   SHORT                      globalorigzone;
extern   SHORT                      globaldestzone;
extern   SHORT                      globalpoint;
extern   SHORT                      globaltopt;
extern   SHORT                      ReadDirection;

extern   struct   fido              FidoNet;
extern   struct   NewShortMenu      Menu[];
extern   struct   ShortMenu         TMenu[];

extern   USHORT                     bpos;


extern   BYTE  Get_Alias(char *, char *);
extern   ULONG Read_Body(LONG Area, LONG MsgNum, char **Buffer, ULONG BufferSize);
extern   BOOL  Read_Header(LONG Area, LONG MsgNum, struct Msg_Header *mh);
extern   BOOL  Write_Header(LONG Area, LONG MsgNum, struct Msg_Header mh);


BYTE  MenuLen(void);
SHORT Read_Message(void);
BYTE  TagMenu(void);

/// Read_Message()
SHORT Read_Message(void)
{
   char                 status;
   char                 dbuffer[880];
   char                 filename[128];
   char                 ramfile[256];
   char                 tempname[128];
   char                 netflag = 0;

   SHORT                counter;
   SHORT                retval;

   globalorigzone = FidoNet.zone;
   globaldestzone = FidoNet.zone;
   globalpoint = 0;
   globaltopt = 0;

   if (LAST == PVTAREA)
   {
      ASPrintf(NULL, filename, "USER:%s/%ld.MSG", RStruct.Name, Message);
      UnderScore(filename);
   }
   else
   {
      ASPrintf(NULL, filename, "MSG:%d/%ld.MSG", LAST, Message);
      BorrowArea(LAST, LPswd, "4", 64, MSGLOCK | WRITELOCK);
   }

   if(!Read_Header(LAST, Message, &Header))
   {
      FreeArea(LAST, LPswd, MSGLOCK | WRITELOCK);
      return (5);
   }

   if ((Header.TimesRead == PRINET_MSG && LAST == PVTAREA) || MsgArea.Flag & NETMAIL_AREA)
      netflag++;

   for (; search[0];)
   {
      char                parsed[80];

      strcpy(parsed, search);

      strcpy(ramfile, Header.From);

      if (DLGPatternMatch(parsed, ramfile))  break;

      strcpy(ramfile, Header.To);

      if (DLGPatternMatch(parsed, ramfile))  break;

      strcpy(ramfile, Header.Title);

      if (DLGPatternMatch(parsed, ramfile))  break;

      FreeArea(LAST, LPswd, MSGLOCK | WRITELOCK);
      return (3);
   }

   if (WheelOn)
   {
      StopWheel();
      Clr(UserDat.Ansi_Flag);
      WheelOn = 0;
   }

   if (Menu[ME].status)                Menu[MR].status = 1;
   if (netflag && !(UserDat.NetMail))  Menu[MR].status = 0;
   if(UserDat.User_Level == 255)       Menu[MPrt].status = 1;

   Menu[MEx].status = 1;
   shortmenuflag = TRUE;

   if (threadmode)                     Menu[MZ].status = 1;

   BCPend(Ext);

   if (  (Header.Attribute & Private) &&  !((Log.Priv_Flag & Sysop_Access) ||
                                          !(Stricmp(Header.To, RStruct.Name)) ||
                                          !(Stricmp(Header.From, RStruct.Name))))
   {
      FreeArea(LAST, LPswd, MSGLOCK | WRITELOCK);
      return (2);
   }

   if (netflag)
   {
      char               *kludge =  NULL;

      kludge = calloc(1,256);
      Read_Body(LAST, Message, &kludge, 256);

      for (counter = 0; counter < 256; counter++)
      {
         if (kludge[counter] != 0 && kludge[counter] != 141 && kludge[counter] != 10 && kludge[counter] != 13 && kludge[counter] != 1)
            break;

         if (kludge[counter] == 1)
         {
            if (!Strnicmp(kludge + counter + 1, "FMPT", 4))
               sscanf(kludge + counter + 5, "%hd", &globalpoint);

            if (!Strnicmp(kludge + counter + 1, "TOPT", 4))
               sscanf(kludge + counter + 5, "%hd", &globaltopt);

            if (!Strnicmp(kludge + counter + 1, "INTL", 4))
               sscanf(kludge + counter + 5, "%hd:%hd/%hd %hd:%hd/%hd",  &globaldestzone,  &Header.DestNet,
                                                                        &Header.DestNode, &globalorigzone,
                                                                        &Header.OrigNet,  &Header.OrigNode);

            for (; kludge[counter] != 13 && counter < 256; counter++) ;
         }
      }

      free(kludge);  kludge = NULL;
   }

   Header.From[35] = 0;

   if ((MsgArea.Flag & HANDLES_AREA) && (LAST != PVTAREA))
      Get_Alias(Header.From, tempname);
   else
      strcpy(tempname, Header.From);

   ScreenName(tempname);

   if (netflag)
   {
      if (globalpoint)
         ASPrintf(NULL, tempname, SA[1083], Header.From, globalorigzone, Header.OrigNet, Header.OrigNode, globalpoint);
      else
         ASPrintf(NULL, tempname, SA[1084], Header.From, globalorigzone, Header.OrigNet, Header.OrigNode);

      ScreenName(tempname);
   }

   ASPrintf(&UserDat, dbuffer, SA[1085], tempname);

   if (Header.Attribute & Received)
      ASPrintf(NULL, dbuffer + strlen(dbuffer), SA[1086]);

   if (Header.Attribute & Sent)
      ASPrintf(NULL, dbuffer + strlen(dbuffer), SA[1087]);

   if (Header.Attribute & Private)
      ASPrintf(NULL, dbuffer + strlen(dbuffer), SA[1088]);

   if (Header.TimesRead == CC_MSG)
      ASPrintf(NULL, dbuffer + strlen(dbuffer), SA[1089]);

   if (Header.Attribute & FileAttached)
      ASPrintf(NULL, dbuffer + strlen(dbuffer), SA[1090]);

   if (Header.Attribute & FFileRequest)
      ASPrintf(NULL, dbuffer + strlen(dbuffer), SA[1091]);

   Header.To[35] = 0;

   if (MsgArea.Flag & HANDLES_AREA && LAST != PVTAREA)
      Get_Alias(Header.To, tempname);
   else
      strcpy(tempname, Header.To);

   ScreenName(tempname);

   if (netflag)
   {
      if (globaltopt)
         ASPrintf(NULL, tempname, SA[1083], Header.To, globaldestzone, Header.DestNet, Header.DestNode, globaltopt);
      else
         ASPrintf(NULL, tempname, SA[1084], Header.To, globaldestzone, Header.DestNet, Header.DestNode);

      ScreenName(tempname);
   }

   ASPrintf(&UserDat, dbuffer + strlen(dbuffer), SA[1092], Message, High_Message, tempname);

   if (!(Stricmp(RStruct.Name, Header.To)) && !(Header.Attribute & Received))
   {
      char                string[80];
      struct WaitingMail  WM;

      Header.Attribute |= Received;
      Write_Header(LAST, Message, Header);

      if (LAST == PVTAREA && Header.Cost != PRINET_MSG)
      {
         ASPrintf(&UserDat, string, SA[1095], Header.To);
         Inform(Header.From, string, Ext, 0);
      }
      else
      {
         ASPrintf(NULL, string, "USER:%s/WaitingMail.dat", Header.To);
         UnderScore(string);
         ASPrintf(NULL, WM.msgid, "%04d:%05ld", LAST, Message);
         DeleteStruct(string, (char *) &WM, sizeof(WM), 11);
      }
   }

   if (Header.ReplyTo || Header.NextReply)
   {
      ASPrintf(&UserDat, dbuffer + strlen(dbuffer), "%a2");

      if (Header.ReplyTo)
         ASPrintf(NULL, dbuffer + strlen(dbuffer), SA[1093], Header.ReplyTo);

      if (Header.NextReply)
         ASPrintf(NULL, dbuffer + strlen(dbuffer), SA[1094], Header.NextReply);
   }

   Header.Title[71] = 0;

   if (transarray)
   {
      unsigned char       a;
      unsigned char       b;

      for (a = 0; !Header.Title[a]; a++)
      {
         b = transarray[Header.Title[a]];
         Header.Title[a] = b;
      }
   }
   else
      ScreenName(Header.Title);

   Header.Date[19] = 0;
   ScreenName(Header.Date);
   ASPrintf(&UserDat, dbuffer + strlen(dbuffer), SA[1096], Header.Date, Header.Title);

   FreeArea(LAST, LPswd, MSGLOCK | WRITELOCK);

   if (!contread) bpos = 1;

   if (!INSTACK)
      retval = DisplayBuffer(sout, dbuffer, UserDat.Screen_Width, UserDat.Screen_Len, UserDat.More_Flag, UserDat.Ansi_Flag, &bpos, 0, NULL, 1, "\003\016\032");
   else
      retval = 0;

   AFPrintf(&UserDat, sout, SA[1097]);
   bpos++;

   if (TagMode)
   {
      retval = TagMenu();

      if (retval == 0)
      {
         TagMode = FALSE;
         return (0);
      }

      if (retval == 2)
      {
         LONG                flags = MSG_MSGAREA;

         bpos = 1;
         DisplayBuffer(sout, dbuffer, UserDat.Screen_Width, UserDat.Screen_Len, UserDat.More_Flag, UserDat.Ansi_Flag, &bpos, 0, NULL, 1, "\003\016\032");
         AFPrintf(&UserDat, sout, SA[1098]);
         bpos++;

         if ((MsgArea.Flag & ECHO_AREA) && !(MsgArea.Flag & NETMAIL_AREA) && LAST != PVTAREA)
            if (MsgArea.Flag & HIDE_SEENBY)
               flags |= MSG_STRIPSB;

         status = Display_Msg(LAST, transarray, LPswd, flags, filename, UserDat.Screen_Width - 1, UserDat.Screen_Len, UserDat.More_Flag, &bpos, 190L, UserDat.Ansi_Flag & ~ANSI_NOFREEZE, "\x1a\x03\x0d\x0e");

         if (!status)   AFPrintf(NULL, sout, "\n");

         Pause();
      }

      return (6);
   }

   if (Header.ReplyTo)
      Menu[MMinus].status = 1;
   else
      Menu[MMinus].status = 0;

   if (Header.NextReply)
      Menu[MPlus].status = 1;
   else
      Menu[MPlus].status = 0;

   if (retval < 0)
      return (1);

   if (retval == 14 || retval == 26)
      return (2);

   status = 0;

   if (!INSTACK)
   {
      LONG                flags = MSG_MSGAREA;

      if ((MsgArea.Flag & ECHO_AREA) && !(MsgArea.Flag & NETMAIL_AREA) && LAST != PVTAREA)
         if (MsgArea.Flag & HIDE_SEENBY)
            flags |= MSG_STRIPSB;

      status = Display_Msg(LAST, transarray, LPswd, flags, filename, UserDat.Screen_Width - 1, UserDat.Screen_Len, UserDat.More_Flag, &bpos, 190L, (contread) ? UserDat.Ansi_Flag | ANSI_NOFREEZE : UserDat.Ansi_Flag & ~ANSI_NOFREEZE, "\x1a\x03\x0d\x0e");
      AFPrintf(NULL, sout, "\n");
   }

   if (status == 0 && UserDat.More_Flag && !contread && bpos + MenuLen() > UserDat.Screen_Len)
      Pause();

   UserDat.Messages_Read++;

   if (Log.Priv_Flag & Sysop_Access)
   {
      Menu[MK].status = 1;
      Menu[MC].status = 1;
      Menu[MF].status = 1;
   }

   Menu[ML].status = 1;

   if ((Log.Priv_Flag & Kill_Priv && (!Stricmp(RStruct.Name, Header.To) || !Stricmp(RStruct.Name, Header.From))) || LEVEL == 255 || LAST == PVTAREA)
      Menu[MK].status = 1;
   else
      Menu[MK].status = 0;

   if (Log.Priv_Flag & Re_Edit && (!Stricmp(RStruct.Name, Header.From)))
      Menu[MC].status = 1;

   // Allow users to get to the forward module if they have forward access */
   if (Log.Priv_Flag & Forward_Priv || Log.Priv_Flag & Hurl_Priv)
      Menu[MF].status = 1;

   if (status == -1)
      return (1);

   if (status == -2)
      status = 0;

   if (status != 0)
      return (4);

   return (0);
}
//-
/// TagMenu
BYTE                TagMenu(void)
{
   char                inp[2];
   char                msgid[12];

   while (TRUE)
      {
         HandleBCMsgs(Ext);
         menu(TMenu, 7, 1);

         DLGInput(NULL, inp, NULL, 1, 1, 1, " => ");
         TwoLines();
         if (inp[0] == '?')
            {
               menu(TMenu, 7, 0);
               AFPrintf(&UserDat, sout, "\n");
               continue;
            }

         break;
      }

   switch (inp[0])
      {
      case '.':
         ASPrintf(NULL, msgid, "%04d:%05ld", MsgArea.Number, Message);
         AFPrintf(&UserDat, sout, SA[1197], Message);
         AddStruct(tagfile, msgid, 11, 11);
         Menu[MT].status = TRUE;
         break;

      case 'A':
         return (FALSE);
         break;

      case 'Z':
         if (threadmode && !highestmsg)
            {
               AFPrintf(&UserDat, sout, SA[1198], SkipRest(LAST, (SHORT) Message, ReadDirection, NULL));
               Header.NextReply = 0;
               Header.ReplyTo = 0;
            }
         break;

      case 'T':
         if (threadmode && !highestmsg)
            AFPrintf(&UserDat, sout, SA[1199], SkipRest(LAST, (SHORT) Message, ReadDirection, tagfile));
         Menu[MT].status = TRUE;
         break;

      case 'R':
         return (2);
         break;

      default:
         break;
      }

   return (1);
}
//-
/// MenuLen
BYTE                MenuLen(void)
{
   if (UserDat.Help_Level == 0 && !shortmenuflag)
      return (10);
   if (UserDat.Help_Level == 1 || (UserDat.Help_Level == 0 && shortmenuflag))
      return (3);
   if (UserDat.Help_Level == 2)
      return (1);
}
//-

