#include <dlg/menu.h>
#include <dlg/msg.h>
#include <dlg/user.h>

#include <link/io.h>
#include <link/msg.h>

#include <pragmas/dlg.h>

#include <proto/dlg.h>

#include "mess.h"
#include "Action.h"

extern   BOOL                       shortmenuflag;

extern   char                       highestmsg;
extern   char                       threadmode;    // Threading mode user chose

extern   struct   NewShortMenu      Menu[];

extern   BYTE  Find_Pointers(long, char *);


long                threadflag = -1;


void New_Area(void)
{
   long                nmessages;
   long                tmessages;
   char                filename[128];

   highestmsg = 0;
   High_Message = -1;
   Low_Message = -1;
   Find_Pointers(LAST, RStruct.Name);

   if (Log.High_Mess > High_Message)
      Log.High_Mess = 0;

   Message = (Log.High_Mess < Low_Message) ? Low_Message - 1 : Log.High_Mess;

   RStruct.area = LAST;
   WriteRam(&RStruct, Ext);

   Menu[MZ].status = 0;
   Menu[MR].status = 0;
   Menu[MEx].status = 0;
   Menu[MPrt].status = 0;
   Menu[MK].status = 0;
   Menu[MC].status = 0;
   Menu[MD].status = 0;
   Menu[ML].status = 0;
   Menu[MF].status = 0;
   Menu[MP].status = 1;
   Menu[MPlus].status = 0;
   Menu[MMinus].status = 0;
   Menu[MPeriod].status = 1;

   ASPrintf(NULL,filename,"User:%s/GlobalAreas.Msg",RStruct.Name);
   UnderScore(filename);

   if(LAST == PVTAREA)
   {
      Menu[MSUB].status = 0;
      Menu[MUNSUB].status = 0;
   }
   else
   {
      if(ExistsGlobalArea(filename,LAST))
      {
         Menu[MSUB].status = 0;
         Menu[MUNSUB].status = 1;
      }
      else
      {
         Menu[MSUB].status = 1;
         Menu[MUNSUB].status = 0;
      }
   }

   shortmenuflag = 0;
   threadflag = -1;

   if (threadmode)
      {
         StartThread(Low_Message, High_Message, Message);
         Header.ReplyTo = 0;
         Header.NextReply = 0;
      }

   if (!INSTACK)
      {
         ASPrintf(NULL, filename, "MSG:%d/EnterArea.txt", LAST);
         DispForm(filename, &UserDat, &UserDat, &RStruct, Ext);

         AFPrintf(&UserDat, sout, SA[1174], LAST, MsgArea.Name);

         tmessages = (High_Message - Low_Message) + 1;
         AFPrintf(&UserDat, sout, SA[1175], tmessages);

         AFPrintf(&UserDat, sout, SA[1176], Low_Message, High_Message);

         nmessages = High_Message - Log.High_Mess;
         if (nmessages > tmessages)
            nmessages = tmessages;
         AFPrintf(&UserDat, sout, SA[1177], nmessages);
      }
}

