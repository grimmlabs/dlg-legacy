#include <exec/types.h>

#include <dlg/msg.h>
#include <dlg/user.h>

#include <link/io.h>

#include <pragmas/dlg.h>

#include <proto/dlg.h>

#include "Mess.h"


BOOL                Echo_Msg(void)
{
   char                filename[128];

   if (LAST == PVTAREA) return (FALSE);

   ASPrintf(NULL, filename, "MSG:%d/Echo.txt", LAST);

   if (Exists(filename))
      DispForm(filename, &UserDat, &UserDat, &RStruct, Ext);
   else
      DispForm("DLGCONFIG:Text/Echo.txt", &UserDat, &UserDat, &RStruct, Ext);

   return (TRUE);
}

