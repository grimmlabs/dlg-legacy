#include <string.h>

#include <dlg/menu.h>
#include <dlg/msg.h>
#include <dlg/user.h>

#include <link/io.h>

#include <pragmas/dlg.h>

#include <proto/dlg.h>

#include "Mess.h"
#include "action.h"

extern   SHORT                      ReadDirection;

extern   struct   NewShortMenu      Menu[];

extern   BYTE  Find_Pointers(long, char *);
extern   BYTE  Read_Pointers(long);


void                EnterPrivateArea(void)
{
   MsgArea.Flag = 0;

   ReadDirection = 1;
   Menu[MGreater].status = 0;
   Menu[MLess].status = 1;

   Menu[MD].status = 1;
   Menu[MP].status = 0;
   Menu[MPeriod].status = 0;
   Menu[MSUB].status = 0;
   Menu[MUNSUB].status = 0;

   strcpy(Header.To, RStruct.Name);
   High_Message = -1;
   Low_Message = -1;

   Find_Pointers(PVTAREA, RStruct.Name);
   Read_Pointers(PVTAREA);
   if (Log.High_Mess > High_Message)
      Log.High_Mess = 0;

   Message = Log.High_Mess;
   LAST = PVTAREA;

   RStruct.area = LAST;
   WriteRam(&RStruct, Ext);

   AFPrintf(&UserDat, sout, SA[1178]);
   AFPrintf(&UserDat, sout, SA[1179], (High_Message - Low_Message) + 1);
   AFPrintf(&UserDat, sout, SA[1176], (High_Message) ? Low_Message : 0L, High_Message);
   AFPrintf(&UserDat, sout, SA[1177], High_Message - Log.High_Mess);

   return;
}

