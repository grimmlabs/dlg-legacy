#include <exec/types.h>
#include <string.h>

#include <dlg/user.h>

#include <link/io.h>

#include <pragmas/dlg.h>

#include <proto/dlg.h>

#include "Mess.h"

BYTE                Get_Alias(char *name, char *alias)
{
   char                filename[128];
   struct USER_DATA    Data;

   ASPrintf(NULL, filename, "USER:%s/User.Data", name);
   UnderScore(filename);

   if(-1 == GetFirstStruct(filename, (char *)&Data, sizeof(struct USER_DATA) ))
   {
      strcpy(alias,name);
      return(1);
   }

   strcpy(alias, Data.Alias);
   Capitalize(alias);
   return (0);
}

