#include <exec/types.h>
#include <stdlib.h>
//#include <string.h>

#include <dlg/msg.h>
#include <dlg/resman.h>
#include <dlg/user.h>
#include <dlg/version.h>

//#include <link/area.h>
#include <link/io.h>

#include <pragmas/dlg.h>

#include <proto/dlg.h>
#include <proto/dos.h>

#include "Mess.h"

extern   BOOL  Read_Header(LONG Area, LONG MsgNum, struct Msg_Header *mh);
extern   ULONG Read_Body(LONG Area, LONG MsgNum, char **Buffer, ULONG BufferSize);


void  PrintMsg(void)
{
   BPTR                 ofh            =  NULL;

   BYTE                 hpos           =  0;

   char                 t[256];
   char                *buf            =  NULL;

   struct   Msg_Header  mh;

   ULONG                size           =  0;
   ULONG                cnt            =  0;
   ULONG                i;
   ULONG                LastSpace      =  0;

   AFPrintf(&UserDat, sout, "%a6Printing, please wait .... ");

   if(LAST == PVTAREA)
   {
      ASPrintf(NULL, t,"USER:%s/%ld.msg", RStruct.Name, Message);
      UnderScore( t );
   }
   else
   {
      ASPrintf(NULL, t,"MSG:%d/%ld.msg", LAST, Message);
      BorrowArea(LAST, LPswd, "4", 64, MSGLOCK | WRITELOCK);
   }

   if(-1 == FileSize(t,&size))
   {
      AFPrintf(&UserDat,sout,"%a3Error! Could not read message!\n");
      if(LAST != PVTAREA)  FreeArea(LAST, LPswd, MSGLOCK | WRITELOCK);
      return;
   }

   if(size <= sizeof(struct Msg_Header))
   {
      AFPrintf(&UserDat,sout,"%a3Error! Message corrupted!\n");
      if(LAST != PVTAREA)  FreeArea(LAST, LPswd, MSGLOCK | WRITELOCK);
      return;
   }

   buf = calloc(1, size);

   if(!buf)
   {
      AFPrintf(&UserDat,sout,"%a3Memory allocation Error!\n");
      if(LAST != PVTAREA)  FreeArea(LAST, LPswd, MSGLOCK | WRITELOCK);
      return;
   }


//   ofh = Open("logs:test.txt", MODE_NEWFILE);
   ofh = Open("PRT:", MODE_NEWFILE);

   if(!ofh)
   {
      AFPrintf(&UserDat,sout,"%a3Error! Could not access printer!\n");
      if(LAST != PVTAREA)  FreeArea(LAST, LPswd, MSGLOCK | WRITELOCK);
      return;
   }

   SDraw_Line(t,UserDat.Screen_Width);
   AFPrintf(NULL, ofh, t);

   SMDate(AmigaTime(), t);
   AFPrintf(NULL, ofh, "### Printed by %s on %s\n", RStruct.Name, t);

   SDraw_Line(t,UserDat.Screen_Width);
   AFPrintf(NULL, ofh, t);

   AFPrintf(NULL, ofh, "Message area [ %ld ] - %s", LAST, MsgArea.Name);

   if(MsgArea.Flag & ECHO_AREA)        AFPrintf(NULL,ofh," (Fidonet Echomail)");
   if(MsgArea.Flag & NETMAIL_AREA)     AFPrintf(NULL,ofh," (Fidonet Netmail)");
   if(MsgArea.Flag & NEWSGROUP_AREA)   AFPrintf(NULL,ofh," (Newsgroup)");
   AFPrintf(NULL, ofh, "\n");

   SDraw_Line(t,UserDat.Screen_Width);
   AFPrintf(NULL, ofh, t);

   if(!Read_Header(LAST, Message, &mh))
   {
      AFPrintf(&UserDat,sout,"%a3Error! Could not read message!\n");
      Close(ofh); ofh = NULL;
      free(buf);  buf = NULL;
      if(LAST != PVTAREA)  FreeArea(LAST, LPswd, MSGLOCK | WRITELOCK);
      return;
   }

   AFPrintf(NULL, ofh, "From      : %s\n", mh.From);
   AFPrintf(NULL, ofh, "To        : %s\n", mh.To);
   AFPrintf(NULL, ofh, "Written on: %s\n", mh.Date);
   AFPrintf(NULL, ofh, "Subject   : %s\n", mh.Title);
   SDraw_Line(t,UserDat.Screen_Width);
   AFPrintf(NULL, ofh, t);

   cnt = Read_Body(LAST, Message, &buf, size);

   for(i = 0; i < cnt; i++)
   {
      if(buf[i] == 13)  buf[i] = '\n';   // CR2LF
      if(buf[i] == 1)   buf[i] = '@';  // Kludges
      if(buf[i] == 0)   buf[i] = ' ';   // NULL bytes
   }

   hpos = 1;

   for(i = 0; i < cnt; i++)
   {
      if(buf[i] == ' ') LastSpace = i;
      if(buf[i] == '\n') hpos = 0;

      hpos++;

      if(hpos >= (UserDat.Screen_Width -1 ))
      {
         buf[LastSpace] = '\n';
         hpos = 1;
      }
   }

   Write(ofh,buf,cnt);
   AFPrintf(NULL, ofh, "\n");

   if(buf)  free(buf);  buf = NULL;

   SDraw_Line(t,UserDat.Screen_Width);
   AFPrintf(NULL, ofh, t);

   AFPrintf(NULL,ofh, "%s\n", DLGLONGNAME);

   SDraw_Line(t,UserDat.Screen_Width);
   AFPrintf(NULL, ofh, t);
   AFPrintf(NULL, ofh, "\n");

   Close(ofh);          ofh = NULL;
   if(LAST != PVTAREA)  FreeArea(LAST, LPswd, MSGLOCK | WRITELOCK);

   AFPrintf(&UserDat, sout, "%a6done!\n\n");
   Clr(UserDat.Ansi_Flag);
   return;
}


