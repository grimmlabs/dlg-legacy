#include <exec/types.h>
#include <string.h>
#include <ctype.h>

#include <dlg/menu.h>
#include <dlg/msg.h>
#include <dlg/resman.h>
#include <dlg/user.h>

#include <link/area.h>
#include <link/io.h>
#include <link/msg.h>

#include <pragmas/dlg.h>

#include <proto/dlg.h>

#include "Mess.h"
#include "Action.h"

extern   BOOL                       sigrestoreflag;
extern   BOOL                       EnterFlag;
extern   BOOL                       contread;
extern   BOOL                       TagRead;
extern   BOOL                       TagMode;

extern   BYTE                       signumber;

extern   char                       areafile[];
extern   char                       savemore;
extern   char                       threadmode;
extern   char                       tempchar;
extern   char                       highestmsg;
extern   char                       search[];
extern   char                       WheelOn;

extern   long                       netarea;
extern   long                       numsigareas;
extern   long                       threadflag;
extern   long                       TagMessage;

extern   SHORT                      ReadDirection;
extern   SHORT                      HighSearchArea;
extern   SHORT                      SearchAreas[];
extern   SHORT                     *sigarray;
extern   SHORT                      TagLast;

extern   struct   NewShortMenu      Menu[];
extern   struct   SIG_Def           Sig;
extern   struct   ShortMenu         TMenu[];

extern   UBYTE                      saveansi;
extern   UBYTE                      sigrestore;

extern   USHORT                     bpos;

extern   BOOL  AbortTagRead(void);
extern   void  DeleteAll(void);
extern   BOOL  Echo_Msg(void);
extern   BYTE  Ed_Mess(void);
extern   void  Edit_Signature(void);
extern   void  EnterPrivateArea(void);
extern   BYTE  Find_Pointers(long area, char *name);
extern   BOOL  Forward_Message(long, long, char *);
extern   BYTE  Kill_Message(long, long, char *);
extern   BYTE  List_Readers(void);
extern   void  New_Area(void);
extern   void  PrintMsg(void);
extern   BOOL  Read_Area(long, struct Msg_Area *, char *);
extern   SHORT Read_Message(void);
extern   BYTE  Read_Pointers(long area);
extern   BOOL  SelectSIG(void);
extern   BOOL  TagExe(void);
extern   BYTE  Write_Bulletin(void);
extern   void  Write_Message(long);
extern   void  Write_Pointers(long);

SHORT               LastSearchArea  =  0;
SHORT               ReadSuccess;

int   HandleBuiltIn(UBYTE cmd)
{
   char                achangesig      =  0;
   char                contall         =  0;
   char                contdontask     =  0;
   char                log[256];
   char                ramfile[256];
   char                tempchar        =  0;

   long                b;
   long                inpvalue;
   long                tempflag;

   SHORT               spos;


   switch (cmd)
   {
///   Abort
      case 0:  Write_Pointers(LAST);

               if (sigrestoreflag)  UserDat.msgsig = sigrestore;

               WriteUser(RStruct.Name, &UserDat);

               if (EnterFlag)
               {
                  EnterFlag = FALSE;
                  LeaveArea(netarea, MSGLOCK | WRITELOCK);
               }

               break;
//-
///   Change Area -  Msg_ChangeArea
      case MA: tempflag = 0;
               Write_Pointers(LAST);

               while (TRUE)
               {
                  if (tempflag)
                  {
                     ListAreas(RStruct.Name, &UserDat, 0, signumber);
                     b = iinput(0, 999999, 0, SA[1202]);
                  }
                  else
                  {
                     b = iinput(0, 999999, 0, SA[1203]);
                  }

                  TwoLines();

                  if (achangesig)
                  {
                     if (!Read_Area(b, &MsgArea, areafile))
                     {
                        tempflag = 1;
                        continue;
                     }
                  }

                  if (b || tempflag)   break;

                  tempflag = 1;
               }

               achangesig = 0;

               if (!b)
               {
                  if (Read_Pointers(LAST))   InsertStack(STK, "\00113;");
                  break;
               }

               if (!Read_Pointers(b))
               {
                  LAST = b;
                  ReadDirection = 1;
                  Menu[MGreater].status = 0;
                  Menu[MLess].status = 1;
                  New_Area();
               }
               else
               {
                  AFPrintf(&UserDat, sout, SA[1204], b);
                  if (Read_Pointers(LAST))   InsertStack(STK, "\00113;");
               }

               break;
//-
/// Cont read
      case MEquals:                          /* Msg_ContRead */
         contread = TRUE;
         bpos = 2;
         if (!contdontask)
            {
               contall = finput(0, SA[1205]);
               TwoLines();
               if (contall)
                  contdontask++;
               AFPrintf(&UserDat, sout, SA[1206]);
               AFPrintf(&UserDat, sout, SA[1207]);
               AFPrintf(&UserDat, sout, SA[1208]);

               saveansi = UserDat.Ansi_Flag;
               savemore = UserDat.More_Flag;
               if (!UserDat.captureflags)
                  {
                     if (UserDat.Ansi_Flag)
                        if (finput(1, SA[1209]))
                           UserDat.Ansi_Flag = 0;
                     TwoLines();

                     if (UserDat.More_Flag)
                        if (finput(1, SA[1210]))
                           UserDat.More_Flag = 0;
                     TwoLines();
                  }
               else
                  {
                     if (!(UserDat.captureflags & 2))
                        UserDat.Ansi_Flag = 0;
                     if (!(UserDat.captureflags & 4))
                        UserDat.More_Flag = 0;
                  }
            }

         if (Message < (Low_Message - 1L) && ReadDirection == 1)
            Message = Low_Message - 1;
         if (Message > High_Message + 1L && ReadDirection == -1)
            Message = High_Message + 1;

         ReadSuccess = 0;
         while (Message <= High_Message + 1 && Message >= Low_Message - 1)
            {
               if (threadmode && LAST != PVTAREA)
                  Message = NextMessage(ReadDirection, Header.ReplyTo, Header.NextReply, ReadSuccess, High_Message);
               else
                  Message += ReadDirection;

               if ((Message > High_Message && ReadDirection == 1) || (Message < Low_Message && ReadDirection == -1))
                  break;

               if (Log.High_Mess < Message && (IsBaseThread() || LAST == PVTAREA) && !TagRead)
                  Log.High_Mess = Message;

               ReadSuccess = Read_Message();
               if (ReadSuccess == 1)
                  {
                     contall = 0;
                     contdontask = 0;
                     break;
                  }
            }

         if (Message > High_Message)
            Message = High_Message;
         if (Message < Low_Message)
            Message = Low_Message;
         if (contall && (LastSearchArea < HighSearchArea && !tempchar))
            strcpy(STK, "\00111;\0013;");
         else
            {
               contall = 0;
               contdontask = 0;
               tempchar = 0;
            }

         if (!contall)
            {
               UserDat.Ansi_Flag = saveansi;
               UserDat.More_Flag = savemore;
            }
         contread = FALSE;
         break;
//-
/// Correct Msg
      case MC:                          /* Msg_Correct */
         Ed_Mess();
         break;
//-
/// Delete all
      case MD:                          /* Msg_DeleteAll */
         if (!Menu[MD].status)
            break;
         DeleteAll();
         break;
//-
/// Edit sig
      case MO:                          /* Msg_EditSig */
         highestmsg = 0;
         Edit_Signature();
         break;
//-
/// Forward Msg
      case MF: Forward_Message(Message, LAST, RStruct.Name);
               break;
//-
/// Forward read
      case MGreater:                          /* Msg_FwdRead */
         highestmsg = 0;
         ReadDirection = 1;
         Menu[MGreater].status = 0;
         Menu[MLess].status = 1;
         AFPrintf(&UserDat, sout, SA[1211]);
         if (Message == Low_Message)
            Message--;
         if (threadmode)
            StartThread(Low_Message, High_Message, Message);
         strcpy(RStruct.Command_Stack, ";");
         break;
//-
/// Kill msg
      case MK:                          /* Msg_Kill */
         Kill_Message(Message, LAST, RStruct.Name);
         break;
//-
/// Lex check
      case ML:
         {                             /* Msg_LexCheck */
         }
         break;
//-
/// List readers
      case MU:                      /* Msg_ListReaders */
         highestmsg = 0;
         List_Readers();
         break;
//-
/// Newscan
      case MN: highestmsg = 0;
               b = LAST;
               Write_Pointers(LAST);
               AFPrintf(&UserDat, sout, SA[1213]);
               spos = 17;

               while (TRUE)
               {
                  tempchar = 0;

                  if (LastSearchArea >= HighSearchArea)
                  {
                     AFPrintf(&UserDat, sout, SA[1214]);
                     tempchar = 1;
                     LastSearchArea = 0;
                     break;
                  }

                  LAST = SearchAreas[LastSearchArea];

                  if (sigarray && !CheckForSig(LAST, sigarray, numsigareas))
                  {
                     LastSearchArea++;
                     continue;
                  }

                  if (_EnterArea(LAST, MSGLOCK | WRITELOCK, 0) == -1)
                  {
                     LastSearchArea++;
                     continue;
                  }

                  LeaveArea(LAST, MSGLOCK | WRITELOCK);

                  if (Read_Pointers(LAST))
                  {
                     LastSearchArea++;
                     continue;
                  }

                  High_Message = -1;
                  Low_Message = -1;
                  Find_Pointers(LAST, RStruct.Name);
                  ASPrintf(&UserDat, log, SA[1215], LAST);
                  spos += strlen(log);

                  if (UserDat.Ansi_Flag & ANSI_COLOR) spos -= 10;

                  if (spos > (UserDat.Screen_Width - 1))
                  {
                     AFPrintf(&UserDat, sout, SA[1216]);
                     spos = 17 + strlen(log);
                     if (UserDat.Ansi_Flag & ANSI_COLOR) spos -= 10;
                  }

                  AFPrintf(&UserDat, sout, log);

                  if (ReadChar(0) == 3)
                  {
                     AFPrintf(&UserDat, sout, SA[1217]);
                     LeaveArea(LAST, MSGLOCK | WRITELOCK);
                     tempchar = 1;
                     break;
                  }

                  if (Log.High_Mess >= High_Message || (MsgArea.Flag & ECHO_AREA && High_Message == 1L) || (!(MsgArea.Flag & ECHO_AREA) && High_Message == 0))
                  {
                     LeaveArea(LAST, MSGLOCK | WRITELOCK);
                     LastSearchArea++;
                     continue;
                  }

                  LastSearchArea++;
                  break;
            }

         if (tempchar)
            {
               LAST = b;

               if (Read_Pointers(LAST))
                  EnterPrivateArea();
               Message = Log.High_Mess;
               High_Message = -1;
               Low_Message = -1;
               Find_Pointers(LAST, RStruct.Name);
               if (threadmode)
                  StartThread(Low_Message, High_Message, Message);
               break;
            }

         if (!INSTACK)
            {
               TwoLines();
               ReadDirection = 1;
               Menu[MGreater].status = 0;
               Menu[MLess].status = 1;
            }

         New_Area();
         break;
//-
/// Private area
      case MP:                      /* Msg_PvtArea */
         highestmsg = 0;
         Write_Pointers(LAST);
         EnterPrivateArea();
         Menu[MR].status = 0;
         break;
//-
/// Read next
      case MNext: Find_Pointers(LAST, RStruct.Name);
                  inpvalue = 0;

                  if (isdigit(STK[0]))
                  {
                     sscanf(STK, "%6ld", &inpvalue);
                     for (b = 0; isdigit(STK[b]); b++) ;
                     movmem(STK + b, STK, strlen(STK) - (b - 1));

                     if (inpvalue < Low_Message)   inpvalue = Low_Message;
                     if (inpvalue > High_Message)  inpvalue = High_Message;
                  }

                  if (TagRead && !inpvalue)
                  {
                     TagExe();
                     break;
                  }

                  if (search[0])
                  {
                     AFPrintf(&UserDat, sout, SA[1218]);
                     StartWheel();
                     WheelOn = 1;
                  }

                  if (Message < Low_Message) Message = Low_Message - 1;

                  if (threadflag != -1)
                  {
                     Message = threadflag;
                     threadflag = -1;
                  }

                  if (inpvalue)
                  {
                     highestmsg = 0;
                     Header.NextReply = 0;
                     Header.ReplyTo = 0;
                     Message = inpvalue - (long) ReadDirection;

                     if (threadmode)   StartThread(Low_Message, High_Message, Message);
                  }

                  do
                  {
                     Find_Pointers(LAST, RStruct.Name);

                     if ((Message > High_Message) && (ReadDirection == 1))
                     {
                        if (WheelOn)
                        {
                           StopWheel();
                           Clr(UserDat.Ansi_Flag);
                           WheelOn = 0;
                        }

                        if (highestmsg)
                        {
                           highestmsg = 0;
                           AFPrintf(&UserDat, sout, SA[1219]);
                           strcat(STK, "\00111;");
                           TagMode = FALSE;
                           break;
                        }
                        else
                        {
                           highestmsg = 1;
                        }

                     AFPrintf(&UserDat, sout, SA[1220]);
                     Message = High_Message;
                     TagMode = FALSE;
                     break;
                  }

                  if (ReadChar(0) == 3)
                  {
                     if (WheelOn)
                     {
                        StopWheel();
                        Clr(UserDat.Ansi_Flag);
                        WheelOn = 0;
                     }

                     AFPrintf(&UserDat, sout, SA[1221]);
                     TagMode = FALSE;
                     break;
                  }

                  if ((Message < Low_Message) && (ReadDirection == -1))
                  {
                     if (WheelOn)
                     {
                        StopWheel();
                        Clr(UserDat.Ansi_Flag);
                        WheelOn = 0;
                     }

                     AFPrintf(&UserDat, sout, SA[1222]);
                     Message = Low_Message;
                     TagMode = FALSE;
                     break;
                  }

                  if (threadmode && LAST != PVTAREA)
                     Message = (long) NextMessage(ReadDirection, Header.ReplyTo, Header.NextReply, ReadSuccess, High_Message);
                  else
                     Message += (long) ReadDirection;

                  if (Log.High_Mess < Message && (IsBaseThread() || LAST == PVTAREA) && !TagRead)
                     Log.High_Mess = Message;

                  ReadSuccess = Read_Message();

                  if (ReadSuccess == 4)   Clr(UserDat.Ansi_Flag);
                  if (ReadSuccess == 3)   TurnWheel();

               }  while ( (ReadSuccess >= 2) && (ReadSuccess <= 6));

               if (WheelOn)
               {
                  StopWheel();
                  Clr(UserDat.Ansi_Flag);
                  WheelOn = 0;
               }

               break;
//-
/// Read original
      case MMinus:                      /* Msg_ReadOrig */
         highestmsg = 0;
         if (threadflag == -1)
            threadflag = Message;
         if (!Header.ReplyTo)
            {
               AFPrintf(&UserDat, sout, SA[1223]);
               break;
            }

         Message = Header.ReplyTo;
         if (Read_Message() == 5)
            AFPrintf(&UserDat, sout, SA[1224]);
         break;
//-
/// Read reply
      case MPlus:                      /* Msg_ReadReply */
         highestmsg = 0;
         if (threadflag == -1)
            threadflag = Message;
         if (!Header.NextReply)
            {
               AFPrintf(&UserDat, sout, SA[1225]);
               break;
            }
         Message = Header.NextReply;
         if (Read_Message() == 5)
            AFPrintf(&UserDat, sout, SA[1226]);
         break;
//-
/// Read tagged
      case MT: if (TagRead)
               {
                  AFPrintf(&UserDat, sout, SA[1227]);
                  AbortTagRead();
                  break;
               }

               threadmode = FALSE;
               AFPrintf(&UserDat, sout, SA[1228]);
               Write_Pointers(LAST);
               TagLast = LAST;
               TagMessage = Message;
               TagRead = TRUE;
               strcpy(STK,"\00114;");
               break;
//-
/// Reply to
      case MR: if (!Menu[MR].status)   break;

               if ( (MsgArea.Flag & ECHO_AREA) && (Log.Priv_Flag & Enter_Priv)) Echo_Msg();

               Write_Message(1);

               if (LAST == PVTAREA)
               {
                  ASPrintf(NULL, ramfile, "USER:%s/%ld.MSG", RStruct.Name, Message);
                  UnderScore(ramfile);
               }
               else
               {
                  ASPrintf(NULL, ramfile, "MSG:%d/%ld.MSG", LAST, Message);
               }

               GetFirstStruct(ramfile, (char *) &Header, sizeof(Header));
               break;
//-
/// Re-read
      case MEx:                      /* Msg_ReRead */
         Read_Message();
         break;
//-
/// Read reverse
      case MLess:                      /* Msg_RevRead */
         highestmsg = 0;
         ReadDirection = -1;
         Menu[MGreater].status = 1;
         Menu[MLess].status = 0;
         if (Message == High_Message)
            Message++;
         if (threadmode)
            StartThread(Low_Message, High_Message, Message);
         AFPrintf(&UserDat, sout, SA[1229]);
         strcpy(RStruct.Command_Stack, ";");
         break;
//-
/// Search (filter)
      case MI:                      /* Msg_Search */
         highestmsg = 0;
         if (!INSTACK)
            {
               AFPrintf(&UserDat, sout, SA[1230]);
               AFPrintf(&UserDat, sout, SA[1231]);
            }
         DLGInput(NULL, ramfile, NULL, 40, 40, 0, SA[1232]);
         TwoLines();
         Upper(ramfile);
         if (!ramfile[0])
            {
               AFPrintf(&UserDat, sout, SA[1233]);
               search[0] = 0;
            }
         else
            {
               ASPrintf(NULL, search, "*%s*", ramfile);
               AFPrintf(&UserDat, sout, SA[1234], search);
               AFPrintf(&UserDat, sout, SA[1235]);
            }
         break;
//-
/// Select SIG
      case MS:                      /* Msg_SelectSig */
         Write_Pointers(LAST);
         SelectSIG();
         if (Sig.number       &&
             LAST != PVTAREA  &&
             !CheckForSig(LAST, sigarray, numsigareas))
            {
               InsertStack(STK, "\0011;");
               achangesig++;
            }
         break;
//-
/// Skip thread
      case MZ:                      /* Msg_SkipThread */
         if (highestmsg)
            break;
         AFPrintf(&UserDat, sout, SA[1198], SkipRest(LAST, (SHORT) Message, ReadDirection, NULL));
         strcpy(STK,"\00114;");
         Header.NextReply = 0;
         Header.ReplyTo = 0;
         break;
//-
/// Tagread
      case MPeriod:                      /* Msg_TagRead */
         if (TagMode)
            {
               TagMode = FALSE;
               AFPrintf(&UserDat, sout, SA[1236]);
            }
         else
            {
               TagMode = TRUE;
               AFPrintf(&UserDat, sout, SA[1237]);
               if (!threadmode)
                  {
                     TMenu[2].status = 0;
                     TMenu[3].status = 0;
                  }
               else
                  {
                     TMenu[2].status = 1;
                     TMenu[3].status = 1;
                  }
            }

            InsertStack(STK, "\00114;");

         break;
//-
/// Thread mode
      case MJ:                      /* Msg_ToggleThread */
         threadmode = !threadmode;
         if (threadmode)
            {
               AFPrintf(&UserDat, sout, SA[1238]);
               Header.ReplyTo = 0;
               Header.NextReply = 0;
               StartThread(Low_Message, High_Message, Message);
            }
         else
            {
               AFPrintf(&UserDat, sout, SA[1239]);
               Menu[MZ].status = 0;
               EndThread();
            }
         UserDat.ThreadMode = threadmode;
         break;
//-
/// Update pointer
      case MTicTac:                      /* Msg_UpdatePtr */
         Log.High_Mess = Message;
         AFPrintf(&UserDat, sout, SA[1240], Message);
         break;
//-
/// Write message
      case ME:
       if ((MsgArea.Flag & ECHO_AREA) && (Log.Priv_Flag & Enter_Priv) )
        Echo_Msg();
       Write_Message(0);
       break;
//-
/// Bulletin
      case MB:                      /* Msg_WriteBltn */
         if (!Menu[MB].status)
            break;
         Write_Bulletin();
         break;
//-
/// Subscribe
      case MSUB:                      /* Msg_Subscribe */
         {
            char filename[256];

            if(!Menu[MSUB].status) break;

            ASPrintf(NULL,filename,"User:%s/GlobalAreas.msg",RStruct.Name);
            UnderScore(filename);

            if(AddArea(filename,LAST))
            {
//             "Subscribed\n\n"
               AFPrintf(&UserDat,sout,SA[1276]);
               Menu[MSUB].status = 0;
               Menu[MUNSUB].status = 1;
            }
            else
            {
//               AFPrintf(&UserDat,sout,"Failed to subscribe.\n\n");
               AFPrintf(&UserDat,sout,SA[1277]);
            }
         }

         break;
//-
/// Unsubscribe
      case MUNSUB:                      /* Msg_Unsubscribe */
         {
            char filename[256];

            if(!Menu[MUNSUB].status) break;

            ASPrintf(NULL,filename,"User:%s/GlobalAreas.msg",RStruct.Name);
            UnderScore(filename);

            if(DelArea(filename,LAST))
            {
//               AFPrintf(&UserDat,sout,"Unsubscribed.\n\n");
               AFPrintf(&UserDat,sout,SA[1278]);
               Menu[MSUB].status = 1;
               Menu[MUNSUB].status = 0;
            }
            else
            {
//               AFPrintf(&UserDat,sout,"Failed to unsubscribe.\n\n");
               AFPrintf(&UserDat,sout,SA[1279]);
            }
         }
         break;
//-
///   Print message
      case MPrt:  if(Menu[MPrt].status != 1) break;
                  PrintMsg();
                  break;
//-

      default:
         return (FALSE);
         break;
   }

   return (TRUE);
}


