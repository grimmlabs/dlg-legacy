#include <string.h>

#include <dlg/log.h>
#include <dlg/msg.h>
#include <dlg/user.h>

#include <link/io.h>

#include <pragmas/dlg.h>

#include <proto/dlg.h>
#include <proto/dos.h>

#include "Mess.h"


void                Edit_Signature(void)
{
   char                filename[128];
   char                inp[2];
   char                stype[6];

   AFPrintf(&UserDat, sout, SA[1147]);

   AFPrintf(&UserDat, sout, SA[1148]);
   DLGInput(NULL, inp, NULL, 1, 1, 1, "");
   TwoLines();

   switch (inp[0])
      {
      case 'N':
         strcpy(stype, "msg");
         break;
      case 'A':
         strcpy(stype, "alias");
         break;
      case 'U':
         strcpy(stype, "uucp");
         break;
      case 'F':
         strcpy(stype, "file");
         break;
      default:
         return;
         break;
      }

   AFPrintf(&UserDat, sout, SA[1149]);
   ASPrintf(NULL, filename, "USER:%s/Signature.%s", RStruct.Name, stype);
   UnderScore(filename);

   if (finput(0, SA[1150]))
      DeleteFile(filename);
   TwoLines();

   if (CallEditor(NULL, NULL, filename, SIGNATURE, &UserDat, &RStruct, Ext))
      TwoLines();

   WriteLog(EDITED_SIGNATURE, RStruct.Name, Ext, "");
}

