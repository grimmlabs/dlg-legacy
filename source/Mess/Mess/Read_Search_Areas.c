#include <exec/types.h>

#include <dlg/user.h>

#include <link/io.h>

#include <pragmas/dlg.h>

#include <proto/dlg.h>

#include "Mess.h"

extern   SHORT                      HighSearchArea;

SHORT               SearchAreas[1024];

void                Read_Search_Areas(void)
{
   ULONG               size            =  0;
   char                filename[256];

   ASPrintf(NULL, filename, "USER:%s/GlobalAreas.msg", RStruct.Name);
   UnderScore(filename);

   FileSize(filename, &size);
   HighSearchArea = size / 2;

   if (HighSearchArea > 1024) HighSearchArea = 1024;

   GetFirstStruct(filename,(char *)&SearchAreas,size);
   return;
}

