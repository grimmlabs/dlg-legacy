/// includes
#include <stdlib.h>
#include <string.h>
#include <ctype.h>

#include <dlg/dlg.h>
#include <dlg/menu.h>
#include <dlg/misc.h>
#include <dlg/msg.h>
#include <dlg/portconfig.h>
#include <dlg/resman.h>
#include <dlg/user.h>

#include <Link/Area.h>
#include <Link/Config.h>
#include <Link/io.h>

#include <pragmas/dlg.h>

#include <proto/dlg.h>
#include <proto/dos.h>
#include <proto/exec.h>

#include "Mess.h"
#include "Menu.h"
#include "Action.h"
//-

#include <private/Version.h>
#define  ObjRev "8"
const UBYTE version[]="\0$VER: Mess " BUILDVER "." ObjRev " �" COPYRIGHT " by Digerati Dreams "__AMIGADATE__;

/// local functions
void                main(int, char **);
BOOL                MsgSubstitute(char *cstr, char *result);
void                _CXBRK(void);
//-
/// extern functions
extern   void  EnterPrivateArea(void);
extern   BYTE  Find_Pointers(long, char *);
extern   int   HandleBuiltIn(UBYTE);
extern   BYTE  Get_Alias(char *, char *);
extern   BOOL  MsgSubstitute(char *cstr, char *result);
extern   void  New_Area(void);
extern   BYTE  Read_Pointers(long);
extern   void  Read_Search_Areas(void);
extern   void  Write_Pointers(long);
//-
/// local vars
BOOL                       contread       =  FALSE;
BOOL                       EnterFlag      =  FALSE;
BOOL                       KilledFlag     =  FALSE;
BOOL                       shortmenuflag  =  FALSE;
BOOL                       sigrestoreflag =  FALSE;
BOOL                       TagMode        =  FALSE;
BOOL                       TagRead        =  FALSE;

BPTR                       sout           =  NULL;

BYTE                       signumber      =  0;          // What SIG are we in?

char                       areafile[64];                 // Holds the name of the file we're loading for areas
char                       bodyfile[13];                 // Name of body file
char                       Ext[5];                       // The "Txx" port
char                       headerfile[13];               // Name of header file
char                       highestmsg     =  0;
char                       LPswd[7]       =  "";         // Password that will be used to lock area
char                     **SA             =  NULL;       // The language string array
char                       savemore;                     // "More" flag for user
char                       search[80];
char                       tagfile[14];                  // Name of tags file
char                       threadmode     =  0;          // Threading mode user chose
char                       WheelOn        =  0;

long                       High_Message   = -1;
long                       Low_Message    = -1;
long                       Message;
long                       netarea;
long                       numsigareas;                  // Number of areas in the SIG loaded (Area.BBS counts)
long                       TagMessage;

SHORT                      globaldestzone;
SHORT                      globalorigzone;
SHORT                      globalpoint;
SHORT                      globaltopt;
SHORT                      HighSearchArea =  0;
SHORT                      ReadDirection  =  1;
SHORT                     *sigarray       =  NULL;       // Array that holds number of areas we have in SIG (areas.bbs counts)
SHORT                      TagLast;

struct   fido              FidoNet;
struct   Global_Settings   Globals;
struct   Library          *DLGBase        =  NULL;
struct   Msg_Area          MsgArea;
struct   Msg_Header        Header;
struct   Msg_Log           Log;
struct   Ram_File          RStruct;
struct   SIG_Def           Sig;
struct   USER_DATA         UserDat;

UBYTE                      PvtSendLevel   =  0;          // What level must the user be to send PVT mail?
UBYTE                      saveansi;                     // ANSI flag for user
UBYTE                      sigrestore;                   // What SIG to put the user back into after startup.

USHORT                     bpos;

/// %switch defs -- for MsgSubstitute
#define NUMELEMENTS 20
char Msg_Trans[NUMELEMENTS][21] =
{
   {"Msg_MsgNumber"},   // 0
   {"Msg_AreaNumber"},  // 1
   {"Msg_AreaName"},    // 2
   {"Msg_Direction"},   // 3
   {"Msg_ThreadMode"},  // 4
   {"Msg_TagRead"},     // 5
   {"Msg_LowMsg"},      // 6
   {"Msg_HighMsg"},     // 7
   {"Msg_SigNumber"},   // 8
   {"Msg_SigName"},     // 9
   {"Msg_UserHighMsg"}, // 10
   {"Msg_AreaType"},    // 11
   {"Msg_Zone"},        // 12
   {"Msg_Net"},         // 13
   {"Msg_Node"},        // 14
   {"Msg_From"},        // 15
   {"Msg_To"},          // 16
   {"Msg_Point"},       // 17
   {"Msg_UFrom"},       // 18
   {"Msg_UTo"}          // 19
};
//-

//-


/// main
void                main(int argc, char **argv)
{
   BOOL                 SetSigFlag     =  FALSE;      // Are we using a SIG? FALSE = not using one initially.
   BOOL                 sigforce       =  FALSE;      // Were we forced into a SIG at startup?

   char                 exitflag       =  FALSE;
   char                *FStack         =  NULL;
   char                 MenuName[50]   = "MSG_Main";  // The name of the menu file we're using

   long                 ForceArea      =  0;

   struct   LangStruct *ls             =  NULL;

   sout = Output();

   if (!(DLGBase = OpenLibrary(DLGNAME, DLGVERSION)))
      exit(5);

/// Parse args
   while (--argc > 0)
   {
      char                *s;

      s = *++argv;

      if (*s++ == '-')
      {
         while (*s)
         {
            switch (tolower(*s))
            {
               // Menu name
               case 'm':   if (!--argc)   break;
                           strcpy(MenuName, *++argv);
                           break;

               // Sig
               case 's':   if (!--argc)   break;
                           SetSigFlag = TRUE;
                           signumber = (BYTE)atoi(*++argv);
                           break;

               // Force the SIG on the user
               case 'f':   sigforce++; Menu[MS].status = 0; break;

               // Dump user into an area
               case 'a':   if (!--argc)   break;
                           ForceArea = atol(*++argv);
                           break;

               // Level required to send private mail.
               case 'p':   if (!--argc)   break;
                           PvtSendLevel = atoi(*++argv);
                           break;

               // Command stack
               case 'c':   if (!--argc)   break;
                           FStack = strdup(*argv);
                           break;
            }

            s++;
         }
      }
   }
//-

   if (GetDevName(Ext) == -1)            _CXBRK();
   if (ReadGlobals(Ext, &Globals) == -1) _CXBRK();
   if (!(ls = GetLang(Ext)))             _CXBRK();

   SA = ls->strings;

   BCPend(Ext);

   if (!ReadUser(&RStruct, &UserDat, Ext))
   {
      AFPrintf(NULL, sout, SA[1079]);
     _CXBRK();
   }

   sigrestore = UserDat.msgsig;
   Sig.number = (SetSigFlag) ? signumber : UserDat.msgsig;

   if (!sigforce && SetSigFlag)  UserDat.msgsig = signumber;
   if (!SetSigFlag)              signumber = UserDat.msgsig;

///  Set area file name to areas.bbs
   if (!Sig.number)
   {
      strcpy(areafile, "MSG:Area.bbs");
   }
//-
///  Unless a SIG is specified, in which case, find out SIG and select IT to load
   else
   {
///    If we can't get SIG, select Areas.bbs anyway
      if (GetStruct("DLGCONFIG:SIGS/SIGS.msg", (char *) &Sig, sizeof(Sig), 1) == -1 || Sig.level > LEVEL)
      {
         if (!sigforce) UserDat.msgsig = 0;

         signumber = 0;
         strcpy(areafile, "MSG:Area.bbs");
         numsigareas = MakeSigArray(areafile, &sigarray, 0);
      }
//-
///    Otherwise, select the SIG file
      else
      {
         ASPrintf(NULL, areafile, "DLGCONFIG:SIGS/%s.msg", Sig.name);
         numsigareas = MakeSigArray(areafile, &sigarray, 0);
      }
//-
   }
//-

   if (ForceArea) LAST = ForceArea;
   if (FStack)    InsertStack(STK, FStack);

   saveansi = UserDat.Ansi_Flag;
   savemore = UserDat.More_Flag;
   threadmode = UserDat.ThreadMode;

   ASPrintf(NULL, tagfile, "T:%s.Tagged", Ext);
   ASPrintf(NULL, headerfile, "T:%s.Header", Ext);
   ASPrintf(NULL, bodyfile, "T:%s.Body", Ext);

   Read_Search_Areas();

   if (HighSearchArea > 0)       Menu[MN].status = 1;
   if (UserDat.Bulletin_Access)  Menu[MB].status = 1;

   strcpy(Header.To, RStruct.Name);
   ASPrintf(NULL, LPswd, "%sMSG", Ext);

   if (GetFirstStruct("dlgconfig:port/FidoNet.Settings", (char *) &FidoNet, sizeof(FidoNet)) == -1)
      FidoNet.net = 0;

   if (RStruct.Command_Stack[0] == '~')
   {
      movmem(STK + 1, STK, strlen(STK));
   }
   else
   {
      Find_Pointers(PVTAREA, RStruct.Name);
      Read_Pointers(PVTAREA);

      if (Log.High_Mess < High_Message && STK[0] != 'p' && STK[0] != 'P')
      {
         AFPrintf(&UserDat, sout, SA[1080]);
         AFPrintf(&UserDat, sout, SA[1081]);

         if (INSTACK)
         {
            AFPrintf(&UserDat, sout, SA[1082], STK);
            STK[0] = '\0';
         }

         LAST = PVTAREA;
      }
   }

   if (Read_Pointers(LAST))   LAST = PVTAREA;

   if (LAST == PVTAREA)
      EnterPrivateArea();
   else
      New_Area();

   while (!exitflag)
   {
      SHORT               hlev;
      char                TempMenuName[50];

      Chk_Abort();

      hlev = UserDat.Help_Level;

      if ((hlev == NOVICE) && shortmenuflag) hlev = INTERMEDIATE;

      strcpy(TempMenuName, MenuName);

      if (Exists(tagfile)) Menu[MT].status = TRUE;

      if (MenuInput(MenuName, Ext, "MSG", Menu, BUILTINCMDS, HandleBuiltIn, &UserDat, &RStruct, hlev,
                       (int (*)()) MsgSubstitute) == MENUNOTFOUND)
        _CXBRK();

      if (Stricmp(MenuName, TempMenuName))   shortmenuflag = FALSE;
   }

   if(FStack)  free(FStack);
}
//-
/// _CXBRK
void _CXBRK(void)
{
   if (contread)
   {
      UserDat.More_Flag = savemore;
      UserDat.Ansi_Flag = saveansi;
   }

   if (EnterFlag) LeaveArea(netarea, MSGLOCK | WRITELOCK);

   Write_Pointers(LAST);
   LogOut(&RStruct, &UserDat, Ext, "Mess");

   DeleteFile(bodyfile);
   DeleteFile(headerfile);

   if (sigrestoreflag)  UserDat.msgsig = sigrestore;

   CloseLibrary(DLGBase);
   exit(0);
}
//-
/// MsgSubstitute
BOOL                MsgSubstitute(char *cstr, char *result)
{
   char               *s;

   *result = 0;
   s = (char *) DLGSearch((char *) Msg_Trans, cstr, 21, 21, NUMELEMENTS);
   if (!s)
      return (FALSE);

   switch ((char) (((long) s - (long) Msg_Trans) / 21L) + 1)
      {
      case 1:
         ASPrintf(NULL, result, "%ld", Message);
         break;

      case 2:
         if (UserDat.Last_Area != PVTAREA)
            ASPrintf(NULL, result, SA[1241], UserDat.Last_Area);
         else
            strcpy(result, SA[1242]);
         break;

      case 3:
         if (UserDat.Last_Area != PVTAREA)
            ASPrintf(NULL, result, SA[1243], MsgArea.Name);
         else
            strcpy(result, SA[1244]);
         break;

      case 4:
         if (ReadDirection == 1)
            ASPrintf(NULL, result, SA[1245], SA[1246]);
         else
            ASPrintf(NULL, result, SA[1247], SA[1248]);
         break;

      case 5:
         if (threadmode)
            ASPrintf(NULL, result, SA[1249], SA[1250]);
         else
            ASPrintf(NULL, result, SA[1251], SA[1252]);
         break;

      case 6:
         if (TagRead)
            ASPrintf(NULL, result, SA[1253], SA[1254]);
         else
            ASPrintf(NULL, result, SA[1255], SA[1256]);
         break;
      case 7:
         ASPrintf(NULL, result, SA[1257], Low_Message);
         break;
      case 8:
         ASPrintf(NULL, result, SA[1258], High_Message);
         break;
      case 9:
         ASPrintf(NULL, result, SA[1259], Sig.number);
         break;

      case 10:
         if (Sig.number)
            ASPrintf(NULL, result, SA[1260], Sig.shortname);
         else
            strcpy(result, SA[1261]);
         break;
      case 11:
         ASPrintf(NULL, result, SA[1262], Log.High_Mess);
         break;

      case 12:
         if (UserDat.Last_Area == PVTAREA)
            {
               strcpy(result, SA[1263]);
               break;
            }

         if (MsgArea.Flag & ECHO_AREA)
            {
               strcpy(result, SA[1264]);
               break;
            }

         if (MsgArea.Flag & NEWSGROUP_AREA)
            {
               strcpy(result, SA[1265]);
               break;
            }

         if (MsgArea.Flag & NETMAIL_AREA)
            {
               strcpy(result, SA[1266]);
               break;
            }

         strcpy(result, SA[1267]);
         break;

      case 13:
         ASPrintf(NULL, result, SA[1268], globalorigzone);
         break;
      case 14:
         ASPrintf(NULL, result, SA[1269], Header.OrigNet);
         break;
      case 15:
         ASPrintf(NULL, result, SA[1270], Header.OrigNode);
         break;
      case 16:
         ASPrintf(NULL, result, SA[1271], Header.From);
         break;
      case 17:
         ASPrintf(NULL, result, SA[1272], Header.To);
         break;
      case 18:
         ASPrintf(NULL, result, SA[1273], globalpoint);
         break;
      case 19:
         ASPrintf(NULL, result, SA[1274], Header.From);
         UnderScore(result);
         break;
      case 20:
         ASPrintf(NULL, result, SA[1275], Header.To);
         UnderScore(result);
         break;

      default:
         return (FALSE);
      }

   return (TRUE);
}
//-

