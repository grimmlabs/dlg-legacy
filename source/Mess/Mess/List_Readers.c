#include <exec/types.h>

#include <dlg/msg.h>
#include <dlg/resman.h>
#include <dlg/user.h>

#include <link/io.h>

#include <pragmas/dlg.h>

#include <proto/dlg.h>
#include <proto/dos.h>

#include "Mess.h"

BYTE                List_Readers(void)
{
   BPTR                fh              =  NULL;
   char                filename[128];
   char                outbuffer[128];
   struct Msg_Log      Reader;

   SHORT               retval;
   SHORT               moreon          =  1;
   USHORT              pos             =  4;

   if (LAST == PVTAREA)
   {
      AFPrintf(&UserDat, sout, SA[1166]);
      return (0);
   }

   ASPrintf(NULL, filename, "MSG:%d/User.Msg", LAST);
   BorrowArea(LAST, LPswd, "7", 64, MSGLOCK | WRITELOCK);

   if (!(fh = Open(filename, MODE_OLDFILE )))
   {
      AFPrintf(&UserDat, sout, SA[1167], filename);
      FreeArea(LAST, LPswd, MSGLOCK | WRITELOCK);
      return (1);
   }

   AFPrintf(&UserDat, sout, SA[1168], MsgArea.Name);
   Draw_Line(UserDat.Screen_Width);

   while ((Read(fh, &Reader, sizeof(struct Msg_Log) )))
   {
      Capitalize(Reader.Name);
      ASPrintf(&UserDat, outbuffer, SA[1169], Reader.Name, Reader.High_Mess);

      retval = DisplayBuffer(sout, outbuffer, UserDat.Screen_Width - 1, UserDat.Screen_Len, (UserDat.More_Flag && moreon), UserDat.Ansi_Flag, &pos, 0, NULL, 1, "\x03");

      if (retval == -2)
         moreon = 0;
      else
         if (retval) break;
   }

   OneLine();
   Close(fh);  fh = NULL;
   FreeArea(LAST, LPswd, MSGLOCK | WRITELOCK);
}

