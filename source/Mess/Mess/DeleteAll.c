#include <dlg/msg.h>
#include <dlg/user.h>

#include <link/io.h>

#include <pragmas/dlg.h>

#include <proto/dlg.h>
#include <proto/dos.h>

#include "Mess.h"

extern   BYTE  Find_Pointers(long area, char *name);

void                DeleteAll(void)
{
   char                delfile[128];
   long                a;

   AFPrintf(&UserDat, sout, SA[1192]);

   for (a = 0; a <= High_Message; a++)
      {
         ASPrintf(NULL, delfile, "USER:%s/%ld.msg", RStruct.Name, a);
         UnderScore(delfile);
         DeleteFile(delfile);
      }

   ASPrintf(NULL, delfile, "USER:%s/Pointers.msg", RStruct.Name);
   UnderScore(delfile);
   DeleteFile(delfile);

   AFPrintf(&UserDat, sout, SA[1193]);
   Find_Pointers(PVTAREA, RStruct.Name);
   Message = 0;
   return;
}

