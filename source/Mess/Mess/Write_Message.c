#include <exec/types.h>
#include <stdlib.h>
#include <string.h>

#include <dlg/cron.h>
#include <dlg/misc.h>
#include <dlg/msg.h>
#include <dlg/portconfig.h>
#include <dlg/resman.h>
#include <dlg/user.h>

#include <link/area.h>
#include <link/io.h>
#include <link/msg.h>

#include <pragmas/dlg.h>

#include <proto/dlg.h>
#include <proto/dos.h>
#include <proto/exec.h>

#include "Mess.h"

extern   BOOL                       EnterFlag;

extern   char                       crash;
extern   char                       headerfile[];
extern   char                       bodyfile[];
extern   char                       highestmsg;

extern   long                       netarea;

extern   SHORT                      globalorigzone;
extern   SHORT                      globaldestzone;
extern   SHORT                      globalpoint;

extern   struct   fido              FidoNet;
extern   struct   Global_Settings   Globals;

extern   UBYTE                      PvtSendLevel;

char                       crash          =  0;
struct   Library          *NodelistBase   =  NULL;

/// Message types
char                MsgType[7][16] =
{
 "Private",
 "Public",
 "Private Netmail",
 "Public Netmail",
 "EchoMail",
 "UseNet",
 "NewsGroup"
};
//-

extern   BYTE  Find_Pointers(long area, char *name);
extern   BYTE  Get_Alias(char *, char *);
extern   BOOL  Read_Header(LONG Area, LONG MsgNum, struct Msg_Header *mh);
extern   BOOL  Write_Header(LONG Area, LONG MsgNum, struct Msg_Header mh);

SHORT CheckNode(SHORT zone, SHORT net, SHORT node);
long  FindNetArea(struct Msg_Area * msgarea);
SHORT GetLine(BPTR fh, char *line, SHORT max);
void  NoAbility(void);
BOOL  PvtOK(char *to);
BOOL  UUGetToLine(char *filename, char *retstr);
BYTE  Write_Message(SHORT mode);

/// Write_Message()
BYTE                Write_Message(SHORT mode)   /* mode: 0 - Write message
                                                 *       1 - Reply Message */
{
 char                privateflag = 0;
 char                netflag = 0;
 char                uuflag = 0;
   char                localflag = 0;
   char                newsflag = 0;
   char                existsflag;
   char                zflag;
   char                msgtype;
   char                signature[128];
   char                tempname[36];
   char                alias[36];
   char                restr[4];
   char                temptitle[74];
   char                repfile[54];
   char                mailtype[2];
   char                uutostr[256];
   char                callstring[80];

   SHORT               cost = 0;
   SHORT               retval;

   struct fido          fn;
   struct Msg_Area      msgarea;
   struct MsgStruct     ms;
   struct Msg_Header    repheader;

   ULONG               size;

   unsigned char      *bodybuf;


   fn.zone = FidoNet.zone;
   fn.net  = FidoNet.net;
   fn.node = FidoNet.node;
   fn.point= FidoNet.point;

   EnterFlag = FALSE;

   movmem(&Header, &repheader, sizeof(struct Msg_Header));

// if reply AND in private area AND it's a private message
 if (mode && (LAST == PVTAREA) && (Header.TimesRead == PRI_MSG))
  localflag++;

// if a reply AND in private area AND it's a UUCP message
   if (mode && (LAST == PVTAREA) && (Header.TimesRead == UUCP_MSG))
   {
//    if user has UUCP access set uuflag
      if (UserDat.UUCP & USENET_ACCESS)
      {
         uuflag++;
      }
      else  // user can't reply to UUCP message -- return
      {
               AFPrintf(&UserDat, sout, SA[1099]);
               return (-1);
      }
   }

// if a reply AND in private area AND it's a netmail message AND user has no access to UUCP
   if (mode && (LAST == PVTAREA) && (Header.TimesRead == PRINET_MSG) && !uuflag)
      netflag++;

// if a netmail area ...
   if (MsgArea.Flag & NETMAIL_AREA)
   {
//    if user doesn't have access to netmail ...
      if (!UserDat.NetMail)
      {
         AFPrintf(&UserDat, sout, SA[1100]);
         return (-1);
      }

      netflag++;
   }

// if in a USENET area and we're NOT in a private area ...
   if ((MsgArea.Flag & NEWSGROUP_AREA) && (LAST != PVTAREA))
      newsflag++;

   if ((LAST == PVTAREA) && !netflag && !uuflag && !localflag && (UserDat.NetMail & 1 || UserDat.UUCP & USENET_ACCESS))
   {
      if (FidoNet.net != 0 || UserDat.UUCP & USENET_ACCESS)
      {

//       Prompt for send type
         AFPrintf(&UserDat, sout, SA[1101]);

         if (FidoNet.net && UserDat.NetMail & 1)   AFPrintf(&UserDat, sout, SA[1102]);
         if (UserDat.UUCP & USENET_ACCESS)         AFPrintf(&UserDat, sout, SA[1103]);

         if (!DLGInput(NULL, mailtype, NULL, 1, 254, 1, "=> "))   AFPrintf(NULL, sout, "L");

         if (mailtype[0] == 'F' && UserDat.NetMail & 1)
            netflag++;
         else
            if (mailtype[0] == 'U' && UserDat.UUCP & USENET_ACCESS)
               uuflag++;
            else
               localflag++;

         TwoLines();
      }
   }

/// figure out name of reply file
   if (LAST == PVTAREA)
   {
      ASPrintf(NULL, repfile, "USER:%s/%ld.msg", RStruct.Name, Message);
      UnderScore(repfile);
   }
   else
      ASPrintf(NULL, repfile, "MSG:%d/%ld.msg", LAST, Message);
//-

   if (uuflag) msgtype = UUCP_MSG;

/// if we're writing news ...
   if (newsflag)
   {
      long                result;

      if (!(Log.Priv_Flag & 1))
      {
         NoAbility();
         return (-1);
      }

      WriteRam(&RStruct, Ext);

      if (mode)
      {
         ASPrintf(NULL, callstring, "DLG:DLGUuSend %s -n -g %s -r %s",
                                    "DLGUuSend",
                                    MsgArea.origin,
                                    repfile);

         result = OverlayProgram(callstring);
      }
      else
      {
         ASPrintf(NULL, callstring, "DLG:DLGUuSend %s -n -g %s",
                                    "DLGUuSend",
                                    MsgArea.origin);

         result = OverlayProgram(callstring);
      }

      if (!result)
         CronEvent(ADDEVENT, 0, "DLG:UNet2DLG -k");

      ReadRam(&RStruct, Ext);
      return (0);
   }
//-
/// if we're replying to UUCP email, get the name of the person the email is TO.
   if (uuflag && mode)
   {
      char                tempstr[255];

      UUGetToLine(repfile, tempstr);
      ASPrintf(NULL, uutostr, "%c%s%c", 34, tempstr, 34);
   }
//-

// if we're writing UUCP mail, and not in PVTAREA, it's news
   if (uuflag & LAST != PVTAREA) msgtype = NEWS_MSG;

/// do for uustuff -- reply
   if (uuflag)
   {
      if (!(Log.Priv_Flag & 1))
      {
         NoAbility();
         return (-1);
      }

      WriteRam(&RStruct, Ext);

      if (mode)
      {
         ASPrintf(NULL, callstring, "DLG:DLGUuSend -m -t %s -r %s", uutostr, repfile);
         OverlayProgram(callstring);
      }
      else
         OverlayProgram("DLG:DLGUUSend -m");

      ReadRam(&RStruct, Ext);
      return (0);
   }
//-

   Header.Attribute = Local;

   if (netflag)   Header.Attribute |= (FidoNet.flags & ~IGNORE_CM);

   msgtype = (MsgArea.Flag & ECHO_AREA) ? ECHO_MSG : PUB_MSG;

   if (!(Log.Priv_Flag & 1))
   {
      AFPrintf(&UserDat, sout, SA[1104]);
      zflag = finput(0, SA[1105]);
      TwoLines();

      if (!zflag) return (1);
      privateflag = 1;
      msgtype = PRI_MSG;
   }

   if (netflag && !FidoNet.net)
   {
      AFPrintf(&UserDat, sout, SA[1106]);
      return (-1);
   }

   if (LAST != PVTAREA && !privateflag)
   {
      if (finput(netflag ? 1 : 0, SA[1107]))
      {
         if (!INSTACK)  AFPrintf(&UserDat, sout, SA[1108]);

         msgtype = PRI_MSG;

         if (netflag)
         {
            Header.Attribute |= Private;
            msgtype = PRINET_MSG;
            privateflag = 0;
         }
         else
         {
            privateflag = 1;
         }
      }
      else
      {
         if (!INSTACK)  AFPrintf(&UserDat, sout, SA[1109]);
         AFPrintf(&UserDat, sout, SA[1110], High_Message + 1, MsgArea.Name);
      }
   }
   else
   {
      if (LAST == PVTAREA)
      {
         if (!netflag)
         {
            AFPrintf(&UserDat, sout, SA[1111]);
            msgtype = PRI_MSG;
            privateflag = 1;
         }
         else
         {
            Header.Attribute |= Private;
            msgtype = PRINET_MSG;
            privateflag = 0;
         }
      }
   }

   // Get who it's to
   if (!mode)
   {
                     // Message to:
      if (!INSTACK)  AFPrintf(&UserDat, sout, SA[1112]);

      if (DLGInput((char *) 1L, Header.To, (privateflag) ? NULL : "All", 35, 35, 3, "") == 0)
      {
         AFPrintf(&UserDat, sout, SA[1113]);
         movmem(&repheader, &Header, sizeof(repheader));
         return (1);
      }

      Capitalize(Header.To);

      if (privateflag && !CheckUser(Header.To))
      {
         AFPrintf(&UserDat, sout, SA[1114]);
         movmem(&repheader, &Header, sizeof(repheader));
         return (-1);
      }

      AFPrintf(&UserDat, sout, SA[1115]);

      if (!PvtOK(Header.To))
      {
         movmem(&repheader, &Header, sizeof(repheader));
         return (-1);
      }

      strcpy(alias, Header.To);
   }
   else
   {
      strcpy(Header.To, Header.From);

      if (MsgArea.Flag & ECHO_AREA && privateflag)
      {
         existsflag = CheckUser(Header.To);

         if (!UserDat.NetMail && (!existsflag || !PvtOK(Header.To)))
         {
            AFPrintf(&UserDat, sout, SA[1116]);
            movmem(&repheader, &Header, sizeof(repheader));
            return (-1);
         }

         if (!existsflag)
         {
            AFPrintf(&UserDat, sout, SA[1117]);
            zflag = finput(1, "");
            TwoLines();

            if (!zflag)
            {
               movmem(&repheader, &Header, sizeof(repheader));
               return (-1);
            }
         }

         if (existsflag)
         {
            if (!UserDat.NetMail)
               zflag = 0;
            else
            {
               AFPrintf(&UserDat, sout, SA[1118]);
               zflag = finput(1, "");
               TwoLines();
            }
         }

         if (zflag)
         {
            netflag++;
            Header.Attribute |= Private;
            msgtype = PRINET_MSG;
            privateflag = 0;
         }
      }

      if (!netflag && !PvtOK(Header.To))
      {
         movmem(&repheader, &Header, sizeof(repheader));
         return (-1);
      }

      Capitalize(Header.To);

      if (MsgArea.Flag & HANDLES_AREA && LAST != PVTAREA)
         Get_Alias(Header.To, tempname);
      else
         strcpy(tempname, Header.To);

      AFPrintf(&UserDat, sout, SA[1112]);
      strcpy(alias, tempname);
      AFPrintf(&UserDat, sout, SA[1119], tempname);
   }

   // Get subject of message
   if (!mode)
   {
      if (!INSTACK)  AFPrintf(&UserDat, sout, SA[1120]);

      if (DLGInput(NULL, Header.Title, NULL, 68, 68, 0, "") == 0)
      {
         AFPrintf(&UserDat, sout, SA[1113]);
         movmem(&repheader, &Header, sizeof(repheader));
         return (1);
      }
      else
      {
         AFPrintf(&UserDat, sout, SA[1121]);
      }
   }
   else
   {
      if (!INSTACK)
      {
         AFPrintf(&UserDat, sout, SA[1122], Header.Title);
         AFPrintf(&UserDat, sout, SA[1123]);
      }

      if (DLGInput(NULL, temptitle, NULL, 68, 68, 0,"") != 0)
      {
         TwoLines();
         strcpy(Header.Title, temptitle);
      }
      else
      {
         strncpy(restr, Header.Title, 3);
         Upper(restr);

         if (strncmp(restr, "RE:", 3) != 0)
         {
            ASPrintf(NULL, temptitle, "Re: %-.64s", Header.Title);
            strcpy(Header.Title, temptitle);
         }

         AFPrintf(NULL, sout, SA[1124], Header.Title);
      }

      AFPrintf(&UserDat, sout, "%a6");
   }

   zflag = 1;
   TwoLines();

// Get addr info for netmail reply
   if (netflag && mode && (zflag = finput(1, SA[1125])))
   {
      SHORT zone;
      SHORT pnt;

      if (!GetOrigin(repfile, &zone, &Header.DestNet, &Header.DestNode, &pnt))
      {
         if (MsgArea.Flag & ECHO_AREA)
         {
            AFPrintf(&UserDat, sout, SA[1126]);
            movmem(&repheader, &Header, sizeof(repheader));
            return (1);
         }
         else
         {
            zone = FidoNet.zone;
            Header.DestNet = Header.OrigNet;
            Header.DestNode = Header.OrigNode;
            pnt = globalpoint;
         }
      }

      fn.zone = zone;
      fn.point = pnt;

      cost = CheckNode(fn.zone, Header.DestNet, Header.DestNode);

      if (msgtype != PRINET_MSG) msgtype = PUBNET_MSG;
   }

   if ((netflag && !mode) || !zflag)
   {
      char               *node_s;

      while (TRUE)
      {

         fn.zone = 0;
         fn.net = 0;
         fn.node = 0;
         fn.point = 0;

         DLGInput(NULL, tempname, NULL, 32, 255, 0, SA[1127]);

         if (!strchr(tempname, ':') && !strchr(tempname, '/') && !strchr(tempname, '.'))
         {
            fn.zone = (SHORT)atoi(tempname);
            tempname[0] = 0;
         }

         if ((node_s = strchr(tempname, ':')))
         {
            fn.zone = (SHORT)atoi(tempname);
            strcpy(tempname, ++node_s);
            fn.net = -1;
         }

         if ((node_s = strchr(tempname, '/')) || fn.net < 0)
         {
               fn.net = (SHORT)atoi(tempname);

               if (node_s)
                  strcpy(tempname, ++node_s);
               else
                  tempname[0] = 0;
         }

         fn.node = atoi(tempname);
         node_s = strchr(tempname, '.');

         if (node_s) fn.point = atoi(++node_s);

         if (!fn.zone)
         {
            DLGInput(NULL, tempname, NULL, 6, 255, 0, SA[1128]);
            fn.zone = atoi(tempname);
         }

         if (!fn.net)
         {
            DLGInput(NULL, tempname, NULL, 6, 255, 0, SA[1129]);
            fn.net = atoi(tempname);
         }

         if (!fn.node)
         {
            DLGInput(NULL, tempname, NULL, 12, 255, 0, SA[1130]);
            fn.node = atoi(tempname);

            if ((node_s = strchr(tempname, '.')))  fn.point = atoi(++node_s);
         }


         TwoLines();
         Header.DestNet = fn.net;
         Header.DestNode = fn.node;
         cost = CheckNode(fn.zone, Header.DestNet, Header.DestNode);

         if (cost == -1)
         {
            if (finput(0, SA[1131]))
            {
               OneLine();
               continue;
            }

            TwoLines();

            if (!(UserDat.NetMail & 2))
            {
               movmem(&repheader, &Header, sizeof(repheader));
               return (1);
            }

            if (!(finput(0, SA[1132])))
            {
               TwoLines();
               movmem(&repheader, &Header, sizeof(repheader));
               return (1);
            }
            else
            {
               TwoLines();
               cost = 0;
            }
         }

         break;
      }
   }

   if (UserDat.credit < cost)
   {
      AFPrintf(&UserDat, sout, SA[1133]);
      movmem(&repheader, &Header, sizeof(repheader));
      return (-1);
   }

   if (netflag && !UserDat.NetMail)
   {
      movmem(&repheader, &Header, sizeof(repheader));
      return (-1);
   }

   if (netflag && (LAST == PVTAREA || MsgArea.Flag & ECHO_AREA))
   {
      if (!(netarea = FindNetArea(&msgarea)))
      {
         movmem(&repheader, &Header, sizeof(repheader));
         return (-1);
      }
      else
      {
         if (_EnterArea(netarea, MSGLOCK | WRITELOCK, 1) == -1)
            {
               movmem(&repheader, &Header, sizeof(repheader));
               return (-1);
            }
            else
               EnterFlag++;
      }
   }

   if (netflag && (UserDat.NetMail & 2))
   {
      zflag = finput(0, SA[1134]);
      AFPrintf(NULL, sout, "\n");

      if (zflag)  Header.Attribute |= FFileRequest;

      if (!zflag)
      {
         zflag = finput(0, SA[1135]);
         AFPrintf(NULL, sout, "\n");

         if (zflag)  Header.Attribute |= FileAttached;
      }

      if (crash || (FidoNet.flags & IGNORE_CM))
      {
         zflag = finput(FidoNet.flags & Crash, SA[1136]);
         AFPrintf(NULL, sout, "\n");

         if (zflag)
               Header.Attribute |= Crash;
         else
               Header.Attribute &= ~Crash;
      }
      else
         Header.Attribute &= ~Crash;
   }
   else if (netflag)
      Pause();

   Header.OrigNet = FidoNet.net;
   Header.OrigNode = FidoNet.node;

   if (MsgArea.Flag & HANDLES_AREA && LAST != PVTAREA)
      ASPrintf(NULL, signature, "USER:%s/Signature.alias", RStruct.Name);
   else
      ASPrintf(NULL, signature, "USER:%s/Signature.msg", RStruct.Name);

   UnderScore(signature);

   if (privateflag)
   {
      char                pvtnote[68];

      ASPrintf(NULL, pvtnote, "DLGConfig:Text/%s.premsg", Header.To);
      UnderScore(pvtnote);

      if (Exists(pvtnote))
      {
         DispForm(pvtnote, &UserDat, &UserDat, &RStruct, Ext);

         if (!(finput(0, SA[1105])))
         {
            TwoLines();
            return (-1);
         }
      }
   }

   strcpy(Header.From, RStruct.Name);
   Capitalize(Header.From);

   Header.Date[0] = 0;
   MDate(Header.Date);

   DeleteFile(bodyfile);
   DeleteFile(headerfile);

   Header.TimesRead = msgtype;

   if (MsgArea.Flag & HANDLES_AREA && LAST != PVTAREA)
   {
      strcpy(tempname, Header.To);
      strcpy(Header.To, alias);
   }

   AddStruct(headerfile, (char *) &Header, sizeof(Header), 1);

   if (mode && MsgArea.Flag & HANDLES_AREA && LAST != PVTAREA)
   {
      struct   Msg_Header  header;

      if(Read_Header(LAST, Message, &header))
      {
         strcpy(header.From, alias);
         Write_Header(LAST, Message, header);
      }
   }

   CallMsgEditor((mode) ? repfile : NULL, headerfile, bodyfile, &MsgArea, msgtype, Ext, (privateflag + 1) & 1, &UserDat, &RStruct);

   if (mode && MsgArea.Flag & HANDLES_AREA && LAST != PVTAREA)
   {
      struct   Msg_Header  header;

      if (Read_Header(LAST, Message, &header))
      {
         strcpy(header.From, tempname);
         Write_Header(LAST, Message, header);
      }
   }

   GetFirstStruct(headerfile, (char *) &Header, sizeof(Header));
   fn.net = Header.DestNet;
   fn.node = Header.DestNode;

   if (FileSize(bodyfile, &size) == -1)
      {
         AFPrintf(&UserDat, sout, SA[1137]);
         if (EnterFlag)
            {
               EnterFlag = FALSE;
               LeaveArea(netarea, MSGLOCK | WRITELOCK);
            }

         movmem(&repheader, &Header, sizeof(Header));
         DeleteFile(headerfile);
         return (1);
      }

   if ((MsgArea.Flag & SIGNATURE_AREA) || (privateflag && !netflag) || (LAST == PVTAREA && !netflag))
      {
         char                skipflag = 0;

         if (netflag && (MsgArea.Flag & ECHO_AREA))
            {
               struct Msg_Area     msgarea;

               if (FindNetArea(&msgarea))
                  if (!(msgarea.Flag & SIGNATURE_AREA))
                     skipflag = 1;
            }

         if (!skipflag)
            size += (long) Cat(bodyfile, signature, "\015");
      }

   bodybuf = calloc(1,(size + 256));
   if (!bodybuf)
      {
         AFPrintf(&UserDat, sout, SA[1139]);
         if (EnterFlag)
            {
               EnterFlag = FALSE;
               LeaveArea(netarea, MSGLOCK | WRITELOCK);
            }

         return (1);
      }
   GetFirstStruct(bodyfile, bodybuf, size);
   bodybuf[size] = 0;

   if (MsgArea.Flag & HANDLES_AREA && LAST != PVTAREA)
      strcpy(Header.To, tempname);

   AFPrintf(&UserDat, sout, SA[1138], MsgType[msgtype], (MsgArea.Flag & HANDLES_AREA && LAST != PVTAREA) ? alias : Header.To);

   ms.header = &Header;
   ms.repheader = NULL;
   ms.body = bodybuf;
   ms.replyto = 0;
   ms.areainfo = &MsgArea;
   ms.flags = 0;
   retval = 0;

   if (privateflag)
      retval = SendPrivateMsg(&Header, bodybuf, msgtype, LPswd, Ext);
   else if (netflag)
      {
         if (!SendNet(&Header, &FidoNet, &fn, bodybuf, &UserDat, cost, LPswd, Ext))
            retval = 1;
      }
   else
      {
         if (mode)
            {
               ms.repheader = &repheader;
               ms.replyto = (USHORT) Message;
            }
         retval = SendPublicMsg(&ms, &FidoNet, LPswd, Ext);
      }

   if (retval)
      AFPrintf(&UserDat, sout, SA[1140]);
   else
      AFPrintf(&UserDat, sout, SA[1141]);

   if (privateflag && retval)
      {
         if (Globals.PrivateArea)
            {
               if (ReadArea(Globals.PrivateArea, &msgarea, 0))
                  {
                     ms.areainfo = &msgarea;
                     SendRawMsg(&ms, NULL, LPswd);
                  }
            }

         zflag = finput(0, SA[1142]);
         TwoLines();
         if (zflag)
            {
               if (MsgArea.Flag & HANDLES_AREA && LAST != PVTAREA)
                  strcpy(Header.To, alias);
               Header.TimesRead = CC_MSG;
               SendRawMsg(&ms, RStruct.Name, LPswd);
            }
      }

   Find_Pointers(LAST, RStruct.Name);
   if (highestmsg == 1)
      if (Message < High_Message)
         highestmsg = 0;

   if ((MsgArea.Flag & ECHO_AREA || MsgArea.Flag & NETMAIL_AREA) && LAST != PVTAREA)
      if (Low_Message < 2)
         Low_Message = 2;

   UserDat.Messages_Entered++;

   DeleteFile(bodyfile);
   DeleteFile(headerfile);
   free(bodybuf);

   if (EnterFlag)
      {
         EnterFlag = FALSE;
         LeaveArea(netarea, MSGLOCK | WRITELOCK);
      }

   return (0);
}
//-
/// NoAbility
void                NoAbility(void)
{
   AFPrintf(&UserDat, sout, SA[1170]);
}
//-
/// UUGetToLine
BOOL  UUGetToLine(char *filename, char *retstr)
{
   BOOL                R2           =  FALSE;
   BOOL                RFound       =  FALSE;
   BPTR                fh           =  NULL;

   char                r2str[256];
   char                fmstr[256];

   SHORT               a;

   if (!(fh = Open(filename, MODE_OLDFILE)))   return (FALSE);

   Seek(fh, sizeof(struct Msg_Header), OFFSET_BEGINNING );

   while ((a = GetLine(fh, retstr, 255)) != -1)
   {
      if (!Strnicmp("Reply-To: ", retstr, 9))
      {
         R2 = TRUE;
         RFound = TRUE;
         memcpy(r2str,retstr,255);
         break;
      }

      if (!Strnicmp("From: ", retstr, 6))
      {
         if(RFound) continue;
         memcpy(fmstr,retstr,255);
         RFound = TRUE;
      }
   }

   Close(fh); fh = NULL;

   if ((a == -1) && !RFound)
   {
      strcpy(retstr, SA[1191]);
      return (FALSE);
   }

   if(R2 == TRUE)
      memcpy(retstr,r2str,255);
   else
      memcpy(retstr,fmstr,255);

   for (a = (R2?10:6); retstr[a] && a < 255; a++)
      if (retstr[a] == 34)
         retstr[a] = 32;

   movmem( (retstr + (R2?10:6)), retstr, (a - (R2?10:6)) );
   retstr[a - (R2?10:6)] = 0;
   return (TRUE);
}
//-
/// PvtOK
BOOL  PvtOK(char *to)
{
   if (PvtSendLevel > (UBYTE)LEVEL)
      if ((UBYTE)GetLevel(to) < PvtSendLevel)
         {
            AFPrintf(&UserDat, sout, SA[1201]);
            return (FALSE);
         }

   return (TRUE);
}
//-
/// CheckNode
SHORT CheckNode(SHORT zone, SHORT net, SHORT node)
{
   NodeList           *the_nodelist = NULL;
   NodeDesc           *node_desc = NULL;
   Addr                find_me;
   SHORT               cost;

   if (!(NodelistBase = OpenLibrary("traplist.library", 3L)))
      {
         AFPrintf(&UserDat, sout, SA[1180]);
         return (-1);
      }

   if (!(the_nodelist = NLOpen("Nodelist:", 0)))
      {
         CloseLibrary(NodelistBase);
         return (-1);
      }

   find_me.Zone = zone;
   find_me.Net = net;
   find_me.Node = node;
   find_me.Point = 0;

   if (node_desc = NLFind(the_nodelist, &find_me, 0))
      {
         AFPrintf(&UserDat, sout, SA[1181], node_desc->Node.Zone,
                  node_desc->Node.Net,
                  node_desc->Node.Node,
                  node_desc->Node.Point,
                  node_desc->System,
                  node_desc->City);
         AFPrintf(&UserDat, sout, SA[1182], node_desc->Sysop);
         AFPrintf(&UserDat, sout, SA[1183], node_desc->Region, node_desc->Hub);
         AFPrintf(&UserDat, sout, SA[1184], node_desc->BaudRate);
         AFPrintf(&UserDat, sout, SA[1185], node_desc->Phone);

         cost = node_desc->Cost;
         if (cost != -1)
            AFPrintf(&UserDat, sout, SA[1186], cost / 100, cost % 100);
         else
            AFPrintf(&UserDat, sout, SA[1187]);

         AFPrintf(&UserDat, sout, SA[1188], node_desc->Flags);

         if (strstr(node_desc->Flags, "CM"))
            crash = TRUE;
         else
            {
               crash = FALSE;
               AFPrintf(&UserDat, sout, SA[1189]);
            }

         AFPrintf(&UserDat, sout, "\n\n");
      }
   else
      {
         AFPrintf(&UserDat, sout, SA[1190]);
         cost = -1;
      }

   if (node_desc)
      NLFreeNode(node_desc);
   if (the_nodelist)
      NLClose(the_nodelist);
   CloseLibrary(NodelistBase);

   return (cost);
}
//-
/// FindNetArea
long  FindNetArea(struct Msg_Area * msgarea)
{
   BPTR                fh  =  NULL;

   if ((fh = Open("MSG:Area.bbs", MODE_OLDFILE )))
      {
         while (Read(fh, msgarea, sizeof(struct Msg_Area) ))
         {
            if (msgarea->Flag & NETMAIL_AREA)
            {
               Close(fh); fh = NULL;
               return (msgarea->Number);
            }
         }

         Close(fh); fh = NULL;
      }

   return (FALSE);
}
//-
/// GetLine
SHORT GetLine(BPTR fh, char *line, SHORT max)
{
   SHORT               a;

   for (a = 0; a < max - 1; a++)
   {
      if (!Read(fh, line + a, 1))   return (-1);

      if (!a)
      {
         if (line[a] == 13 || line[a] == 10 || line[a] == 141)
         {
            a--;
            continue;
         }
      }

      if (line[a] == 13)   break;
   }

   line[a] = 0;
   return (a);
}
//-

