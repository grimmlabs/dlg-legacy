#include <exec/types.h>

#include <dlg/log.h>
#include <dlg/msg.h>
#include <dlg/user.h>

#include <link/io.h>

#include <pragmas/dlg.h>

#include <proto/dlg.h>

#include "Mess.h"

extern   BOOL                       KilledFlag;


/// Kill_Message
BYTE Kill_Message(long message, long area, char *username)
{
   char                filename[128];
   char                zflag;

   if (area != PVTAREA)
   {
      ASPrintf(NULL, filename, "MSG:%ld/%ld.MSG", area, message);
   }

   else
   {
      ASPrintf(NULL, filename, "USER:%s/%ld.MSG", username, message);
      UnderScore(filename);
   }


   if (!INSTACK)
      AFPrintf(&UserDat, sout, SA[1143], message);

   zflag = finput(0, SA[1144]);
   TwoLines();

   if (!zflag)
      return (1);

   if (!KillMsg(message, area, username, LPswd))
      AFPrintf(&UserDat, sout, SA[1145]);

   else
      {
         WriteLog(KILLED_MESSAGE, RStruct.Name, Ext, filename);
         AFPrintf(&UserDat, sout, SA[1146], message);
         if (LAST == PVTAREA)
            KilledFlag = 1;
      }

   return (0);
}
//-
