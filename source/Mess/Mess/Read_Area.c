#include <string.h>

#include <dlg/msg.h>

#include <proto/dos.h>

BOOL Read_Area(long area, struct Msg_Area * msgarea, char *filename)
{
   BPTR                fh        =  NULL;
   struct Msg_Area     temparea;

   if ((fh = Open(filename, MODE_OLDFILE )))
   {
      while (Read(fh, &temparea, sizeof(struct Msg_Area) ))
      {
         if (temparea.Number == area)
         {
            movmem(&temparea, msgarea, sizeof(temparea));
            Close(fh); fh = NULL;
            return (TRUE);
         }
      }

      Close(fh); fh = NULL;
   }

   return (FALSE);
}
