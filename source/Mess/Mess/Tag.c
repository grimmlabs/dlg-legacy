#include <exec/types.h>
#include <string.h>

#include <dlg/menu.h>
#include <dlg/msg.h>
#include <dlg/resman.h>
#include <dlg/user.h>

#include <link/io.h>
#include <link/msg.h>

#include <pragmas/dlg.h>

#include <proto/dlg.h>

#include "Mess.h"
#include "Action.h"

extern   BOOL                       TagRead;

extern   char                       tagfile[];
extern   char                       threadmode;

extern   long                       TagMessage;

extern   SHORT                      TagLast;

extern   struct   NewShortMenu      Menu[];

BOOL  TagExe(void);
BOOL  AbortTagRead(void);

extern   BYTE  Find_Pointers(long area, char *name);
extern   SHORT Read_Message(void);
extern   BYTE  Read_Pointers(long area);

/// TagExe
BOOL                TagExe(void)
{
   long                area;
   long                templast;
   long                message;
   char                msgid[12];

   if (GetFirstStruct(tagfile, msgid, 11) == -1)
   {
      Menu[MT].status = FALSE;
      AFPrintf(&UserDat, sout, SA[1200]);
      AbortTagRead();
      return (FALSE);
   }

   sscanf(msgid, "%d:%d", &area, &message);

   if (area != LAST)
   {
      LeaveArea(LAST, MSGLOCK | WRITELOCK);
      templast = LAST;
      LAST = area;

      if (Read_Pointers(LAST))
      {
         LAST = templast;
         Read_Pointers(LAST);
         Find_Pointers(LAST, RStruct.Name);
         strcpy(STK, "\00114;");
         DeleteStruct(tagfile, msgid, 11, 11);
         return (TRUE);
      }

      Find_Pointers(LAST, RStruct.Name);
   }

   Message = message;

   if (Read_Message() > 1) strcpy(STK, "\00114;");

   Menu[MP].status = 1;
   Menu[MD].status = 0;

   DeleteStruct(tagfile, msgid, 11, 11);
   return (TRUE);
}
//-
/// AbortTagRead
BOOL                AbortTagRead(void)
{
   TagRead = FALSE;
   threadmode = UserDat.ThreadMode;

   LeaveArea(LAST, MSGLOCK | WRITELOCK);

   LAST = TagLast;

   if (LAST == PVTAREA) strcpy(STK, "\00113;");

   if (Read_Pointers(LAST) == 0)
   {
      Find_Pointers(LAST, RStruct.Name);
      Message = TagMessage;
      BaseThread(Message);
   }
   else
      strcpy(STK, "\00113;");

   return (TRUE);
}
//-

