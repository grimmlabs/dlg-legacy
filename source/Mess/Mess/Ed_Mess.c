#include <exec/types.h>
#include <stdlib.h>
#include <string.h>

#include <dlg/log.h>
#include <dlg/misc.h>
#include <dlg/msg.h>
#include <dlg/resman.h>
#include <dlg/user.h>
#include <dlg/version.h>

#include <link/io.h>
#include <Link/msg.h>

#include <pragmas/dlg.h>

#include <proto/dlg.h>
#include <proto/dos.h>

#include "Mess.h"

extern   char                       headerfile[];
extern   char                       bodyfile[];
extern   char                      *transarray;

extern   SHORT                      globalorigzone;
extern   SHORT                      globaldestzone;
extern   SHORT                      globalpoint;
extern   SHORT                      globaltopt;

extern   struct   fido              FidoNet;

extern   BYTE  Get_Alias(char *, char *);
extern   void  NoAbility(void);
extern   ULONG Read_Body(LONG Area, LONG MsgNum, char **Buffer, ULONG BufferSize);
extern   BOOL  Read_Header(LONG Area, LONG MsgNum, struct Msg_Header *mh);

BYTE                Ed_Mess(void)
{
   BPTR           fh             =  NULL;

   char          *dbody          =  NULL;
   char           filename[128];
   char           cdate[40];
   char           comment[128];
   char           toname[40];
   char           zflag;

   unsigned char *buffer = NULL;

   ULONG          size;


   globalorigzone = FidoNet.zone;
   globaldestzone = FidoNet.zone;
   globalpoint = 0;
   globaltopt = 0;

/* Check for ability */
   if (!(Log.Priv_Flag & Special_ReEdit) && !(Log.Priv_Flag & Re_Edit))
      {
         NoAbility();
         return (0);
      }

/* Get Message path */
   if (LAST != PVTAREA)
      ASPrintf(NULL, filename, "MSG:%ld/%ld.msg", LAST, Message);
   else
      {
         ASPrintf(NULL, filename, "USER:%s/%ld.msg", RStruct.Name, Message);
         UnderScore(filename);
      }

/* CleanUp any old head/body files */
   DeleteFile(headerfile);
   DeleteFile(bodyfile);

   if (LAST != PVTAREA)
      BorrowArea(LAST, LPswd, "5", 64, MSGLOCK | WRITELOCK);

/* Move header into the ram header file */
   if (GetFirstStruct(filename, (char *) &Header, sizeof(Header)) == -1)
      {
         FreeArea(LAST, LPswd, MSGLOCK | WRITELOCK);
         return (FALSE);
      }

   if (Header.Attribute & Sent)
      {
         zflag = finput(0, SA[1151]);
         TwoLines();
         if (!zflag)
            {
               FreeArea(LAST, LPswd, MSGLOCK | WRITELOCK);
               return (FALSE);
            }
      }

   if (Header.TimesRead == PRINET_MSG)
   {
      struct   Msg_Header  header;
      SHORT                counter;
      char                *kludge   =  NULL;

      kludge = calloc(1,256);

//      if ((fh = Open(filename, MODE_READWRITE)))
      if (kludge)
      {
         Read_Header(LAST, Message, &header);
         Read_Body(LAST,Message, &kludge, 256);

         for (counter = 0; counter < 256; counter++)
         {
            if (kludge[counter] != 0 && kludge[counter] != 141 && kludge[counter] != 10 && kludge[counter] != 13 && kludge[counter] != 1)
               break;

            if (kludge[counter] == 1)
            {
               if (!Strnicmp(kludge + counter + 1, "FMPT", 4))
                  sscanf(kludge + counter + 5, "%hd", &globalpoint);

               if (!Strnicmp(kludge + counter + 1, "TOPT", 4))
                  sscanf(kludge + counter + 5, "%hd", &globaltopt);

               if (!Strnicmp(kludge + counter + 1, "INTL", 4))
                  sscanf(kludge + counter + 5, "%hd:%hd/%hd %hd:%hd/%hd",  &globaldestzone,  &Header.DestNet,
                                                                           &Header.DestNode, &globalorigzone,
                                                                           &Header.OrigNet,  &Header.OrigNode);

               for (; kludge[counter] != 13 && counter < 256; counter++) ;
            }
         }
      }

      if(kludge)  {  free(kludge);  kludge = NULL; }
   }

   FreeArea(LAST, LPswd, MSGLOCK | WRITELOCK);

   if ((Stricmp(Header.From, RStruct.Name)) && LEVEL < 254 && !(Log.Priv_Flag & 32))
      {
         AFPrintf(&UserDat, sout, SA[1152]);
         return (FALSE);
      }

   if (MsgArea.Flag & HANDLES_AREA && LAST != PVTAREA)
      {
         strcpy(toname, Header.To);
         Get_Alias(toname, Header.To);
      }

   if (AddStruct(headerfile, (char *) &Header, sizeof(Header), 1) == -1)
      return (FALSE);

/* Copy the stripped body of the message to ram */
   if (!MakeBody(filename, bodyfile, NULL, transarray, &dbody))
      return (FALSE);

/* Call the message editor to allow the body to be edited */
   if (CallMsgEditor(NULL, headerfile, bodyfile, &MsgArea, Header.TimesRead, Ext, 0, &UserDat, &RStruct))
      {
         AFPrintf(&UserDat, sout, SA[1153]);
         DeleteFile(bodyfile);
         DeleteFile(headerfile);
         return (-1);
      }

   if (LAST != PVTAREA)
      BorrowArea(LAST, LPswd, "6", 64, MSGLOCK | WRITELOCK);

   DeleteFile(filename);

   GetFirstStruct(headerfile, (char *) &Header, sizeof(Header));

   if (MsgArea.Flag & HANDLES_AREA && LAST != PVTAREA)
      strcpy(Header.To, toname);

   if (!(fh = Open(filename, MODE_NEWFILE)))
   {
      FreeArea(LAST, LPswd, MSGLOCK | WRITELOCK);
      DeleteFile(bodyfile);
      DeleteFile(headerfile);
      return (0);
   }

   FileSize(bodyfile, &size);

   if (!(buffer = (unsigned char *) calloc(1,(size + 1) )) || GetFirstStruct(bodyfile, buffer, size) == -1)
   {
      Close(fh);
      FreeArea(LAST, LPswd, MSGLOCK | WRITELOCK);
      DeleteFile(bodyfile);
      DeleteFile(headerfile);
      return (0);
   }

   buffer[size] = '\0';

   if (MsgArea.savecharset && LAST != PVTAREA)
   {
      char                transbuf[256];
      SHORT               a;
      unsigned char       b;
      struct CharSet      set;
      BPTR                cfh;
      char                setname[128];

      set.number = MsgArea.savecharset;

      if (GetStruct("DLGConfig:CharSets/CharSets.bbs", (char *) &set, sizeof(set), 1) != -1)
      {
         ASPrintf(NULL, setname, "DLGConfig:CharSets/%s.set", set.name);

         if (cfh = Open(setname, MODE_OLDFILE))
         {
            Seek(fh, 256, OFFSET_BEGINNING);
            Read(fh, transbuf, 256);

            for (a = 0; buffer[a] != '\0'; a++)
            {
               b = transbuf[buffer[a]];
               buffer[a] = b;
            }

            for (a = 0; Header.Title[a] != '\0'; a++)
            {
               b = transbuf[Header.Title[a]];
               Header.Title[a] = b;
            }
         }
      }
   }
   else
   {
      SHORT               a;

      for (a = 0; buffer[a] != '\0'; a++)
         buffer[a] &= ~128;

      for (a = 0; Header.Title[a] != '\0'; a++)
         Header.Title[a] &= ~128;
   }

   {
      SHORT               savenet;
      SHORT               savenode;

      savenet = Header.DestNet;
      savenode = Header.DestNode;

      if (Header.TimesRead == PRINET_MSG)
      {
         if (globaldestzone != FidoNet.zone)
         {
            Header.DestNet = FidoNet.zone;
            Header.DestNode = globaldestzone;
         }
      }

      Write(fh, &Header, sizeof(struct Msg_Header));

      Header.DestNet = savenet;
      Header.DestNode = savenode;
   }

   if (Header.TimesRead == PRINET_MSG)
   {
      char                tempstring[80];

      if (globaldestzone != FidoNet.zone)
      {
         ASPrintf(NULL, tempstring, "%cINTL %d:%d/%d %d:%d/%d%c", 1, globaldestzone, Header.DestNet, Header.DestNode, globalorigzone, Header.OrigNet, Header.OrigNode, 13);
         Write(fh, tempstring, strlen(tempstring));
      }

      if (globaltopt)
      {
         ASPrintf(NULL, tempstring, "%cTOPT %d%c", 1, globaltopt, 13);
         Write(fh, tempstring, strlen(tempstring));
      }

      if (FidoNet.point)
      {
         ASPrintf(NULL, tempstring, "%cFMPT %d%c", 1, FidoNet.point, 13);
         Write(fh, tempstring, strlen(tempstring));
      }
   }

   Write(fh, buffer, size);

   if (MsgArea.Flag & ECHO_AREA && LAST != PVTAREA)
   {
      SHORT               dummy;
      SHORT               a;
      char                origin[80];
      char                temp[128];
      char                tear[255];

      if (!(MsgArea.Flag & NEWSGROUP_AREA) && strlen(MsgArea.origin))
         strcpy(origin, MsgArea.origin);
      else
         strcpy(origin, FidoNet.origin);

      ASPrintf(NULL, tear, "\x0d--- %s\x0d * Origin: ", DLGSHORTNAME);

      for (a = strlen(origin) - 1; origin[a] != '(' && a; a--) ;

      if (sscanf(origin + (a + 1), "%hd:%hd/%hd.%hd", &dummy, &dummy, &dummy, &dummy) > 2)
         strcpy(temp, origin);
      else
         ASPrintf(NULL, temp, "%s (%d:%d/%d)", origin, FidoNet.zone, FidoNet.net, FidoNet.node);

      a = strlen(temp);

      movmem(temp, tear + (17 + strlen(DLGSHORTNAME)), a);
      movmem("\x0d", tear + (17 + strlen(DLGSHORTNAME)) + a, 2);

      Write(fh, tear, strlen(tear));
   }

   Close(fh); fh = NULL;

   FreeArea(LAST, LPswd, MSGLOCK | WRITELOCK);

   WriteLog(RE_EDITED_MESSAGE, RStruct.Name, Ext, filename);
   DeleteFile(bodyfile);
   DeleteFile(headerfile);

   AFPrintf(&UserDat, sout, SA[1154]);

   MDate(cdate);
   ASPrintf(NULL, comment, "Corrected: %s, by %s", cdate, RStruct.Name);
   SetComment(filename, comment);
   return (TRUE);
}
