#define BUILTINCMDS 31

// Main menu

struct NewShortMenu Menu[BUILTINCMDS] =
{
   {"Abort", 1},           // 0
   {"MSG_ChangeArea", 1},  // 1
   {"MSG_ContRead", 1},    // 2
   {"MSG_Correct", 0},     // 3
   {"MSG_DeleteAll", 0},   // 4
   {"MSG_EditSig", 1},     // 5
   {"MSG_Forward", 0},     // 6
   {"MSG_FwdRead", 0},     // 7
   {"MSG_Kill", 0},        // 8
   {"MSG_LexCheck", 0},    // 9
   {"MSG_ListReaders", 1}, // 10
   {"MSG_NewScan", 0},     // 11
   {"MSG_PrintMsg", 0},    // 12
   {"MSG_PvtArea", 1},     // 13
   {"MSG_ReadNext", 1},    // 14
   {"MSG_ReadOrig", 0},    // 15
   {"MSG_ReadReply", 0},   // 16
   {"MSG_ReadTagged", 0},  // 17
   {"MSG_Reply", 0},       // 18
   {"MSG_ReRead", 0},      // 19
   {"MSG_RevRead", 1},     // 20
   {"MSG_Search", 1},      // 21
   {"MSG_SelectSig", 1},   // 22
   {"MSG_SkipThread", 0},  // 23
   {"MSG_Subscribe", 1},   // 24
   {"MSG_TagRead", 1},     // 25
   {"MSG_ToggleThread", 1},// 26
   {"MSG_Unsubscribe", 1}, // 27
   {"MSG_UpdatePtr", 1},   // 28
   {"MSG_Write", 1},       // 29
   {"MSG_WriteBltn", 0}    // 30
};

// Tag read menu

struct ShortMenu    TMenu[7] =
{
   {'R', "Read Msg", 1},
   {'.', "Tag Msg", 1},
   {'Z', "Skip Thread", 1},
   {'T', "Tag Thread", 1},
   {'A', "Abort", 1},
   {'\0', "Next Msg", 1},
   {'?', "Disp List", 1}
};

