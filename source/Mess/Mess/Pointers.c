#include <stdlib.h>
#include <string.h>

#include <dlg/menu.h>
#include <dlg/misc.h>
#include <dlg/msg.h>
#include <dlg/resman.h>
#include <dlg/user.h>

#include <link/io.h>
#include <link/msg.h>
#include <link/area.h>
#include <link/user.h>

#include <pragmas/dlg.h>

#include <proto/dlg.h>
#include <proto/dos.h>

#include "Mess.h"
#include "Action.h"

extern   BOOL                       KilledFlag;
extern   BOOL                       TagRead;

extern   char                       search[];
extern   char                       highestmsg;
extern   char                       areafile[]; // Holds the name of the file we're loading for areas
extern   char                       threadmode; // Threading mode user chose

extern   struct   NewShortMenu      Menu[];

char                      *transarray     =  NULL;
struct   Msg_Log           TempLog;

extern   BOOL  Read_Area(long, struct Msg_Area *, char *);

BYTE  Find_Pointers(long area, char *name);
BYTE  Read_Pointers(long area);
BOOL  RenumberDir(char *);
void  Write_Pointers(long area);

/// Write_Pointers()
void  Write_Pointers(long area)
{
   char                filename[128];
   char                path[128];

   FreeArea(area, LPswd, MSGLOCK | WRITELOCK);

   if (area != PVTAREA)
      {
         ASPrintf(NULL, filename, "MSG:%d/User.Msg", area);
         BorrowArea(area, LPswd, "1", 64, MSGLOCK | WRITELOCK);
      }
   else
      {
         ASPrintf(NULL, filename, "USER:%s/User.Msg", RStruct.Name);
         UnderScore(filename);

         if (KilledFlag)
            {
               ASPrintf(NULL, path, "USER:%s", RStruct.Name);
               UnderScore(path);

               RenumberDir(path);
               KilledFlag = 0;
            }
      }

   if (Log.High_Mess > High_Message && (IsBaseThread() || LAST == PVTAREA) && !TagRead)
      Log.High_Mess = High_Message;

   AddStruct(filename, (char *) &Log, sizeof(Log), 36);

   if (area != PVTAREA)
      {
         FreeArea(area, LPswd, MSGLOCK | WRITELOCK);
         LeaveArea(area, MSGLOCK | WRITELOCK);
      }
}
//-
/// Read_Pointers
BYTE                Read_Pointers(long area)
{
   char                filename[128];

   search[0] = 0;
   highestmsg = 0;

/// free translation table if it exists
   if (transarray)
      {
         free(transarray);
         transarray = NULL;
      }
//-

   if (area != PVTAREA)
///   do if not private area
      {
         if (!Read_Area(area, &MsgArea, (TagRead) ? "MSG:Area.bbs" : areafile))
            return (1);

         if (_EnterArea(area, MSGLOCK | WRITELOCK, 1) == -1)
            return (1);

         ASPrintf(NULL, filename, "MSG:%d/User.Msg", area);
         BorrowArea(area, LPswd, "2", 64, MSGLOCK | WRITELOCK);

         if (MsgArea.savecharset)
            {
               struct CharSet      set;
               char                setname[46];

               set.number = MsgArea.savecharset;
               if (GetStruct("DLGConfig:CharSets/CharSets.bbs", (char *) &set, sizeof(set), 1) != -1)
                  {
                     ASPrintf(NULL, setname, "DLGConfig:CharSets/%s.set", set.name);

                     if (transarray = malloc(256))
                        {
                           if (GetFirstStruct(setname, transarray, 256) == -1)
                              {
                                 free(transarray);
                                 transarray = NULL;
                              }
                        }
                  }
            }
      }
//-
   else
///   do if private area
      {
         MsgArea.AADef = 255;
         MsgArea.Flag = 0;
         MsgArea.processor[0] = 0;

         ASPrintf(NULL, filename, "USER:%s/User.Msg", RStruct.Name);
         UnderScore(filename);
      }
//-

   strcpy(TempLog.Name, RStruct.Name);
   Upper(TempLog.Name);

   if (GetStruct(filename, (char *) &TempLog, sizeof(TempLog), 36) != -1)
      {
         FreeArea(area, LPswd, MSGLOCK | WRITELOCK);
         movmem(&TempLog, &Log, sizeof(Log));

         // build the priv_flag based on all fields

         BuildPrivFlag(&MsgArea, &Log, LEVEL);

         if (LEVEL == 255)
            Log.Priv_Flag = 255;

         if (area == PVTAREA)
            {
               Log.Priv_Flag = 255;
               Menu[MU].status = 0;
            }
         else
            {
               if (MsgArea.Flag & HANDLES_AREA && !(Log.Priv_Flag & Sysop_Access))
                  Menu[MU].status = Menu[MI].status = 0;
               else
                  Menu[MU].status = Menu[MI].status = 1;
            }

         if (threadmode)
            Menu[MZ].status = 1;

         if (Log.Priv_Flag & Enter_Priv)
            Menu[ME].status = 1;
         else
            Menu[ME].status = 0;

         return (0);
      }

   FreeArea(area, LPswd, MSGLOCK | WRITELOCK);

   if ((MsgArea.Flag & 1 && LEVEL >= MsgArea.llevel && LEVEL <= MsgArea.ulevel) || area == PVTAREA || LEVEL == 255)
      {
         strcpy(Log.Name, RStruct.Name);
         Upper(Log.Name);

         Log.High_Mess = (MsgArea.Flag & NETMAIL_AREA || MsgArea.Flag & ECHO_AREA) ? 1L : 0L;
         Log.uflag = Log.dflag = Log.special = 0;

         if (area != PVTAREA)
            BorrowArea(area, LPswd, "3", 64, MSGLOCK | WRITELOCK);

         AddStruct(filename, (char *) &Log, sizeof(Log), 36);

         FreeArea(area, LPswd, MSGLOCK | WRITELOCK);

         BuildPrivFlag(&MsgArea, &Log, LEVEL);

         if (LEVEL == 255)
            Log.Priv_Flag = 255;

         if (area == PVTAREA)
            {
               Log.Priv_Flag = 255;
               Menu[MU].status = 0;
            }
         else
            Menu[MU].status = Menu[MI].status = 1;

         if (threadmode)
            Menu[MZ].status = 1;

         if (Log.Priv_Flag & Enter_Priv)
            Menu[ME].status = 1;
         else
            Menu[ME].status = 0;

         return (0);
      }

   if (area != PVTAREA)
      LeaveArea(area, MSGLOCK | WRITELOCK);
   return (1);
}
//-
/// Find_Pointers
BYTE  Find_Pointers(long area, char *name)
{
   GetHiLowPointers(area, name, &Low_Message, &High_Message, LPswd);

   if(LAST == PVTAREA)  return(0);

   if ((MsgArea.Flag & ECHO_AREA || MsgArea.Flag & NETMAIL_AREA))
      {
         if (Low_Message < 2)
            Low_Message = 2;
         if (High_Message < 1)
            High_Message = 1;
      }

   return (0);
}
//-
/// RenumberDir()
BOOL                RenumberDir(char *dir)
{
   long                Low;
   long                High;
   long                from;
   long                to;
   char                fromname[128];
   char                toname[128];
   char                shift1 = FALSE;
   char                shift2 = FALSE;

   CD(dir);
   GetHiLowPointers((USHORT)PVTAREA, RStruct.Name, &Low, &High, LPswd);

   if (Message > High)
      Message = High;
   if (Log.High_Mess > High)
      Log.High_Mess = High;

   AFPrintf(&UserDat, sout, SA[1171]);

/* Scan up to first blank fd */
   to = 0;

   do
      {
         to++;
         ASPrintf(NULL, toname, "%ld.msg", to);
         if (!shift1 && Message == to && Exists(toname))
            shift1 = TRUE;
         if (!shift2 && Log.High_Mess == to && Exists(toname))
            shift2 = TRUE;

         if (to >= High && Exists(toname))
            {
               AFPrintf(&UserDat, sout, SA[1172]);
               return (FALSE);
            }
      }
   while (Exists(toname));

/* Renumber the remaining messages and re-set comments */
   from = to;
   while (TRUE)
      {
         from++;
         if (from > High)
            {
               if (!shift1 && Message <= from)
                  Message = (Message == from) ? to : to - 1L;

               if (!shift2 && Log.High_Mess <= from)
                  Log.High_Mess = (Log.High_Mess == from) ? to : to - 1L;

               PutHiLowPointers((SHORT) PVTAREA, RStruct.Name, 1, to - 1, LPswd);
               AFPrintf(&UserDat, sout, SA[1172]);
               return (TRUE);
            }

         ASPrintf(NULL, fromname, "%ld.msg", from);

         if (Exists(fromname))
            {
               ASPrintf(NULL, toname, "%ld.msg", to);
               if (Exists(toname))
                  DeleteFile(toname);
               if (!Rename(fromname, toname))
                  {
                     AFPrintf(&UserDat, sout, SA[1173]);
                     return (FALSE);
                  }

               if (!shift1 && Message <= from)
                  {
                     Message = (Message == from) ? to : to - 1L;
                     shift1 = TRUE;
                  }

               if (!shift2 && Log.High_Mess <= from)
                  {
                     Log.High_Mess = (Log.High_Mess == from) ? to : to - 1L;
                     shift2 = TRUE;
                  }

               to++;
            }
      }

   return (FALSE);
}
//-


