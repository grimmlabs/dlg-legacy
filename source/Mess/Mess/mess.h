#define IGNORE_CM      1024
#define STK            RStruct.Command_Stack
#define INSTACK        RStruct.Command_Stack[0]
#define HOT            UserDat.Hot_Keys
#define LEVEL          UserDat.User_Level
#define LAST           UserDat.Last_Area

extern   BPTR                       sout;

extern   char                       Ext[];
extern   char                       LPswd[];
extern   char                     **SA;

extern   long                       Low_Message;
extern   long                       Message;
extern   long                       High_Message;

extern   struct   Library          *DLGBase;
extern   struct   Msg_Area          MsgArea;
extern   struct   Msg_Header        Header;
extern   struct   Msg_Log           Log;
extern   struct   Ram_File          RStruct;
extern   struct   USER_DATA         UserDat;

