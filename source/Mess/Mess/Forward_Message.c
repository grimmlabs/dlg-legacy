#include <exec/types.h>
#include <string.h>

#include <dlg/msg.h>
#include <dlg/user.h>

#include <link/io.h>

#include <pragmas/dlg.h>

#include <proto/dlg.h>

#include "Mess.h"

extern   UBYTE                      PvtSendLevel;


extern   BYTE  Find_Pointers(long area, char *name);


/// Forward_Message
BOOL                Forward_Message(long sourcenum, long sourcearea, char *username)
{
   char                callstring[128];
   char                string[128];

   if (sourcearea == PVTAREA)
      {
         strcpy(string, username);
         UnderScore(string);
      }
   else
      ASPrintf(NULL, string, "%ld", sourcearea);

   WriteRam(&RStruct, Ext);
   ASPrintf(NULL, callstring, "DLG:Forward -a %s -m %ld -p %d", string, sourcenum, PvtSendLevel);
   OverlayProgram(callstring);
   ReadRam(&RStruct, Ext);
   Find_Pointers(LAST, RStruct.Name);

   return (TRUE);
}
//-
