#include <exec/types.h>
#include <string.h>

#include <dlg/msg.h>
#include <dlg/user.h>

#include <link/area.h>
#include <link/io.h>

#include <pragmas/dlg.h>

#include <proto/dlg.h>

#include "Mess.h"

extern   BOOL                       sigrestoreflag;

extern   BYTE                       signumber;

extern   char                       areafile[];

extern   long                       numsigareas;

extern   SHORT                     *sigarray;

extern   struct   SIG_Def           Sig;


BOOL                SelectSIG(void)
{
   SHORT               tempnumber;

/* List the sigs */
   if (!INSTACK)
      ListSIGS(&UserDat, 0, 1);

   tempnumber = Sig.number;

   Sig.number = iinput(0, 255, 0, SA[1194]);
   sigrestoreflag = FALSE;
   TwoLines();

/* If user pressed return, we want no sig */
   if (!Sig.number)
      {
         AFPrintf(&UserDat, sout, SA[1195]);
         strcpy(areafile, "MSG:Area.bbs");
         numsigareas = MakeSigArray(areafile, &sigarray, 0);
         UserDat.msgsig = 0;
         signumber = 0;
         return (TRUE);
      }

   if (GetStruct("DLGCONFIG:SIGS/SIGS.msg", (char *) &Sig, sizeof(Sig), 1) == -1 || Sig.level > LEVEL)
      {
         AFPrintf(&UserDat, sout, SA[1196]);
         Sig.number = tempnumber;
         return (FALSE);
      }

   ASPrintf(NULL, areafile, "DLGCONFIG:SIGS/%s.msg", Sig.name);
   numsigareas = MakeSigArray(areafile, &sigarray, 0);
   UserDat.msgsig = Sig.number;
   signumber = Sig.number;
   return (TRUE);
}


