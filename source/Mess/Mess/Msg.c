#include <exec/types.h>

#include <dlg/msg.h>
#include <dlg/resman.h>
#include <dlg/user.h>

#include <link/io.h>

#include <pragmas/dlg.h>

#include <proto/dlg.h>
#include <proto/dos.h>

#include "Mess.h"

/// Read_Header()
BOOL  Read_Header(LONG Area, LONG MsgNum, struct Msg_Header *mh)
{
   BPTR  fh             =  NULL;
   char  filename[256];

   if (Area == PVTAREA)
   {
      ASPrintf(NULL, filename, "USER:%s/%ld.MSG", RStruct.Name, MsgNum);
      UnderScore(filename);
   }
   else
   {
      ASPrintf(NULL, filename, "MSG:%d/%ld.MSG", Area, MsgNum);
   }

   if (!(fh = Open(filename, MODE_OLDFILE))) return (FALSE);

   Read(fh, mh, sizeof(struct Msg_Header));
   Close(fh);
   fh = NULL;

   Capitalize(mh->From);
   Capitalize(mh->To);

   return(TRUE);
}
//-
/// Read_Body()
ULONG  Read_Body(LONG Area, LONG MsgNum, char **Buffer, ULONG BufferSize)
{
   BPTR  fh             =  NULL;
   char  filename[256];
   ULONG Actual;

   if (Area == PVTAREA)
   {
      ASPrintf(NULL, filename, "USER:%s/%ld.MSG", RStruct.Name, MsgNum);
      UnderScore(filename);
   }
   else
   {
      ASPrintf(NULL, filename, "MSG:%d/%ld.MSG", Area, MsgNum);
   }

   if (!(fh = Open(filename, MODE_OLDFILE))) return (0L);

   Seek(fh, sizeof(struct Msg_Header), OFFSET_BEGINNING);
   Actual = Read(fh, *Buffer, BufferSize);
   Close(fh);
   fh = NULL;

   return(Actual);
}
//-
/// Write_Header()
BOOL  Write_Header(LONG Area, LONG MsgNum, struct Msg_Header mh)
{
   BPTR  fh             =  NULL;
   char  filename[256];

   if (Area == PVTAREA)
   {
      ASPrintf(NULL, filename, "USER:%s/%ld.MSG", RStruct.Name, MsgNum);
      UnderScore(filename);
   }
   else
   {
      ASPrintf(NULL, filename, "MSG:%d/%ld.MSG", Area, MsgNum);
   }

   if (!(fh = Open(filename, MODE_READWRITE))) return (FALSE);

   Write(fh, &mh, sizeof(struct Msg_Header));
   Close(fh);
   fh = NULL;

   return(TRUE);
}
//-

