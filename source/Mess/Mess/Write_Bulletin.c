#include <exec/types.h>
#include <stdlib.h>
#include <string.h>

#include <dlg/Bulletin.h>
#include <dlg/log.h>
#include <dlg/msg.h>
#include <dlg/user.h>

#include <link/io.h>
#include <link/msg.h>

#include <pragmas/dlg.h>

#include <proto/dlg.h>
#include <proto/dos.h>

#include "Mess.h"

extern   char                       headerfile[];
extern   char                       bodyfile[];

BYTE                Write_Bulletin(void)
{
   char               *bodybuf;
   ULONG               templong;
   char                stackflag = 0;
   char                zflag;

   SHORT               days;
   struct Bulletin     Header;
   struct Msg_Area     msgarea;

   msgarea.Flag = 0;
   Header.Attribute = 0;

   if (LEVEL > 254)
      {
         zflag = finput(0, SA[1155]);
         TwoLines();
         if (zflag)
            {
               DLGInput(NULL, Header.Title, NULL, 72, 72, 7, SA[1156]);
               TwoLines();
               if (strlen(Header.Title) == 0)
                  {
                     AFPrintf(&UserDat, sout, SA[1157]);
                     return (0);
                  }

               Header.Attribute += STACK;
               stackflag = 1;
            }
      }

   if (!stackflag)
      {
         if (!INSTACK)
            {
               AFPrintf(&UserDat, sout, SA[1158]);
               AFPrintf(&UserDat, sout, SA[1159]);
            }

         DLGInput((char *) 1, Header.To, "All", 35, 35, 3, "");
         TwoLines();
         Capitalize(Header.To);
         AFPrintf(&UserDat, sout, "%a2");
         if (!INSTACK)
            AFPrintf(&UserDat, sout, SA[1160]);

         if (DLGInput(NULL, Header.Title, NULL, 28, 28, 0,"") == 0)
            {
               AFPrintf(&UserDat, sout, SA[1161]);
               return (1);
            }

         AFPrintf(&UserDat, sout, SA[1162]);
         strcpy(Header.From, RStruct.Name);
         Capitalize(Header.From);
         MDate(Header.Date);
      }

   days = iinput(1, 365, 0, SA[1163]);
   TwoLines();
   if (!days)
      return (1);

   Header.Expire = (long) (AmigaTime() + ((long) days * 86400L));

   if (!stackflag)
      {
         DeleteFile(bodyfile);

         if (CallMsgEditor(NULL, "::", bodyfile, &msgarea, BULLETIN, Ext, 0, &UserDat, &RStruct) == -1)
            return (-1);
         GetFirstStruct(headerfile, (char *) &Header, sizeof(Header));
      }

   if (FileSize(bodyfile, &templong) == -1)
      templong = 0;

   if (!(bodybuf = (char *) malloc(templong + 1)))
      {
         AFPrintf(&UserDat, sout, SA[1164]);
         return (-1);
      }

   GetFirstStruct(bodyfile, bodybuf, templong);
   bodybuf[templong] = 0;

   if (SendBulletin(&Header, bodybuf, LPswd))
      {
         WriteLog(WROTE_BULLETIN, RStruct.Name, Ext, "");
         AFPrintf(&UserDat, sout, SA[1165]);
      }

   DeleteFile(bodyfile);
   DeleteFile(headerfile);
   free(bodybuf);
}

