#define GOODREAD    0
#define TIMEOUT     1
#define USERABORT   2
#define NOCARRIER   3
#define XSECSIZ   0x80
#define TTIME_SHORT 5L        /* number of seconds for short timeout */
#define TTIME_LONG  50L       /* number of seconds for long  timeout */
#define ERRORMAX 10          /* Max errors before abort */
#define RETRYMAX 10          /* Maximum retrys before abort */
#define SOH      1           /* Start of sector char */
#define STX	 2		/* Start of sector for 1K packet */
#define EOT      4           /* end of transmission char */
#define ACK      6           /* acknowledge sector transmission */
#define NAK      21          /* error in transmission detected */
#define CAN	0x18		/* Character to abort a transfer */
#define CPMEOF  0x1A		/* Pad character */
