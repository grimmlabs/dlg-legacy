// Miscellaneous functions

#include "Include.h"

/// bytecopy
/* bytecopy:   copy a string...with length restrictions
** ALWAYS NULL terminate
**
** char *dst;  destination of copied string
** char *src;  source
** int maxlen; maximum length
 ***********************************************************************/
char *REG bytecopy(char *dst, char *src, int maxlen)
{
   char *dptr; /* ptr into dst */

   dptr = dst;
   while (*src && (maxlen-- > 0))
      *dptr++ = *src++;
   *dptr = 0;
   return(dst);
}
//-

/// fixnull()
/* Don't return NULL pointers! */
char *REG fixnull(char *s)
{
   if (s == NULL)
      return("");
   else
      return(s);
}
//-


