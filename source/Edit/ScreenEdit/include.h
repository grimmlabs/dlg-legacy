#include <exec/types.h>
#include <stdio.h>
#include <time.h>
#include <fcntl.h>
#include <dos.h>

#include "estruct.h"
#include "eproto.h"

#ifdef   maindef
   #include "efunc.h"
#endif

#include "edef.h"

#include "Globals.h"

#include <clib/rexxsyslib_protos.h>

#include <exec/libraries.h>
#include <exec/ports.h>
#include <exec/io.h>
#include <exec/memory.h>

#include <libraries/dos.h>
#include <libraries/reqtools.h>

#include <intuition/intuitionbase.h>

#include <sys/types.h>

#include <rexx/rxslib.h>
#include <rexx/errors.h>

#include <sys/types.h>

#include <proto/dos.h>
#include <proto/exec.h>
#include <proto/dlg.h>
#include <proto/reqtools.h>

#include <pragmas/rexxsyslib_pragmas.h>
#include <pragmas/dlg.h>

#include <dlg/misc.h>
#include <dlg/msg.h>
#include <dlg/file.h>
#include <dlg/user.h>

