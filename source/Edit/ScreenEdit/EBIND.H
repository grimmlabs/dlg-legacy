// Default key bindings

/*
 * Command table.
 * This table  is *roughly* in ASCII order, left to right across the
 * characters of the command. This explains the funny location of the
 * control-X commands.
 */

#define XX(a) nullproc

NOSHARE KEYTAB keytab[NBINDS] = {
   {CTRL|'A',     BINDFNC, gotobol},
   {CTRL|'B',     BINDFNC, backchar},
   {CTRL|'C',     BINDFNC, insspace},
   {CTRL|'D',     BINDFNC, forwdel},
   {CTRL|'E',     BINDFNC, gotoeol},
   {CTRL|'F',     BINDFNC, forwchar},
   {CTRL|'G',     BINDFNC, ctrlg},
   {CTRL|'H',     BINDFNC, backdel},
   {CTRL|'I',     BINDFNC, tab},
   {CTRL|'J',     BINDFNC, dummy1},
   {CTRL|'K',     BINDFNC, killtext},
   {CTRL|'L',     BINDFNC, refresh},
   {CTRL|'M',     BINDFNC, newline},
   {CTRL|'N',     BINDFNC, forwline},
   {CTRL|'O',     BINDFNC, openline},
   {CTRL|'P',     BINDFNC, backline},
   {CTRL|'R',     BINDFNC, backsearch},
   {CTRL|'S',     BINDFNC, forwsearch},
   {CTRL|'T',     BINDFNC, twiddle},
   {CTRL|'U',     BINDFNC, nullproc},
   {CTRL|'V',     BINDFNC, forwpage},
   {CTRL|'W',     BINDFNC, killregion},
   {CTRL|'X',     BINDFNC, cex},
   {CTRL|'Y',     BINDFNC, yank},
   {CTRL|'@',     BINDFNC, setmark},
   {CTRL|'[',     BINDFNC, meta},
   // DLG
   {CTLX|CTRL|'I',   BINDFNC, insfile},
   {CTLX|CTRL|'L',   BINDFNC, lowerregion},
   {CTLX|CTRL|'O',   BINDFNC, deblank},
   {CTLX|CTRL|'T',   BINDFNC, trim},
   {CTLX|CTRL|'U',   BINDFNC, upperregion},
   // DLG
   {CTLX|CTRL|'W',   BINDFNC, filewrite},
   {CTLX|CTRL|'X',   BINDFNC, swapmark},
   {CTLX|'(',     BINDFNC, ctlxlp},
   {CTLX|')',     BINDFNC, ctlxrp},
   {CTLX|'E',     BINDFNC, ctlxe},
   {CTLX|'F',     BINDFNC, setfillcol},
   {CTLX|'I',        BINDFNC, insfile},
   {META|CTRL|'R',   BINDFNC, qreplace},
   {META|' ',     BINDFNC, setmark},
   {META|')',     BINDFNC, indent_region},
   {META|'(',     BINDFNC, undent_region},
   {META|'.',     BINDFNC, setmark},
   {META|'>',     BINDFNC, gotoeob},
   {META|'<',     BINDFNC, gotobob},
   {META|'[',     BINDFNC, ansikey},
   {0x9b,         BINDFNC, ansikey},
   {META|'~',     BINDFNC, unmark},
   {META|'A',     BINDFNC, fillpara},
   {META|'B',     BINDFNC, backword},
   {META|'C',     BINDFNC, capword},
   {META|'D',     BINDFNC, delfword},
   {META|'F',     BINDFNC, forwword},
   {META|'G',     BINDFNC, gotoline},
   {META|'K',     BINDFNC, killtext},
   {META|'L',     BINDFNC, lowerword},
   {META|'N',     BINDFNC, gotoeop},
   {META|'P',     BINDFNC, gotobop},
   {META|'Q',     BINDFNC, fillpara},
   {META|'R',     BINDFNC, sreplace},
   {META|'S',     BINDFNC, forwsearch},
   {META|'U',     BINDFNC, not_implemented},
   {META|'V',     BINDFNC, backpage},
   {META|'W',     BINDFNC, copyregion},
   {META|'Z',     BINDFNC, opts},
   {CTRL|'?',     BINDFNC, forwdel},
   {0,         BINDNUL, NULL}
};
