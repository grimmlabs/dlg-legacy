// Environment variables

/// structure to hold user variables and their definitions (UVAR)
typedef struct UVAR
{
   char u_name[NVSIZE + 1];   /* name of user variable */
   char *u_value;             /* value (string) */
} UVAR;
//-

/* current user variables (This structure will probably change)   */

#define MAXVARS   150 /* was 512 */

UVAR NOSHARE uv[MAXVARS + 1]; /* user variables */

/* list of recognized environment variables  */

NOSHARE char *envars[] = {
   "aread",    /* access level for read */
   "awrite",      /* access level for write */
   "bufhook",     /* enter buffer switch hook */
   "cbflags",     /* current buffer flags */
   "cbufname",    /* current buffer name */
   "cfname",      /* current file name */
   "cmdhook",     /* command loop hook */
   "cmode",    /* mode of current buffer */
   "curchar",     /* current character under the cursor */
   "curcol",      /* current column pos of cursor */
   "curline",     /* current line in file */
   "curwidth",    /* current screen width */
   "curwind",     /* current window ordinal on current screen */
   "cwline",      /* current screen line in window */
   "debug",    /* macro debugging */
   "deskcolor",      /* desktop color */
   "diagflag",    /* diagonal mouse movements enabled? */
   "discmd",      /* display commands on command line */
   "disinp",      /* display command line input characters */
   "disphigh",    /* display high bit characters escaped */
   "exbhook",     /* exit buffer switch hook */
   "fcol",        /* first displayed column in curent window */
   "fillcol",     /* current fill column */
   "fini",        /* XENO_EMACS finished with emacsxl.rc */
   "fmtlead",     /* format command lead characters */
   "from",        /* XENO_ EMACS from */
   "gflags",      /* global internal emacs flags */
   "gmode",    /* global modes */
   "hardtab",     /* current hard tab size */
   "hjump",    /* horizontal screen jump size */
   "hscroll",     /* horizontal scrolling flag */
   "kill",     /* kill buffer (read only) */
   "kool",        /* separator character between windows */
   "lastkey",     /* last keyboard char struck */
   "lastmesg",    /* last string mlwrite()ed */
   "line",     /* text of current line */
   "lterm",    /* current line terminator for writes */
   "lwidth",      /* width of current line */
   "match",    /* last matched magic pattern */
   "modeflag",    /* Modelines displayed flag */
   "msflag",      /* activate mouse? */
   "numwind",     /* number of windows on current screen */
   "oldcrypt",    /* use old(broken) encryption */
   "orgcol",      /* screen origin column */
   "orgrow",      /* screen origin row */
   "pagelen",     /* number of lines used by editor */
   "palette",     /* current palette string */
   "paralead",    /* paragraph leadin characters */
   "pending",     /* type ahead pending flag */
   "popflag",     /* pop-up windows active? */
   "progname",    /* returns current prog name - "MicroEMACS" */
   "qstyle",      /* quote style */
   "readhook",    /* read file execution hook */
   "region",      /* current region (read only) */
   "replace",     /* replacement pattern */
   "rval",     /* child process return value */
   "scrname",     /* current screen name */
   "search",      /* search pattern */
   "searchpnt",      /* differing search styles (term point) */
   "seed",     /* current random number seed */
   "softtab",     /* current soft tab size */
   "sres",     /* current screen resolution */
   "ssave",    /* safe save flag */
   "sscroll",     /* smooth scrolling flag */
   "status",      /* returns the status of the last command */
   "sterm",    /* search terminator character */
   "subject",     /* XENO_ EMACS subject */
   "target",      /* target for line moves */
   "time",        /* date and time */
   "to",       /* XENO_ EMACS to */
   "tpause",      /* length to pause for paren matching */
   "version",     /* current version number */
   "wline",    /* # of lines in current window */
   "wraphook",    /* wrap word execution hook */
   "writehook",      /* write file hook */
   "xpos",     /* current mouse X position */
   "yankflag",    /* point placement at yanked/included text */
   "ypos",        /* current mouse Y position */
   "zabort",
   "zbye",
   "zdest",
   "zdir",
   "zfask",
   "zfquery",
   "zftop",
   "zhbc",
   "zhdr",             /* DLG header display: 0=msg, 1=file desc, or other */
   "zhello",
   "zhelpc",
   "zmlc",
   "zmore",
   "zmsgc",
   "znewsub",
   "znewto",
   "zno",         /* XENO_ EMACS "N" */
   "zqc",
   "zqh",
   "zscheck",
   "zsexit",
   "zsrexx",
   "zsrun",
   "zsp1",
   "zsp2",
   "zsp3",
   "zsp4",
   "zsub",
   "zto",
   "ztop",
   "ztype",    /* DLG message header type -- text description */
   "zwbc",
   "zwho",
   "zyes",        /* XENO_ EMACS "Y" */
   "zz008",    //"[Aborted]"
   "zz009",    //"[Mark %d set]"
   "zz019",    //"[Key not bound]"
   "zz070",    //"[region copied]"
   "zz076",    //"No mark set in this window"
   "zz078",    //"Search"
   "zz079",    //"Not found"
   "zz081",    //"Reverse search"
   "zz084",    //"Replace"
   "zz086",    //"with"
   "zz109",    //"[Key illegal in VIEW mode]"
   "zz148",    //"[Writing...]"
};

#define NEVARS sizeof(envars) / sizeof(char *)

/* and its preprocesor definitions  */

#define EVAREAD      0
#define EVAWRITE  1
#define  EVBUFHOOK   2
#define EVCBFLAGS 3
#define EVCBUFNAME   4
#define EVCFNAME  5
#define EVCMDHK   6
#define EVCMODE   7
#define EVCURCHAR 8
#define EVCURCOL  9
#define EVCURLINE 10
#define EVCURWIDTH   11
#define  EVCURWIND   12
#define EVCWLINE  13
#define EVDEBUG         14
#define  EVDESKCLR   15
#define EVDIAGFLAG      16
#define EVDISCMD        17
#define EVDISINP        18
#define  EVDISPHIGH  19
#define EVEXBHOOK       20
#define EVFCOL    21
#define EVFILLCOL 22
#define EVFLICKER 23
#define EVFINI    23 /*XENO_ EMACS*/
#define  EVFMTLEAD   24
#define EVFROM    25
#define EVGFLAGS  26
#define EVGMODE   27
#define  EVHARDTAB   28
#define EVHJUMP      29
#define EVHSCROLL 30
#define EVKILL          31
#define EVKOOL    32
#define EVLASTKEY       33
#define EVLASTMESG      34
#define EVLINE          35
#define  EVLTERM     36
#define EVLWIDTH        37
#define EVMATCH         38
#define EVMODEFLAG      39
#define EVMSFLAG        40
#define  EVNUMWIND   41
#define  EVOCRYPT 42
#define  EVORGCOL 43
#define  EVORGROW 44
#define EVPAGELEN       45
#define EVPALETTE       46
#define  EVPARALEAD  47
#define EVPENDING       48
#define  EVPOPFLAG   49
#define EVPROGNAME      50
#define EVQSTYLE  51
#define EVREADHK        52
#define  EVREGION 53
#define EVREPLACE       54
#define EVRVAL          55
#define EVSCRNAME 56
#define EVSEARCH        57
#define EVSEARCHPNT  58
#define EVSEED          59
#define EVSOFTTAB 60
#define EVSRES          61
#define EVSSAVE         62
#define EVSSCROLL 63
#define EVSTATUS  64
#define EVSTERM   65
#define EVSUBJECT    66
#define EVTARGET  67
#define EVTIME    68
#define EVTO      69
#define EVTPAUSE  70
#define EVVERSION 71
#define EVWLINE   72
#define EVWRAPHK  73
#define  EVWRITEHK   74
#define EVXPOS    75
#define  EVYANKFLAG  76
#define EVYPOS    77
#define EVZABORT        78
#define EVZBYE          79
#define EVZDEST         80
#define EVZDIR          81
#define EVZFASK         82
#define EVZFQUERY       83
#define EVZFTOPIC       84
#define EVXHBCOLOR      85
#define EVZHDR          86
#define EVZHELLO        87
#define EVXHELPCOLOR    88
#define EVXMLCOLOR      89
#define EVZMORE         90
#define EVXMSGCOLOR     91
#define EVZNEWSUB       92
#define EVZNEWTO        93
#define EVZNO           94
#define EVXQUOTECOLOR   95
#define EVZQH           96
#define EVZSCHECK       97
#define EVZSEXIT        98
#define EVZSREXX        99
#define EVZSRUN         100
#define EVZSP1          101
#define EVZSP2          102
#define EVZSP3          103
#define EVZSP4          104
#define EVZSUB          105
#define EVZTO           106
#define EVZTOP          107
#define EVZTYPE         108
#define EVXWBCOLOR      109
#define EVZWHO          110
#define EVZYES          111
#define EVTEXT8         112
#define EVTEXT9         113
#define EVTEXT19        114
#define EVTEXT70  115
#define EVTEXT76  116
#define EVTEXT78  117
#define EVTEXT79        118
#define EVTEXT81  119
#define EVTEXT84  120
#define EVTEXT86  121
#define EVTEXT109 122
#define EVTEXT148 123




/* list of recognized user functions   */

typedef struct UFUNC
{
   char f_name[4];
   int f_type; /* 1 = monamic, 2 = dynamic */
} UFUNC;

#define NILNAMIC  0
#define MONAMIC   1
#define DYNAMIC   2
#define TRINAMIC  3

NOSHARE UFUNC funcs[] =
{
   "abs", MONAMIC,   /* absolute value of a number */
   "add", DYNAMIC,        /* add two numbers together */
   "and", DYNAMIC,   /* logical and */
   "asc", MONAMIC,   /* char to integer conversion */
   "ban", DYNAMIC,   /* bitwise and  9-10-87  jwm */
   "bin", MONAMIC,   /* loopup what function name is bound to a key */
   "bno", MONAMIC,   /* bitwise not */
   "bor", DYNAMIC,   /* bitwise or   9-10-87  jwm */
   "bxo", DYNAMIC,   /* bitwise xor  9-10-87  jwm */
   "cat", DYNAMIC,   /* concatinate string */
   "chr", MONAMIC,   /* integer to char conversion */
   "div", DYNAMIC,   /* division */
   "env", MONAMIC,   /* retrieve a system environment var */
   "equ", DYNAMIC,   /* logical equality check */
   "exi", MONAMIC,   /* check if a file exists */
   "fin", MONAMIC,   /* look for a file on the path... */
   "gre", DYNAMIC,   /* logical greater than */
   "gro", MONAMIC,      /* return group match in MAGIC mode */
   "gtc", NILNAMIC,  /* get 1 emacs command */
   "gtk", NILNAMIC,  /* get 1 charater */
   "ind", MONAMIC,   /* evaluate indirect value */
   "isn", MONAMIC,      /* is the arg a number? */
   "lef", DYNAMIC,   /* left string(string, len) */
   "len", MONAMIC,   /* string length */
   "les", DYNAMIC,   /* logical less than */
   "low", MONAMIC,   /* lower case string */
   "mid", TRINAMIC,  /* mid string(string, pos, len) */
   "mod", DYNAMIC,   /* mod */
   "neg", MONAMIC,   /* negate */
   "not", MONAMIC,   /* logical not */
   "or",  DYNAMIC,   /* logical or */
   "rig", DYNAMIC,   /* right string(string, pos) */
   "rnd", MONAMIC,   /* get a random number */
   "seq", DYNAMIC,   /* string logical equality check */
   "sgr", DYNAMIC,   /* string logical greater than */
   "sin", DYNAMIC,   /* find the index of one string in another */
   "sle", DYNAMIC,   /* string logical less than */
   "slo", DYNAMIC,      /* set lower to upper char translation */
   "sub", DYNAMIC,   /* subtraction */
   "sup", DYNAMIC,      /* set upper to lower char translation */
   "tim", DYNAMIC,   /* multiplication */
   "tri", MONAMIC,      /* trim whitespace off the end of a string */
   "tru", MONAMIC,   /* Truth of the universe logical test */
   "upp", MONAMIC,   /* uppercase string */
   "xla", TRINAMIC      /* XLATE character string translation */
};

#define NFUNCS sizeof(funcs) / sizeof(UFUNC)

/* and its preprocesor definitions  */

#define UFABS     0
#define UFADD     1
#define UFAND     2
#define UFASCII   3
#define UFBAND    4
#define UFBIND    5
#define UFBNOT    6
#define UFBOR     7
#define UFBXOR    8
#define UFCAT     9
#define UFCHR     10
#define UFDIV     11
#define UFENV     12
#define UFEQUAL   13
#define UFEXIST   14
#define UFFIND    15
#define UFGREATER 16
#define UFGROUP      17
#define UFGTCMD   18
#define UFGTKEY   19
#define UFIND     20
#define  UFISNUM     21
#define UFLEFT    22
#define UFLENGTH  23
#define UFLESS    24
#define UFLOWER   25
#define UFMID     26
#define UFMOD     27
#define UFNEG     28
#define UFNOT     29
#define UFOR      30
#define UFRIGHT   31
#define UFRND     32
#define UFSEQUAL  33
#define UFSGREAT  34
#define UFSINDEX  35
#define UFSLESS   36
#define  UFSLOWER 37
#define UFSUB     38
#define  UFSUPPER 39
#define UFTIMES   40
#define  UFTRIM      41
#define UFTRUTH   42
#define UFUPPER   43
#define UFXLATE   44
