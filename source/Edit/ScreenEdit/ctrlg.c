#include "Include.h"

/// ctrlg()
/*
** Abort.
** Beep the beeper. Kill off any keyboard macro, etc., that is in progress.
** Sometimes called as a routine, to do general aborting of stuff.
 ***************************************************************************/
STD ctrlg(f, n)
{
   TTbeep();
   kbdmode = STOP;
   mlwrite(TEXT8);
/*    "[Aborted]" */
   return(ABORT);
}
//-




