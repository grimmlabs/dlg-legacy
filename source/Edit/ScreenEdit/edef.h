// global variable definitions

#define FLINE_SIZE 2000

#ifdef   maindef

/* for MAIN.C */

/* initialized global definitions */

NOSHARE int DNEAR fillcol = 76;  /* Current fill column     */
NOSHARE short kbdm[NKBDM];    /* Macro       */
NOSHARE char *execstr /*= NULL*/;   /* pointer to string to execute */
NOSHARE char paralead[3] = " \t";   /* paragraph leadin chars  */
NOSHARE char mainbuf[] = "main"; /* name of main buffer     */
NOSHARE int DNEAR exec_error /*= FALSE*/; /* macro execution error pending? */
NOSHARE int DNEAR fini /*= FALSE*/;    /* XENO_EMACS did startup finish*/
NOSHARE int DNEAR gmode /*= 0*/;    /* global editor mode      */
NOSHARE int DNEAR gflags = GFREAD;  /* global control flag     */
NOSHARE int DNEAR gfcolor = 7;      /* global forgrnd color (white) */
NOSHARE int DNEAR gbcolor /*= 0*/;     /* global backgrnd color (black)*/
NOSHARE int DNEAR sgarbf = TRUE;    /* TRUE if screen is garbage  */
NOSHARE int DNEAR mpresf /*= FALSE*/;  /* TRUE if message in last line */
NOSHARE int DNEAR clexec /*= FALSE*/;  /* command line execution flag   */
NOSHARE int DNEAR discmd /*= FALSE*/;  /* display command flag    */
NOSHARE int DNEAR disinp = TRUE;    /* display input characters   */
NOSHARE int DNEAR modeflag = TRUE;  /* display modelines flag  */
NOSHARE int DNEAR popflag = TRUE;   /* pop-up windows enabled? */
NOSHARE int DNEAR hscroll /*= FALSE*/; /* horizontal scrolling flag  */
NOSHARE int DNEAR hjump = 1;     /* horizontal jump size    */
NOSHARE int DNEAR ssave = TRUE;  /* safe save flag    */
NOSHARE int DNEAR ttcol = HUGE;  /* Column location of HW cursor */
NOSHARE int DNEAR lbound /*= 0*/;      /* leftmost column of current line
                  being displayed      */
NOSHARE int DNEAR tabsize = 8;      /* current hard tab size   */
NOSHARE int DNEAR stabsize = 8;     /* current soft tab size (0: use hard tabs)  */
NOSHARE int DNEAR abortc = CTRL | 'G'; /* current abort command char */
NOSHARE int DNEAR sterm = CTRL | 'M';  /* search terminating character */
NOSHARE int DNEAR searchtype = SRNORM; /* current search style    */
NOSHARE int DNEAR yankflag /*= FALSE*/;   /* current yank style      */
NOSHARE int DNEAR quotec = CTRL | 'Q'; /* quote char during mlreply() */
NOSHARE CONST char *cname[] = {     /* names of colors      */
   "BLACK", "RED", "GREEN", "YELLOW", "BLUE",
   "MAGENTA", "CYAN", "GREY",
   "GRAY", "LRED", "LGREEN", "LYELLOW", "LBLUE",
   "LMAGENTA", "LCYAN", "WHITE"};
NOSHARE KILL *kbufp = NULL;      /* current kill buffer chunk pointer*/
NOSHARE KILL *kbufh = NULL;      /* kill buffer header pointer */
NOSHARE  int kskip /*= 0*/;         /* # of bytes to skip in 1st kill chunk */
NOSHARE int kused = KBLOCK;      /* # of bytes used in last kill chunk*/
NOSHARE short *kbdptr;        /* current position in keyboard buf */
NOSHARE short *kbdend = &kbdm[0];   /* ptr to end of the keyboard */
NOSHARE int DNEAR kbdmode = STOP;   /* current keyboard macro mode   */
NOSHARE int DNEAR kbdrep /*= 0*/;      /* number of repetitions   */
NOSHARE int DNEAR restflag /*= FALSE*/;   /* restricted use?      */
NOSHARE int DNEAR lastkey /*= 0*/;     /* last keystoke     */
NOSHARE int DNEAR seed /*= 0*/;     /* random number seed      */
NOSHARE int DNEAR macbug /*= FALSE*/;  /* macro debugging flag    */
NOSHARE int DNEAR cmdstatus = TRUE; /* last command status     */
NOSHARE char lastmesg[NSTRING];  /* last message posted     */
char fline[FLINE_SIZE+1];
NOSHARE int DNEAR rval /*= 0*/;     /* return value of a subprocess */
NOSHARE int DNEAR eexitflag /*= FALSE*/;  /* EMACS exit flag      */
NOSHARE int xpos /*= 0*/;     /* current column mouse is positioned to*/
NOSHARE int ypos /*= 0*/;     /* current screen row        "      */
NOSHARE int disphigh /*= FALSE*/;   /* display high bit chars escaped   */

/* uninitialized global definitions */

NOSHARE int DNEAR curcol;  /* Cursor column     */
NOSHARE int DNEAR thisflag;   /* Flags, this command     */
NOSHARE int DNEAR lastflag;   /* Flags, last command     */
NOSHARE int DNEAR curgoal; /* Goal for C-P, C-N    */
NOSHARE WINDOW *curwp;     /* Current window    */
NOSHARE BUFFER *curbp;     /* Current buffer    */
NOSHARE WINDOW *wheadp;    /* Head of list of windows */
NOSHARE BUFFER *bheadp;    /* Head of list of buffers    */
NOSHARE SCREEN *first_screen; /* Head and current screen in list */
NOSHARE  char lowcase[HICHAR];   /* lower casing map     */
NOSHARE  char upcase[HICHAR]; /* upper casing map     */

NOSHARE char pat[NPAT];    /* Search pattern    */
NOSHARE char tap[NPAT];    /* Reversed pattern array. */
NOSHARE char rpat[NPAT];   /* replacement pattern     */

/* Various "Hook" execution variables  */

NOSHARE KEYTAB readhook;   /* executed on all file reads */
NOSHARE KEYTAB wraphook;   /* executed when wrapping text */
NOSHARE KEYTAB cmdhook;    /* executed before looking for a command */
NOSHARE KEYTAB writehook;  /* executed on all file writes */
NOSHARE KEYTAB exbhook;    /* executed when exiting a buffer */
NOSHARE KEYTAB bufhook;    /* executed when entering a buffer */

/* The variable patmatch holds the
   string that satisfies the search command.
*/
NOSHARE char *patmatch = NULL;


/* directive name table:
   This holds the names of all the directives....  */

CONST char *dname[] = {
   "if", "else", "endif",
   "goto", "return", "endm",
   "while", "endwhile", "break",
   "force"
};



#else

/* for all the other .C files */

/* initialized global external declarations */

NOSHARE extern int DNEAR fillcol;   /* Current fill column     */
NOSHARE extern short kbdm[];     /* Holds kayboard macro data  */
NOSHARE extern char *execstr;    /* pointer to string to execute */
NOSHARE extern char paralead[];     /* paragraph leadin chars  */
NOSHARE extern char fmtlead[];      /* format command leadin chars   */
NOSHARE extern char mainbuf[];      /* name of main buffer     */
NOSHARE extern char lterm[];     /* line terminators on file write */
NOSHARE extern unsigned char wordlist[];/* characters considered "in words" */
NOSHARE extern int DNEAR wlflag; /* word list enabled flag  */
NOSHARE extern int DNEAR exec_error;   /* macro execution error pending? */
NOSHARE extern int DNEAR fini;
CONST extern char modecode[];    /* letters to represent modes */
NOSHARE extern KEYTAB keytab[];     /* key bind to functions table   */
NOSHARE extern NBIND names[];    /* name to function table  */
NOSHARE extern int DNEAR gmode;     /* global editor mode      */
NOSHARE extern int DNEAR gflags; /* global control flag     */
NOSHARE extern int DNEAR gfcolor;   /* global forgrnd color (white) */
NOSHARE extern int DNEAR gbcolor;   /* global backgrnd color (black)*/
NOSHARE extern int DNEAR deskcolor; /* desktop background color   */
NOSHARE extern int DNEAR gasave; /* global ASAVE size    */
NOSHARE extern int DNEAR gacount;   /* count until next ASAVE  */
NOSHARE extern int DNEAR sgarbf; /* State of screen unknown */
NOSHARE extern int DNEAR mpresf; /* Stuff in message line   */
NOSHARE extern int DNEAR clexec; /* command line execution flag   */
NOSHARE extern int DNEAR discmd; /* display command flag    */
NOSHARE extern int DNEAR disinp; /* display input characters   */
NOSHARE extern int DNEAR modeflag;  /* display modelines flag  */
NOSHARE extern int DNEAR popflag;   /* pop-up windows enabled? */
NOSHARE extern int DNEAR sscroll;   /* smooth scrolling enabled flag*/
NOSHARE extern int DNEAR hscroll;   /* horizontal scrolling flag  */
NOSHARE extern int DNEAR hjump;     /* horizontal jump size    */
NOSHARE extern int DNEAR ssave;     /* safe save flag    */
NOSHARE extern int DNEAR ttcol;     /* Column location of HW cursor */
NOSHARE extern int DNEAR lbound; /* leftmost column of current line
                  being displayed      */
NOSHARE extern int DNEAR tabsize;   /* current hard tab size   */
NOSHARE extern int DNEAR stabsize;  /* current soft tab size (0: use hard tabs)  */
NOSHARE extern int DNEAR abortc; /* current abort command char */
NOSHARE extern int DNEAR sterm;     /* search terminating character */
NOSHARE extern int DNEAR searchtype;   /* current search style    */
NOSHARE extern int DNEAR yankflag;  /* current yank style      */
NOSHARE extern int DNEAR quotec; /* quote char during mlreply() */
NOSHARE extern CONST char *cname[]; /* names of colors      */
NOSHARE extern KILL *kbufp;      /* current kill buffer chunk pointer */
NOSHARE extern KILL *kbufh;      /* kill buffer header pointer */
NOSHARE  extern int kskip;    /* # of bytes to skip in 1st kill chunk */
NOSHARE extern int kused;     /* # of bytes used in kill buffer*/
NOSHARE extern int cryptflag;    /* currently encrypting?   */
NOSHARE extern int oldcrypt;     /* using old(broken) encryption? */
NOSHARE extern short *kbdptr;    /* current position in keyboard buf */
NOSHARE extern short *kbdend;    /* ptr to end of the keyboard */
NOSHARE extern int kbdmode;      /* current keyboard macro mode   */
NOSHARE extern int kbdrep;    /* number of repetitions   */
NOSHARE extern int restflag;     /* restricted use?      */
NOSHARE extern int lastkey;      /* last keystoke     */
NOSHARE extern int seed;      /* random number seed      */
NOSHARE extern int DNEAR macbug; /* macro debugging flag    */
NOSHARE extern int DNEAR mouseflag; /* use the mouse?    */
NOSHARE extern int DNEAR diagflag;  /* diagonal mouse movements?  */
NOSHARE extern int DNEAR cmdstatus; /* last command status     */
NOSHARE extern char palstr[];    /* palette string    */
NOSHARE extern char lastmesg[];     /* last message posted     */
extern char fline[];
NOSHARE extern int DNEAR flen;      /* current length of fline */
NOSHARE extern int DNEAR rval;      /* return value of a subprocess */
NOSHARE extern int DNEAR eexitflag; /* EMACS exit flag */
NOSHARE extern int xpos;      /* current column mouse is positioned to */
NOSHARE extern int ypos;      /* current screen row        "    */
NOSHARE extern int disphigh;     /* display high bit chars escaped*/

/* uninitialized global external declarations */

NOSHARE extern int DNEAR curcol; /* Cursor column     */
NOSHARE extern int DNEAR thisflag;  /* Flags, this command     */
NOSHARE extern int DNEAR lastflag;  /* Flags, last command     */
NOSHARE extern int DNEAR curgoal;   /* Goal for C-P, C-N    */
NOSHARE extern WINDOW *curwp;       /* Current window    */
NOSHARE extern BUFFER *curbp;       /* Current buffer    */
NOSHARE extern WINDOW *wheadp;      /* Head of list of windows */
NOSHARE extern BUFFER *bheadp;      /* Head of list of buffers */
NOSHARE extern SCREEN *first_screen;   /* Head and current screen in list */
NOSHARE extern BUFFER *slistp;      /* Buffer for A-B    */

NOSHARE extern char sres[NBUFN]; /* current screen resolution  */

NOSHARE  extern char lowcase[HICHAR];  /* lower casing map     */
NOSHARE  extern char upcase[HICHAR];   /* upper casing map     */

NOSHARE extern char pat[];    /* Search pattern    */
NOSHARE extern char tap[];    /* Reversed pattern array. */
NOSHARE extern char rpat[];      /* replacement pattern     */

/* Various "Hook" execution variables  */

NOSHARE extern KEYTAB readhook;     /* executed on all file reads */
NOSHARE extern KEYTAB wraphook;     /* executed when wrapping text */
NOSHARE extern KEYTAB cmdhook;      /* executed before looking for a cmd */
NOSHARE extern KEYTAB writehook; /* executed on all file writes */
NOSHARE extern KEYTAB exbhook;      /* executed when exiting a buffer */
NOSHARE extern KEYTAB bufhook;      /* executed when entering a buffer */

NOSHARE extern char *patmatch;


CONST extern char *dname[];      /* directive name table    */



#endif

NOSHARE extern TERM  term;    /* Terminal information.   */

#define STYLE_MESSAGE 0
#define STYLE_FILEDESCR 1
#define STYLE_OTHER 2
#define STYLE_HACKNSLASH 3


