/* possible names and paths of help files under different OSs  */

NOSHARE char *pathname[] =
{
    // EmacsDLG:
    // pathname[0] is config file
    // pathname[1] is currently unused
    // pathname[2] is set to the config directory
    // pathname[3] is the current directory
   // pathname[3] gets filled in later with emacsxl config directory
   // actually, it's pathname[2] for emacsdlg -- not using ram anymore
   // "EmacsXL.CFG", "EmacsXL.RC",  "RAM:",  NULL, "",
   "ScreenEdit.CFG",      NULL, "DLGconfig:Misc/",   "",
};


#define  NPNAMES  (sizeof(pathname)/sizeof(char *))
