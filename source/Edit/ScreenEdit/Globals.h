// Global variables

extern char    bc_displayed;           // determine when it's ok to erase broadcast message
extern char   *dlg_body;
extern char   *dlg_header;
extern char   *dlg_port;
extern char   *dlg_initials;
extern char   *dlg_reply;
extern char    dlg_local;
extern char   *hack_quote;             // name of quote file if hack&slash
extern char   *cfg_fname;              // set during startup to EmacDLG.cfg path
extern char    color_ok;
extern char    id_ok;
extern char    ok_broadcast;           // allows broadcast and sleepy timer messages
extern char    xl_no[];
extern char    xl_yes[];
extern char    xl_NO[];
extern char    xl_YES[];
extern char   *Zhello;
extern char   *Zabort;
extern char   *Zbye;
extern char   *Zmore;
extern char   *Ztop;
extern char   *Zqh;
extern char   *Zto;
extern char   *Zsub;
extern char   *Znewto;
extern char   *Znewsub;
extern char   *Zsp1;
extern char   *Zsp2;
extern char   *Zsp3;
extern char   *Zsp4;
extern char   *Zdir;                   // file requester default directory
extern char   *Zfquery;
extern char   *Xmsgcolor;
extern char   *Xquotecolor;
extern char   *Xhelpcolor;
extern char   *Xhbcolor;
extern char   *Xmlcolor;               // Message line color
extern char   *Xwbcolor;               // Window border color
extern char   *Zwhoports;              // default is in code to prevent hacking
extern char   *dlg_uname;              // DLG user name
extern char   *SpellPortName;
extern char   *SpellQuitCmd;
extern char   *SpellCheckCmd;
extern char   *SpellExecCmd;
extern char   *pathname[];

extern int  quote_style;               // 0 for initials, 1 for first name
extern int  DNEAR numfunc;             // number of bindable functions

extern short mode_color;
extern short ml_color;
extern short dlg_hdr_style;            // see defines in evar.h

extern unsigned char kool_array[];     // different types of window seperators
extern unsigned char kool;             // Index that points to char to use in window seperator
extern unsigned char hack_ignore;      // number of lines to ignore in hack&slash quote
extern unsigned char xl_access;
extern unsigned char xl_acc_read;
extern unsigned char xl_acc_write;
extern unsigned char Zfask;            // ask for fortunes?  0=never 1=ask 2=always
extern unsigned char Zftopic;          // fortune cookie topic: 0=never 1=ask 2+=topic


extern struct Library     *DLGBase;
extern struct Msg_Header  *dlg_quote;
extern struct Msg_Header   dlg_msg_hdr;
extern struct USER_DATA    user_data;
extern struct Ram_File     ram_data;

