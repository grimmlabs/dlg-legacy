/*
** The routines in this file deal with the region, that magic space
** between "." and mark. Some functions are commands. Some functions are just for
** internal use.
**
 *********************************************************************************/

#include "Include.h"

/// reglines()
/* reglines:   how many lines in the current region
**       used by the trim/entab/detab-region commands
********************************************************/
int STD reglines(void)
{
        register LINE *linep; /* position while scanning */
   register int n;      /* number of lines in this current region */
        REGION region;

   /* check for a valid region first */
        if (getregion(&region) != TRUE)
                return(0);

   /* start at the top of the region.... */
        linep = region.r_linep;
   region.r_size += region.r_offset;
        n = 0;

        /* scan the region... counting lines */
        while (region.r_size > 0L) {
      region.r_size -= llength(linep) + 1;
      linep = lforw(linep);
      n++;
   }

   /* place us at the beginning of the region */
        curwp->w_dotp = region.r_linep;
        curwp->w_doto = region.r_offset;

        return(n);
}
//-

/// killregion()
/*
** Kill the region. Ask "getregion" to figure out the bounds of the region.
** Move "." to the start, and kill the characters.
** Bound to "C-W".
 ***********************************************************************/
STD killregion(int f, int n)
{
        register int    s;
        REGION          region;

   if (curbp->b_mode&MDVIEW)  /* don't allow this command if   */
      return(rdonly()); /* we are in read only mode   */
        if ((s=getregion(&region)) != TRUE)
                return(s);
        if ((lastflag&CFKILL) == 0)             /* This is a kill type  */
                kdelete();                      /* command, so do magic */
        thisflag |= CFKILL;                     /* kill buffer stuff.   */
        curwp->w_dotp = region.r_linep;
        curwp->w_doto = region.r_offset;
        return(ldelete(region.r_size, TRUE));
}
//-

/// copyregion()
/*
** Copy all of the characters in the
** region to the kill buffer. Don't move dot
** at all. This is a bit like a kill region followed
** by a yank. Bound to "M-W".
******************************************************/
STD copyregion(f, n)
{
        register LINE   *linep;
        register int    loffs;
        register int    s;
        REGION          region;

        if ((s=getregion(&region)) != TRUE)
                return(s);
        if ((lastflag&CFKILL) == 0)             /* Kill type command.   */
                kdelete();
        thisflag |= CFKILL;
        linep = region.r_linep;                 /* Current line.        */
        loffs = region.r_offset;                /* Current offset.      */
        while (region.r_size--) {
                if (loffs == llength(linep)) {  /* End of line.         */
                        if ((s=kinsert(FORWARD, '\r')) != TRUE)
                                return(s);
                        linep = lforw(linep);
                        loffs = 0;
                } else {                        /* Middle of line.      */
                        if ((s=kinsert(FORWARD, lgetc(linep, loffs))) != TRUE)
                                return(s);
                        ++loffs;
                }
        }
   mlwrite(TEXT70);
/*              "[region copied]" */
        return(TRUE);
}
//-

/// lowerregion()
/*
** Lower case region. Zap all of the upper
** case characters in the region to lower case. Use
** the region code to set the limits. Scan the buffer,
** doing the changes. Call "lchange" to ensure that
** redisplay is done in all buffers. Bound to
** "C-X C-L".
 ***********************************************************************/
STD lowerregion(f, n)
{
        register LINE   *linep;
        register int    loffs;
        register int    s;
        int c;
        REGION          region;

   if (curbp->b_mode&MDVIEW)  /* don't allow this command if   */
      return(rdonly()); /* we are in read only mode   */
        if ((s=getregion(&region)) != TRUE)
                return(s);
        lchange(WFHARD);
        linep = region.r_linep;
        loffs = region.r_offset;
        while (region.r_size--) {
                if (loffs == llength(linep)) {
                        linep = lforw(linep);
                        loffs = 0;
                } else {
                        c = lgetc(linep, loffs);
         c = lowerc(c);
                        lputc(linep, loffs, c);
                        ++loffs;
                }
        }
        return(TRUE);
}
//-

/// upperregion()
/*
** Upper case region. Zap all of the lower
** case characters in h�SRCo upper case. Use
** the region code to set the limits. Scan the buffer,
** doing the changes. Call "lchange" to ensure that
** redisplay is done in all buffers. Bound to
** "C-X C-L".
 ***********************************************************************/
STD upperregion(int f, int n)
{
        register LINE   *linep;
        register int    loffs;
        register int    s;
        int c;
        REGION          region;

   if (curbp->b_mode&MDVIEW)  /* don't allow this command if   */
      return(rdonly()); /* we are in read only mode   */
        if ((s=getregion(&region)) != TRUE)
                return(s);
        lchange(WFHARD);
        linep = region.r_linep;
        loffs = region.r_offset;
        while (region.r_size--) {
                if (loffs == llength(linep)) {
                        linep = lforw(linep);
                        loffs = 0;
                } else {
                        c = lgetc(linep, loffs);
         c = upperc(c);
                        lputc(linep, loffs, c);
                        ++loffs;
                }
        }
        return(TRUE);
}
//-

/// getregion()
/*
** This routine figures out the bounds of the region in the current
** window, and fills in the fields of the "REGION" structure pointed to by
** "rp". Because the dot and mark are usually very close together, we scan
** outward from dot looking for mark. This should save time. Return a
** standard code. Callers of this routine should be prepared to get an
** "ABORT" status; we might make this have the confirm thing later.
**
** register REGION *rp;
**
 ***********************************************************************/
STD getregion(register REGION *rp)
{
        register LINE   *flp;
        register LINE   *blp;
        long fsize;
        long bsize;

        if (curwp->w_markp[0] == (LINE *)NULL) {
                mlwrite(TEXT76);
/*                      "No mark set in this window" */
                return(FALSE);
        }
        if (curwp->w_dotp == curwp->w_markp[0]) {
                rp->r_linep = curwp->w_dotp;
                if (curwp->w_doto < curwp->w_marko[0]) {
                        rp->r_offset = curwp->w_doto;
                        rp->r_size = (long)(curwp->w_marko[0]-curwp->w_doto);
                } else {
                        rp->r_offset = curwp->w_marko[0];
                        rp->r_size = (long)(curwp->w_doto-curwp->w_marko[0]);
                }
                return(TRUE);
        }
        blp = curwp->w_dotp;
        bsize = (long)curwp->w_doto;
        flp = curwp->w_dotp;
        fsize = (long)(llength(flp)-curwp->w_doto+1);
        while (flp!=curbp->b_linep || lback(blp)!=curbp->b_linep) {
                if (flp != curbp->b_linep) {
                        flp = lforw(flp);
                        if (flp == curwp->w_markp[0]) {
                                rp->r_linep = curwp->w_dotp;
                                rp->r_offset = curwp->w_doto;
                                rp->r_size = fsize+curwp->w_marko[0];
                                return(TRUE);
                        }
                        fsize += llength(flp)+1;
                }
                if (lback(blp) != curbp->b_linep) {
                        blp = lback(blp);
                        bsize += llength(blp)+1;
                        if (blp == curwp->w_markp[0]) {
                                rp->r_linep = blp;
                                rp->r_offset = curwp->w_marko[0];
                                rp->r_size = bsize - curwp->w_marko[0];
                                return(TRUE);
                        }
                }
        }
        mlwrite(TEXT77);
/*              "Bug: lost mark" */
        return(FALSE);
}
//-

/// regtostr()
/* Copy all of the characters in the region to the string buffer.
** It is assumed that the buffer size is at least one plus the
** region size.
**
** char *buf;
** REGION *region;
**********************/
char *STD regtostr(char *buf, REGION *region)
{
   register LINE  *linep;
   register int   loffs;
   register long  rsize;
   register char  *ptr;

   ptr = buf;
   linep = region->r_linep;      /* Current line.  */
   loffs = region->r_offset;     /* Current offset.   */
   rsize = region->r_size;
   while (rsize--) {
      if (loffs == llength(linep)) {   /* End of line.      */
         *ptr = '\r';
         linep = lforw(linep);
         loffs = 0;
      } else {       /* Middle of line.   */
         *ptr = lgetc(linep, loffs);
         ++loffs;
      }
      ptr++;
   }
   *ptr = '\0';
   return buf;
}
//-

/// indent_region()
/*
** indent a region n tab-stops
 ***********************************************************************/
STD indent_region(f, n)
{
   register int inc; /* increment to next line [sgn(n)] */
   int count;

   if (curbp->b_mode&MDVIEW)  /* don't allow this command if   */
      return(rdonly()); /* we are in read only mode   */

   if (f == FALSE)
      count = 1;
   else
      count = n;
   n = reglines();

   /* loop thru indenting n lines */
   inc = ((n > 0) ? 1 : -1);
   while (n) {
      curwp->w_doto = 0;   /* start at the beginning */

      /* shift current line using tabs */
      if (!((curbp->b_mode & MDCMOD) &&
           (lgetc(curwp->w_dotp, curwp->w_doto) == '#')) ) {
            linsert(count, '\t');
      }

      /* advance/or back to the next line */
      forwline(TRUE, inc);
      n -= inc;
   }

   curwp->w_doto = 0;
   thisflag &= ~CFCPCN; /* flag that this resets the goal column */
   lchange(WFEDIT);  /* yes, we have made at least an edit */
   return(TRUE);
}
//-

/// undent_region()
/*
**undent a region n tab-stops
 ***********************************************************************/
STD undent_region(int f, int n)
{
   register int inc; /* increment to next line [sgn(n)] */
   int i, count;

   if (curbp->b_mode&MDVIEW)  /* don't allow this command if   */
      return(rdonly()); /* we are in read only mode   */

   if (f == FALSE)
      count = 1;
   else
      count = n;
   n = reglines();

   /* loop thru undenting n lines */
   inc = ((n > 0) ? 1 : -1);
   while (n) {
      /* unshift current line using tabs */
      for (i = 0; i < count; i++) {
         curwp->w_doto = 0;   /* start at the beginning */
         if ((curwp->w_dotp->l_used > 0) &&
             (lgetc(curwp->w_dotp, curwp->w_doto) == '\t')) {
            ldelete(1L, FALSE);
         }
      }

      /* advance/or back to the next line */
      forwline(TRUE, inc);
      n -= inc;
   }

   thisflag &= ~CFCPCN; /* flag that this resets the goal column */
   lchange(WFEDIT);  /* yes, we have made at least an edit */
   return(TRUE);
}
//-
