#include "Include.h"

/// XenoRC default definition
const char* xrc =
"set %qz div $wline 2\n"
"set %qv 0\n"
"set %hb $ztop\n"
"set %yes &lef $zyes 1\n"
"set %no &lef $zno 1\n"
"set %mb &cat &cat $progname \" \" $version\n"
"add-global-mode \"wrap\"\n"
//
// quote one line
//
"st-p q1\n"
    "beginning-of-line\n"
    "!force next-line\n"
    "!if &seq $status F\n"
        "!return\n"
    "!endif\n"
    "prev-line\n"
    "set $discmd F\n"
    "set-mark\n"
    "end-of-line\n"
    "!force forward-char\n"
    "copy-region\n"
    "prev-win\n"
    "!if &gre $curcol 1\n"
        "end-of-line\n"
        "newline\n"
    "!endif\n"
    "set %f $fillcol\n"
    "set $fillcol 0\n"
    "yank\n"
    "set $fillcol %f\n"
    "next-win\n"
    "beginning-of-line\n"
    "set $discmd T\n"
"!endm\n"
//
// quote all when quote window is displayed
//
"st-p qa\n"
    "set $discmd F\n"
    "!if &seq $cbufname %mb\n"
        "next-win\n"
    "!endif\n"
    "beginning-of-file\n"
    "set-mark\n"
    "end-of-file\n"
    "copy-region\n"
    "q0\n"
    "set $discmd F\n"
    "!if &gre $curcol 1\n"
        "end-of-line\n"
        "newline\n"
    "!endif\n"
    "set %f $fillcol\n"
    "set $fillcol 80\n"
    "yank\n"
    "newline\n"
    // empty the kill-buffer
    "set-mark\n"
    "copy-region\n"
    "set $fillcol %f\n"
    "set $discmd T\n"
"!endm\n"
//
// quote all when quote window is hidden
//
"st-p qz\n"
    "qw\n"
    "qa\n"
"!endm\n"
//
// remove quote window
//
"st-p q0\n"
    "set $discmd F\n"
    "!if &equal %qv 1\n"
        "!if &seq $cbufname %mb\n"
            "next-win\n"
        "!endif\n"
        "bind newline ^M\n"
        "mac qz ^XQ\n"
        "del-win\n"
        "set %qv 0\n"
    "!endif\n"
    "set $discmd T\n"
"!endm\n"
//
// display quote window / goto quote window
//
"st-p qw\n"
    "set $discmd F\n"
    "!if &seq $cbufname $zqh\n"
        "prev-win\n"
        "bind newline ^M\n"
    "!else\n"
        "!if &equal %qv 1\n"
            "next-win\n"
        "!else\n"
            "1 split-win\n"
            "next-win\n"
            "1 select-buf $zqh\n"
            "add-mode $zqc\n"
            "add-mode \"view\"\n"
            "!force %qz resize-win\n"
            "set %qv 1\n"
        "!endif\n"
        "mac q1 ^M\n"
        "mac qa ^XQ\n"
                "quote-msg\n"
    "!endif\n"
    "set $discmd T\n"
"!endm\n"
"mac qw M-Q\n"
"mac qw ^XO\n"
"mac qw ^Q\n"
"mac q0 ^C\n"
"mac q0 ^X1\n"
"mac qz ^XQ\n"
//
// spelling check
//
"st-p sp\n"
    "q0\n"
    "xspell\n"
"!endm\n"
"mac sp M-C\n"
"mac sp ^XV\n"
//
// update message header
//
"st-p mm\n"
    "!if &seq $cbufname $zqh\n"
        "next-win\n"
        "set %qq 1\n"
    "!else\n"
        "prev-win\n"
        "set %qq 0\n"
    "!endif\n"
    "end-of-file\n"
    "set-mark\n"
    "beginning-of-file\n"
    "kill-region\n"
    // $zhdr == 0 for message, 1 for file description, else normal file
    // $zhdr == 3 for hack&slash hacked message
    "!if &equ $zhdr 0\n"
        "msg-area $ztype\n"  // BOGUS this is now RIGHT-JUSTIFY
        "1 goto-line\n"
        "overwrite-string &cat &cat &cat $zto $to \" \" $zdest\n"
        "2 goto-line\n"
        "insert-string &cat $zsub $subject\n"
    "!else\n"
        // using $scrname as ~b body name
        "!if &equ $zhdr 1\n"
            "insert-string &cat \"File description for \" $scrname\n"
        "!else\n"
            "!if &equ $zhdr 3\n"
                "insert-string &cat $zto $to\n"
                "newline\n"
                "insert-string &cat $zsub $subject\n"
            "!else\n"
                "insert-string &cat \"File: \" $scrname\n"
            "!endif\n"
        "!endif\n"
    "!endif\n"
    "1 goto-line\n"
    "add-mode $zhbc\n"
    "next-win\n"
    // return to quote window
    "!if &equ %qq 1\n"
        "next-win\n"
    "!endif\n"
"!endm\n"
//
// change subject
//
"st-p sub\n"
    "set $discmd F\n"
    "set %default $subject\n"
    "set %a @$zsub\n"
    "!if &seq %a $zsub\n"
        "set %a \"\"\n"
    "!endif\n"
    "!if &not &seq %a \"\"\n"
        "set $subject %a\n"
        "mm\n"
    "!endif\n"
    "set $discmd T\n"
"!endm\n"
"mac sub M-J\n"
//
// change recipient
//
"st-p rec\n"
    "set $discmd F\n"
    "set %default $to\n"
    "set %a @$zto\n"
    "!if &seq %a $zto\n"
        "set %a \"\"\n"
    "!endif\n"
    "!if &not &seq %a \"\"\n"
        "set $to %a\n"
        "mm\n"
    "!endif\n"
    "set $discmd T\n"
"!endm\n"
"mac rec M-T\n"
//
// send broadcast message
//
"st-p bc\n"
    "set $discmd F\n"
    "q0\n"
    "who\n"
    "set %default \"\"\n"
    "set %a @\"Send to which port (i.e. TR0): \"\n"
    "!if &not &seq %a \"\"\n"
        "set %b @\"Message: \"\n"
        "!if &not &seq %b \"\"\n"
            "set %c &cat &cat \"dlg:broadcast >nil: -p ~\"\" %a \"~\" -f ~\"\"\n"
            "set %c &cat &cat &cat %c $from \"~\" -m ~\"\" %b\n"
            "set %c &cat %c \"~\"\"\n"
            "exec-program %c\n"
            "print \"Message sent\"\n"
        "!endif\n"
    "!endif\n"
    "set $discmd T\n"
"!endm\n"
"mac bc M-!\n"
//
// toggle modes: word wrap, overstrike
//
"st-p tog\n"
"!if &seq $cbufname %mb\n"
  "set $discmd F\n"
  "set %default \"\"\n"
  "set %a @\"w=wrap on/off, i=insert on/off? \"\n"
  "!if &seq %a \"w\"\n"
    "set $cmode &bxo $cmode 1\n"
    "!if &ban $cmode 1\n"
      "print \"wrap on\"\n"
    "!else\n"
      "print \"wrap off\"\n"
    "!endif\n"
  "!endif\n"
  "!if &seq %a \"i\"\n"
    "set $cmode &bxo $cmode 32\n"
    "!if &ban $cmode 32\n"
      "print \"insert off\" \n"
    "!else\n"
      "print \"insert on\"\n"
    "!endif\n"
  "!endif\n"
  "set $discmd T\n"
"!endif\n"
"!endm\n"
"mac tog ^XM\n"
//
// abort message
//
"st-p oops\n"
    "print $zabort\n"
    "set %a &gtk\n"
    "!if &seq &lower %a %yes\n"
        "1 exit-emacs\n"
    "!endif\n"
    "clr-msg\n"
"!endm\n"
"mac oops M-^[\n"
//
// save and quit
//
"st-p bye\n"
    "print $zbye\n"
    "set %a &gtk\n"
    "!if &seq %a \"~r\"\n"
        "set %a %yes\n"
    "!endif\n"
    "!if &seq &lower %a %yes\n"
        "q0\n" // be friendly for fortune cookie
        //"select-buf %mb\n"
        "save-file\n"
        "0 exit-emacs\n"
    "!endif\n"
    "clr-msg\n"
"!endm\n"
"mac bye ^Z\n"
"mac bye ^X^C\n"
//
// c-net syndrome: when they type an s, check if it is /s or .s at start of line
//
"st-p cnet\n"
"!if &equ $curcol 1\n"
  // "s" typed at second column, check what is in first column
  "backward-char\n"
  "!if &or &equ $curchar 46 &equ $curchar 47\n"
    "del-next-char\n"
    "update-screen\n"
    "bye\n"
    "!return\n"
  "!endif\n"
  "forward-char\n"
"!endif\n"
// "s" typed at other than second column
// or not following a dot or slash
"!if &ban $cmode 32\n"
  "!force overwrite-string \"s\"\n"
"!else\n"
  // force word-wrap if necessary
  "!if &les $fillcol $curcol\n"
    "wrap-word\n"
  "!endif\n"
  "!force insert-string \"s\"\n"
"!endif\n"
"!endm\n"
"mac cnet s\n"
//
// print file
//
"st-p prt\n"
    "print \"Write to printer? (yes/NO) \"\n"
    "set %a &gtk\n"
    "!if &seq &lower %a %yes\n"
        "!force write-file \"PRT:\"\n"
    "!endif\n"
    "clr-msg\n"
"!endm\n"
"mac prt ^X^P\n"

//
// display help window
//
"st-p hep\n"
    "set $discmd F\n"
    "add-mode $zhelpc\n"
    "del-other-wins\n"
    "select-buf $zmore\n"
    "!force read-file &fin \"ScreenEdit.hlp\"\n"
    // find end of file so we know when to quit
    "end-of-file\n"
    "set %n $curline\n"
    "beginning-of-file\n"
"*again\n"
    "update-screen\n"
    "set %a &gtk\n"
    "!if &seq %a \"~r\"\n"
        "set %a %yes\n"
    "!endif\n"
    "!if &seq $curline %n\n"
        "set %a %no\n"
    "!endif\n"
    "!if &seq &lower %a %yes\n"
        "$wline next-line\n"
        "!if &seq $curline %n\n"
             "set %a %no\n"
        "!else\n"
             "$wline prev-line\n"
             "next-page\n"
             "!goto again\n"
        "!endif\n"
    "!endif\n"
    "!if &seq &lower %a %no\n"
        "select-buf %hb\n"
        "add-mode $zhbc\n"
        "1 split-win\n"
        "2 resize-win\n"
        "1 goto-line\n"
        "next-win\n"
        "1 select-buf %mb\n"
        "add-mode $zmsgc\n"
        "bind newline ^M\n"
        "set %qv 0\n"
        "set $discmd T\n"
        "!return\n"
    "!endif\n"
    // answered something other than y or n
    "!goto again\n"
"!endm\n"
"mac hep M-?\n"
//
// update colors in all active windows
//
"st-p upc\n"
    "!if &seq $cbufname $zqh\n"
        "prev-win\n"
    "!endif\n"
    // current buf is message buffer
    "add-mode $zmsgc\n"
    "next-win\n"
    "!if &seq $cbufname $zqh\n"
        "add-mode $zqc\n"
        "next-win\n"
    "!endif\n"
    "add-mode $zhbc\n"
    "next-win\n"
"!endm\n"
//
"name-buf %mb\n"
"add-mode \"wrap\"\n"
"1 select-buf \"q\"\n"
"name-buf $zqh\n"
"greet $zhello\n"
"select-buf %hb\n"
"1 split-win\n"
"2 resize-win\n"
"next-win\n"
"1 select-buf %mb\n"
"mm\n"
"add-mode $zmsgc\n"
"set $discmd T\n"
"set $fini T\n"
;
//-

/// init_xenorc()
BUFFER *init_xenorc(void)
{
    BUFFER *xenorc_buf;
    LINE *lp1;
    LINE *lp2;
    int nbytes;
    const char *p;

    xenorc_buf = bfind("RC", TRUE, BFINVS);
    if (!xenorc_buf) return NULL;

    for (p = xrc; *p; p += nbytes+1) {
        for (nbytes = 0; p[nbytes] != '\n'; ++nbytes) {}
        if ((lp1=lalloc(nbytes)) == NULL) {
            return NULL;
        }
        lp2 = lback(xenorc_buf->b_linep);
        lp2->l_fp = lp1;
        lp1->l_fp = xenorc_buf->b_linep;
        lp1->l_bp = lp2;
        xenorc_buf->b_linep->l_bp = lp1;
        memcpy(lp1->l_text, p, nbytes);
    }
    return xenorc_buf;
}
//-

