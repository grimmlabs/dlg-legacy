#include "Include.h"

/// modename
CONST char *modename[] =        /* name of modes     */
{
   "WRAP",
   "",
   "",
   "EXACT",
   "VIEW",
   "OVER",
   "MAGIC",
   "",
   "",
   ""
};
//-

// Local functions

STD adjustmode(int, int);


/// setmod()
/* prompt and set an editor mode */
STD setmod(int f, int n)
{
   return(adjustmode(TRUE, FALSE));
}
//-

/// delmode()
/* prompt and delete an editor mode */
STD delmode(int f, int n)
{
   return(adjustmode(FALSE, FALSE));
}
//-

/// setgmode()
/* prompt and set a global editor mode */
STD setgmode(int f, int n)
{
   return(adjustmode(TRUE, TRUE));
}
//-

/// delgmode()
/* prompt and delete a global editor mode */
STD delgmode(int f, int n)
{
   return(adjustmode(FALSE, TRUE));
}
//-


// Local functions

/// adjustmode()
/* change the editor mode status
**
** int kind;      TRUE = set,    FALSE = delete
** int global;    TRUE = global flag,  FALSE = current buffer flag
****************/
STD adjustmode(int kind, int global)
{
   register char *scan;    /* scanning pointer to convert prompt */
   register int i;         /* loop index */
   register int status;    /* error return on input */
   register int uflag;     /* was modename uppercase? */
   char prompt[50];  /* string to prompt user with */
   char cbuf[NPAT];     /* buffer to recieve mode name into */

   /* build the proper prompt string */
   if (global)
      strcpy(prompt,TEXT62);
/*                            "Global mode to " */
   else
      strcpy(prompt,TEXT63);
/*                            "Mode to " */

   if (kind == TRUE)
      strcat(prompt, TEXT64);
/*                             "add: " */
   else
      strcat(prompt, TEXT65);
/*                             "delete: " */

   /* prompt the user and get an answer */

   status = mlreply(prompt, cbuf, NPAT - 1);
   if (status != TRUE)
      return(status);

   /* make it uppercase */

   scan = cbuf;
   uflag = (*scan >= 'A' && *scan <= 'Z');
   while (*scan)
      uppercase(scan++);

   /* test it first against the colors we know */
   if ((i = lookup_color(cbuf)) != -1) {

      /* finding the match, we set the color */
      if (global) {
         if (uflag)
            gfcolor = i;
         else
            gbcolor = i;
      } else
         if (uflag)
            curwp->w_fcolor = i;
         else
            curwp->w_bcolor = i;

      curwp->w_flag |= WFCOLR;
      mlerase();
      return(TRUE);
   }

   /* test it against the modes we know */

   for (i=0; i < NUMMODES; i++) {
      if (strcmp(cbuf, modename[i]) == 0) {
         /* finding a match, we process it */
         if (kind == TRUE)
            if (global) {
               gmode |= (1 << i);
               if ((1 << i) == MDOVER)
                  gmode &= ~MDREPL;
               else if ((1 << i) == MDREPL)
                  gmode &= ~MDOVER;
            } else {
               curbp->b_mode |= (1 << i);
               if ((1 << i) == MDOVER)
                  curbp->b_mode &= ~MDREPL;
               else if ((1 << i) == MDREPL)
                  curbp->b_mode &= ~MDOVER;
            }
         else
            if (global)
               gmode &= ~(1 << i);
            else
               curbp->b_mode &= ~(1 << i);
         /* display new mode line */
         if (global == 0)
            upmode();
         mlerase();  /* erase the junk */
         return(TRUE);
      }
   }

   mlwrite(TEXT66);
/*              "No such mode!" */
   return(FALSE);
}
//-



