#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <dos.h>
#include <exec/types.h>

#include <dlg/user.h>
#include <dlg/input.h>

#include <proto/dlg.h>

#include <pragmas/dlg.h>


extern struct USER_DATA UserDat;
extern struct Ram_File  RStruct;
extern struct Library  *DLGBase;

/// DLGInput
long __stdargs DLGInput(   char *template,      char *string,  char *defstring,  USHORT length,
                           USHORT typelength,   char  ftype,   char *query,      ...)

{
   char                 t[256];
   struct   UserInfo   *ui       =  NULL;
   struct   Query      *q        =  NULL;

   XASPrintf(&UserDat, t, query, (char *)(&query+1));

   ui = calloc(1,sizeof(struct UserInfo));
   q =  calloc(1,sizeof(struct Query));

 q->prompt     = t;
 q->template   = template;
 q->string     = string;
 q->defstring  = (char *)defstring;
 q->valid      = NULL;
 q->length     = length;
 q->typelength = typelength;
 q->flags      = 0;

 switch(ftype)
       {case 1: q->flags |=  QUERY_UPCASE;
                break;
        case 2: q->flags |=  QUERY_HIDDEN;
                break;
        case 3: q->flags |=  QUERY_CAPITAL;
                break;
        case 4: q->valid  = "0123456789-";
                break;
        case 5: q->flags |=  QUERY_UPCASE;
                q->valid  = "YyNn";
                break;
        case 6: q->flags |=  QUERY_GUESS;
                break;
        case 7: q->flags |=  QUERY_NOSTACK;
                break;
        case 8: q->flags |=  QUERY_CAPITAL;
                q->flags |=  QUERY_GUESS;
                break;
       }

 if ((LONG)template == 1)
    {q->template = NULL;
     q->flags   |= QUERY_NODEFEDT;
    }

 ui->User = &UserDat;
 ui->Ram  = &RStruct;

 Chk_Abort();
 return(DLGQuery(q, ui));
}
//-
/// iinput
long __stdargs iinput(long lower, long upper, long def, char *query, ...)
{
   char              t[256];
   struct   UserInfo ui;

   ui.User = &UserDat;
   ui.Ram  = &RStruct;

   XASPrintf(&UserDat, t, query, (char *)(&query+1));

   Chk_Abort();
   return(IntQuery(t, lower, upper, def, &ui));
}
//-
/// finput
char __stdargs finput(char opt, char *query, ...)
{
   char              t[256];
   struct   UserInfo ui;

   ui.User = &UserDat;
   ui.Ram  = &RStruct;

   XASPrintf(&UserDat, t, query, (char *)(&query+1));

   Chk_Abort();
   return((char)BoolQuery(t, opt, &ui));
}
//-
