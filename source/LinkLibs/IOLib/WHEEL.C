#include <dos/dosextens.h>
#include <proto/dos.h>
#include <stdio.h>
#include <fcntl.h>
#include <dlg/user.h>
#include <proto/dlg.h>

#include <link/io.h>

#include <pragmas/dlg.h>

extern struct USER_DATA UserDat;

char wheel[9] = "\010|\010/\010-\010\\";
char wheelpos;
BPTR wheelfh;

void StartWheel(void)

{wheelfh  = Output();
 wheelpos = 0;

 AFPrintf(&UserDat, wheelfh, "%a3 ");
}

void TurnWheel(void)

{Write(wheelfh, wheel+wheelpos, 2L);

 wheelpos += 2;
 if (wheelpos == 8)  wheelpos=0;
}


void StopWheel(void)

{AFPrintf(&UserDat, Output(), "%a6\010 \010");
}
