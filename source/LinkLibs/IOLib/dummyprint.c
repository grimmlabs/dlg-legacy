#include <exec/types.h>
#include <stdio.h>
#include <dos/dosextens.h>
#include <proto/dos.h>
#include <proto/dlg.h>
#include <dlg/user.h>

#include <pragmas/dlg.h>

extern struct Library   *DLGBase;
extern struct USER_DATA  UserDat;


int DummyPrintf(const char *fmt,...)

{return(XAFPrintf(&UserDat,Output(),(char *)fmt,(char *)(&fmt+1)));
}


int DummySPrintf(char *buf,const char *fmt,...)

{return(XASPrintf(&UserDat,buf,(char *)fmt,(char *)(&fmt+1)));
}
