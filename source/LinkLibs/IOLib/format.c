#include <exec/types.h>
#include <dos/dosextens.h>
#include <dlg/user.h>

#include <proto/dos.h>
#include <proto/dlg.h>

#include <pragmas/dlg.h>

extern struct Library   *DLGBase;

LONG __stdargs AFPrintf(struct USER_DATA *User,BPTR fh,char *fmt,...)

{return(XAFPrintf(User,fh,fmt,(char *)(&fmt+1)));
}


LONG __stdargs ASPrintf(struct USER_DATA *User,char *str,char *fmt,...)

{return(XASPrintf(User,str,fmt,(char *)(&fmt+1)));
}
