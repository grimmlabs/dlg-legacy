#include <exec/types.h>
#include <string.h>
#include <stdlib.h>

#include <pragmas/dlg.h>

BOOL InsertStack(char *stack, char *string)

{long  len1;
 long  len2;
 char  tempstring[255];

 len1 = strlen(stack);
 len2 = strlen(string);

 if (len1+len2 > 254)  return(FALSE);

 movmem(stack,      tempstring, len1);
 movmem(string,     stack,      len2);
 movmem(tempstring, stack+len2, len1);
 stack[len1+len2] = 0;

 return(TRUE);
}
