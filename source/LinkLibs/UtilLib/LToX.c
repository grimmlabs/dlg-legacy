#include <exec/types.h>

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <proto/dlg.h>
#include <pragmas/dlg.h>

extern struct Library *DLGBase;

void LToX(ULONG num, char *rc)
{
	char t[10];
	int z;

	strcpy(rc,"00000000");
	strcpy(t,"");

	z = stcl_h(t,num);

	rc[(8-z)] = '\0';

	strcat(rc,t);
	Upper(rc);

	return;
}
