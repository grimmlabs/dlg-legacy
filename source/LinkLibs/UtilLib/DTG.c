#include <exec/types.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <time.h>

char *DTG(char *timestring,int FidoTime)
{
	UBYTE clock[10];

	char *day[7];
	char *mon[13];

	mon[1]="Jan";
	mon[2]="Feb";
	mon[3]="Mar";
	mon[4]="Apr";
	mon[5]="May";
	mon[6]="Jun";
	mon[7]="Jul";
	mon[8]="Aug";
	mon[9]="Sep";
	mon[10]="Oct";
	mon[11]="Nov";
	mon[12]="Dec";

	day[0]="Sun";
	day[1]="Mon";
	day[2]="Tue";
	day[3]="Wed";
	day[4]="Thu";
	day[5]="Fri";
	day[6]="Sat";

	getclk(clock);

	if(!FidoTime)
		sprintf(timestring,"%s %02d %s %2.2d %02d:%02d:%02d",day[clock[0]],clock[3],mon[clock[2]],clock[1]+80,clock[4],clock[5],clock[6]);
	else
		sprintf(timestring,"%s %2d %s %2.2d %02d:%02d",day[clock[0]],clock[3],mon[clock[2]],clock[1]+80,clock[4],clock[5]);

	return(timestring);
}

