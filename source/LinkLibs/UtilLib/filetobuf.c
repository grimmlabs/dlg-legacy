#include <stdlib.h>
#include <stdio.h>
#include <fcntl.h>
#include <exec/types.h>

#include <pragmas/dlg.h>

BOOL FileToBuf(char **buf,char *filename,long filesize,long padding,long offset)

{FILE *fp;
 char *dbuf;

 filesize -= offset;

 /* Allocate memory plus padding for buffer */
 if (!(*buf))
     if (!(*buf = malloc(filesize+padding+1L)))  return(FALSE);

 dbuf = *buf;

 /* open the file for read only */
 if (!(fp=fopen(filename, "r")))   return(FALSE);

 /* seek to the offset in the file */
 if (offset)  fseek(fp, offset, SEEK_SET);

 /* read entire file */
 fread(dbuf, filesize, 1, fp);
 fclose(fp);

 /* null terminate buffer */
 dbuf[filesize] = 0;

 return(TRUE);
}
