#include <exec/types.h>
#include <string.h>
#include <stdio.h>

#include <pragmas/dlg.h>

#define PERDAY 86400L
#define YEARS (year-78)
#define LEAPS ((year-77)/4)

BOOL StringToDate(char *dstring, long *retval)

{char month[28];
 long day;
 long year;
 long hours;
 long mins;
 long days;

 sscanf(dstring+4, "%d %s %d %d:%d", &day,month,&year,&hours,&mins);

 days = (long)((YEARS*365)+LEAPS);

 if (!strcmp(month,"Feb"))
      days += 31L;
  else
     {if (!strcmp(month,"Mar"))  days +=  59L;
 else if (!strcmp(month,"Apr"))  days +=  90L;
 else if (!strcmp(month,"May"))  days += 120L;
 else if (!strcmp(month,"Jun"))  days += 151L;
 else if (!strcmp(month,"Jul"))  days += 181L;
 else if (!strcmp(month,"Aug"))  days += 212L;
 else if (!strcmp(month,"Sep"))  days += 243L;
 else if (!strcmp(month,"Oct"))  days += 273L;
 else if (!strcmp(month,"Nov"))  days += 304L;
 else if (!strcmp(month,"Dec"))  days += 334L;

      if ((year/4) * 4 == year)  days++;
     }

 days += (day - 1L);

 *retval = (((long)days*PERDAY)+((long)hours*3600L)+((long)mins*60L));
 return(TRUE);
}
