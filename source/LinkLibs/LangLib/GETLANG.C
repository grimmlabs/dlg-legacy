#include <stdio.h>
#include <string.h>
#include <fcntl.h>
#include <exec/types.h>

#include <dlg/portconfig.h>
#include <dlg/resman.h>

#include <Link/Config.h>

#include <proto/dlg.h>

#include <pragmas/dlg.h>


extern struct Library *DLGBase;


char **getlang(char *port)

{struct LangStruct      *ls;
 struct Global_Settings  Globals;
 char                    lport[4];

 if (port)
     if (*port)
        {if (!(ls = GetLang(port)))
               if (!ReadGlobals(port, &Globals))
                  {if (LoadLang(port, Globals.Language) != RMNOERR)
                       LoadLang(port, "English");
                   ls = GetLang(port);
                  }

         if (ls)  return(ls->strings);
        }

 if (GetDevName(lport) != -1)
    {if (!(ls = GetLang(lport)))
           if (!ReadGlobals(lport, &Globals))
              {if (LoadLang(lport, Globals.Language) != RMNOERR)
                   LoadLang(lport, "English");
               ls = GetLang(lport);
              }
     if (ls)  return(ls->strings);
    }

 if (!(ls = GetLang("TL0")))
       if (!ReadGlobals("TL0", &Globals))
          {if (LoadLang("TL0", Globals.Language) != RMNOERR)
               LoadLang("TL0", "English");
           ls = GetLang("TL0");
          }

 if (ls)  return(ls->strings);
 return(NULL);
}
