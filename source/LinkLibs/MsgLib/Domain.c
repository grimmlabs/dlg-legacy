#include <exec/types.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <dos.h>

#include <dlg/misc.h>

#include <link/io.h>

#include <proto/exec.h>
#include <proto/dos.h>
#include <proto/dlg.h>

#include <pragmas/dlg.h>

#define  DOMAIN_FILE_NAME  "DLGConfig:Fido/Domains"

extern struct Library *DLGBase;

// 0  Total failure
// 1  No match, returning default
// 2  Found match

int   GetMyAddress(char *BaseAddr, char *MyAddr, struct fido *SrcFido)
{
   BOOL InADomain =  FALSE;
   BOOL FoundDefault =  FALSE;
   BOOL MyAddrOK  =  TRUE;

   BPTR fh = NULL;

   char  t[512];
   char *p;
   char *z;
   char *DefaultAddr;
   char *DomainAddr;
   char *ZoneToFind;
   char BackupMyAddr[64] = "";

   int i;

   if(!strlen(BaseAddr)) return(0);

   if(!strlen(MyAddr))
   {
      if(!SrcFido)   return(0);

      ASPrintf(NULL,BackupMyAddr,"%ld:%ld/%ld.%ld",SrcFido->zone, SrcFido->node, SrcFido->net, SrcFido->point);
      MyAddr = strdup(BackupMyAddr);
      MyAddrOK = FALSE;
   }

   z = strdup(BaseAddr);
   ZoneToFind = strdup(strtok(z,":"));

   fh = Open(DOMAIN_FILE_NAME,MODE_OLDFILE);

   if(!fh) return(0);

   while(FGets(fh,t,511))
   {
      char *args[3];
      int   cnt;

      StripSpaces(t);

      p = strdup(strtok(t,"\n"));

///   Strip comments
      for(i = 0; i < strlen(p); i++)
      {
         if(*(p+i) == ';')
         {
            *(p+i) = 0;
            break;
         }
      }
//-

      if(!strlen(p)) continue;

      cnt = ArgParse(p,args,2);
      if(cnt == 0) continue;

///   DEFAULT
      if(!Stricmp(args[0],"DEFAULT"))
      {
         FoundDefault = TRUE;
         DefaultAddr = strdup(args[1]);
         DomainAddr = strdup(DefaultAddr);
         continue;
      }
//-

///   DOMAIN
      if(!Stricmp(args[0],"DOMAIN"))
      {
         InADomain = TRUE;
         continue;
      }
//-

///   @DOMAIN
      if(!Stricmp(args[0],"@DOMAIN"))
      {
         if(!InADomain) continue;

         InADomain = FALSE;
         continue;
      }
//-

///   Address
      if(!Stricmp(args[0],"Address"))
      {
         if(!InADomain) continue;

         DomainAddr = strdup(args[1]);
         continue;
      }
//-

///   Zone
      if(!Stricmp(args[0],"Zone"))
      {
         if(!InADomain) continue;

         if(!Stricmp(ZoneToFind,args[1]))
         {
            Close(fh); fh = NULL;
            if(MyAddrOK) strcpy(MyAddr,DomainAddr);

            if(SrcFido)
            {
               char *r;

               r = strdup(DomainAddr);

               SrcFido->zone  = atoi(strtok(r,":"));
               SrcFido->net   = atoi(strtok(NULL,"/"));
               SrcFido->node  = atoi(strtok(NULL,"."));
               SrcFido->point = atoi(strtok(NULL,"\0"));
            }

            return(2);
         }
         else
         {
            continue;
         }
      }
//-
   }

   Close(fh); fh = NULL;

   if(FoundDefault)
   {
      if(MyAddrOK) strcpy(MyAddr,DefaultAddr);

      if(SrcFido)
      {
         char *r;

         r = strdup(DefaultAddr);

         SrcFido->zone  = atoi(strtok(r,":"));
         SrcFido->net   = atoi(strtok(NULL,"/"));
         SrcFido->node  = atoi(strtok(NULL,"."));
         SrcFido->point = atoi(strtok(NULL,"\0"));
      }

      return(1);
   }

   return(0);
}

