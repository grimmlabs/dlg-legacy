#include <devices/tpt.h>

#include <DLG/Debug.h>

#include <proto/exec.h>

extern struct MsgPort     *drport;
extern struct MsgPort     *dport;
extern UBYTE               tdevname[];

extern int                 DEBUG_PORT_TYPE;

void TDebug(char *str)
{
   struct debug_mess   dmess;

   if(!drport)  return;

   dmess.num = DEBUG_PORT_TYPE;
   dmess.str = str;

   if(DEBUG_PORT_TYPE == HANDLER_PORT)
      dmess.port = (char *) tdevname;
   else
      dmess.port = NULL;

   dmess.mess.mn_Node.ln_Type = NT_MESSAGE;
   dmess.mess.mn_ReplyPort = drport;
   dmess.mess.mn_Length = sizeof(struct debug_mess);

   PutMsg(dport, (struct Message *) &dmess);

   WaitPort(drport);
   GetMsg(drport);

   return;
}


