#include <stdio.h>
#include <dlg/msg.h>
#include <dlg/file.h>

void BuildPrivFlag(struct Msg_Area *MsgArea,struct Msg_Log *Log,int level)
{
  if(level==255 || (level>=MsgArea->lsysop && level<=MsgArea->usysop)) {
    Log->Priv_Flag=0xff;
    return;
  }

  Log->Priv_Flag=0;

  if(level>=MsgArea->lwrite && level<=MsgArea->uwrite)
    Log->Priv_Flag|=Enter_Priv;

  if(level>=MsgArea->lkill && level<=MsgArea->ukill)
    Log->Priv_Flag|=Kill_Priv;

  if(level>=MsgArea->lforward && level<=MsgArea->uforward)
    Log->Priv_Flag|=Forward_Priv;

  if(level>=MsgArea->lcopy && level<=MsgArea->ucopy)
    Log->Priv_Flag|=Hurl_Priv;

  if(level>=MsgArea->ledit && level<=MsgArea->uedit)
    Log->Priv_Flag|=Re_Edit;

  Log->Priv_Flag |= Log->uflag;
  Log->Priv_Flag &= ~Log->dflag;
}

void BuildFPrivFlag(struct Msg_Area *MsgArea,struct Msg_Log *Log,int level)
{
  if(level==255 || (level>=MsgArea->lsysop && level<=MsgArea->usysop)) {
    Log->Priv_Flag=0xff;
    return;
  }

  Log->Priv_Flag=0;

  if(level>=MsgArea->lwrite && level<=MsgArea->uwrite)
    Log->Priv_Flag|=Upload_Priv;

  if(level>=MsgArea->lkill && level<=MsgArea->ukill)
    Log->Priv_Flag|=Kill_Priv;

  if(level>=MsgArea->lforward && level<=MsgArea->uforward)
    Log->Priv_Flag|=Download_Priv;

  if(level>=MsgArea->lcopy && level<=MsgArea->ucopy)
    Log->Priv_Flag|=Hurl_Priv;

  Log->Priv_Flag |= Log->uflag;
  Log->Priv_Flag &= ~Log->dflag;
}
