#include <exec/types.h>
#include <proto/dos.h>
#include <stdio.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <dlg/misc.h>
#include <proto/dlg.h>

#include <pragmas/dlg.h>

FILE   *questfp;
USHORT  qpos;

extern struct Global_Settings Globals;
extern struct USER_DATA       UserDat;
extern struct Ram_File        RStruct;

extern struct Library *DLGBase;


BYTE OpenQuest(char *filename)

{qpos = 0;

 if ((questfp = fopen(filename,"r"))==NULL)
      return(-1);

 return(0);
}


BOOL NextQuest(struct Question *q)

{char buffer [1024];
 char obuffer[1024];
 char port[4];

 *(q->template) = 0;

 while(fgets(buffer, 1024, questfp))
      {if (buffer[0] != '%')
          {qpos = 0;
           GetDevName(port);
           TranslateBuffer(buffer, obuffer, 1024, &UserDat, &RStruct, port);
           DispBuffer(Output(), obuffer, &qpos, 0, NULL, 0, NULL, &UserDat);
           continue;
          }

       switch(buffer[1])
             {case 's':
              case 'S': return(FALSE);

              case 't':
              case 'T': buffer[1] = '\n';
                        buffer[strlen(buffer)-1] = 0;
                        strncpy(q->template, &buffer[1], 61);
                        q->template[60] = 0;
                        break;

              case 'q':
              case 'Q': buffer[1] = '\n';
                        buffer[strlen(buffer)-1] = 0;
                        strncpy(q->quest, &buffer[1], 61);
                        q->quest[60] = 0;
                        return(TRUE);
             }
      }

 return(FALSE);
}


void CloseQuest(void)

{fclose(questfp);
}
