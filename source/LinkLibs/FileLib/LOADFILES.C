#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <fcntl.h>
#include <exec/types.h>
#include <dlg/resman.h>
#include <dlg/file.h>

#include <link/io.h>

#include <proto/dos.h>
#include <proto/dlg.h>

#include <pragmas/dlg.h>

extern struct Library  *DLGBase;
extern char           **SA;
extern struct USER_DATA UserDat;

SHORT LoadFiles(long area, char **pointer, char *pswd)
{
   ULONG size;
   SHORT numfiles;
   char *mypointer;
   char  filename[24];

   /* if pointer is already allocated to something, free it */
   if (*pointer)
   {
      free(*pointer);
      *pointer = NULL;
   }

   BorrowArea(area,pswd,"",64,FILELOCK|WRITELOCK);

   ASPrintf(NULL, filename, "FILE:%d/File.dat", area);

   if (FileSize(filename, &size) == -1)
   {
      FreeArea(area,pswd,FILELOCK|WRITELOCK);
      return(FALSE);
   }

   if (!size)
   {
      FreeArea(area,pswd,FILELOCK|WRITELOCK);
      return(FALSE);
   }

   if (!(*pointer = malloc(size)))
   {
      FreeArea(area,pswd,FILELOCK|WRITELOCK);
      AFPrintf(&UserDat,Output(),SA[3620],size);
      return(FALSE);
   }

   mypointer = *pointer;
   GetFirstStruct(filename, mypointer, size);

   FreeArea(area,pswd,FILELOCK|WRITELOCK);
   numfiles = (size/sizeof(struct QuickFile));
   return(numfiles);
}
