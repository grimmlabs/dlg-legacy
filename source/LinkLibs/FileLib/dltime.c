#include <stdio.h>
#include <exec/types.h>

#include <proto/dlg.h>

#include <pragmas/dlg.h>

extern struct Library *DLGBase;

LONG DLTime(LONG size,LONG baud,UBYTE eff,char *ext)

{long dlt;

 if (!Strnicmp(ext, "TL", 2))  return(0);
 if (!baud)                    return(0);

 dlt = size / ((baud * (long)eff)/1000);

 return(dlt);
}
