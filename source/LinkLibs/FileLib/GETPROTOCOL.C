#include <stdio.h>
#include <string.h>
#include <dlg/file.h>
#include <dlg/user.h>

#include <Link/File.h>
#include <Link/io.h>

#include <proto/dos.h>
#include <proto/dlg.h>

#include <pragmas/dlg.h>

extern struct Library *DLGBase;

BYTE GetProtocol(char *query,struct USER_DATA *user,SHORT flags,char forceflag,char *ext,char *stack)

{char valid[80],inp[2];

 inp[0]='\0';

 if (!Strnicmp(ext,"tl",2))  return(1);

 if (!forceflag)
    {if ((flags & PROTO_SEND) && user->send)     inp[0] = user->send;
     if ((flags & PROTO_REC)  && user->receive)  inp[0] = user->receive;
    }

 if (inp[0])
    {ListProtocols(user->User_Level,valid,flags,0);
     if (!index(valid,inp[0])) inp[0]='\0';
    }

 if (!inp[0])
    {ListProtocols(user->User_Level,valid,flags,1);
     DLGInput(NULL,inp,NULL,1,255,1,query);
     AFPrintf(NULL, Output(), "\n\n");
     if(!index(valid,inp[0])) return(0);
    }

 return(inp[0]);
}
