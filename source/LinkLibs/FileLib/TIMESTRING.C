#include <stdio.h>
#include <string.h>
#include <exec/types.h>

#include <link/io.h>

#include <proto/dlg.h>

#include <pragmas/dlg.h>


BOOL TimeString(LONG secs,char *str)

{long hours;
 long mins;

 if (!secs)
    {strcpy(str,"n/a");
     return(TRUE);
    }

 hours = (secs / 3600);
 if (hours)  secs = secs % 3600;

 mins = (secs / 60);
 if (mins)  secs = secs % 60;

 if (hours)
     ASPrintf(NULL, str, "%1d:%02d:%02ld", hours, mins, secs);
    else
     if (mins)
         ASPrintf(NULL, str, "%d:%02ld", mins, secs);
       else
         ASPrintf(NULL, str, "0:%02ld", secs);

 return(TRUE);
}
