#include <fcntl.h>
#include <stdio.h>
#include <dlg/file.h>
#include <proto/dlg.h>

#include <pragmas/dlg.h>

extern struct Library *DLGBase;

LONG LoadProtocol(struct Protocol *protocol,char letter)

{protocol->letter = letter;

 return(GetStruct("dlgconfig:misc/Protocols.bbs",(char *)protocol,sizeof(struct Protocol),1));
}
