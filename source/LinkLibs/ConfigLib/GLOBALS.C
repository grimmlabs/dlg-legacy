#include <stdio.h>
#include <string.h>
#include <fcntl.h>
#include <exec/types.h>
#include <dlg/portconfig.h>
#include <proto/dlg.h>

#include <link/io.h>

#include <pragmas/dlg.h>

extern struct Library *DLGBase;

int                 ReadGlobals(char *port, struct Global_Settings *globals)

{
   struct Port         PortInfo;
   char                filename[80];

   ASPrintf(NULL, filename, "DlgConfig:Port/%s.port", port);
   if (GetFirstStruct(filename, (char *) &PortInfo, sizeof(PortInfo)) == -1)
      return (-1);

   ASPrintf(NULL, filename, "DlgConfig:Port/%s", PortInfo.GlobalFile);
   if (GetFirstStruct(filename, (char *) globals, sizeof(struct Global_Settings)) == -1)
                          return (-1);

   return (0);
}
