#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <proto/dlg.h>
#include <dlg/misc.h>

#include <link/io.h>

#include <pragmas/dlg.h>


extern struct Library *DLGBase;

BOOL UseCharSet(UBYTE number,char *ext)

{struct CharSet Set;
 char           filename[80];
 char           array   [512];
 short          counter;

 for(counter=0; counter < 256; counter++)
    {array[counter]     = counter;
     array[counter+256] = counter;
    }

 if (number)
    {Set.number = number;

     if (!GetStruct("DLGConfig:CharSets/CharSets.bbs", (char *)&Set, sizeof(Set), 1))
        {ASPrintf(NULL, filename, "DLGConfig:CharSets/%s.set", Set.name);
         GetFirstStruct(filename, array, 512);
        }
    }

 TInTrans (array,     ext);
 TOutTrans(array+256, ext);
 return(TRUE);
}
