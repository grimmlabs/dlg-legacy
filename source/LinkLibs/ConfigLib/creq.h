#include "images.h"

SHORT  BordVectors1[]    = {0,0, 136,0, 136,12, 0,12, 0,0};
struct Border B1         = {-2,-1,              /* border XY origin relative to container TopLeft */
                             1, 0, JAM1,        /* front pen, back pen and drawmode */
                             5,                 /* number of XY vectors */
                             BordVectors1,      /* pointer to XY vectors */
                             NULL               /* next border in list */
                           };
struct IntuiText CIText4 = {3, 0, JAM1,         /* front and back text pens and drawmode */
                            13, 2,              /* XY origin relative to container TopLeft */
                            NULL,               /* font pointer or NULL for defaults */
                  (UBYTE *)"D E F A U L T",     /* pointer to text */
                            NULL                /* next IntuiText structure */
                           };
struct Gadget defaultgad = {NULL,               /* next gadget */
                            153, 36,            /* origin XY of hit box relative to window TopLeft */
                            133, -2,            /* hit box width and height */
                            GFLG_GADGHCOMP,     /* gadget flags */
                            GACT_RELVERIFY,     /* activation flags */
                            GTYP_BOOLGADGET,    /* gadget type */
                     (APTR)&B1,                 /* gadget border or image to be rendered */
                            NULL,               /* alternate imagery for selection */
                           &CIText4,            /* first IntuiText structure */
                            0,                  /* gadget mutual-exclude long word */
                            NULL,               /* SpecialInfo structure for string gadgets */
                            50,                 /* user-definable data (ordinal gadget number) */
                            NULL                /* pointer to user-definable data */
                           };

SHORT BordVectors2[]     = {0,0, 135,0, 135,12, 0,12, 0,0};
struct Border B2         = {-2,-1,              /* border XY origin relative to container TopLeft */
                             1, 0, JAM1,        /* front pen, back pen and drawmode */
                             5,                 /* number of XY vectors */
                             BordVectors2,      /* pointer to XY vectors */
                             NULL               /* next border in list */
                           };
struct IntuiText CIText3 = {3, 0, JAM1,         /* front and back text pens and drawmode */
                            30, 2,              /* XY origin relative to container TopLeft */
                            NULL,               /* font pointer or NULL for defaults */
                  (UBYTE *)"R E S E T",         /* pointer to text */
                            NULL                /* next IntuiText structure */
                           };
struct Gadget resetgad   = {&defaultgad,        /* next gadget */
                             13,  36,           /* origin XY of hit box relative to window TopLeft */
                             132, 11,           /* hit box width and height */
                             GFLG_GADGHCOMP,    /* gadget flags */
                             GACT_RELVERIFY,    /* activation flags */
                             GTYP_BOOLGADGET,   /* gadget type */
                      (APTR)&B2,                /* gadget border or image to be rendered */
                             NULL,              /* alternate imagery for selection */
                            &CIText3,           /* first IntuiText structure */
                             0,                 /* gadget mutual-exclude long word */
                             NULL,              /* SpecialInfo structure for string gadgets */
                             51,                /* user-definable data (ordinal gadget number) */
                             NULL               /* pointer to user-definable data */
                           };

struct Gadget CGadget14  = {&resetgad,          /* next gadget */
                             363, 28,           /* origin XY of hit box relative to window TopLeft */
                             14,  9,            /* hit box width and height */
                             GFLG_GADGIMAGE,    /* gadget flags */
                             GACT_RELVERIFY,    /* activation flags */
                             GTYP_BOOLGADGET,   /* gadget type */
                      (APTR)&downsel,           /* gadget border or image to be rendered */
                      (APTR)&downun,            /* alternate imagery for selection */
                             NULL,              /* first IntuiText structure */
                             0,                 /* gadget mutual-exclude long word */
                             NULL,              /* SpecialInfo structure for string gadgets */
                             114,               /* user-definable data (ordinal gadget number) */
                             NULL               /* pointer to user-definable data */
                           };

struct Gadget CGadget13  = {&CGadget14,         /* next gadget */
                             363, 14,           /* origin XY of hit box relative to window TopLeft */
                             14,   8,           /* hit box width and height */
                             GFLG_GADGIMAGE,    /* gadget flags */
                             GACT_RELVERIFY,    /* activation flags */
                             GTYP_BOOLGADGET,   /* gadget type */
                      (APTR)&upsel,             /* gadget border or image to be rendered */
                      (APTR)&upun,              /* alternate imagery for selection */
                             NULL,              /* first IntuiText structure */
                             0,                 /* gadget mutual-exclude long word */
                             NULL,              /* SpecialInfo structure for string gadgets */
                             113,               /* user-definable data (ordinal gadget number) */
                             NULL               /* pointer to user-definable data */
                           };

struct Gadget CGadget12  = {&CGadget13,         /* next gadget */
                             331, 28,           /* origin XY of hit box relative to window TopLeft */
                             14,   8,           /* hit box width and height */
                             GFLG_GADGIMAGE,    /* gadget flags */
                             GACT_RELVERIFY,    /* activation flags */
                             GTYP_BOOLGADGET,   /* gadget type */
                      (APTR)&downsel,           /* gadget border or image to be rendered */
                      (APTR)&downun,            /* alternate imagery for selection */
                             NULL,              /* first IntuiText structure */
                             0,                 /* gadget mutual-exclude long word */
                             NULL,              /* SpecialInfo structure for string gadgets */
                             112,               /* user-definable data (ordinal gadget number) */
                             NULL               /* pointer to user-definable data */
                           };

struct Gadget CGadget11  = {&CGadget12,         /* next gadget */
                             331, 14,           /* origin XY of hit box relative to window TopLeft */
                             14,  8,            /* hit box width and height */
                             GFLG_GADGIMAGE,    /* gadget flags */
                             GACT_RELVERIFY,    /* activation flags */
                             GTYP_BOOLGADGET,   /* gadget type */
                      (APTR)&upsel,             /* gadget border or image to be rendered */
                      (APTR)&upun,              /* alternate imagery for selection */
                             NULL,              /* first IntuiText structure */
                             0,                 /* gadget mutual-exclude long word */
                             NULL,              /* SpecialInfo structure for string gadgets */
                             111,               /* user-definable data (ordinal gadget number) */
                             NULL               /* pointer to user-definable data */
                           };

struct Gadget CGadget10  = {&CGadget11,         /* next gadget */
                             299, 28,           /* origin XY of hit box relative to window TopLeft */
                             14,  8,            /* hit box width and height */
                             GFLG_GADGIMAGE,    /* gadget flags */
                             GACT_RELVERIFY,    /* activation flags */
                             GTYP_BOOLGADGET,	/* gadget type */
                      (APTR)&downsel,           /* gadget border or image to be rendered */
                      (APTR)&downun,            /* alternate imagery for selection */
                             NULL,              /* first IntuiText structure */
                             0,                 /* gadget mutual-exclude long word */
                             NULL,              /* SpecialInfo structure for string gadgets */
                             110,               /* user-definable data (ordinal gadget number) */
                             NULL               /* pointer to user-definable data */
                           };

struct IntuiText CIText2 = {3, 0, JAM1,         /* front and back text pens and drawmode */
                            5, 25,             /* XY origin relative to container TopLeft */
                            NULL,               /* font pointer or NULL for defaults */
                  (UBYTE *)"R   G   B",         /* pointer to text */
                            NULL                /* next IntuiText structure */
                           };
struct IntuiText CIText1 = {2, 0, JAM1,         /* front and back text pens and drawmode */
                            2, 25,             /* XY origin relative to container TopLeft */
                            NULL,               /* font pointer or NULL for defaults */
                  (UBYTE *)"R   G   B",         /* pointer to text */
                           &CIText2             /* next IntuiText structure */
                           };
struct Gadget CGadget9   = {&CGadget10,         /* next gadget */
                             299, 14,           /* origin XY of hit box relative to window TopLeft */
                             14,   8,           /* hit box width and height */
                             GFLG_GADGIMAGE,    /* gadget flags */
                             GACT_RELVERIFY,    /* activation flags */
                             GTYP_BOOLGADGET,   /* gadget type */
                      (APTR)&upsel,             /* gadget border or image to be rendered */
                      (APTR)&upun,              /* alternate imagery for selection */
                            &CIText1,           /* first IntuiText structure */
                             0,                 /* gadget mutual-exclude long word */
                             NULL,              /* SpecialInfo structure for string gadgets */
                             109,               /* user-definable data (ordinal gadget number) */
                             NULL               /* pointer to user-definable data */
                           };

SHORT  CVectors[]        = {0,0, 30,0, 30,15, 0,15, 0,0};
struct Border CBorder    = {-2, -1,             /* border XY origin relative to container TopLeft */
                             1,  0, JAM1,       /* front pen, back pen and drawmode */
                             5,                 /* number of XY CVectors */
                             CVectors,          /* pointer to XY CVectors */
                             NULL               /* next border in list */
                           };

struct Gadget CGadget8   = {&CGadget9,          /* next gadget */
                             258, 16,           /* origin XY of hit box relative to window TopLeft */
                             27,  14,           /* hit box width and height */
                             GFLG_GADGHBOX,     /* gadget flags */
                             GACT_RELVERIFY|GACT_TOGGLESELECT,    /* activation flags */
                             GTYP_BOOLGADGET,   /* gadget type */
                      (APTR)&CBorder,           /* gadget border or image to be rendered */
                             NULL,              /* alternate imagery for selection */
                             NULL,              /* first IntuiText structure */
                             0,                 /* gadget mutual-exclude long word */
                             NULL,              /* SpecialInfo structure for string gadgets */
                             108,               /* user-definable data (ordinal gadget number) */
                             NULL               /* pointer to user-definable data */
                           };

struct Gadget CGadget7   = {&CGadget8,          /* next gadget */
                             223, 16,           /* origin XY of hit box relative to window TopLeft */
                             27,  14,           /* hit box width and height */
                             GFLG_GADGHBOX,     /* gadget flags */
                             GACT_RELVERIFY|GACT_TOGGLESELECT,    /* activation flags */
                             GTYP_BOOLGADGET,   /* gadget type */
                      (APTR)&CBorder,           /* gadget border or image to be rendered */
                             NULL,              /* alternate imagery for selection */
                             NULL,              /* first IntuiText structure */
                             0,                 /* gadget mutual-exclude long word */
                             NULL,              /* SpecialInfo structure for string gadgets */
                             107,               /* user-definable data (ordinal gadget number) */
                             NULL               /* pointer to user-definable data */
                           };

struct Gadget CGadget6   = {&CGadget7,          /* next gadget */
                             188, 16,           /* origin XY of hit box relative to window TopLeft */
                             27,  14,           /* hit box width and height */
                             GFLG_GADGHBOX,     /* gadget flags */
                             GACT_RELVERIFY|GACT_TOGGLESELECT,    /* activation flags */
                             GTYP_BOOLGADGET,   /* gadget type */
                      (APTR)&CBorder,           /* gadget border or image to be rendered */
                             NULL,              /* alternate imagery for selection */
                             NULL,              /* first IntuiText structure */
                             0,                 /* gadget mutual-exclude long word */
                             NULL,              /* SpecialInfo structure for string gadgets */
                             106,               /* user-definable data (ordinal gadget number) */
                             NULL               /* pointer to user-definable data */
                           };

struct Gadget CGadget5   = {&CGadget6,          /* next gadget */
                             153, 16,           /* origin XY of hit box relative to window TopLeft */
                             27,  14,           /* hit box width and height */
                             GFLG_GADGHBOX,     /* gadget flags */
                             GACT_RELVERIFY|GACT_TOGGLESELECT,    /* activation flags */
                             GTYP_BOOLGADGET,   /* gadget type */
                      (APTR)&CBorder,           /* gadget border or image to be rendered */
                             NULL,              /* alternate imagery for selection */
                             NULL,              /* first IntuiText structure */
                             0,                 /* gadget mutual-exclude long word */
                             NULL,              /* SpecialInfo structure for string gadgets */
                             105,               /* user-definable data (ordinal gadget number) */
                             NULL               /* pointer to user-definable data */
                           };

struct Gadget CGadget4   = {&CGadget5,          /* next gadget */
                             118, 16,           /* origin XY of hit box relative to window TopLeft */
                             27,  14,           /* hit box width and height */
                             GFLG_GADGHBOX,     /* gadget flags */
                             GACT_RELVERIFY|GACT_TOGGLESELECT,    /* activation flags */
                             GTYP_BOOLGADGET,   /* gadget type */
                      (APTR)&CBorder,           /* gadget border or image to be rendered */
                             NULL,              /* alternate imagery for selection */
                             NULL,              /* first IntuiText structure */
                             0,                 /* gadget mutual-exclude long word */
                             NULL,              /* SpecialInfo structure for string gadgets */
                             104,               /* user-definable data (ordinal gadget number) */
                             NULL               /* pointer to user-definable data */
                           };

struct Gadget CGadget3   = {&CGadget4,          /* next gadget */
                             83, 16,            /* origin XY of hit box relative to window TopLeft */
                             27, 14,            /* hit box width and height */
                             GFLG_GADGHBOX,     /* gadget flags */
                             GACT_RELVERIFY|GACT_TOGGLESELECT,    /* activation flags */
                             GTYP_BOOLGADGET,   /* gadget type */
                      (APTR)&CBorder,           /* gadget border or image to be rendered */
                             NULL,              /* alternate imagery for selection */
                             NULL,              /* first IntuiText structure */
                             0,                 /* gadget mutual-exclude long word */
                             NULL,              /* SpecialInfo structure for string gadgets */
                             103,               /* user-definable data (ordinal gadget number) */
                             NULL               /* pointer to user-definable data */
                           };

struct Gadget CGadget2   = {&CGadget3,          /* next gadget */
                             48, 16,            /* origin XY of hit box relative to window TopLeft */
                             27, 14,            /* hit box width and height */
                             GFLG_GADGHBOX,     /* gadget flags */
                             GACT_RELVERIFY|GACT_TOGGLESELECT,    /* activation flags */
                             GTYP_BOOLGADGET,   /* gadget type */
                      (APTR)&CBorder,           /* gadget border or image to be rendered */
                             NULL,              /* alternate imagery for selection */
                             NULL,              /* first IntuiText structure */
                             0,                 /* gadget mutual-exclude long word */
                             NULL,              /* SpecialInfo structure for string gadgets */
                             102,               /* user-definable data (ordinal gadget number) */
                             NULL               /* pointer to user-definable data */
                           };

struct Gadget CGadget1   = {&CGadget2,          /* next gadget */
                             13, 16,            /* origin XY of hit box relative to window TopLeft */
                             27, 14,            /* hit box width and height */
                             GFLG_GADGHBOX,     /* gadget flags */
                             GACT_RELVERIFY|GACT_TOGGLESELECT,    /* activation flags */
                             GTYP_BOOLGADGET,   /* gadget type */
                      (APTR)&CBorder,           /* gadget border or image to be rendered */
                             NULL,              /* alternate imagery for selection */
                             NULL,              /* first IntuiText structure */
                             0,                 /* gadget mutual-exclude long word */
                             NULL,              /* SpecialInfo structure for string gadgets */
                             101,               /* user-definable data (ordinal gadget number) */
                             NULL               /* pointer to user-definable data */
                           };

/* CGadget list */
