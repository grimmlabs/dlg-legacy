#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <dlg/dlg.h>

#include <proto/exec.h>
#include <proto/dos.h>
#include <proto/dlg.h>

#include <pragmas/dlg.h>

#include <private/Version.h>
#define  ObjRev "1"
const UBYTE version[]="\0$VER: LFtoCR " BUILDVER "." ObjRev " �" COPYRIGHT " by Digerati Dreams "__AMIGADATE__;

void             main(int, char **);
void __regargs __chkabort(void);

struct Library *DLGBase = NULL;


void main(int argc, char *argv[])

{int    fp;
 char  *buffer;
 ULONG  size;
 ULONG  counter;

 if (argc < 2)                                       exit(0);
 if (!(DLGBase = OpenLibrary(DLGNAME, DLGVERSION)))  exit(0);

 argv++;

 if (!FileSize(*argv, &size))
     if ((buffer = malloc(size)))
        {if ((fp=open(*argv, O_RDWR)) != EOF)
            {read(fp, buffer, size);

             for(counter = 0; counter < size; counter++)
                 if (buffer[counter]==10)  buffer[counter]=13;

             lseek(fp, 0, 0);
             write(fp, buffer, size);
             close(fp);
            }

         free(buffer);
        }

 CloseLibrary(DLGBase);
}


void __regargs __chkabort()

{
}
