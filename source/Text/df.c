#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <fcntl.h>
#include <dos.h>
#include <exec/types.h>

#include <dlg/dlg.h>
#include <dlg/user.h>

#include <devices/tpt.h>

#include <proto/exec.h>
#include <proto/dos.h>
#include <proto/dlg.h>

#include <pragmas/dlg.h>

#include <private/Version.h>
#define  ObjRev "2"
const UBYTE version[]="\0$VER: DisplayFile (DF) " BUILDVER "." ObjRev " �" COPYRIGHT " by Digerati Dreams "__AMIGADATE__;

void                main(int, char **);
void                _CXBRK(void);

struct Ram_File     RUser;
struct USER_DATA    UserDat;
struct Library     *DLGBase;

unsigned long       OldFlags = 0;

void                main(int argc, char *argv[])
{
   char                filename[80];
   char                Ext[5];

   if (argc < 2)
      exit(5);
   if (!(DLGBase = OpenLibrary(DLGNAME, DLGVERSION)))
      exit(5);
   if (GetDevName(Ext) == -1)
      _CXBRK();

   OldFlags = TSetFlags(0, Ext);

   TUnSetFlags(T_ECHO, Ext);
   TSetFlags(T_RAW, Ext);

   strcpy(filename, *++argv);
   ReadUser(&RUser, &UserDat, Ext);

   DispForm(filename, &UserDat, &UserDat, &RUser, Ext);

   if (OldFlags)
      {
         TSetFlags(OldFlags, Ext);
         TUnSetFlags(~OldFlags, Ext);
      }

   _CXBRK();
}

void                _CXBRK(void)

{
   CloseLibrary(DLGBase);
   exit(0);
}
