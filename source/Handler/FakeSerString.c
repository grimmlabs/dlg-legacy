#include <exec/types.h>

#include <devices/tpt.h>

extern   void  ReadSer(struct TPTSess *, unsigned short);

/* */
/* Send a String as if Recieved */
/* */
void                FakeSerString(struct TPTSess *sess, UBYTE * str, int len)
{
   while (len--)
      ReadSer(sess, *str++);
}

