#include <exec/types.h>

#include <devices/tpt.h>

#include <proto/exec.h>

extern long                tptflags;

extern struct IOStdReq    *consoleWriteMsg;

/* */
/* Write string to Console */
/* */
void                WriteCon(UBYTE * buf, long len)

{
   if (consoleWriteMsg && !(tptflags & T_PASS_THRU))
      {
         if (tptflags & T_CWRITE_PEND)
            WaitIO((struct IORequest *) consoleWriteMsg);

         consoleWriteMsg->io_Length = len;
         consoleWriteMsg->io_Data = (APTR) buf;
         SendIO((struct IORequest *) consoleWriteMsg);

         tptflags |= T_CWRITE_PEND;
      }
}

