#include <exec/types.h>

#include <devices/serial.h>

#include <proto/exec.h>

extern UBYTE               in_c;

extern struct IOExtSer    *ReadSER;

/* */
/* Start a asynchronous Read request to serial device */
/* */
void                set_read(void)
{
   ReadSER->IOSer.io_Length = 1;
   ReadSER->IOSer.io_Command = CMD_READ;
   ReadSER->IOSer.io_Data = (APTR) & in_c;
   SendIO((struct IORequest *) ReadSER);
}

