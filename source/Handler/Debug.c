#include <exec/types.h>
#include <string.h>

#include <dlg/Debug.h>

#include <devices/tpt.h>

#include <proto/exec.h>

#if MAKE_DEBUG == 1
#define DO_DEBUG 1
#else
#undef DO_DEBUG
#endif

int   DEBUG_PORT_TYPE = HANDLER_PORT;

void InitDebug(void);
void KillDebug(void);

extern   void  TDebug(char *);

struct MsgPort     *dport = NULL;
struct MsgPort     *drport = NULL;

extern   UBYTE               tdevname[];

///   InitDebug()
void InitDebug(void)
{
#ifdef DO_DEBUG
   UBYTE dname[32];

   if (dport = (struct MsgPort *) FindPort(DEBUG))
   {
      strcpy(dname, tdevname);
      strcat(dname, "Debug");

      drport = (struct MsgPort *) CreateMsgPort();
      drport->mp_Node.ln_Name = dname;
      AddPort(drport);

      TDebug("Started up Handler debug port");
   }
#endif
}
//-
///   KillDebug()
void KillDebug(void)
{
   if (!drport) return;

   RemPort(drport);
   DeleteMsgPort(drport);
}
//-

