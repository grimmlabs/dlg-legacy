#include <exec/types.h>
#include <string.h>

#include <dos/dosextens.h>

#include <devices/tpt.h>

#include <proto/exec.h>

extern long                tptflags;

extern SHORT               in_len;
extern SHORT               num_times;
extern SHORT               aux_avail;

extern struct timerequest *Timer;
extern struct MsgPort     *Timer_Port;

extern UBYTE               localflag;
extern UBYTE               inline;

extern   void  NiceAbort(struct IORequest *);
extern   void  returnpktplain(struct DosPacket *, struct Process *);
extern   void  set_con_read(void);
extern   void  set_read(void);

/*
 * Check our buf to see if we have a line to return yet. Or
 * if raw mode give 'em all we got. */
void                CheckPending(struct TPTSess *sess)

{
   UBYTE              *p;
   SHORT               i = 0;

   if ((in_len && (tptflags & T_RAW)) || aux_avail)
      {
         if (tptflags & T_SER_TIMEOUT)
            {
               num_times = 0;

               if (tptflags & T_WAIT_FOR)
                  {
                     NiceAbort((struct IORequest *) Timer);
                     Wait(1L << Timer_Port->mp_SigBit);

                     Timer->tr_time.tv_secs = 5;
                     Timer->tr_time.tv_micro = 0;
                     SendIO((struct IORequest *) Timer);

                     tptflags &= ~T_WAIT_FOR;
                     if (tptflags & T_TYPEAHEAD_FULL)
                        {
                           tptflags &= ~T_TYPEAHEAD_FULL;
                           if (!localflag)
                              set_read();
                           if (tptflags & T_WINDOW)
                              set_con_read();
                        }

                     sess->creader->rdpkt->dp_Res1 = DOS_TRUE;
                     returnpktplain(sess->creader->rdpkt, sess->port->myproc);
                  }
            }

         if (tptflags & T_RPEND)
            {
               inline = FALSE;
               tptflags &= ~T_RPEND;   /* clear the pending
                                        * read  */

               if (tptflags & T_RAW)
                  aux_avail = 0;       /* since we're
                                        * sending all */
               else
                  --aux_avail;

     /* if in_len is zero, then aux_avail must of been set
      * up to force us here to reply with Res1 = 0; thus
      * actually returning an EOF. */
               if (in_len)
                  {
                     for (i = 1, p = (UBYTE *) sess->creader->rdpkt->dp_Arg2;
                          ((p[i - 1] = sess->auxbuf[i - 1]) != 10 || (tptflags & T_RAW)) &&
                          i < (int) sess->creader->rdpkt->dp_Arg3 && i < in_len; ++i) ;

           /* reader asked for less than 256 chars but no
            * cr was found. If not in raw mode then bump
            * avail back up since we still have the
            * remaining CR terminated line in
            * sess->auxbuf[]. */
                     if (p[i - 1] != 10 && !(tptflags & T_RAW))
                        ++aux_avail;
                  }

               in_len -= i;
               movmem(sess->auxbuf + i, sess->auxbuf, in_len);

               if (tptflags & T_TYPEAHEAD_FULL)
                  {
                     tptflags &= ~T_TYPEAHEAD_FULL;
                     if (!localflag)
                        set_read();
                     if (tptflags & T_WINDOW)
                        set_con_read();
                  }

               sess->creader->rdpkt->dp_Res1 = (long) i;
               returnpktplain(sess->creader->rdpkt, sess->port->myproc);
            }
      }

}

