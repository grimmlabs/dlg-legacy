#include <devices/tpt.h>

#include <DLG/Debug.h>

#include <proto/exec.h>

extern struct MsgPort     *drport;
extern struct MsgPort     *dport;
extern UBYTE               tdevname[];


void TDebug(char *str)
{
   struct debug_mess   dmess;

   if(!dport)  return;

   if (dport)
   {
      dmess.num = HANDLER_PORT;
      dmess.str = str;
      dmess.port = (char *) tdevname;
      dmess.mess.mn_Node.ln_Type = NT_MESSAGE;
      dmess.mess.mn_ReplyPort = drport;
      dmess.mess.mn_Length = sizeof(struct debug_mess);

      PutMsg(dport, (struct Message *) &dmess);

      WaitPort(drport);
      GetMsg(drport);
   }

   return;
}


