#include <proto/exec.h>

void                NiceAbort(struct IORequest *ioreq)
{
   Forbid();

   if (!CheckIO((struct IORequest *) ioreq))
      AbortIO((struct IORequest *) ioreq);

   Permit();

   WaitIO((struct IORequest *) ioreq);
}


