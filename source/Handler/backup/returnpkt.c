/*
 *  misc.c  - support routines - Phillip Lindsay (C) Commodore 1986
 *  You may freely distribute this source and use it for Amiga Development -
 *  as long as the Copyright notice is left intact.
 *
 * 30-SEP-86
 *
 */

#include <libraries/dosextens.h>

#include <proto/exec.h>

void returnpktplain(struct DosPacket *, struct Process *);
void returnpkt(struct DosPacket *, struct Process *, ULONG, ULONG);

void returnpktplain(struct DosPacket *packet, struct Process *myproc)
{
   returnpkt(packet, myproc, packet->dp_Res1, packet->dp_Res2);
}

/* returnpkt() - packet support routine
 * here is the guy who sends the packet back to the sender...
 *
 * (I modeled this just like the BCPL routine [so its a little redundant] )
 */

void returnpkt(struct DosPacket *packet, struct Process *myproc, ULONG res1, ULONG res2)
{
 struct Message *mess;
 struct MsgPort *replyport;

 packet->dp_Res1          = res1;
 packet->dp_Res2          = res2;
 replyport                = packet->dp_Port;
 mess                     = packet->dp_Link;
 packet->dp_Port          = &myproc->pr_MsgPort;
 mess->mn_Node.ln_Name    = (char *) packet;
 mess->mn_Node.ln_Succ    = NULL;
 mess->mn_Node.ln_Pred    = NULL;
 PutMsg(replyport, mess);
}


