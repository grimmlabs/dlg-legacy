#include <exec/io.h>

#include <proto/exec.h>

extern struct IOStdReq    *consoleReadMsg;

extern UBYTE               cin_c;

/* */
/* Start a asynchronous Read request to console device */
/* */
void                set_con_read(void)

{
   consoleReadMsg->io_Length = 1;
   consoleReadMsg->io_Command = CMD_READ;
   consoleReadMsg->io_Data = (APTR) & cin_c;
   SendIO((struct IORequest *) consoleReadMsg);
}

