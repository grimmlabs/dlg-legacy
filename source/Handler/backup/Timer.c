#include <exec/types.h>

#include <devices/tpt.h>

#include <proto/exec.h>

#define TIMER_SIG   (1L << Timer_Port->mp_SigBit)

extern long                timersig;
extern long                tptflags;

extern SHORT               num_times;

extern struct timerequest *Timer;
extern struct MsgPort     *Timer_Port;

extern   void  NiceAbort(struct IORequest *);
extern   void  TDebug(char *);

///   OpenTimer
BOOL                OpenTimer(void)
{
   if (Timer_Port = CreateMsgPort())
   {
      if ((Timer = (struct timerequest *) CreateIORequest(Timer_Port, sizeof(*Timer))))
      {
         if (!OpenDevice(TIMERNAME, UNIT_VBLANK, (struct IORequest *) Timer, NULL))
         {
            Timer->tr_node.io_Command = TR_ADDREQUEST;
            Timer->tr_node.io_Flags = NULL;
            Timer->tr_node.io_Error = NULL;

            num_times = 0;
            Timer->tr_time.tv_secs = 5;
            Timer->tr_time.tv_micro = 0;
            SendIO((struct IORequest *) Timer);

            timersig = TIMER_SIG;
// debug
//            TDebug("OpenTimer: Opened timer");
// end debug
            return (TRUE);
         }
      }

      DeleteIORequest((struct IORequest *) Timer);
   }

   DeleteMsgPort(Timer_Port);
// debug
   TDebug("OpenTimer: Failed to open timer");
// end debug
   return (FALSE);
}
//-
///   CloseTimer
void                CloseTimer(void)
{
   if (tptflags & T_SER_TIMEOUT)
   {

      NiceAbort((struct IORequest *) Timer);
      Wait(1L << Timer_Port->mp_SigBit);
   }

   if (Timer)
   {
      if (Timer->tr_node.io_Device)
      CloseDevice((struct IORequest *) Timer);
      DeleteIORequest((struct IORequest *) Timer);
   }

   if (Timer_Port)
      DeleteMsgPort(Timer_Port);

   Timer = NULL;
   Timer_Port = NULL;
// debug
   TDebug("CloseTimer: Closed timer");
// end debug
   return;
}
//-
