#include <exec/types.h>
#include <string.h>

#include <devices/tpt.h>

#include <dlg/portconfig.h>

#include <graphics/text.h>
#include <graphics/display.h>

#include <intuition/intuition.h>
#include <intuition/screens.h>

#include <proto/intuition.h>
#include <proto/graphics.h>
#include <proto/dos.h>

extern long                tptflags;

extern SHORT               TScrDepth;

extern struct TextAttr     ta;
extern struct Screen      *TScrn;

extern UBYTE               localflag;

extern UWORD               colortable[];


extern   void  TDebug(char *);


///   OpenScrStuff
BOOL OpenScrStuff(UBYTE * name, struct ScrStruct *scrstruct)
{
   struct NewScreen    ns;

   // new ATIS
   ta.ta_Name = scrstruct->fontname;
   ta.ta_YSize = scrstruct->fontsize;
   ta.ta_Style = FS_NORMAL;
   ta.ta_Flags = FPF_ROMFONT | FPF_PROPORTIONAL | FPF_DESIGNED;
   // end new ATIS

   ns.LeftEdge = 0;
   ns.TopEdge = 0;
   ns.Width = scrstruct->width;
   ns.Height = scrstruct->height;
   ns.Depth = scrstruct->depth;
   ns.DetailPen = 0;
   ns.BlockPen = (1 << ns.Depth) - 1;
   ns.ViewModes = NULL;

   if (scrstruct->hires)
      ns.ViewModes |= HIRES;
   
   if (scrstruct->interlace)
      ns.ViewModes |= INTERLACE;
   
   if (localflag || !(scrstruct->flags & DISP_BKGRND))
      ns.Type = CUSTOMSCREEN;
   else
      ns.Type = CUSTOMSCREEN | SCREENBEHIND;

   ns.Font = &ta;
   ns.DefaultTitle = name;
   ns.Gadgets = NULL;
   ns.CustomBitMap = NULL;

   TScrn = (struct Screen *) OpenScreen(&ns);
   
   if (TScrn == NULL)
   {
// debug
      TDebug("OpenScrStuff: failed to open screen");
// end debug
      return (FALSE);
   }

   TScrDepth = scrstruct->depth;
   movmem(scrstruct->colortable, colortable, 16);
   LoadRGB4(&(TScrn->ViewPort), colortable, (1 << TScrDepth));

   tptflags |= T_SCREEN;
// debug
//  TDebug("OpenScrStuff: Opened screen OK");
// end debug
   return (TRUE);
}
//-
///   CloseScrStuff
void                CloseScrStuff(void)
{
   if (TScrn)
   {
      while (!CloseScreen(TScrn))
      {
// debug
         TDebug("CloseScrStuff: Trying to close screen");
         Delay(50);
// end debug
      }
   }

   tptflags &= ~T_SCREEN;
   TScrn = NULL;
}
//-
