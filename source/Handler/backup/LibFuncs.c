#include "handler.h"

BOOL                OpenLibs(void)
{
   if (IntuitionBase = (struct IntuitionBase *) OpenLibrary("intuition.library", NULL))
   {
      if (GfxBase = (struct GfxBase *) OpenLibrary("graphics.library", NULL))
      {
// debug
         TDebug("OpenLibs: opened libraries OK");
// end debug
         return (TRUE);
      }
   }

// debug
   TDebug("OpenLibs: failed to open libraries");
// end debug
   CloseLibs();
   return (FALSE);
}

void CloseLibs(void)
{
   /* DO NOT put TDEBUG statements in this
   ** function!! The debug port is already CLOSED!
   */

   if (IntuitionBase)
      CloseLibrary((struct Library *) IntuitionBase);

   if (GfxBase)
      CloseLibrary((struct Library *) GfxBase);

   IntuitionBase = NULL;
   GfxBase = NULL;

   return;
}
