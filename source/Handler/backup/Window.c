#include <exec/types.h>

#include <devices/conunit.h>
#include <devices/tpt.h>

#include <intuition/intuition.h>

#include <proto/exec.h>
#include <proto/intuition.h>
#include <proto/graphics.h>

#define CONSOLE_SIG (1L << consoleReadPort->mp_SigBit)

extern long                consolesig;
extern long                tptflags;

extern SHORT               TScrDepth;

extern struct IOStdReq    *consoleWriteMsg;
extern struct IOStdReq    *consoleReadMsg;
extern struct Window      *TWind;
extern struct TextAttr     ta;
extern struct TextFont    *tf;

extern UBYTE               conwrname[14];
extern UBYTE               conrdname[13];

extern   void  NiceAbort(struct IORequest *);
extern   void  set_con_read(void);
extern   void  TDebug(char *);
extern   void  WriteCon(UBYTE *, long);

char                hackstring[8] = "[3xm";

struct MsgPort     *consoleWritePort = NULL;
struct MsgPort     *consoleReadPort = NULL;

void  CloseWinStuff(void);

///   OpenWinStuff
BOOL OpenWinStuff(SHORT x, SHORT y, SHORT w, SHORT h, ULONG flags,
                  UBYTE * title, struct Screen * screen, SHORT minw,
                  SHORT minh, SHORT maxw, SHORT maxh, USHORT type,
                  UBYTE * fontname, UBYTE fontsize)

{
   struct NewWindow    nw;

   consoleWritePort = CreateMsgPort();

   if (consoleWritePort == NULL)
   {
      CloseWinStuff();
// debug
      TDebug("OpenWinStuff: Failed to open console write port");
// end debug
      return (FALSE);
   }

   consoleWritePort->mp_Node.ln_Name = conwrname;
   AddPort(consoleWritePort);

   consoleWriteMsg = CreateStdIO(consoleWritePort);

   if (consoleWriteMsg == NULL)
   {
      CloseWinStuff();
// debug
      TDebug("OpenWinStuff: Failed to open console write message port");
// end debug
      return (FALSE);
   }

   consoleReadPort = CreateMsgPort();

   if (consoleReadPort == NULL)
   {
      CloseWinStuff();
// debug
      TDebug("OpenWinStuff: Failed to open console read port");
// end debug
      return (FALSE);
   }

   consoleReadPort->mp_Node.ln_Name = conrdname;
   AddPort(consoleReadPort);

   consoleReadMsg = CreateStdIO(consoleReadPort);

   if (consoleReadMsg == NULL)
   {
      CloseWinStuff();
// debug
      TDebug("OpenWinStuff: Failed to open console read message port");
// end debug
      return (FALSE);
   }

   nw.LeftEdge = x;
   nw.TopEdge = y;
   nw.Width = w;
   nw.Height = h;
   nw.DetailPen = (UBYTE) - 1;
   nw.BlockPen = (UBYTE) - 1;
   nw.IDCMPFlags = IDCMP_MOUSEBUTTONS;
   nw.Flags = (ULONG) flags | WFLG_REPORTMOUSE | WFLG_SIMPLE_REFRESH;
   nw.FirstGadget = NULL;
   nw.CheckMark = NULL;
   nw.Title = (UBYTE *) title;
   nw.Screen = (struct Screen *) screen;
   nw.BitMap = NULL;
   nw.MinWidth = minw;
   nw.MinHeight = minh;
   nw.MaxWidth = maxw;
   nw.MaxHeight = maxh;
   nw.Type = (USHORT) type;

   TWind = (struct Window *) OpenWindow(&nw);

   if (TWind == NULL)
   {
      CloseWinStuff();
// debug
      TDebug("OpenWinStuff: Failed to open window");
// end debug
      return (FALSE);
   }

   ta.ta_Name = fontname;
   ta.ta_YSize = fontsize;
   ta.ta_Style = FS_NORMAL;
   ta.ta_Flags = FPF_ROMFONT | FPF_PROPORTIONAL | FPF_DESIGNED;

   tf = (struct TextFont *) OpenFont(&ta);
   
   if (tf)
      SetFont(TWind->RPort, tf);

   consoleWriteMsg->io_Data = (APTR) TWind;
   consoleWriteMsg->io_Length = sizeof(struct Window);

   if (OpenDevice("console.device", CONU_SNIPMAP, (struct IORequest *) consoleWriteMsg, 1))
// if (OpenDevice("console.device", NULL, (struct IORequest *)consoleWriteMsg, NULL))
   {
      CloseWinStuff();
// debug
      TDebug("OpenWinStuff: Failed to open console.device");
// end debug
      return (FALSE);
   }
   
   consoleReadMsg->io_Device = consoleWriteMsg->io_Device;
   consoleReadMsg->io_Unit = consoleWriteMsg->io_Unit;
   consolesig = CONSOLE_SIG;

   consoleWriteMsg->io_Command = CMD_WRITE;
   tptflags |= T_WINDOW;

// call new hack function to set text color locally to white

   if (TScrDepth)
   {
      hackstring[3] = (48 + ((1 << TScrDepth) - 1));
      WriteCon(hackstring, 5);
   }
// end hack

   set_con_read();
   return (TRUE);
}
//-
///   CloseWinStuff
void CloseWinStuff(void)
{
   if (!(tptflags & T_TYPEAHEAD_FULL) && consoleReadMsg)
      NiceAbort((struct IORequest *) consoleReadMsg);

   if ((tptflags & T_CWRITE_PEND) && consoleWriteMsg)
   {
      WaitIO((struct IORequest *) consoleWriteMsg);
      tptflags &= ~T_CWRITE_PEND;
   }

   if (tf)
      CloseFont(tf);

   if (consoleWriteMsg)
      CloseDevice((struct IORequest *) consoleWriteMsg);

   if (consoleReadMsg)
      DeleteIORequest((struct IORequest *) consoleReadMsg);

   if (consoleReadPort)
   {
      RemPort(consoleReadPort);
      DeleteMsgPort(consoleReadPort);
   }

   if (consoleWriteMsg)
      DeleteIORequest((struct IORequest *) consoleWriteMsg);
   
   if (consoleWritePort)
   {
      RemPort(consoleWritePort);
      DeleteMsgPort(consoleWritePort);
   }

   if (TWind)
      CloseWindow(TWind);
   
   tf = NULL;
   TWind = NULL;
   consoleReadMsg = NULL;
   consoleReadPort = NULL;
   consoleWriteMsg = NULL;
   consoleWritePort = NULL;

   tptflags &= ~T_WINDOW;
// debug
   TDebug("CloseWinStuff: Closed window");
// end debug
   return;
}
//-

