#include <exec/types.h>

#include <devices/tpt.h>

#include <proto/exec.h>

#define CONTROL_SIG (1L << ctrl_port->mp_SigBit)

extern BOOL                SERGOOD;

extern long                tptflags;
extern long                controlsig;

extern SHORT               time_delay;
extern SHORT               num_kills;

extern struct MsgPort     *ctrl_port;

extern UBYTE               localflag;

extern   void  CloseLibs(void);
extern   void  ClosePorts(void);
extern   void  CloseSer(void);
extern   BOOL  OpenSer(UBYTE *, ULONG, ULONG);
extern   BOOL  OpenLibs(void);
extern   BOOL  OpenTimer(void);
extern   void  set_read(void);


/// OpenStuff
BOOL                OpenStuff(UBYTE * sername, UBYTE serunit, ULONG serflags, UBYTE * controlname)
{
   SERGOOD = TRUE;

   tptflags = T_CRLF | T_BREAK | T_DO_TIMEOUT | T_PAUSE;
   time_delay = 24;
   num_kills = 0;

   ctrl_port = CreateMsgPort();

   if (ctrl_port)
   {
      ctrl_port->mp_Node.ln_Name = controlname;
      AddPort(ctrl_port);
      controlsig = CONTROL_SIG;

      if (OpenLibs())
      {
         if(!OpenSer(sername,serunit,serflags)) SERGOOD = FALSE;

         if (localflag || SERGOOD)
         {
            if (OpenTimer())
            {
               if (!localflag || SERGOOD)
               {
                  set_read();
               }

               tptflags |= T_SER_TIMEOUT;
               return (TRUE);
            }
         }

         CloseSer();
      }

      CloseLibs();
   }

   ClosePorts();
   return (FALSE);
}
//-
/// ClosePorts
void                ClosePorts(void)
{
   if (ctrl_port)
   {
      RemPort(ctrl_port);
      DeleteMsgPort(ctrl_port);
   }

   ctrl_port = NULL;
}
//-

