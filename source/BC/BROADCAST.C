#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <dos.h>
#include <exec/types.h>

#include <dlg/dlg.h>
#include <dlg/broadcast.h>
#include <dlg/input.h>
#include <dlg/user.h>

#include <proto/exec.h>
#include <proto/dos.h>
#include <proto/dlg.h>

#include <link/io.h>
#include <link/lang.h>

#include <devices/tpt.h>

#include <pragmas/dlg.h>

#include <private/Version.h>
#define  ObjRev "1"
const UBYTE version[]="\0$VER: Broadcast " BUILDVER "." ObjRev " �" COPYRIGHT " by Digerati Dreams "__AMIGADATE__;

void  main(int, char **);
void _CXBRK(void);
void  CleanUp(char *);
void  Usage(char *);


BPTR             sout    = NULL;
struct Library  *DLGBase = NULL;
char           **SA;

struct USER_DATA UserDat;
struct Ram_File  RStruct;
struct UserInfo  ui      = {&UserDat, &RStruct};
struct Query     q       = {NULL, NULL, NULL, NULL, NULL, 0, 0, 0};

char             port[4] = "";
char             tobuf   [80]  = "";
char             msgbuf  [80]  = "";
char             frombuf [80]  = "";
char             transbuf[160] = "";

unsigned long    OldFlags = 0;

void main(int argc, char **argv)
{
   char *s;
   char *to   = NULL;
   char *from = NULL;
   char *msg  = NULL;

   sout = Output();
   if (!(DLGBase = OpenLibrary(DLGNAME, DLGVERSION)))  exit(5);

   if (GetDevName(port) != -1)
   {
      if (!ReadUser(&RStruct,&UserDat,port))  CleanUp("Unable to read user data");

      OldFlags = TSetFlags(0,port);

      TUnSetFlags(T_ECHO,port);
      TSetFlags(T_RAW,port);
   }

   if (!(SA = getlang(port)))                 CleanUp("Can't read language file");

   AFPrintf(NULL, sout, "\n");

   while(--argc>0)
   {
      s = *++argv;

      if (*s++=='-')
      {
         while(*s)
         {
            switch(toupper(*s))
            {
               case 'P':   if (!--argc)  break;
                           to = *++argv;
                           strcpy(tobuf, to);
                           break;

               case 'F':   if (!--argc)  break;
                           from = *++argv;
                           break;

               case 'M':   if (!--argc)  break;
                           msg = *++argv;
                           break;
            }

            s++;
         }
      }
   }

   if (!port[0]  &&  !(to && from && msg))
      Usage(SA[2581]);

   if (!to)
   {
      while(!tobuf[0])
      {
         Chk_Abort();

         if (!DLGInput(NULL,tobuf,NULL,3,3,1, SA[2582]))
         {
            AFPrintf(NULL, sout, SA[2583]);
            CleanUp(NULL);
         }

         AFPrintf(NULL, sout, "\n\n");

         if (tobuf[0] == '?')
         {
            OverlayProgram("DLG:Who");
            tobuf[0] = 0;
         }
      }
   }

   if (tobuf[0] == '*')
   {
      ListPorts(tobuf, "BBS");

      if (tobuf[0])
      {
         for(s = tobuf; *s && Strnicmp(s,port,3); s+=3);

         if (*s)  movmem(s+3,s,strlen(tobuf)-((int)((long)s-(long)&tobuf)+1));
      }
   }
   else
   {
      ASPrintf(NULL, frombuf, "t:%s.user", tobuf);
      if (!Exists(frombuf))   CleanUp(SA[2584]);
   }

   if (!from)
      ASPrintf(NULL, frombuf, SA[2585], RStruct.Name,port);
   else
      ASPrintf(NULL, frombuf, SA[2586], from);

   if (!msg)
   {
      Chk_Abort();

      if (!DLGInput(NULL,msgbuf,NULL,75,75,7, SA[2587]))
      {
         AFPrintf(NULL, sout, SA[2583]);
         CleanUp(NULL);
      }

      if (ScreenBuffer(msgbuf,transbuf,159,"DLGConfig:Misc/Screen.dat"))
         msg = transbuf;
      else
         msg = msgbuf;

      AFPrintf(NULL, sout, "\n\n");
   }

   if (BroadCast(tobuf, frombuf, 0)  ||  BroadCast(tobuf, msg, 0))
      CleanUp(SA[2588]);

   AFPrintf(NULL, sout, SA[2589]);
   CleanUp(NULL);
}


void _CXBRK(void)
{
   CleanUp(NULL);
}


void CleanUp(char *s)
{

   if(OldFlags)
   {
      TSetFlags(OldFlags, port);
      TUnSetFlags(~OldFlags, port);
   }

   CloseLibrary(DLGBase);

   if (s)
   {
      Write(sout, "\n Error: ", 9);
      Write(sout, s, strlen(s));
      Write(sout, "\n\n", 2);
   }

   exit(s?5:0);
}


void Usage(char *string)
{
   AFPrintf(NULL, sout, SA[2590], string);
   AFPrintf(NULL, sout, SA[2591]);

   CleanUp(NULL);
}
