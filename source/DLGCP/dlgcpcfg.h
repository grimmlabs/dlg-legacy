window 100 50 350 100
numbanks 4

configbutton 1 0 Port

configbutton 1 1 Kill tkill %PORT
configbutton 1 2 ScrOpen tscreenopen %PORT
configbutton 1 3 ScrClose tscreenclose %PORT
configbutton 1 4 WinOpen twindowopen %port
configbutton 1 5 WinClose twindowclose %port
configbutton 1 6 Test ttitle %port This is the title


configbutton 2 0 Mail

configbutton 2 1 Process dmc PROCESS
configbutton 2 2 Export dmc export
configbutton 2 3 Call dmc call
configbutton 2 4 TDAbort traptell "abort f"

configbutton 3 0 Misc

configbutton 3 1 SendMsg
