#define MYLISTVIEW 0
#define BANKSTART  1
#define NUMBANKS   4

#define WINXOFS   100
#define WINYOFS    50
#define WINWIDTH  350
#define WINHEIGHT 100


struct CPButton   {char           name[36];
                   char          *command;
                   struct Gadget *gad;
                  };

struct CPBank     {char                 name[36];
                   char                 port[4];
                   struct NotifyRequest nr;                  
                   struct CPButton     *buttons;
                   struct Gadget       *gad;
                   struct Gadget       *glist;
                  };

struct CPPortNode {struct Node          node;
                   struct NotifyRequest nr;
                  };


struct DLGCPRxCmd {char *name;
                   char *template;
                  };

struct DLGCPRxCmd rxcmds[] = {
  {"CONFIGBUTTON","BANK/N/A,BUTTON/N/A,NAME/A,COMMAND/F"},
  {"NUMBANKS","NUM/N/A"},
  {"WINDOW","LEFTEDGE/N/A,TOPEDGE/N/A,WIDTH/N/A,HEIGHT/N/A"},
  {"PUBSCREEN","SCREEN/F/A"},
  {"BUTTONFONT","NAME/A,SIZE/N/A"},
  {"EXE","COMMAND/F"},
  {"RX","SCRIPT/F"},
  {"SENDREXX","PORT/A,COMMAND/F"},
  {"TRAPTELL","PORT/K,COMMAND/F"},
  {"DMC","COMMAND/F"},
  {"TKILL","PORT/A"},
  {"TBAUD","PORT/A,BAUD/N/A"},
  {"TCOLORS","PORT/A,COLORS/F"},
  {"TCONT","PORT/A"},
  {"TFLAGS","PORT/A,FLAGS/A"},
  {"TFREEZE","PORT/A"},
  {"TRECOVER","PORT/A"},
  {"TSCREENOPEN","PORT/A,W=WIDTH/K/N,H=HEIGHT/K/N,D=DEPTH/K/N,C=COLORS/K,HIRES/S,LACE/S,BKGRND/S,F=FONT/K,P=POINTSIZE/K/N"},
  {"TSCREENCLOSE","PORT/A"},
  {"TSTRING","PORT/A,STRING/F"},
  {"TTIMEDELAY","PORT/A,DELAY/N/A"},
  {"TTITLE","PORT/A,TITLE/F"},
  {"TWINDOWOPEN","PORT/A,L=LEFTEDGE/K/N,T=TOPEDGE/K/N,W=WIDTH/K/N,H=HEIGHT/K/N,BKGRND/S,F=FONT/K,P=POINTSIZE/K/N"},
  {"TWINDOWCLOSE","PORT/A"},
  {"TRANSLATE","STRING/F"},
  {"GETSTRING","N=NUMCHARS/K/N,PROMPT/F"},
  {"ICONIFYLOC","LEFTEDGE/N/A,TOPEDGE/N/A"},
  {"ICONIFY",NULL},
  {"DEICONIFY",NULL},

  {NULL,NULL}
};

#define DLGCP_MAXARGS 10

#define DLGCP_CONFIGBUTTON  0
#define DLGCP_NUMBANKS      1
#define DLGCP_WINDOW        2
#define DLGCP_PUBSCREEN     3
#define DLGCP_BFONT         4
#define DLGCP_EXE           5
#define DLGCP_RX            6
#define DLGCP_SENDREXX      7
#define DLGCP_TRAPTELL      8
#define DLGCP_DMC           9
#define DLGCP_TKILL        10
#define DLGCP_TBAUD        11
#define DLGCP_TCOLORS      12
#define DLGCP_TCONT        13
#define DLGCP_TFLAGS       14
#define DLGCP_TFREEZE      15
#define DLGCP_TRECOVER     16
#define DLGCP_TSCREENOPEN  17
#define DLGCP_TSCREENCLOSE 18
#define DLGCP_TSTRING      19
#define DLGCP_TTIMEDELAY   20
#define DLGCP_TTITLE       21
#define DLGCP_TWINDOWOPEN  22
#define DLGCP_TWINDOWCLOSE 23
#define DLGCP_TRANSLATE    24
#define DLGCP_GETSTRING    25
#define DLGCP_ICONIFYLOC   26
#define DLGCP_ICONIFY      27
#define DLGCP_DEICONIFY    28
