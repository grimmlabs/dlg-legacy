#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <fcntl.h>
#include <exec/types.h>
#include <libraries/dosextens.h>

#include <dlg/dlg.h>

#include <proto/exec.h>
#include <proto/dos.h>
#include <proto/dlg.h>

#include <pragmas/dlg.h>

static char version[]="$VER: PatchLang 2.0 � 1995-1997 by DLG Development ("__AMIGADATE__")";

int  main(int, char **);
void CleanUp(char *);


struct Library   *DLGBase = NULL;
BPTR              sout    = NULL;

int main(int argc, char **argv)
{
   char LangFile[256];
   char OutFile[256];
   char buf[256];
   char dir[256];

   char **lbuf;
   char **rbuf;

   FILE *fp;

   long l;
   long llines = 0;
   long rlines = 0;

   sout = Output();
   
   if (argc < 2)
      CleanUp("Usage: PatchLang <language>");
      
   if (!(DLGBase = OpenLibrary(DLGNAME, DLGVERSION)))
      CleanUp("Can't open dlg.library");

   sprintf(LangFile, "DLGConfig:Languages/%.20s.lang", argv[1]);
   sprintf(OutFile, "T:%.20s.lang", argv[1]);

   printf("Reading language file ...\n");

   fp = fopen(LangFile,"r");

   if(!fp) CleanUp("Could not open language file!");

   while(fgets(buf,255,fp))
      llines++;

   lbuf = calloc(llines,sizeof(char *));

   if(!lbuf)
   {
      fclose(fp);
      CleanUp("Memory allocation error!");
   }

   rewind(fp);

   for(l = 0; l < llines; l++)
   {
      if(fgets(buf,255,fp))
      {
         lbuf[l] = strdup(buf);
      }
      else
      {
         break;
      }
   }

   fclose(fp);

   printf("Reading patchlines ...\n");

   fp = fopen("Patchlines.lang","r");

   if(!fp) CleanUp("Could not open patchlines file!");

   while(fgets(buf,255,fp))
      rlines++;

   rbuf = calloc(rlines,sizeof(char *));

   if(!rbuf)
   {
      fclose(fp);
      CleanUp("Memory allocation error!");
   }

   rewind(fp);

   for(l = 0; l < rlines; l++)
   {
      if(fgets(buf,255,fp))
      {
         rbuf[l] = strdup(buf);
      }
      else
      {
         break;
      }
   }

   fclose(fp);

   printf("Reading patch instructions ... \n\n");

   fp = fopen("Patchfile.lang","r");

   if(!fp) CleanUp("Could not open patchfile file!");

   while(fgets(buf,255,fp))
   {
      if(!strnicmp(buf,"C",1))
      {
         char *t[5];

         long LineToReplace;
         long SourceLine;

         ArgParse(buf,t,4);

         LineToReplace = atol(t[1]);
         SourceLine = atol(t[2]);

         printf("\tReplacing line %ld with patchfile line %ld\n",LineToReplace,SourceLine);

         lbuf[LineToReplace - 1] = strdup(rbuf[SourceLine]);
      }
   }

   fclose(fp);

   printf("Writing output file ...\n");
   fp = fopen(OutFile,"w");

   if(!fp) CleanUp("Could not open output file!");

   for(l = 0; l < llines; l++)
      fputs(lbuf[l],fp);

   fclose(fp);
   free(rbuf);
   free(lbuf);

   if(0 == Copy(OutFile,LangFile))
      DeleteFile(OutFile);

   printf("done!\n\n");

   getcwd(dir,256);
//   printf("Cur dir = %s\n",dir);
   chdir("DLGConfig:Languages");   
   printf("Running CompileLang, this will take a while .... \n");
   
   sprintf(buf,"DLG:CompileLang %s",argv[1]);
   system(buf);
   chdir(dir);
   printf("All done!\n\nYou MUST reboot for the patch to take effect!!!\n\n");

   CleanUp(NULL);

}


void CleanUp(char *s)
{
   if (DLGBase)  CloseLibrary(DLGBase);

   if(s)
      printf("\n Error: %s\n\n",s);

   exit(s?5:0);
}
