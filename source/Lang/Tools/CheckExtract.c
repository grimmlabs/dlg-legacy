#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <fcntl.h>
#include <fctype.h>
#include <error.h>
#include <dos.h>
#include <exec/types.h>
#include <libraries/dosextens.h>


#include <proto/exec.h>
#include <proto/dos.h>
#include <proto/dlg.h>

int  main(int, char **);
int  cmpnames(struct NameStruct *, struct NameStruct *);
void CleanUp(char *);

BPTR              sout;
BPTR              ifp     = NULL;

struct Library   *DLGBase = NULL;
struct NameStruct Files[1000];

int main(int argc, char **argv)

{long                 nfile = 0;
 long                 cfile = 0;
 struct SearchCookie *sc;
 char                *s;

 char                 buf  [1024];
 char                 err  [256];
 char                 ifile[128];

 sout = Output();
 if (argc < 2)  CleanUp("Not enough args  CheckExtract <Input Dir>");

 if (!(DLGBase = OpenLibrary(DLGNAME, DLGVERSION)))
     CleanUp("Can't open dlg.library");

 if (!(sc = SearchStart(argv[1], "*")))
    {ASPrintf(NULL, err, "No files in [%s]", argv[1]);
     CleanUp(err);
    }

 while((s = SearchNext(sc)))
      {strcpy(Files[nfile].name, s);
       nfile++;
      }
 SearchEnd(sc);
 qsort(Files, nfile, sizeof(struct NameStruct), cmpnames);

 while(cfile < nfile)
      {s = Files[cfile].name;
       cfile++;

       ASPrintf(NULL, ifile, "%s/%s", argv[1], s);

       AFPrintf(NULL, sout, "File %s...", s);
       if (!DLGPatternMatch("*.c", s))
          {AFPrintf(NULL, sout, "Skipped\n");
           continue;
          }

       ifp = Open(ifile, MODE_OLDFILE);
       if (!ifp)
          {ASPrintf(NULL, err, "Unable to open input [%s]", ifile);
           CleanUp(err);
          }

       while(FGets(ifp, buf, 1024))
            {if (strstr(buf, "\""))
                 AFPrintf(NULL, sout, "\nFound: [%s]", buf);
            }

       Close(ifp);
       AFPrintf(NULL, sout, "Done\n");
      }

 CleanUp(NULL);
}


int cmpnames(struct NameStruct *st1, struct NameStruct *st2)

{return(Stricmp(st1->name, st2->name));
}


void CleanUp(char *s)

{if (DLGBase)  CloseLibrary(DLGBase);

 if (s)
    {Write(sout,"\n Error: ", 9);
     Write(sout, s,    strlen(s));
     Write(sout, "\n\n",      2);
    }

 exit(s?5:0);
}
