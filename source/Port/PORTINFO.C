#include <stdlib.h>
#include <string.h>
#include <exec/types.h>
#include <ctype.h>

#include <dlg/dlg.h>
#include <dlg/resman.h>

#include <link/io.h>
#include <link/lang.h>

#include <proto/exec.h>
#include <proto/dos.h>
#include <proto/dlg.h>

#include <pragmas/dlg.h>

#include <private/Version.h>
#define  ObjRev "1"
const UBYTE version[]="\0$VER: PortInfo " BUILDVER "." ObjRev " �" COPYRIGHT " by Digerati Dreams "__AMIGADATE__;

void main   (int, char **);
void Usage  (char *);
void CleanUp(char *);

BPTR            sout;
char          **SA;
struct Library *DLGBase = NULL;


void main(int argc,char **argv)

{struct PortInfo istruct;
 long            retval;
 char            port[4] = "";

 sout    = Output();
 DLGBase = OpenLibrary(DLGNAME, DLGVERSION);
 if (!DLGBase)               CleanUp("Unable to open dlg.library");
 if (!(SA = getlang(NULL)))  CleanUp("Can't read language file");

 AFPrintf(NULL, sout, "\n");

/// parse args
 while(--argc>0)
      {char *s;

       s = *++argv;
       if (*s++=='-')
          {while(*s)
                {switch(*s++)
                       {case 'p':
                        case 'P': if (!--argc)  break;
                                  strncpy(port, *++argv, 3);
                                  port[3] = 0;
                                  Upper(port);
                                  break;
                       }
                }
          }
      }
 //-

 if (!port[0])  Usage(SA[2889]);

 istruct.port = port;
 retval       = GetPortInfo(&istruct);

 if (retval == RMNOERR)
    {AFPrintf(NULL, sout, SA[2890],istruct.port);
     AFPrintf(NULL, sout, SA[2891],istruct.passwd);
     AFPrintf(NULL, sout, SA[2892],istruct.reason);
     AFPrintf(NULL, sout, SA[2893],istruct.priority);
     AFPrintf(NULL, sout, SA[2894],istruct.breakcommand);

     FreePortInfo(&istruct);
    }
  else
    if (retval == NORM)
        CleanUp(SA[2895]);
      else
        if (retval == BADPORT)
            CleanUp(SA[2896]);
          else
            if (retval == NOTLOCKED)
                CleanUp(SA[2897]);
              else
                CleanUp(SA[2898]);

 AFPrintf(NULL, sout, "\n");
 CleanUp(NULL);
}


void Usage(char *string)

{
 AFPrintf(NULL, sout, SA[2899],string);
 AFPrintf(NULL, sout, SA[2900]);

 CleanUp(NULL);
}


void CleanUp(char *s)

{if (DLGBase)  CloseLibrary(DLGBase);

 if (s)
    {Write(sout, "\n Error: ", 9);
     Write(sout,  s,    strlen(s));
     Write(sout, "\n\n",       2);
    }

 exit(s?5:0);
}
