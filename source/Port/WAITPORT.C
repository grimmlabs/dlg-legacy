#include <stdlib.h>
#include <string.h>
#include <dos.h>
#include <exec/types.h>
#include <ctype.h>

#include <dlg/dlg.h>
#include <dlg/resman.h>

#include <link/io.h>
#include <link/lang.h>

#include <proto/exec.h>
#include <proto/dos.h>
#include <proto/dlg.h>

#include <pragmas/dlg.h>

#include <private/Version.h>
#define  ObjRev "1"
const UBYTE version[]="\0$VER: WaitPort " BUILDVER "." ObjRev " �" COPYRIGHT " by Digerati Dreams "__AMIGADATE__;

void  main   (int, char **);
void  Usage  (char *);
void _CXBRK  (void);
void  CleanUp(char *);

BPTR            sout;
char          **SA;
struct Library *DLGBase = NULL;


void main(int argc,char **argv)

{struct PortInfo istruct;
 long            retval;
 char           *passwd  = "DIALOG";
 char            port[4] = "";

 sout    = Output();
 DLGBase = OpenLibrary(DLGNAME,DLGVERSION);
 if (!DLGBase)               CleanUp("Unable to open dlg.library");
 if (!(SA = getlang(NULL)))  CleanUp("Can't read language file");

 AFPrintf(NULL, sout, "\n");

/// parse args
 while(--argc>0)
      {char *s;
       s = *++argv;

       if (*s++ == '-')
          {while(*s)
                {switch(*s++)
                       {case 'p':
                        case 'P': if (!--argc)  break;
                                  strncpy(port, *++argv, 3);
                                  port[3] = 0;
                                  Upper(port);
                                  break;

                        case 'k':
                        case 'K': if (!--argc)  break;
                                  passwd = *++argv;
                                  break;
                       }
                }
          }
      }
//-

 if (!port[0])  Usage(SA[2964]);

 istruct.port = port;

 for(;;)
    {retval = GetPortInfo(&istruct);
     if (retval == RMNOERR  ||  retval == NOTLOCKED)
        {if (retval == RMNOERR)
            {if (!Stricmp(istruct.passwd,passwd))
                {FreePortInfo(&istruct);
                 break;
                }

             FreePortInfo(&istruct);
            }

         chkabort();
         Delay(50);
         continue;
        }
      else
        if (retval == NORM)
            CleanUp(SA[2965]);
          else
            if (retval == BADPORT)
                CleanUp(SA[2966]);
              else
                CleanUp(SA[2967]);

     AFPrintf(NULL, sout, "\n");
     CleanUp(NULL);
    }

 CleanUp(NULL);
}


void _CXBRK(void)

{AFPrintf(NULL, sout, SA[2968]);
 CleanUp(NULL);
}


void Usage(char *string)

{AFPrintf(NULL, sout, SA[2969],string);
 AFPrintf(NULL, sout, SA[2970]);

 CleanUp(NULL);
}


void CleanUp(char *s)

{if (DLGBase)  CloseLibrary(DLGBase);

 if (s)
    {Write(sout, "\n Error: ", 9);
     Write(sout,  s,    strlen(s));
     Write(sout, "\n\n",       2);
    }

 exit(s?5:0);
}
