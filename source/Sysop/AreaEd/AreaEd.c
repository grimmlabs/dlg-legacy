#include <exec/types.h>
#include <stdlib.h>
#include <string.h>
#include <dos.h>

#include <dlg/dlg.h>
#include <dlg/user.h>
#include <dlg/menu.h>
#include <dlg/resman.h>
#include <dlg/log.h>

#include <link/io.h>

#include <proto/dos.h>
#include <proto/exec.h>
#include <proto/dlg.h>


#define  FILE_AREA_TYPE 1

#include <pragmas/dlg.h>

#include <private/Version.h>
#define  ObjRev "6"
const UBYTE version[]="\0$VER: AreaEditor " BUILDVER "." ObjRev " �" COPYRIGHT " by Digerati Dreams "__AMIGADATE__;

void  main(int , char **);
int   HandleBuiltIn(UBYTE );
void _CXBRK(void);

extern   BYTE  Add_Area(void);
extern   void  List_Templates(void);
extern   void  Delete_Area(void);
extern   void  Copy_Template( BOOL );
extern   void  Edit_Area(struct Msg_Area *);
extern   BOOL  Move_Area(void);

BOOL              Overlay  =  FALSE;
char              Ext[4];
int               AreaType =  0;
struct USER_DATA  UserDat;
struct Ram_File   RStruct;

/// Menu definition
struct NewShortMenu Menu[8]={{"Abort",1},
                             {"AreaEd_Delete",1},
                             {"AreaEd_Edit",1},
                             {"AreaEd_Exit",1},
                             {"AreaEd_List",1},
                             {"AreaEd_Move",1},
                             {"AreaEd_New",1},
                             {"AreaEd_Temp",1},
                            };
//-

char  exitflag     =  FALSE;
char  MenuName[13] = "AreaEd_Main";

BPTR               sout;
struct Library    *DLGBase = NULL;
struct LangStruct *ls;
char             **SA;

///   Startup, initialize, loop until exit
void main(int argc, char *argv[])
{
   char *s;
   char *stack = NULL;

   sout = Output();

   if (!(DLGBase = OpenLibrary(DLGNAME, DLGVERSION)))  exit(5);

///   parse args
 while(--argc>0)
      {s=*++argv;
       if (*s++=='-')
          {while(*s)
                {switch(*s++)
                       {
                        case 'S':
                        case 's':  if (!--argc)  break;
                                   stack = *++argv;
                                   break;

                        case 'M':
                        case 'm':  if (!--argc)  break;
                                   strncpy(MenuName, *++argv, 12);
                                   MenuName[12]='\0';
                                   break;

                        case 'F':
                        case 'f':  AreaType = FILE_AREA_TYPE; break;

                        case  'O':
                        case  'o':  Overlay = TRUE;
                                    break;


                       }
                }
          }
      }
//-

   if (GetDevName(Ext) == -1)  _CXBRK();
   if (!(ls = GetLang(Ext)))   _CXBRK();
   SA = ls->strings;

   if (!ReadUser(&RStruct,&UserDat,Ext)) _CXBRK();
//   if (stack)                             InsertStack(STK, stack);

   WriteLog(SYSOP_CONFIG, RStruct.Name,Ext, "Area Editor");

   while(!exitflag)
   {
      Chk_Abort();

      if ( MenuInput(MenuName,Ext,"AreaEd",Menu,9,HandleBuiltIn,&UserDat,&RStruct,UserDat.Help_Level,NULL) == MENUNOTFOUND)
          _CXBRK();
   }

 _CXBRK();
}
//-
///   HandleBuiltIn -- Event handler
int HandleBuiltIn(UBYTE cmd)
{
   char  t[64];

   switch(cmd)
   {
      case 0:  exitflag = TRUE;                       break;   // Abort
      case 1:  Delete_Area();                         break;   // Delete
      case 2:  Edit_Area(NULL);                       break;   // Edit
      case 3: _CXBRK();                               break;   // Exit
      case 4:  ListAreas(NULL,&UserDat,AreaType,0);   break;   // List
      case 5:  Move_Area();                           break;   // Move
      case 6:  Add_Area();                            break;   // New
      case 7:                                                  // Template editor
               ASPrintf(NULL, t, "DLG:AreaTemplate -o %s", (AreaType ? "-f" : ""));
               OverlayProgram(t);
               break;

      default: return(FALSE);
   }

   return(TRUE);
}  
//-

///   _CXBRK
void _CXBRK(void)
{
   WriteRam(&RStruct, Ext);

   if (!Overlay)  ChainProgram("DLG:Menu", Ext);

   CloseLibrary(DLGBase);
   exit(0);
}
//-
