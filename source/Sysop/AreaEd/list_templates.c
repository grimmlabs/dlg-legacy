#include <exec/types.h>
#include <string.h>

#include <dlg/misc.h>
#include <dlg/user.h>

#include <link/io.h>

#include <proto/dlg.h>

#include <pragmas/dlg.h>

extern   BPTR                 sout;

extern   int                  AreaType;

extern   struct   Library    *DLGBase;
extern   struct   USER_DATA   UserDat;

extern   void GetNameOnly(char *filename);

void  List_Templates(void)
{
   char                   *filename;
   int                     position = 0;
   struct   SearchCookie  *sc;

   OneLine();
   AFPrintf(&UserDat,sout,"%s Area Templates\n",(AreaType ? "File" : "Message"));
   Draw_Line(UserDat.Screen_Width);

   sc = SearchStart("DLGConfig:Template",(AreaType ? "*.file" : "*.msg"));

   if(!sc)  { AFPrintf(&UserDat,sout,"Memory allocation error!\n\n"); return; }

   while(filename = SearchNext(sc))
   {
      char  t[36];

      strcpy(t,filename);
      GetNameOnly(t);

      AFPrintf(&UserDat,sout,"%-32.32s%s",t,(position ? "\n" : ""));

      if(position == 0)
         position++;
      else
         position = 0;
   }

   if(position == 1) OneLine();

   SearchEnd(sc);

   Draw_Line(UserDat.Screen_Width);
   TwoLines();

}

