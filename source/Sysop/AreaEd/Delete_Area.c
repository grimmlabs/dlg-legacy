#include <exec/types.h>

#include <link/io.h>
#include <link/user.h>

#include <dlg/msg.h>

#include <proto/dos.h>
#include <proto/dlg.h>

#include <pragmas/dlg.h>

extern   BPTR                 sout;

extern   char                 Ext[4];

extern   int                  AreaType;

extern   struct   Library    *DLGBase;
extern   struct   USER_DATA   UserDat;


void  Delete_Area(void);
BOOL  DeleteMsgs(LONG area);
BOOL _Delete_Area(LONG area, char *filename);

/// Delete_Area()
void  Delete_Area(void)
{
   BPTR              fh =  NULL;
   char              t[128];
   LONG              AreaNum = 0;
   struct   Msg_Area ma;
   struct   SIG_Def  Sig;
/// Get area to delete
   AreaNum = iinput(1,9999,0, "Delete which area? %a7-> ");
   TwoLines();

   if(0 == AreaNum)
   {
      ListAreas(NULL,&UserDat,AreaType,0);
      AreaNum = iinput(1,9999,0, "Delete which area? %a7-> ");
      TwoLines();
   }

   if(0 == AreaNum)
   {
      AFPrintf(&UserDat,sout,"Aborted ...\n\n");
      return;
   }
//-
/// Check if area exists
   if(!ReadArea(AreaNum,&ma,AreaType))
   {
      AFPrintf(&UserDat,sout, "%a3Area not found!\n\n%a7Aborted ...\n\n");
      return;
   }
//-
/// Make very, very sure

   if(0 == finput(0, "%a3Delete area %a2[%a7%ld%a2] %a7\"%s\" %a3- are you sure",AreaNum,ma.Name))
   {
      AFPrintf(&UserDat,sout, "%a7\n\nAborted ...\n\n");
      return;
   }

   TwoLines();
//-

   DeleteMsgs(AreaNum);
   AFPrintf(&UserDat,sout, "%a6Deleting directory %a3%s%ld %a6... ",(AreaType ? "FILE:" : "MSG:"),AreaNum);
   ASPrintf(NULL,t, "%s%ld",(AreaType ? "FILE:" : "MSG:"),AreaNum);

   if (DelDir(t, NULL))
      AFPrintf(&UserDat,sout, "%a3done.\n");
   else
      AFPrintf(&UserDat,sout, "%a1error! Could not delete directory!\n\n");

   AFPrintf(&UserDat,sout, "%a6Deleting area ... ");

   if (_Delete_Area(AreaNum,(AreaType ? "File:Area.BBS" : "MSG:Area.bbs")))
   {
      ASPrintf(NULL,t,"DLGConfig:Sigs/Sigs.%s",(AreaType ? "file" : "msg"));

      if ((fh = Open(t,MODE_OLDFILE)) != NULL)
      {
         while(Read(fh, &Sig, sizeof(struct SIG_Def)) == sizeof(struct SIG_Def))
         {
            ASPrintf(NULL,t,"DLGCONFIG:Sigs/%s.%s",Sig.name,(AreaType ? "File" : "Msg"));
            _Delete_Area(AreaNum,t);
         }

         if(fh)   Close(fh);  fh = NULL;
      }

      AddGlobalArea(0,0,AreaNum,AreaType,1,Ext);

      AFPrintf(&UserDat,sout, "%a3done.\n\n");
   }

   return;
}
//-
/// DeleteMsgs
BOOL DeleteMsgs(LONG area)
{
   LONG low;
   LONG high;

   GetHiLowPointers(area,NULL,&low,&high,"AreaEd");
   AFPrintf(&UserDat,sout, "%a6Deleting %s %ld to %ld ... ",(AreaType ? "files" : "messages"),low,high);

   for(;low<=high;low++)
   KillMsg(low,area,NULL,"AreaEd");
   AFPrintf(&UserDat,sout, "%a3done.\n");

   return(TRUE);
}
//-
/// _Delete_Area
BOOL _Delete_Area(LONG area,char *filename)
{
   char              Temp[128];
   BPTR              fh =  NULL;
   BPTR              ft =  NULL;
   struct   Msg_Area ma;

/// Open files
   if ((fh = Open(filename,MODE_OLDFILE)) == NULL)
   {
      AFPrintf(&UserDat,sout, "%a1_Delete_Area(): %a7Error - cannot open %s!\n",filename);
      return(FALSE);
   }

   ASPrintf(NULL,Temp,"%s:temp.bbs",(AreaType ? "FILE" : "MSG"));

   if ((ft = Open(Temp, MODE_NEWFILE)) == NULL)
   {
      AFPrintf(&UserDat,sout, "%a1_Delete_Area(): %a7Error - cannot open %s!\n",Temp);
      if(fh)   Close(fh);  fh = NULL;
      return(FALSE);
   }
//-

   while(Read(fh, &ma, sizeof(struct Msg_Area)) == sizeof(struct Msg_Area))
      if(ma.Number != area)
         Write(ft, &ma, sizeof(struct Msg_Area));

   if(fh)   Close(fh);  fh = NULL;
   if(ft)   Close(ft);  ft = NULL;

   if(0 == Copy(Temp,filename))
   {
      DeleteFile(Temp);
      return(TRUE);
   }
   else
   {
      AFPrintf(&UserDat,sout, "%a1_Delete_Area(): %a7Error! Cannot copy temp file!");
      return( FALSE );
   }
}
//-


