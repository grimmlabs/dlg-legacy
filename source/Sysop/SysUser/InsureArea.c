#include "SysUser.h"

BOOL InsureArea(long area, char mode)
{
   BPTR fp;
 
   struct Msg_Area Area;

   if ((fp = Open(areafile,MODE_OLDFILE))==EOF)
      return(1);
 
   while(Read(fp,&Area,sizeof(Area))==sizeof(Area))
   {
      if (Area.Number==area)
      {
         Close(fp);
         return(TRUE);
      }
   }
         
   Close(fp);
 
   return(FALSE);
}

