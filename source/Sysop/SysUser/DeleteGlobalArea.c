#include "SysUser.h"

BOOL DeleteGlobalAreas()
{
   struct Msg_Area area;
 
   BPTR            fp;
   USHORT          counter;

   if ((fp=Open(areafile,MODE_OLDFILE))==EOF)
      return(FALSE);

   while(Read(fp,&area,sizeof(area))==sizeof(area))
   {
      for(counter = 0; counter < HighSearchArea; counter++)
      {
         if (SearchArea[counter]==area.Number)
         {
            movmem((SearchArea+counter)+1,SearchArea+counter,(HighSearchArea-counter-1)*2);
            HighSearchArea--;
         }
      }
   }

   Close(fp);
 
   return(TRUE);
}
