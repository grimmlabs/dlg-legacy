#include "SysUser.h"


void UpdateData(struct USER_DATA *main,struct USER_DATA *aux,char mode)
{
   if(mode || main->User_Level < aux->User_Level)
      main->User_Level=aux->User_Level;

   // if we are applying attributes regardless of upgrades
   // OR his current ratio is lower than the new one and his 
   // current is not zero OR the new ratio is zero

   if(mode || ((main->Ratio < aux->Ratio && main->Ratio) || !aux->Ratio))
      main->Ratio=aux->Ratio;

   if(mode || main->Pop_Screen > aux->Pop_Screen)
      main->Pop_Screen=aux->Pop_Screen;

   if(mode || main->Bulletin_Access < aux->Bulletin_Access)
      main->Bulletin_Access=aux->Bulletin_Access;

   if(mode || main->Daily_Limit < aux->Daily_Limit)
      main->Daily_Limit=aux->Daily_Limit;

   if(mode || main->Session_Limit < aux->Session_Limit)
      main->Session_Limit=aux->Session_Limit;

   main->Bytes_Uploaded+=aux->Bytes_Uploaded;

   main->Bytes_Downloaded+=aux->Bytes_Downloaded;

   if(mode || main->UUCP < aux->UUCP)
      main->UUCP=aux->UUCP;

   if(mode || main->DirLimit < aux->DirLimit)
      main->DirLimit=aux->DirLimit;

   if(mode || main->credit < aux->credit)
      main->credit=aux->credit;

   if(mode || main->NetMail < aux->NetMail)
      main->NetMail=aux->NetMail;

   return;
}

