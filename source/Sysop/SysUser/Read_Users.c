#include "SysUser.h"

long Read_Users()
{
   ULONG  filesize = 0;

   if (users)  free(users);

   if (FileSize("DLGConfig:Misc/Users.bbs",&filesize)==-1)  return(FALSE);
   if (!(users = malloc(filesize)))                         return(FALSE);

   GetFirstStruct("DLGConfig:Misc/Users.bbs", (char *)users, filesize);
   return((long)(filesize/(long)sizeof(struct NameStruct)));
}

