#include "SysUser.h"

#include <link/io.h>

void Get_Computer_Type(char type,char *retstr)
{
   struct Computer_Type Ct;

   *retstr    =  0;
   Ct.Number = type;

   if (!GetStruct("DlgConfig:Port/ComputerTypes.bbs", (char *)&Ct, sizeof(struct Computer_Type), 1))
      ASPrintf(NULL,retstr, SA[2117], Ct.Name);

   return;
}

