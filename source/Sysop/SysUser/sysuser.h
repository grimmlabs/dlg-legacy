#define ANSICOL

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <dos.h>
#include <time.h>
#include <fcntl.h>
#include <stat.h>
#include <exec/types.h>
#include <libraries/dosextens.h>

#include <devices/tpt.h>

#include <dlg/dlg.h>
#include <dlg/misc.h>
#include <dlg/portconfig.h>
#include <dlg/user.h>
#include <dlg/msg.h>
#include <dlg/menu.h>
#include <dlg/resman.h>
#include <dlg/log.h>
#include <dlg/file.h>

#include <proto/dlg.h>
#include <proto/dos.h>
#include <proto/exec.h>

#include <pragmas/dlg.h>

#define INSTACK RStruct.Command_Stack[0]
#define STK     RStruct.Command_Stack
#define HOT     UserDat.Hot_Keys


extern int    HandleBuiltIn(UBYTE );
extern void  _CXBRK(void);
extern void   Print_Users(void);
extern int    Delete_User(char *, int);
extern long   Read_Users(void);
extern int    Get_UserName(char *);
extern int    Add_User(void);
extern void   Purge_Users(void);
extern int    Validate_Users(void);
extern int    RawValidate(struct USER_DATA *, char);
extern void   UpdateData(struct USER_DATA *, struct USER_DATA *, char );
extern void   Get_Computer_Type(char , char *);
extern int    Print_User(char *);
extern int    Edit_User(char *);
extern int    SRead_User(char *);
extern void   GetAnsi(void );
extern int    AddTemplate(void);
extern int    ListTemplates(char );
extern int    SelectTemplate(char *);
extern int    EditTemplate(void);
extern void   DisplayTemplate(char *, struct USER_DATA *);
extern int    DeleteTemplate(void);
extern int    GlobalTemplate(void);
extern void   List_Groups(void);
extern BOOL   PurgeAll(char *, int );

extern void   Edit_Search_Areas (char *, char);
extern void   List_Search_Areas (char);
extern void   Read_Search_Areas (char *, char);
extern void   Write_Search_Areas(char *, char);
extern BOOL   SelectSIG         (char);
extern int    SortSearchArea    (USHORT *, USHORT *);
extern BOOL   InsureArea        (long, char);
extern BOOL   Area_Clearance    (long, char *, char);
extern BOOL   DeleteGlobalAreas (void);


extern struct Global_Settings Globals;
extern struct Group_Def       Group;
extern struct USER_DATA       UserDat;
extern struct USER_DATA       User;
extern struct Ram_File        RStruct;
extern struct NameStruct     *users;
extern long                   NumUsers;

extern struct SIG_Def         Sig;
extern USHORT HighSearchArea;
extern USHORT *SearchArea;
extern char   areafile[];

extern char   Ext[];
extern char   Overlay;
extern char   exitflag;
extern char   MenuName[];

extern char *deleted;
extern int   numdeleted;

extern struct ShortMenu AreaMenu[];
extern struct ShortMenu GlobalMenu[];
extern struct NewShortMenu Menu[];
extern struct twocoldata data2[];
extern struct threecoldata data3[];

extern BPTR               sout;
extern struct Library    *DLGBase;
extern struct LangStruct *ls;
extern char             **SA;

