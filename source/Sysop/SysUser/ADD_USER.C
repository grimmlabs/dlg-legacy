#include "SysUser.h"

#include <link/io.h>
#include <link/user.h>

int Add_User(void)
{
   BPTR  fp;
   BPTR lock;
   char filename[48];
   char UserName[80];
   struct USER_DATA User;
   struct Msg_Log Log;

   UserName[0] = 0;

   if (New_User(UserName,&User,1))
   {
      AFPrintf(NULL,sout,SA[2095]);
      return(-1);
   }

   Upper(UserName);
   ASPrintf(NULL,filename, "USER:%s", UserName);
   UnderScore(filename);
   if ((lock=CreateDir(filename)) == 0L)
   {
      AFPrintf(NULL,sout,SA[2096]);
      return(-1);
   }

   UnLock(lock);
   AFPrintf(NULL,sout,SA[2097]);
   strcat(filename,"/User.Data");
   if ((fp = Open(filename,MODE_NEWFILE)) == EOF)
   {
      AFPrintf(NULL,sout,SA[2098]);
      return(-1);
   }
 
   Write(fp, &User, sizeof(User));
   Close(fp);
 
   AFPrintf(NULL,sout,SA[2099]);

   ASPrintf(NULL,filename,"USER:%s/User.Msg",UserName);
   UnderScore(filename);

   strcpy(Log.Name, UserName);
   Upper(Log.Name);

   Log.High_Mess =   0;
   Log.uflag     = -128;
   Log.dflag     =   0;
   Log.special   =   0;
   Log.Priv_Flag =   0;
   AddStruct(filename,(char *)&Log,sizeof(Log),36);

   ASPrintf(NULL,filename,"USER:%s/User.File",UserName);
   UnderScore(filename);
   AddStruct(filename,(char *)&Log,sizeof(Log),36);

   Upper(UserName);
   AddStruct("DLGCONFIG:Misc/Users.bbs", UserName, 36, 36);
   AddGlobalAreas(UserName,User.User_Level,User.User_Level,0|128,"MSG:Area.bbs");
   AddGlobalAreas(UserName,User.User_Level,User.User_Level,1|128,"FILE:Area.bbs");
   return(0);
}

