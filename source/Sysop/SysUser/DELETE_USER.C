#include "SysUser.h"

#include <link/io.h>

LONG   ADelete_User(char *, long);
LONG   FADelete_User(char *, long);

int Delete_User(char *user,int totaldelete)
{
   BPTR              fp;
   char              deldir[80];
   struct Msg_Area   Area;
   struct Msg_Area   FArea;
   struct NameStruct Name;

   ASPrintf(NULL,deldir,"USER:%s",user);
   UnderScore(deldir);
   AFPrintf(NULL,sout,SA[2086],deldir);
   DelDir(deldir, &UserDat);

   ASPrintf(NULL,deldir, "FILE:TempUploads/%s",   user);
   UnderScore(deldir);
   DelDir(deldir, &UserDat);

   ASPrintf(NULL,deldir, "FILE:TempDownloads/%s", user);
   UnderScore(deldir);
   DelDir(deldir, &UserDat);

   if (totaldelete)
   {
      AFPrintf(NULL,sout,SA[2087]);
      if ((fp = Open("MSG:Area.BBS",MODE_OLDFILE)) == EOF)  return(1);
      while(Read(fp,&Area,sizeof(Area)) == sizeof(Area))
         ADelete_User(user,Area.Number);
      Close(fp);
      ADelete_User(user, 0);

      AFPrintf(NULL,sout,SA[2088]);
      if ((fp = Open("File:Area.BBS",MODE_OLDFILE)) == EOF)  return(1);
      while(Read(fp,&FArea,sizeof(FArea)) == sizeof(FArea))
           FADelete_User(user,FArea.Number);
      Close(fp);
      AFPrintf(NULL,sout,SA[2089]);

      if ((fp=Open("DLGCONFIG:Group/Group.bbs",MODE_OLDFILE))!=EOF)
      {
         char tempname[37];

         AFPrintf(NULL,sout,SA[2090]);
         Capitalize(user);
         while(Read(fp,&Group,sizeof(Group))==sizeof(Group))
         {
            ASPrintf(NULL,deldir,"DLGCONFIG:Group/%s.Group",Group.name);
            strcpy(tempname,user);
            if (DeleteStruct(deldir,tempname,36,36)!=-1)
               AFPrintf(NULL,sout,SA[2091],user,Group.name);
         }
         
         AFPrintf(NULL,sout,SA[2092]);
         Close(fp);
      }
   }

   DeScore(user);
   strcpy(Name.name, user);
   Upper(Name.name);

   DeleteStruct("DLGCONFIG:Misc/Users.bbs",  (char *)&Name, sizeof(Name), 36);
   DeleteStruct("DLGCONFIG:Misc/NewUser.bbs",(char *)&Name, sizeof(Name), 36);

   ASPrintf(NULL,deldir, "DLGConfig:Notes/%s.notes", user);
   UnderScore(deldir);
   DeleteFile(deldir);

   if (totaldelete)
   {
      if (!(NumUsers=Read_Users()))
      {
         AFPrintf(NULL,sout,SA[2075]);
         _CXBRK();
      }
   }

   return(0);
}


LONG ADelete_User(char *name, long area)
{
   char           filename[80];
   struct Msg_Log Log;

   strcpy(Log.Name,name);
   Upper(Log.Name);

   ASPrintf(NULL,filename,"MSG:%ld/User.Msg",area);
   return(DeleteStruct(filename,(char *)&Log,sizeof(Log),36));
}


LONG FADelete_User(char *name, long area)
{
   char           filename[80];
   struct Msg_Log Log;

   strcpy(Log.Name,name);
   Upper(Log.Name);

   ASPrintf(NULL,filename,"FILE:%ld/User.File",area);
   return(DeleteStruct(filename,(char *)&Log,sizeof(Log),36));
}

