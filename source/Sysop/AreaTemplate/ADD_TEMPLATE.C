#include <exec/types.h>
#include <string.h>
#include <stdlib.h>

#include <dlg/msg.h>
#include <dlg/file.h>
#include <dlg/ansi.h>

#include <link/io.h>
#include <link/config.h>

#include <proto/dlg.h>
#include <proto/dos.h>

#include <pragmas/dlg.h>

extern   BPTR                 sout;

extern   int                  AreaType;

extern   struct   Library    *DLGBase;
extern   struct   USER_DATA   UserDat;

static   char  pr[256];

void  Set_Auto_Access(struct Msg_Area_Template *);
void  Set_Write_Access(struct Msg_Area_Template *);
void  Set_Kill_Access(struct Msg_Area_Template *);
void  Set_Forward_Access(struct Msg_Area_Template *);
void  Set_Copy_Access(struct Msg_Area_Template *);
void  Set_Edit_Access(struct Msg_Area_Template *);
void  Set_Sysop_Access(struct Msg_Area_Template *);
BOOL  Save_Template(struct Msg_Area_Template *, char *);
BOOL  IsAreaValid( SHORT );

/// Add_Template
BYTE Add_Template(void)
{
   char                       TemplateName[32];
   char                       TemplateFileName[37];
   char                       TemplateFileAndPath[256];

   long                       t;

   struct   Msg_Area_Template plate;

///   Get file name
   while(1)
   {
      t = DLGInput(NULL,TemplateName,NULL,31,31,3, "Enter name of new template %a7-> ");

      OneLine();

      if(t == 0) { AFPrintf(&UserDat,sout,"Aborting ... \n\n");   return(0); }

      UnderScore(TemplateName);
      ASPrintf(NULL,TemplateFileName,"%s%s",TemplateName,(AreaType ? ".File" : ".Msg"));
      ASPrintf(NULL,TemplateFileAndPath,"DLGConfig:Template/%s",TemplateFileName);

      if(Exists(TemplateFileAndPath))
      {
         AFPrintf(&UserDat,sout,"%s template [ %s ] already exists. Try again.\n\n",
                                    (AreaType ? "File" : "Message"),TemplateName);
         continue;
      }

      break;
   }
//-

   TwoLines();

   plate.Flag = 0;

///   Auto-access
   t = finput(1, "Is this an auto-access area? %a7-> ");
   TwoLines();

   if(t)
   {
      plate.Flag = plate.Flag | AUTO_ACCESS_AREA;
      Set_Auto_Access(&plate);
   }
   else
   {
      plate.llevel = 1;
      plate.ulevel = 1;
   }
//-

   plate.lwrite = plate.lkill = plate.lforward = plate.lcopy = plate.ledit = plate.lsysop = 1;
   plate.uwrite = plate.ukill = plate.uforward = plate.ucopy = plate.uedit = plate.usysop = 1;

   Set_Write_Access(&plate);     // upload or write
   Set_Kill_Access(&plate);      // kill
   Set_Forward_Access(&plate);   // forward or download
   Set_Copy_Access(&plate);      // copy or xfer

   if(AreaType == 0) Set_Edit_Access(&plate);

   Set_Sysop_Access(&plate);

///   Alias flag
   if (finput(0, "Allow aliases (handles)? %a7-> "))         plate.Flag |= HANDLES_AREA;
   OneLine();
//-
///   Signature flag
   if (finput(1,"Use signatures? %a7-> "))                  plate.Flag |= SIGNATURE_AREA;
   OneLine();
//-
///   Echomail flag
   if(AreaType == 0) // msg only
   {
      if (finput(0, "Will this be an echomail area? %a7-> "))   plate.Flag |= ECHO_AREA;
      OneLine();
   }
//-
///   Validation flag
   if(AreaType == 1) // file only
   {
      if (finput(0, "Will uploaded files require validation? %a7-> "))   plate.Flag |= VALIDATION_AREA;
      OneLine();
   }
//-
///   FREQ flag
   if(AreaType == 1) // file only
   {
      if (finput(0, "File Requestable (FREQ) area? %a7-> "))   plate.Flag |= FREQ_AREA;
      OneLine();
   }
//-
///   Temp_Download flag
   if(AreaType == 1) // file only
   {
      if (finput(0, "Copy files before downloading? %a7-> "))   plate.Flag |= TEMP_DOWNLOAD;
      OneLine();
   }
//-

   strcpy(plate.origin,"");

///   If echo area, set up flags and origin, otherwise check for newsgroup flag and set up newsgroup
   if(AreaType == 0) // msg only
   {
      if (plate.Flag & ECHO_AREA)
      {
///      Set up echo area
         if (finput(0, "Hide SEEN-BY kludges? %a7-> "))         plate.Flag |= HIDE_SEENBY;
         OneLine();

         DLGInput(NULL,plate.origin,NULL,74,74,0, "Custom Origin line (return for default) %a7-> ");
         OneLine();
//-
      }
      else
      {
///      Netmail Flag
         if (finput(0, "Is this a Netmail area? %a7-> "))       plate.Flag |= NETMAIL_AREA;
         OneLine();
//-
///      Newsgroup flag
         if(!(plate.Flag & NETMAIL_AREA))
         {
            if (finput(0, "Is this a UseNet newsgroup? %a7->"))
               plate.Flag |= NEWSGROUP_AREA;

            OneLine();
         }
//-
      }
   }
//-

///   Message processor batch file
   if(AreaType == 0) // msg only
   {
      DLGInput(NULL,plate.processor,NULL,11,11,0, "Message processor batch file  DLGConfig:Batch/");
      OneLine();
   }
//-
///   Capacity /  Validation area
   if(AreaType == 0) // msg area
   {
      plate.Capacity   = iinput(10,9000,100, "How many messages do you wish to store in this area? %a7-> ");
      OneLine();
   }
   else  // file area
   {
      if(plate.Flag & VALIDATION_AREA)
      {
         BOOL  b  =  FALSE;

         while(!b)
         {
            plate.Capacity   = iinput(0,9999,0, "Which file area do unvalidated files get sent to? %a7-> ");
            OneLine();

            if(plate.Capacity != 0)
               b = IsAreaValid(plate.Capacity);
            else
               b = TRUE;
         }
      }
   }
//-
///   Renumber
   if(AreaType == 0)
   {
      plate.UpperLimit = (SHORT)iinput(10,9000,500, "How many messages will trigger a renumber? %a7-> ");
      TwoLines();
   }
//-
///   Character sets
   if(AreaType == 0)
   {
      ListSets(255);
      plate.savecharset = iinput(1,255,0, "Character set to store messages in [RETURN for default] %a7-> ");
      OneLine();
   }
//-

   AFPrintf(&UserDat,sout,"\nSaving template ... ");

   if(!Save_Template(&plate,TemplateFileAndPath)) return(0);

   AFPrintf(&UserDat,sout,"Done!\n\n");
   return(1);
}
//-

/// Set_Auto_Access
void Set_Auto_Access(struct Msg_Area_Template *plate)
{
   long l   =  plate->llevel;
   long u   =  plate->ulevel;

   if((l == 1) && (u == 1))   u = 255;

   plate->llevel = iinput(0,255,l, "\tUser level required (lower)" WHT " -> ");
   OneLine();

   plate->ulevel = iinput(plate->llevel,255,u, "\tUser level required (upper)" WHT " -> ");
   TwoLines();
}
//-
/// Set_Write_Access
void Set_Write_Access(struct Msg_Area_Template *plate)
{
   BOOL  b;
   long l   =  plate->lwrite;
   long u   =  plate->uwrite;

   b = (plate->Flag & AUTO_ACCESS_AREA);

   if(l == u){l = (b ? plate->llevel : 1) ; u = 255;}

   plate->lwrite=iinput((b ? plate->llevel : 1), (b ? plate->ulevel : 255), l, "\tLevel required to %s (lower)" WHT " -> ",(AreaType ? "upload files" : "write messages"));
   OneLine();

   plate->uwrite=iinput(plate->lwrite, (b ? plate->ulevel : 255),u, "\tLevel required to %s (upper)" WHT " -> ",(AreaType ? "upload files" : "write messages"));
   TwoLines();
}
//-
/// Set_Kill_Access
void Set_Kill_Access(struct Msg_Area_Template *plate)
{
   BOOL  b;
   long l   =  plate->lkill;
   long u   =  plate->ukill;

   b = (plate->Flag & AUTO_ACCESS_AREA);

   if(l == u){l = (b ? plate->llevel : 1) ; u = 255;}

   plate->lkill=iinput( (b ? plate->llevel : 1), (b ? plate->ulevel : 255), l, "\tLevel required to kill %s (lower)" WHT " -> ",(AreaType ? "files" : "messages"));
   OneLine();

   plate->ukill=iinput(plate->lkill, (b ? plate->ulevel : 255), u, "\tLevel required to kill %s (upper)" WHT " -> ",(AreaType ? "files" : "messages"));
   TwoLines();
}
//-
/// Set_Forward_Access
void Set_Forward_Access(struct Msg_Area_Template *plate)
{
   BOOL  b;
   long l   =  plate->lforward;
   long u   =  plate->uforward;

   b = (plate->Flag & AUTO_ACCESS_AREA);

   if(l == u){l = (b ? plate->llevel : 1) ; u = 255;}

   plate->lforward=iinput((b ? plate->llevel : 1), (b ? plate->ulevel : 255), l, "\tLevel required to %s (lower)" WHT " -> ",(AreaType ? "download files" : "forward messages"));
   OneLine();

   plate->uforward=iinput(plate->lforward, (b ? plate->ulevel : 255), u, "\tLevel required to %s (upper)" WHT " -> ",(AreaType ? "download files" : "forward messages"));
   TwoLines();
}
//-
/// Set_Copy_Access
void Set_Copy_Access(struct Msg_Area_Template *plate)
{
   BOOL  b;
   long l   =  plate->lcopy;
   long u   =  plate->ucopy;

   b = (plate->Flag & AUTO_ACCESS_AREA);

   if(l == u){l = (b ? plate->llevel : 1) ; u = 255;}

   plate->lcopy = iinput((b ? plate->llevel : 1), (b ? plate->ulevel : 255), l, "\tLevel required to %s (lower)" WHT" -> ",(AreaType ? "transfer files" : "copy/move messages"));
   OneLine();

   plate->ucopy = iinput(plate->lcopy, (b ? plate->ulevel : 255), u, "\tLevel required to %s (upper)" WHT " -> ",(AreaType ? "transfer files" : "copy/move messages"));
   TwoLines();
}
//-
/// Set_Edit_Access
void Set_Edit_Access(struct Msg_Area_Template *plate)
{
   BOOL  b;
   long l   =  plate->ledit;
   long u   =  plate->uedit;

   if(AreaType) return;

   b = (plate->Flag & AUTO_ACCESS_AREA);

   if(l == u){l = (b ? plate->llevel : 1) ; u = 255;}

   plate->ledit=iinput((b ? plate->llevel : 1), (b ? plate->ulevel : 255), l, "\tLevel required to edit messages (lower)" WHT " -> ");
   OneLine();

   plate->uedit=iinput(plate->ledit, (b ? plate->ulevel : 255), u, "\tLevel required to edit messages (upper)" WHT " -> ");
   TwoLines();
}
//-
/// Set_Sysop_Access
void Set_Sysop_Access(struct Msg_Area_Template *plate)
{
   BOOL  b;
   long l   =  plate->lsysop;
   long u   =  plate->usysop;

   if(l == u == 1){ l = 255; u = 255;}

   b = (plate->Flag & AUTO_ACCESS_AREA);

   plate->lsysop=iinput((b ? plate->llevel : 1), (b ? plate->ulevel : 255), l, "\tLevel required for Sysop access (lower)" WHT " -> ");
   OneLine();

   plate->usysop=iinput(plate->lsysop, (b ? plate->ulevel : 255), u, "\tLevel required for Sysop access (upper)" WHT " -> ");
   TwoLines();
}
//-

/// Save_Template
BOOL Save_Template(struct Msg_Area_Template *plate, char *Name)
{
   BPTR  fh;
   int   i;

   Upper(Name);

   fh = Open(Name,MODE_NEWFILE);

   if(!fh)
   {
      AFPrintf(&UserDat,sout,"Save_Template(): Serious error -- could not save file!\n\n");
      return( FALSE );
   }

   i = Write(fh,plate,sizeof(struct Msg_Area_Template));
   Close(fh);

   if(i != sizeof(struct Msg_Area_Template))
   {
      AFPrintf(&UserDat,sout,"Save_Template(): Serious error -- data corrupted!\n\n");
      return( FALSE );
   }

   return( TRUE );
}
//-

/// IsAreaValid
BOOL  IsAreaValid( SHORT Area)
{
   char                 t[256];
   int                  i;
   struct   Msg_Area   *a  =  NULL;
   ULONG                l  =  0;


   if(-1 == FileSize(t,&l))   return( FALSE );

   a  =  calloc(1,l);

   if(!a) return( FALSE );

   ASPrintf(NULL,t,"%s:Area.BBS",(AreaType ? "FILE" : "MSG"));

   if(-1 == GetFirstStruct(t,(char *)a,l))   return( FALSE );

   for(i = 0; i < (l/sizeof(struct Msg_Area)); i++)
   {
      if(a[i].Number == Area)   { free(a); a = NULL; return( TRUE ); }
   }

   free(a); a = NULL;
   return( FALSE );
}
//-
