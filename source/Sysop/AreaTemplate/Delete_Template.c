#include <exec/types.h>
#include <stdlib.h>

#include <link/io.h>

#include <proto/dos.h>

extern   BPTR                 sout;

extern   int                  AreaType;

extern   struct   Library    *DLGBase;
extern   struct   USER_DATA   UserDat;

extern   int   Create_Array(char **);
extern   void  List_Templates(void);

void  Delete_Template(void)
{
   char *Array =  NULL;
   char  t[128];
   char  Victim[36];
   int   Entries;

   Entries = Create_Array(&Array);

   if(Entries == 0) return;

   List_Templates();

   DLGInput(Array,Victim,0,36,Entries,6, "Delete which template? %a7-> ");
   TwoLines();

   Entries = finput(0,"Really delete %s template [%s] ? ",(AreaType ? "File" : "Message"),Victim);
   TwoLines();

   if(Entries == 1)
   {
      ASPrintf(NULL,t,"DLGConfig:Template/%s%s",Victim,(AreaType ? ".file" : ".msg"));
      if(!DeleteFile(t))
         AFPrintf(&UserDat,sout,"Delete_Template(): could not delete file %s\n\n",t);
      else
         AFPrintf(&UserDat,sout,"Template deleted.\n\n");
   }

   if(Array)   free(Array);
   return;
}

