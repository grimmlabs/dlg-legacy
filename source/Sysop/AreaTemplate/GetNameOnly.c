#include <exec/types.h>
#include <string.h>

void GetNameOnly(char *filename)
{
   char *t;
   int   i;

   t = strdup(filename);

   for(i = (strlen(t) - 1); i > 0; i--)
   {
      if(t[i] == '.')
      {
         t[i] = 0;
         break;
      }
   }

   strcpy(filename,t);
   return;
}

