#include <exec/types.h>
#include <stdlib.h>

#include <dlg/ansi.h>

#include <link/io.h>

#include <proto/dlg.h>
#include <proto/dos.h>

#include <pragmas/dlg.h>

extern   BPTR                 sout;

extern   int                  AreaType;

extern   struct   Library    *DLGBase;
extern   struct   USER_DATA   UserDat;

extern   int   Create_Array(char **);
extern   void  List_Templates(void);

static   char  pr[256];

void  Copy_Template( BOOL CopyMode)
{
   char *Array =  NULL;
   char  t[128];
   char  p[128];
   char  Source[36];
   char  Dest[36];
   int   Entries;

   Entries = Create_Array(&Array);

   if(Entries == 0) return;

   List_Templates();

   DLGInput(Array,Source,0,36,Entries,6, "%s which template? " WHT "-> ",(CopyMode ? "Copy" : "Rename"));
   TwoLines();

   Entries = DLGInput(NULL,Dest,NULL,36,36,1, "%s to? " WHT "-> ",(CopyMode ? "Copy" : "Rename"));
   TwoLines();

   if(Entries == 0)
   {
      AFPrintf(&UserDat,sout,"Aborted...\n\n");
      if(Array)   free(Array);
      return;
   }

   ASPrintf(NULL,t,"DLGConfig:Template/%s%s",Source,(AreaType ? ".file" : ".msg"));
   ASPrintf(NULL,p,"DLGConfig:Template/%s%s",Dest,(AreaType ? ".file" : ".msg"));

   if(CopyMode)
      Entries = Copy(t,p);
   else
      Entries = SmartRename(t,p);

   switch(Entries)
   {
      case 2: AFPrintf(&UserDat,sout, RED "Error: " YEL "cannot open destination file.\n\n");  break;

      case 1: AFPrintf(&UserDat,sout, RED "Error: " YEL "cannot open source file.\n\n");  break;

      case 0:  AFPrintf(&UserDat,sout,"Done.\n\n");   break;

      case -1: if(CopyMode)
                  AFPrintf(&UserDat,sout, RED "Error: " YEL "drive is full.\n\n");
               else
                  AFPrintf(&UserDat,sout, RED "Error: " YEL "destination file already exists.\n\n");

               break;

      case -2: AFPrintf(&UserDat,sout, RED "Error " YEL "renaming file.\n\n");   break;
   }

   if(Array)   free(Array);
   return;
}

