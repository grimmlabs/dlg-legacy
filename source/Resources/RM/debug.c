#include <string.h>
#include <devices/tpt.h>
#include <dlg/Debug.h>
#include <proto/exec.h>

#if MAKE_DEBUG == 1
   #define DO_DEBUG 1
#else
   #undef DO_DEBUG
#endif

extern   void                 TDebug(char *);

struct MsgPort     *dport = NULL;
struct MsgPort     *drport = NULL;

int   DEBUG_PORT_TYPE = RM_PORT;

void InitDebug(void);
void KillDebug(void);

/// InitDebug()
void InitDebug(void)
{
#ifdef DO_DEBUG
   if (dport = (struct MsgPort *) FindPort(DEBUG))
   {
      drport = (struct MsgPort *) CreateMsgPort();
      drport->mp_Node.ln_Name = strdup("RMDebug");
      AddPort(drport);

      TDebug("Started up RM debug port");
   }
#endif
}
//-
/// KillDebug()
void KillDebug(void)
{
#ifdef DO_DEBUG
   if (drport)
   {
      RemPort(drport);
      DeleteMsgPort(drport);
   }
#endif
}
//-

