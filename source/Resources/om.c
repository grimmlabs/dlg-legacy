#include <exec/types.h>
#include <dos/dosextens.h>
#include <dos/var.h>
#include <dos.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <proto/dos.h>
#include <proto/exec.h>

#include <dlg/dlg.h>
#include <dlg/user.h>

#include <proto/dlg.h>

#include <pragmas/dlg.h>

#include <private/Version.h>
#define  ObjRev "1"
const UBYTE version[]="\0$VER: Overlay Manager " BUILDVER "." ObjRev " �" COPYRIGHT " by Digerati Dreams "__AMIGADATE__;

void  main   (int, char **);
void  Usage  (void);
void _CXBRK  (void);
void  CleanUp(char *s);

struct Library  *DLGBase = NULL;
BPTR             sout;
char             port[4] = "";

void main(int argc, char **argv)

{char  buf[256];

 sout = Output();
 if (argc<2)  Usage();

 DLGBase = OpenLibrary(DLGNAME, DLGVERSION);
 if (!DLGBase)                CleanUp("Unable to open DLG.library");
 if (GetDevName(port) == -1)  CleanUp("Unable To Identify Port");

 SetVar(port, GetArgStr(), -1, GVF_LOCAL_ONLY);
 while(GetVar(port, buf, sizeof(buf), GVF_LOCAL_ONLY) > 0)
      {SetVar(port, "", -1, GVF_LOCAL_ONLY);
       OverlayProgram(buf);
      }

 CleanUp(NULL);
}


void Usage(void)

{Write(sout, "\n Usage: OM <prog> [<args>]\n\n", 29);

 CleanUp(NULL);
}


void _CXBRK()

{CleanUp(NULL);
}

void CleanUp(char *s)

{if (s)
    {Write(sout, "\n Error: ", 9);
     Write(sout,  s,   strlen(s));
     Write(sout, "\n\n",       2);
    }

 if (DLGBase)  CloseLibrary(DLGBase);
 if (port[0])  DeleteVar(port, GVF_LOCAL_ONLY);

 exit(s?5:0);
}
