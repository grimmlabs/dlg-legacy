#include <stdlib.h>
#include <string.h>
#include <exec/types.h>
#include <ctype.h>

#include <dlg/dlg.h>
#include <dlg/resman.h>

#include <link/io.h>
#include <link/lang.h>

#include <proto/exec.h>
#include <proto/dos.h>
#include <proto/dlg.h>

#include <pragmas/dlg.h>

#include <private/Version.h>
#define  ObjRev "1"
const UBYTE version[]="\0$VER: GetResource " BUILDVER "." ObjRev " �" COPYRIGHT " by Digerati Dreams "__AMIGADATE__;

void main   (int, char **);
void Usage  (char *);
void CleanUp(char *);

BPTR            sout;
char          **SA;
struct Library *DLGBase = NULL;


void main(int argc, char **argv)

{long  retval;
 char *name    =  NULL;
 char *passwd  = "DIALOG";
 char *reason  = "Unspecified";
 char  pri     =  0;
 char  flags   =  WRITELOCK | PENDLOCK;

 sout    = Output();
 DLGBase = OpenLibrary(DLGNAME,DLGVERSION);
 if (!DLGBase)               CleanUp("Unable to open dlg.library");
 if (!(SA = getlang(NULL)))  CleanUp("Can't read language file");

 AFPrintf(NULL, sout, "\n");

 while(--argc>0)
      {char *s;

       s = *++argv;
       if (*s++=='-')
          {while(*s)
                {switch(toupper(*s))
                       {case 'N': if (!--argc) break;
                                  name = *++argv;
                                  break;

                        case 'K': if (!--argc) break;
                                  passwd = *++argv;
                                  break;

                        case 'R': if (!--argc) break;
                                  reason = *++argv;
                                  break;

                        case 'L': if (!--argc) break;
                                  pri = atoi(*++argv);
                                  break;

                        case 'D': flags &= ~WRITELOCK;
                                  break;

                        case 'I': flags &= ~PENDLOCK;
                                  break;
                       }

                 s++;
                }
          }
      }
 if (!name)  Usage(SA[2823]);

 retval = LockResource(name,passwd,reason,pri,flags);
 if (retval == RMNOERR)
     AFPrintf(NULL, sout, SA[2824]);
   else
     if (retval == NORM)
         CleanUp(SA[2825]);
       else
         if (retval == NOTLOCKED)
             CleanUp(SA[2826]);
           else
             CleanUp(SA[2827]);

 AFPrintf(NULL, sout, "\n");
 CleanUp(NULL);
}


void Usage(char *string)

{
 AFPrintf(NULL, sout, SA[2828],string);
 AFPrintf(NULL, sout, SA[2829]);
 AFPrintf(NULL, sout, SA[2830]);
 AFPrintf(NULL, sout, SA[2831]);
 AFPrintf(NULL, sout, SA[2832]);
 AFPrintf(NULL, sout, SA[2833]);
 AFPrintf(NULL, sout, SA[2834]);
 AFPrintf(NULL, sout, SA[2835]);

 CleanUp(NULL);
}


void CleanUp(char *s)

{if (DLGBase)  CloseLibrary(DLGBase);

 if (s)
    {Write(sout, "\n Error: ", 9);
     Write(sout,  s,    strlen(s));
     Write(sout, "\n\n",       2);
    }

 exit(s?5:0);
}
