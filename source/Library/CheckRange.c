#include <exec/types.h>

#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include <exec/libraries.h>

#include <proto/exec.h>
#include <proto/dos.h>
#include <proto/DLG.h>

#include <pragmas/dlg.h>

LONG __saveds __asm LIBArgParse(register __a0 char  *, register __a1 char **, register __d0 UBYTE);
BOOL __saveds __asm LIBDLGPatternMatch(register __a0 char *, register __a1 char *);




/****************************************************************
**
** CheckRange - Check to see if a number is within a specified range
**
** Inputs
**
** long  Number   The number to investigate
**
** char *ORange   A string with an area range pattern in it. Spaces, commas,
**                dashes, and the + symbol are all recognized.
**
** long  Max      Maximum number to go up to
*/


BOOL __saveds __asm LIBCheckRange(  register __d0 long Number,
                                    register __a1 char *ORange,
                                    register __a0 long Max)
{
   char    *t[100];
   char    *Range;

   int      c = 0;
   int      i;

   if(Number > Max) return(FALSE);
   if(strlen(ORange) == 0) return(FALSE);

/// Pre-condition range string
   Range = strdup(ORange);          // We make a copy because this function is destructive

   for(i = 0; i < strlen(Range); i++)
   {
      if(Range[i] == ',') Range[i] = ' ';
   }
//-

   c = LIBArgParse(Range,t,99);

   if(c == 0) return(FALSE);

   for(i = 0; i < c; i++)
   {
      if(LIBDLGPatternMatch("*-*",t[i]))
      {
         char *p[3];
         int   d;
         long  j;

         for(j = 0; j < strlen(t[i]); j++)
         {
            if(t[i][j] == '-') t[i][j] = ' ';
         }

         d = LIBArgParse(t[i],p,2);

         if(d > 1)
         {
            for(j = atol(p[0]); j <= atol(p[1]); j++)
            {
               if(Number == j) return(TRUE);
            }
         }
      }

      if(LIBDLGPatternMatch("*+",t[i]))
      {
         long  j;

         for(j = 0; j < strlen(t[i]); j++)
         {
            if(t[i][j] == '+') t[i][j] = ' ';
         }

         for(j = atol(t[i]); j < Max; j++)
         {
            if(Number == j) return(TRUE);
         }
      }

      if(Number == atol(t[i])) return(TRUE);
   }

   return(FALSE);
}


