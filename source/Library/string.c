#include <string.h>
#include <ctype.h>
#include <exec/types.h>

#include <pragmas/dlg.h>

/* Compares two strings ignoring case */
LONG __saveds __asm LIBStricmp(register __a0 char *str1,
                               register __a1 char *str2)
{for(; *str1 && *str2; str1++,str2++)
     if (toupper(*str1) != toupper(*str2))
         break;

 return(toupper(*str1)-toupper(*str2));
}


/* Compares the first 'n' letters of two strings ignoring case */
LONG __saveds __asm LIBStrnicmp(register __a0 char *str1,
                                register __a1 char *str2,
                                register __d0 USHORT n)

{for(;*str1 && *str2 && --n; str1++,str2++)
      if (toupper(*str1) != toupper(*str2))
          break;

 return(toupper(*str1)-toupper(*str2));
}


void __saveds __asm LIBUpper(register __a0 char *string)

{for(;*string; string++)
     *string = toupper(*string);
}


void __saveds __asm LIBCapitalize(register __a0 char *string)

{char upperflag;

 upperflag = 1;

 for(; *string; string++)
    {if (upperflag)
        {*string   = toupper(*string);
         upperflag = 0;
        }
      else
        *string = tolower(*string);

    if (*string == 32  ||  *string == '-'  ||  *string == 39)
        upperflag=1;
   }
}


void __saveds __asm LIBUnderScore(register __a0 char *string)

{for(; *string; string++)
     if (*string == ' ')  *string = '_';
}


void __saveds __asm LIBDeScore(register __a0 char *string)

{for(; *string; string++)
     if (*string == '_')  *string = ' ';
}


void __saveds __asm LIBStripSpaces(register __a0 char *string)

{char *s;
 long  len;

 for(s = string; *s == ' '; s++);
 len = strlen(s);
 if (s != string)   movmem(s, string, len+1);

 if (len)
    {for(s = string + len - 1; *s == ' '; s--);
     s++;
    *s = 0;
    }
}
