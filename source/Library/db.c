#include <string.h>

#include <libraries/dos.h>
#include <libraries/dosextens.h>

#include <proto/dos.h>
#include <proto/exec.h>
#include <proto/dlg.h>

#include <pragmas/dlg.h>

extern char            nline;

void __saveds __asm LIBMDate(register __a0 char *);


BOOL __saveds __asm LIBDB(register __a0 char *string)

{BPTR sout;
 char date[30];

#ifndef DO_DEBUG
 sout = Output();
#endif

#ifdef DO_DEBUG
 if (!(sout = Open("DLGConfig:Misc/DB.log", MODE_OLDFILE)))
     if (!(sout = Open("DLGConfig:Misc/DB.log", MODE_NEWFILE)))
           return(FALSE);
 Seek(sout, 0, OFFSET_END);
#endif

 Forbid();
 LIBMDate(date);

 Write(sout, &date[4], strlen(&date[4]));
 Write(sout,  " ", 1);
 Write(sout,  string, strlen(string));
 Write(sout, &nline, 1);
 Permit();

#ifdef DO_DEBUG
 Close(sout);
#endif

#ifndef DO_DEBUG
 Delay(50);
#endif

 return(TRUE);
}
