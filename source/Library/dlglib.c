/*                                       */
/* User library interface to dlg.library */
/*                                       */

#include <exec/types.h>

#include <devices/timer.h>

#include <dos/dos.h>
#include <proto/exec.h>

#include <pragmas/dlg.h>

extern struct ExecBase   *SysBase; 
       struct DosLibrary *DOSBase  = NULL;
       struct timerequest tr;


int __saveds __asm __UserLibInit(register __a6 struct MyLibrary *libbase)

{long   error;

 error   = OpenDevice(TIMERNAME, UNIT_MICROHZ, (struct IORequest *)&tr, 0);
 DOSBase = (struct DosLibrary *)OpenLibrary(DOSNAME, 0);

 if (!DOSBase || error)  return(1);

 tr.tr_node.io_Message.mn_Node.ln_Pri  = 0;
 tr.tr_node.io_Message.mn_Node.ln_Name = NULL;
 return(0);
}


void __saveds __asm __UserLibCleanup(register __a6 struct MyLibrary *libbase)

{CloseLibrary((struct Library *)DOSBase);
 CloseDevice((struct IORequest *)&tr);
 return;
}
