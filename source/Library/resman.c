#include <exec/types.h>
#include <exec/io.h>

#include <libraries/dosextens.h>

#include <dlg/resman.h>

#include <proto/exec.h>
#include <proto/dlg.h>

#include <pragmas/dlg.h>

LONG __saveds __asm LIBGetDevName(register __a0 char *);
BOOL __saveds __asm LIBDB        (register __a0 char *);

extern struct DosLibrary *DOSBase;

/// ResourceMsg
LONG __saveds __asm LIBResourceMsg(register __a0 struct RMMessage *rmess)

{struct MsgPort *rmctl;
 struct MsgPort *myport;

 Forbid();
 rmctl = FindPort(RMCONTROL);
 Permit();
 if (!rmctl)  return(NORM);

 myport = CreateMsgPort();
 if (!myport)  return(RMNOMEM);

 rmess->mess.mn_Node.ln_Type = NT_MESSAGE;
 rmess->mess.mn_ReplyPort    = myport;
 rmess->mess.mn_Length       = sizeof(struct RMMessage);

 PutMsg(rmctl, (struct Message *)rmess);

 while(!GetMsg(myport))
        WaitPort(myport);
 DeleteMsgPort(myport);

 return((LONG)rmess->type);
}
//-

/// ActivatePort
LONG __saveds __asm LIBActivatePort(register __a0 char *port,
                                    register __a1 char *bgcommand)
{struct RMMessage rmess;

 rmess.type         = ACTIVATEPORT;
 rmess.port         = port;
 rmess.breakcommand = bgcommand;

 return(LIBResourceMsg(&rmess));
}
//-

/// DeActivatePort
LONG __saveds __asm LIBDeActivatePort(register __a0 char *port,
                                      register __a1 char *passwd)

{struct RMMessage rmess;

 rmess.type   = DEACTIVATEPORT;
 rmess.port   = port;
 rmess.passwd = passwd;

 return(LIBResourceMsg(&rmess));
}
//-

/// LockPort
LONG __saveds __asm LIBLockPort(register __a0 char *port,
                                register __a1 char *passwd,
                                register __a2 char *reason,
                                register __d0 char  pri,
                                register __a3 char *bc)

{struct RMMessage rmess;

 rmess.type         = LOCKPORT;
 rmess.port         = port;
 rmess.passwd       = passwd;
 rmess.reason       = reason;
 rmess.priority     = pri;
 rmess.breakcommand = bc;
 rmess.flags        = PENDLOCK|WRITELOCK;

 return(LIBResourceMsg(&rmess));
}
//-

/// ImmedLockPort
LONG __saveds __asm LIBImmedLockPort(register __a0 char *port,
                                     register __a1 char *passwd,
                                     register __a2 char *reason,
                                     register __d0 char pri,
                                     register __a3 char *bc)

{struct RMMessage rmess;

 rmess.type         = LOCKPORT;
 rmess.port         = port;
 rmess.passwd       = passwd;
 rmess.reason       = reason;
 rmess.priority     = pri;
 rmess.breakcommand = bc;
 rmess.flags        = WRITELOCK;

 return(LIBResourceMsg(&rmess));
}
//-

/// TransferPortLock
LONG __saveds __asm LIBTransferPortLock(register __a0 char *port,
                                        register __a1 char *passwd,
                                        register __a2 char *newpasswd,
                                        register __a3 char *reason,
                                        register __d0 char pri,
                                        register __d1 char *abc)

{char            *bc = abc;
 struct RMMessage rmess;

 rmess.type         = TRANSFER;
 rmess.port         = port;
 rmess.passwd       = passwd;
 rmess.newpasswd    = newpasswd;
 rmess.reason       = reason;
 rmess.priority     = pri;
 rmess.breakcommand = bc;

 return(LIBResourceMsg(&rmess));
}
//-

/// FreePort
LONG __saveds __asm LIBFreePort(register __a0 char *port,
                                register __a1 char *passwd)

{struct RMMessage rmess;

 rmess.type   = FREEPORT;
 rmess.port   = port;
 rmess.passwd = passwd;

 return(LIBResourceMsg(&rmess));
}
//-

/// GetPortInfo
LONG __saveds __asm LIBGetPortInfo(register __a0 struct PortInfo *istruct)

{struct RMMessage rmess;

 rmess.type    = GETPORTINFO;
 rmess.port    = istruct->port;
 rmess.dataptr = istruct;

 return(LIBResourceMsg(&rmess));
}
//-

/// FreePortInfo
LONG __saveds __asm LIBFreePortInfo(register __a0 struct PortInfo *istruct)

{struct RMMessage rmess;

 rmess.type    = FREEPORTINFO;
 rmess.port    = istruct->port;
 rmess.dataptr = istruct;

 return(LIBResourceMsg(&rmess));
}
//-

/// LockArea
LONG __saveds __asm LIBLockArea(register __d0 USHORT area,
            register __a0 char *passwd,
            register __a1 char *reason,
            register __d1 char pri,
            register __d2 UBYTE flags)

{struct RMMessage rmess;

 rmess.type     = LOCKAREA;
 rmess.area     = area;
 rmess.port     = "";
 rmess.passwd   = passwd;
 rmess.reason   = reason;
 rmess.priority = pri;
 rmess.flags    = flags;

 return(LIBResourceMsg(&rmess));
}
//-

/// BorrowArea
LONG __saveds __asm LIBBorrowArea(register __d0 USHORT area,
                                  register __a0 char  *passwd,
                                  register __a1 char  *reason,
                                  register __d1 char   pri,
                                  register __d2 UBYTE  flags)

{struct RMMessage rmess;

 rmess.type     = LOCKAREA;
 rmess.area     = area;
 rmess.port     = "";
 rmess.passwd   = passwd;
 rmess.reason   = reason;
 rmess.priority = pri;
 rmess.flags    = flags|QUICKLOCK;

 return(LIBResourceMsg(&rmess));
}
//-

/// FreeArea
LONG __saveds __asm LIBFreeArea(register __d0 USHORT area,
                                register __a0 char  *passwd,
                                register __d1 UBYTE  flags)

{struct RMMessage rmess;

 rmess.type   = FREEAREA;
 rmess.area   = area;
 rmess.port   = "";
 rmess.passwd = passwd;
 rmess.flags  = flags;

 return(LIBResourceMsg(&rmess));
}
//-

/// EnterArea
LONG __saveds __asm LIBEnterArea(register __d0 USHORT area,
                                 register __d1 UBYTE flags)
{struct RMMessage rmess;


 if (flags & ENTERPEND)
    {flags &= ~ENTERPEND;
     LIBBorrowArea(area, "RM", "Entering Area", 64, flags);
     LIBFreeArea  (area, "RM",  flags);
    }

 rmess.type  = ENTERAREA;
 rmess.area  = area;
 rmess.port  = "";
 rmess.flags = flags;

 return(LIBResourceMsg(&rmess));
}
//-

/// LeaveArea
LONG __saveds __asm LIBLeaveArea(register __d0 USHORT area,
                                 register __d1 UBYTE flags)

{struct RMMessage rmess;

 rmess.type  = LEAVEAREA;
 rmess.area  = area;
 rmess.port  = "";
 rmess.flags = flags;

 return(LIBResourceMsg(&rmess));
}
//-

/// GetAreaInfo
LONG __saveds __asm LIBGetAreaInfo(register __a0 struct DLGAreaInfo *istruct,
                                   register __d0 UBYTE flags)

{struct RMMessage rmess;

 rmess.type    = GETAREAINFO;
 rmess.area    = istruct->area;
 rmess.port    = "";
 rmess.flags   = flags;
 rmess.dataptr = istruct;

 return(LIBResourceMsg(&rmess));
}
//-

/// FreeAreaInfo
LONG __saveds __asm LIBFreeAreaInfo(register __a0 struct AreaInfo *istruct)

{struct RMMessage rmess;

 rmess.type    = FREEAREAINFO;
 rmess.port    = "";
 rmess.dataptr = istruct;

 return(LIBResourceMsg(&rmess));
}
//-

/// ListPorts
LONG __saveds __asm LIBListPorts(register __a0 char *buf,
                                 register __a1 char *passwd)

{struct RMMessage rmess;

 rmess.type   = LISTPORTS;
 rmess.port   = buf;
 rmess.passwd = passwd;

 return(LIBResourceMsg(&rmess));
}
//-

/// LoadLang
LONG __saveds __asm LIBLoadLang(register __a0 char *port,
            register __a1 char *name)
{struct RMMessage rmess;

 rmess.type    = LOADLANG;
 rmess.port    = port;
 rmess.dataptr = name;

 return(LIBResourceMsg(&rmess));
}
//-

/// GetLang
struct LangStruct * __saveds __asm LIBGetLang(register __a0 char *port)

{struct RMMessage rmess;

 rmess.type = GETLANG;
 rmess.port = port;

 if (LIBResourceMsg(&rmess) == RMNOERR)
     return((struct LangStruct *)rmess.dataptr);

 return(NULL);
}
//-

/// LockResource
LONG __saveds __asm LIBLockResource(register __a0 char *name,
            register __a1 char *passwd,
            register __a2 char *reason,
            register __d0 char pri,
            register __d1 UBYTE flags)

{struct RMMessage rmess;

 rmess.type     = LOCKRESOURCE;
 rmess.dataptr  = name;
 rmess.port     = "";
 rmess.passwd   = passwd;
 rmess.reason   = reason;
 rmess.priority = pri;
 rmess.flags    = flags;

 return(LIBResourceMsg(&rmess));
}
//-

/// FreeResource
LONG __saveds __asm LIBFreeResource(register __a0 char *name,
                                    register __a1 char *passwd)

{struct RMMessage rmess;

 rmess.type    = FREERESOURCE;
 rmess.dataptr = name;
 rmess.port    = "";
 rmess.passwd  = passwd;

 return(LIBResourceMsg(&rmess));
}
//-

/// GetResReport
struct List * __saveds __asm LIBGetResReport(void)

{struct RMMessage rmess;

 rmess.type = GETRESREPORT;
 rmess.port = "";

 LIBResourceMsg(&rmess);

 return(rmess.dataptr);
}
//-

/// FreeResReport
LONG __saveds __asm LIBFreeResReport(register __a0 struct List *lst)

{struct RMMessage rmess;

 rmess.type    = FREERESREPORT;
 rmess.dataptr = lst;
 rmess.port    = "";

 return(LIBResourceMsg(&rmess));
}
//-

/// LockMenu
LONG __saveds __asm LIBLockMenu(register __a0 char             *port,
            register __a1 struct MenuStuff *ms,
            register __d0 USHORT            custnum,
            register __a2 char             *passwd,
            register __a3 char             *reason,
            register __d1 char              pri,
            register __d2 UBYTE             flags)

{struct RMMessage rmess;

 rmess.type     = LOCKMENU;
 rmess.dataptr  = ms;
 rmess.area     = custnum;
 rmess.port     = port;
 rmess.passwd   = passwd;
 rmess.reason   = reason;
 rmess.priority = pri;
 rmess.flags    = flags;

 return(LIBResourceMsg(&rmess));
}
//-

/// FreeMenu
LONG __saveds __asm LIBFreeMenu(register __a0 char *port,
            register __a1 char *name,
            register __a2 char *passwd)

{struct RMMessage rmess;

 rmess.type    = FREEMENU;
 rmess.dataptr = name;
 rmess.port    = port;
 rmess.passwd  = passwd;

 return(LIBResourceMsg(&rmess));
}
//-

/// PurgeMenu
LONG __saveds __asm LIBPurgeMenu(register __a0 char *port,
                                 register __a1 char *name)

{struct MenuStuff ms;
 struct RMMessage rmess;

 ms.name = name;
 LIBLockMenu(port, &ms, 0, "Purge", "Purging Menu", 0, WRITELOCK|PENDLOCK);

 rmess.type    =  PURGEMENU;
 rmess.dataptr =  name;
 rmess.port    =  port;
 rmess.passwd  = "Purge";

 return(LIBResourceMsg(&rmess));
}
//-

