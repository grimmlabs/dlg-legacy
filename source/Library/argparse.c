#include <exec/types.h>

#include <ctype.h>

LONG __saveds __asm LIBArgParse(register __a0 char  *ptr,
                                register __a1 char **argarray,
                                register __d0 UBYTE  maxnum)

{long  argcount  = 0;
 long  charcount = 0;

 UBYTE intext    = FALSE;
 UBYTE inquote   = FALSE;

 while(*ptr)
      {if (intext)
          {if (*ptr=='"')  inquote=FALSE;

           if (isspace(*ptr) && !inquote)
              {*ptr = 0;
               if (argcount==maxnum)  break;
               intext = FALSE;
              }
          }
        else
          {if (!isspace(*ptr))
              {if (*ptr == '"')  inquote=TRUE;
               argarray[argcount++] = ptr;
               charcount            = 0;
               intext               = TRUE;
              }
          }

       ptr++;
       charcount++;
       if (charcount > 60)  return(FALSE);
      }

 argarray[argcount] = NULL;
 return(argcount);
}


char * __saveds __asm LIBStripPath(register __a0 char *path)

{char *ind;

 for(ind = path; *path; path++)
     if (*path == ':'  ||  *path == '/')  ind = path+1;

 return(ind);
}
