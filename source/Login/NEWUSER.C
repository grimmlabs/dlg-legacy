#define   ANSICOL

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <fcntl.h>
#include <ctype.h>
#include <dos.h>
#include <exec/types.h>
#include <libraries/dosextens.h>

#include <dlg/dlg.h>
#include <dlg/portconfig.h>
#include <dlg/user.h>
#include <dlg/input.h>
#include <dlg/msg.h>
#include <dlg/resman.h>
#include <dlg/log.h>

#include <Link/Config.h>
#include <Link/io.h>
#include <Link/user.h>

#include <proto/dlg.h>
#include <proto/dos.h>
#include <proto/exec.h>

#include <pragmas/dlg.h>

#include <private/Version.h>
#define  ObjRev "3"
const UBYTE version[]="\0$VER: NewUser " BUILDVER "." ObjRev " �" COPYRIGHT " by Digerati Dreams "__AMIGADATE__;

void  main    (int, char **);
void  Add_User(void);
void _CXBRK   (void);


struct Global_Settings Globals;
struct USER_DATA       UserDat   = {"", 24, 80, 0, 1, 0, "", 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
struct Ram_File        RStruct;
struct USER_DATA       User;
struct UserInfo        fake_ui  = {&UserDat, &RStruct};

struct Msg_Area        Area;
struct Msg_Log         Log;

struct Msg_Area        FArea;
struct Msg_Log         FLog;

char                   UserName[36];
char                   Ext[5];
USHORT                 pos;

struct Library        *DLGBase;
struct LangStruct     *ls;
char                  **SA;


void main(int argc, char **argv)
{
   if (!(DLGBase = OpenLibrary(DLGNAME, DLGVERSION)))  exit(5);

   if (argc < 3)
   {
      printf("Usage: NewUser name ansi_setting\n\n");
      _CXBRK();
   }

   if (GetDevName(Ext) == -1)
   {
      printf("Unable to determine port\n\n");
      _CXBRK();
   }

   if (!(ls=(struct LangStruct *)GetLang(Ext)))  _CXBRK();
   SA = ls->strings;

   if (ReadGlobals(Ext,&Globals) == -1)
   {
      printf(SA[1460]);
      return;
   }

   RStruct.Command_Stack[0] = 0;
   strcpy(UserName, argv[1]);
   if (!Stricmp(argv[2], "COLOR"))
   UserDat.Ansi_Flag = Globals.Flags & (ANSI_COLOR + ANSI_CLR + ANSI_POS + ANSI_SCROLL + ANSI_NOFREEZE);

   DispForm("DLGCONFIG:Text/NewUser.txt", &UserDat, &UserDat, &RStruct, Ext);

   if (!(BoolQuery(SA[1459], 1, &fake_ui)))
   {
      printf("\n");
      DispForm("DLGCONFIG:Text/NoRegistration.txt", &UserDat, &UserDat, &RStruct, Ext);
      printf("\n");
   }
   else
   {
      printf("\n\n");
      Add_User();
   }

   _CXBRK();
}


void Add_User(void)
{
   int  fp;
   BPTR lock;
   char filename[48];
   char batchname[50];

   if (New_User(UserName,&User,0))
   {
      printf(SA[1461]);
      return;
   }

   strcpy(RStruct.Name, UserName);
   UserDat.Ansi_Flag = User.Ansi_Flag;
   printf("%a7\n\n");

   sprintf(filename, "USER:%s", UserName);
   UnderScore(filename);
   Upper(filename);
   if (!(lock = CreateDir(filename)))
   {
      printf(SA[1462]);
      return;
   }
   UnLock(lock);
   printf(SA[1463]);

   strcat(filename,"/User.Data");
   if ((fp = open(filename,O_CREAT+O_WRONLY)) == EOF)
   {
      printf(SA[1464]);
      return;
   }

   write(fp, &User, sizeof(User));
   close(fp);
   printf(SA[1465]);

   strcpy(Log.Name, UserName);
   Upper(Log.Name);
   Log.High_Mess = 0;
   Log.dflag     = 0;
   Log.special   = 0;
   Log.uflag     =(unsigned char)0xff;

   sprintf(filename,"USER:%s/User.Msg",UserName);
   UnderScore(filename);
   AddStruct(filename,(char *)&Log,sizeof(Log),36);

   sprintf(filename,"USER:%s/User.File",UserName);
   UnderScore(filename);
   AddStruct(filename,(char *)&Log,sizeof(Log),36);

   printf(SA[1466]);
   AddGlobalAreas(UserName,0,User.User_Level,0|128,"MSG:Area.bbs");
   AddGlobalAreas(UserName,0,User.User_Level,1|128,"FILE:Area.bbs");
   DispForm("DLGCONFIG:Text/FinishedApplication.txt", &UserDat, &UserDat, &RStruct, Ext);
   printf("%a7\n");
   WriteLog(NEW_USER,UserName,Ext,"");

   Upper(UserName);
   AddStruct("DLGCONFIG:Misc/Users.bbs",UserName,36,36);
   AddStruct("DLGCONFIG:Misc/NewUser.bbs",UserName,36,36);

   Capitalize(UserName);
   strcpy(RStruct.Name,UserName);

   sprintf(batchname,"DlgConfig:batch/NewUser.batch %s",Ext);
   if (Exists("DlgConfig:batch/NewUser.batch"))
   Execute(batchname,0L,0L);

   if (Exists("DlgConfig:batch/NewUser.dlgbatch"))
   DialogBatch("DLGCONFIG:Batch/NewUser.dlgbatch",&User,&RStruct,Ext);

   return;
}


void _CXBRK(void)

{
   CloseLibrary(DLGBase);
   exit(0);
}
