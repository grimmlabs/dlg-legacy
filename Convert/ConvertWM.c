#include <exec/types.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include <time.h>

#include <proto/exec.h>
#include <proto/dos.h>
#include <proto/dlg.h>

#include <dlg/user.h>
#include <dlg/msg.h>

#include <pragmas/dlg.h>

/// Old WaitingMail
struct OldWM
{
  char  msgid[12];     // format:    area:message   eg:  0001:00123
  char  from [36];     //                                ^^^^^^^^^^
  char  subject[72];   // Must be padded with ZEROS in this exact format
  long  areanum;       // Area number
  char  areaname[34];  // Name of area
  SHORT messagenum;    // Number of message
};
//-

static char version[]="$VER: ConvertWM 1.2 � 1997 by DLG Development "__AMIGADATE__"";

BPTR  sout;
BPTR  fh =  NULL;

struct NameStruct   *Users = NULL;
struct OldWM        *old = NULL;
struct WaitingMail  *wm  = NULL;

struct   Library    *DLGBase  =  NULL;

void  ShutDown(int, char *);
void  Usage(void);
void _CXBRK(void);

/// Main
int main(int argc, char **argv)
{
   BPTR    fh = NULL;
   ULONG   size = 0;
   long    limit;

   struct NameStruct *ns;


   sout = Output();

   DLGBase = OpenLibrary("dlg.library",2L);

   if(!DLGBase) ShutDown(10,"Could not open dlg.library!");
   if(-1 == FileSize("DLGConfig:Misc/Users.bbs",&size)) ShutDown(5,"No users");

   Users = calloc(1,size);

   if(!Users) ShutDown(10,"Not enough memory");

   if(-1 == GetFirstStruct("DLGConfig:Misc/Users.bbs",(char *)Users,size)) ShutDown(10,"Cannot load users");

   limit = size / sizeof(struct NameStruct);

   for(ns = Users; ns < (Users + limit); ns++)
   {
      char fname[256];
      char bname[256];
      ULONG wsize = 0;
      long  NumEntries;
      long z;

      Chk_Abort();

      AFPrintf(NULL,sout,"User [%-30.30s]: ",ns->name);

///   Convert WaitingMail
      ASPrintf(NULL,fname,"User:%s/WaitingMail.dat",ns->name);
      UnderScore(fname);
      ASPrintf(NULL,bname,"%sbak",fname);

      if(-1 == FileSize(fname,&wsize))
      {
         AFPrintf(NULL,sout,"No Waiting Mail\n");
         continue;
      }

      if(wsize % sizeof(struct OldWM))
      {
         AFPrintf(NULL,sout,"Waitingmail already converted or corrupt.\n");
         continue;
      }

      Copy(fname,bname);

      NumEntries = wsize / sizeof(struct OldWM);

      old = calloc(1,wsize);

      if(!old)
      {
         AFPrintf(NULL,sout,"Could not allocate mem\n");
         continue;
      }

      wm = calloc(NumEntries,sizeof(struct WaitingMail));

      if(!wm)
      {
         AFPrintf(NULL,sout,"Could not allocate mem\n");
         free(old);
         old = NULL;
         continue;
      }

      if(-1 == GetFirstStruct(fname,(char *)old,sizeof(struct OldWM)))
      {
         AFPrintf(NULL,sout,"Could not load data\n");
         free(wm);
         wm = NULL;
         free(old);
         old = NULL;
         continue;
      }

///   Actual conversion happens here
      for(z = 0; z < NumEntries; z++)
      {
         memcpy(wm[z].msgid,     old[z].msgid,12);
         strcpy(wm[z].from,      old[z].from);
         strcpy(wm[z].subject,   old[z].subject);
         wm[z].areanum        =  (SHORT)old[z].areanum;
         strcpy(wm[z].areaname,  old[z].areaname);
         wm[z].messagenum     =  old[z].messagenum;
      }
//-

      fh = Open(fname,MODE_NEWFILE);

      if(fh)
      {
         Write(fh,wm,(NumEntries * sizeof(struct WaitingMail)));
         Close(fh);
         fh = NULL;
      }
      else
      {
         AFPrintf(NULL,sout,"Could not write new data\n");
         continue;
      }
//-

      AFPrintf(NULL,sout,"Done!\n");
   }


   AFPrintf(NULL,sout,"All Done!\n\n");
   ShutDown(0,NULL);
}
//-

/// ShutDown
void ShutDown(int Error, char *ErrStr)
{
   if(Users)   free(Users);
   if(wm)      free(wm);
   if(old)     free(old);

   if(fh)      Close(fh);

   if(DLGBase) CloseLibrary(DLGBase);

   if(Error)
   {
      FPuts(sout,ErrStr);
      FPuts(sout,"\n\n");
   }

   exit(Error);
}
//-

/// _CXBRK
void _CXBRK()
{
   ShutDown(5,"Control-C Detected");
}
//-


