#include <exec/types.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include <time.h>

#include <proto/exec.h>
#include <proto/dos.h>
#include <proto/dlg.h>

#include <DLG/Version.h>
#include <dlg/user.h>

#include <pragmas/dlg.h>

static char version[]="$VER: ConvertAutoScan 1.3 � 1997-1998 by Digerati Dreams "__AMIGADATE__"";

BPTR  sout;
BPTR  fh =  NULL;

ULONG  *a = NULL;
USHORT *b = NULL;

struct NameStruct *Users = NULL;

struct   Library    *DLGBase  =  NULL;

void  ShutDown(int, char *);
void  Usage(void);
void _CXBRK(void);

/// Main
int main(int argc, char **argv)
{
   ULONG  *i;

   ULONG   size = 0;
   long    limit;
   long    ulimit;
   USHORT *j;

   struct NameStruct *ns;

   sout = Output();

   DLGBase = OpenLibrary("dlg.library",4L);

   if(!DLGBase) ShutDown(10,"Could not open dlg.library!");
   if(-1 == FileSize("USER:Users.bbs",&size)) ShutDown(5,"No users");

   Users = calloc(1,size);

   if(!Users) ShutDown(10,"Not enough memory");

   if(-1 == GetFirstStruct("USER:Users.bbs",(char *)Users,size)) ShutDown(10,"Cannot load users");

   ulimit = size / sizeof(struct NameStruct);

   for(ns = Users; ns < (Users + ulimit); ns++)
   {
      char fname[256];
      char dname[256];

      Chk_Abort();

      AFPrintf(NULL,sout,"User [%-30.30s]: ",ns->name);

///   Convert file newscan
      AFPrintf(NULL,sout,"Files ... ");
      ASPrintf(NULL,fname,"User:%s/GlobalAreas.file",ns->name);
      UnderScore(fname);

      if(-1 != FileSize(fname,&size))
      {
         a = calloc(1,size);
         limit = size / sizeof(ULONG);
         b = calloc(limit,sizeof(USHORT));

         if(a && b)
         {
            if(-1 != GetFirstStruct(fname,(char *)a,size))
            {
               for(i = a,j = b; i < (a + limit); i++,j++)
               {
                  Chk_Abort();

                  if(*i > 9999)
                  {
                     *j = 1;
                     continue;
                  }
                  else
                     *j = *i;
               }

               ASPrintf(NULL,dname,"User:%s/GlobalAreas.FileBak",ns->name);
               UnderScore(dname);
               Copy(fname,dname);

               ASPrintf(NULL,fname,"User:%s/GlobalAreas.file",ns->name);
               UnderScore(fname);

               fh = Open(fname,MODE_NEWFILE);

               if(fh)
               {
                  Write(fh,b,limit * sizeof(USHORT));
                  Close(fh);
                  fh = NULL;
               }

               free(a);
               a = NULL;
               free(b);
               b = NULL;
            }
            else
            {
               free(a);
               a = NULL;
               free(b);
               b = NULL;
            }

         }
         else
         {
            if(a)
            {
               free(a);
               a = NULL;
            }

            if(b)
            {
               free(b);
               b = NULL;
            }
         }
      }
//-

///   Convert message newscan
      AFPrintf(NULL,sout,"Messages ... ");
      ASPrintf(NULL,fname,"User:%s/GlobalAreas.msg",ns->name);
      UnderScore(fname);

      if(-1 != FileSize(fname,&size))
      {
         a = calloc(1,size);
         limit = size / sizeof(ULONG);
         b = calloc(limit,sizeof(USHORT));

         if(a && b)
         {
            if(-1 != GetFirstStruct(fname,(char *)a,size))
            {
               for(i = a,j = b; i < (a + limit); i++,j++)
               {
                  Chk_Abort();

                  if(*i > 9999)
                  {
                     *j = 1;
                     continue;
                  }
                  else
                     *j = *i;
               }

               ASPrintf(NULL,dname,"User:%s/GlobalAreas.MsgBak",ns->name);
               UnderScore(dname);
               Copy(fname,dname);

               ASPrintf(NULL,fname,"User:%s/GlobalAreas.msg",ns->name);
               UnderScore(fname);

               fh = Open(fname,MODE_NEWFILE);

               if(fh)
               {
                  Write(fh,b,limit * sizeof(USHORT));
                  Close(fh);
                  fh = NULL;
               }

               free(a);
               a = NULL;
               free(b);
               b = NULL;
            }
            else
            {
               free(a);
               a = NULL;
               free(b);
               b = NULL;
            }

         }
         else
         {
            if(a)
            {
               free(a);
               a = NULL;
            }

            if(b)
            {
               free(b);
               b = NULL;
            }
         }
      }
//-

///   Convert archive newscan
      AFPrintf(NULL,sout,"Archive ... ");
      ASPrintf(NULL,fname,"User:%s/GlobalAreas.archive",ns->name);
      UnderScore(fname);

      if(-1 != FileSize(fname,&size))
      {
         a = calloc(1,size);
         limit = size / sizeof(ULONG);
         b = calloc(limit,sizeof(USHORT));

         if(a && b)
         {
            if(-1 != GetFirstStruct(fname,(char *)a,size))
            {
               for(i = a,j = b; i < (a + limit); i++,j++)
               {
                  Chk_Abort();

                  if(*i > 9999)
                  {
                     *j = 1;
                     continue;
                  }
                  else
                     *j = *i;
               }

               ASPrintf(NULL,dname,"User:%s/GlobalAreas.archivebak",ns->name);
               UnderScore(dname);
               Copy(fname,dname);

               ASPrintf(NULL,fname,"User:%s/GlobalAreas.archive",ns->name);
               UnderScore(fname);

               fh = Open(fname,MODE_NEWFILE);

               if(fh)
               {
                  Write(fh,b,limit * sizeof(USHORT));
                  Close(fh);
                  fh = NULL;
               }

               free(a);
               a = NULL;
               free(b);
               b = NULL;
            }
            else
            {
               free(a);
               a = NULL;
               free(b);
               b = NULL;
            }

         }
         else
         {
            if(a)
            {
               free(a);
               a = NULL;
            }

            if(b)
            {
               free(b);
               b = NULL;
            }
         }
      }
//-

      AFPrintf(NULL,sout,"Done!\n");
   }


   AFPrintf(NULL,sout,"All Done!\n\n");
   ShutDown(0,NULL);
}
//-

/// ShutDown
void ShutDown(int Error, char *ErrStr)
{
   if(a)       free(a);
   if(b)       free(b);
   if(Users)   free(Users);

   if(fh)      Close(fh);

   if(DLGBase) CloseLibrary(DLGBase);

   if(Error)
   {
      FPuts(sout,ErrStr);
      FPuts(sout,"\n\n");
   }

   exit(Error);
}
//-

/// _CXBRK
void _CXBRK()
{
   ShutDown(5,"Control-C Detected");
}
//-


