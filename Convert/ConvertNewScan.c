#include <exec/types.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include <time.h>

#include <proto/exec.h>
#include <proto/dos.h>
#include <proto/dlg.h>

#include <DLG/Version.h>
#include <dlg/user.h>

#include <pragmas/dlg.h>

static char version[]="$VER: ConvertNewScan 2.0 � 1997-1998 by Digerati Dreams "__AMIGADATE__"";

BPTR  sout;
BPTR  fh =  NULL;

USHORT *a = NULL;

struct NameStruct *Users = NULL;

struct   Library    *DLGBase  =  NULL;

void  ShutDown(int, char *);
void  Usage(void);
void _CXBRK(void);

/// Main
int main(int argc, char **argv)
{
   USHORT *i;

   ULONG   size = 0;
   long    limit;
   long    ulimit;

   struct NameStruct *ns;

   sout = Output();

   DLGBase = OpenLibrary("dlg.library",4L);

   if(!DLGBase) ShutDown(10,"Could not open dlg.library!");
   if(-1 == FileSize("DLGConfig:Misc/Users.bbs",&size)) ShutDown(5,"No users");

   Users = calloc(1,size);

   if(!Users) ShutDown(10,"Not enough memory");

   if(-1 == GetFirstStruct("DLGConfig:Misc/Users.bbs",(char *)Users,size)) ShutDown(10,"Cannot load users");

   ulimit = size / sizeof(struct NameStruct);

   for(ns = Users; ns < (Users + ulimit); ns++)
   {
      char fname[256];
      char dname[256];

      Chk_Abort();

      AFPrintf(NULL,sout,"User [%-30.30s]: ",ns->name);

///   Convert file newscan
      AFPrintf(NULL,sout,"File ... ");
      ASPrintf(NULL,fname,"User:%s/GlobalAreas.file",ns->name);
      UnderScore(fname);

      if(-1 != FileSize(fname,&size))
      {
         a = calloc(1,size); limit = size / sizeof(USHORT);

         if(a)
         {
            if(-1 != GetFirstStruct(fname,(char *)a,size))
            {
               ASPrintf(NULL,dname,"User:%s/GlobalAreas.FileBak",ns->name);
               UnderScore(dname);
               Copy(fname,dname);

               ASPrintf(NULL,fname,"User:%s/GlobalAreas.file",ns->name);
               UnderScore(fname);

               fh = Open(fname,MODE_NEWFILE);

               if(fh)
               {
                  for(i = a; i < (a + limit); i++)
                  {
                     Chk_Abort();
                     if(*i == 0) continue;
                     Write(fh,i,sizeof(USHORT));
                  }

                  Close(fh);
                  fh = NULL;
               }
            }

            free(a); a = NULL;
         }
      }
//-

///   Convert Message newscan
      AFPrintf(NULL,sout,"Message ... ");
      ASPrintf(NULL,fname,"User:%s/GlobalAreas.msg",ns->name);
      UnderScore(fname);

      if(-1 != FileSize(fname,&size))
      {
         a = calloc(1,size); limit = size / sizeof(USHORT);

         if(a)
         {
            if(-1 != GetFirstStruct(fname,(char *)a,size))
            {
               ASPrintf(NULL,dname,"User:%s/GlobalAreas.MsgBak",ns->name);
               UnderScore(dname);
               Copy(fname,dname);

               ASPrintf(NULL,fname,"User:%s/GlobalAreas.Msg",ns->name);
               UnderScore(fname);

               fh = Open(fname,MODE_NEWFILE);

               if(fh)
               {
                  for(i = a; i < (a + limit); i++)
                  {
                     Chk_Abort();
                     if(*i == 0) continue;
                     Write(fh,i,sizeof(USHORT));
                  }

                  Close(fh);
                  fh = NULL;
               }
            }

            free(a); a = NULL;
         }
      }
//-

///   Convert Archive newscan
      AFPrintf(NULL,sout,"Archive ... ");
      ASPrintf(NULL,fname,"User:%s/GlobalAreas.Archive",ns->name);
      UnderScore(fname);

      if(-1 != FileSize(fname,&size))
      {
         a = calloc(1,size); limit = size / sizeof(USHORT);

         if(a)
         {
            if(-1 != GetFirstStruct(fname,(char *)a,size))
            {
               ASPrintf(NULL,dname,"User:%s/GlobalAreas.ArcBak",ns->name);
               UnderScore(dname);
               Copy(fname,dname);

               ASPrintf(NULL,fname,"User:%s/GlobalAreas.Archive",ns->name);
               UnderScore(fname);

               fh = Open(fname,MODE_NEWFILE);

               if(fh)
               {
                  for(i = a; i < (a + limit); i++)
                  {
                     Chk_Abort();
                     if(*i == 0) continue;
                     Write(fh,i,sizeof(USHORT));
                  }

                  Close(fh);
                  fh = NULL;
               }
            }

            free(a); a = NULL;
         }
      }
//-

      AFPrintf(NULL,sout,"Done!\n");

   }


   AFPrintf(NULL,sout,"All Done!\n\n");
   ShutDown(0,NULL);
}
//-

/// ShutDown
void ShutDown(int Error, char *ErrStr)
{
   if(a)       free(a);
   if(Users)   free(Users);

   if(fh)      Close(fh);

   if(DLGBase) CloseLibrary(DLGBase);

   if(Error)
   {
      FPuts(sout,ErrStr);
      FPuts(sout,"\n\n");
   }

   exit(Error);
}
//-

/// _CXBRK
void _CXBRK()
{
   ShutDown(5,"Control-C Detected");
}
//-


