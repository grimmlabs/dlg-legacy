
extern char _TaskPri;
extern unsigned long _Stack;
extern unsigned long _CallInterval;    // AUTOCALL
extern int _CallZone;                  // AUTOCALLZONE

// LOGLINES -- Discontinued
extern char *_ScreenName[128];         // PUBSCN_NAME
extern BOOL _Lace;                     // PUBSCN_LACE
extern BOOL _OScan;                    // PUBSCN_OSCAN 
extern BOOL _Open_Behind;              // PUBSCN_BEHIND
// SCREEN # -- ????
extern long _Stat_X;                   // STATUS_X
extern long _Stat_Y;                   // STATUS_Y
extern long _Stat_W;                   // STATUS_WIDTH

extern BOOL _ProcWin;                  // PROCESSWINDOW
extern long _Proc_X;                   // PROCESS_X
extern long _Proc_Y;                   // PROCESS_Y 
extern long _Proc_W;                   // PROCESS_WIDTH
extern long _Proc_H;                   // PROCESS_HEIGHT
extern int  _PktRoute;                 // PKTROUTE
extern int  _NetMailRoute;             // NETMAILROUTE
extern int  _Log;                      // LOG
// QUICKLOG -- Discontinued
extern unsigned long _PktBufLen;       // PKTBUFFERLENGTH
extern BOOL _LinkReplies;              // LINKREPLIES
extern BOOL _CheckDupes;               // CHECKDUPES #
extern BOOL _SaveDMG;                  // SAVEDAMAGED
extern BOOL _SecureImp;                // SECUREIMP
extern BOOL _NoCallBun;                // NOCALLBUNDLE
extern BOOL _NoCallProc;               // NOCALLPROCESS
extern BOOL _ForceINTL;                // FORCEINTL
extern BOOL _LogUsrMess;               // LOGUSERMESSAGE
extern BOOL _NotifyActOnly;            // NOTIFYACTIVEONLY
extern BOOL _Borrow;                   // BORROW #
extern BOOL _Net2User;                 // NET2USERDIR
extern BOOL _ActLog;                   // ACTIVITYLOG
extern int  _AreaFix;                  // AreaAREAFIX
extern int  _Tick;                     // TICK
extern int  _Hub;                      // HUB
extern BOOL _TDSpawn;                  // TDSPAWN
extern unsigned long _NetMailDir;      // NETMAILDIR
extern unsigned long _BadMsgDir;       // BADMSGSDIR
extern BOOL _PointEnable;              // POINTENABLE
extern BOOL _StripPointNet;            // STRIPPOINTNET
extern int _PointNet;                  // POINTNET
extern char _Address[128];            // ADDRESS (Main)
extern char *_AKA[50];                 // POINTNET or AKA
extern char _TDPort[50];              // TDPORT
extern char _TDConfig[128];           // TDLOCKNAME
extern char _BBSPort[5];              // BBSPORT
extern char _Origin[80];              // ORIGIN 
extern char _Operator[50];            // OPERATOR
extern char _CallTypes[25];           // CALLTYPES
extern BOOL _PCFriendly;               // PCFRIENDLY
extern char _PCAddr[128];             // PCADDRESS
extern char _ArcAdd[128];             // ARCADD
extern char _ArcExt[128];             // ARCEXTRACT
extern char _ZooAdd[128];             // ZOOADD
extern char _ZooExt[128];             // ZOOEXTRACT
extern char _LHAAdd[128];             // LHAADD
extern char _LHAExt[128];             // LHAEXTRACT  
extern char _LZHAdd[128];             // LZHADD
extern char _LZHExt[128];             // LZHEXTRACT
extern char _ZipAdd[128];             // ZIPADD 
extern char _ZipExt[128];             // ZIPEXTRACT
extern char _ArjAdd[128];             // ARJADD
extern char _ArjExt[128];             // ARJEXTRACT

