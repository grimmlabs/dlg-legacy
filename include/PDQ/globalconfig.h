/*
The premise here is that we can create a global configuration structure
and have it read in by zproc, with the address of the structure passed to
all programs that need it. each program would use #define so that the old
references would still work OK.
*/

#define GLOBALCONFIG_H 1

#include <exec/types.h>

#include "Ross.h"

#define MAGIC_PDQ_ENABLE 1017


#define GCERROR_NO_MEMORY	-400
#define GCERROR_NO_CONFIGFILE	-100
#define GCERROR_MISSING_ENTRY	-50
#define GCERROR_FAULTY_FORMAT	-51

#define TOTALAKAS 30

extern struct GConfig *GCFG;
extern char *GCFG_block;
extern int GCFG_size;

/*
struct CoProcCFG
{
	char	       *cpw_screenname[32];
	struct Screen  *cpw_screen;
	struct Window  *cpw_win;
	BPTR            cpw_fh;
	int		cpw_x;
	int		cpw_y;
	int		cpw_width;
	int		cpw_height;
	int		cpw_stacksize;
	int		cpw_priority;
};
*/

/* ======================================================================== */
/* === MyRemember ========================================================= */
/* ======================================================================== */
/* === Identical to intuition/intuition.h's struct Remember =============== */
/* ======================================================================== */
/* this structure is used for remembering what memory has been allocated to
 * date by a given routine, so that a premature abort or systematic exit
 * can deallocate memory cleanly, easily, and completely
 */
struct MyRemember
{
    struct Remember *NextRemember;
    ULONG RememberSize;
    UBYTE *Memory;
};


struct GConfig
{
	int		DUMMY_1;                                        //  2    2
	int		DUMMY_2;                                        //  2    4
	int		DUMMY_3;                                        //  2    6
	int		DUMMY_4;                                        //  2    8

	int		PDQ1OK;			/* special flag for PDQ utils */ //  2   10
#define _PDQ1OK 	(GCFG->PDQ1OK)

	int		MARKER1;		/* set this to 5551212 */           //  2   12

	int		PDQ1NOT;			/* verification copy */          //  2   14
#define _PDQ1NOT	(GCFG->PDQ1NOT)

	int		ZTASKPRI;		/* task priority of all z processes */
#define	_ZTASKPRI	(GCFG->ZTASKPRI)                       //  2   16
#define	_TASKPRI	(GCFG->ZTASKPRI)

	int		STACKSIZE;		/* subprocess stacksize */
#define	_STACKSIZE	(GCFG->STACKSIZE)

	int		AUTOCALL;		/* how often in seconds do we call */
#define	_AUTOCALL 	(GCFG->AUTOCALL)

	int		AUTOCALLZONE;		/* autocall everywhere or just this zone? */
#define _AUTOCALLZONE	(GCFG->AUTOCALLZONE)

	int		LOGLINES;		/* size it and forget it */
#define _LOGLINES 	(GCFG->LOGLINES)


/* v2.04 */
	char		SCREENNAME[32];		/* name of screen to open on */
#define _SCREENNAME	(GCFG->SCREENNAME)

/* v2.04 */
	int		PUBSCNLACE;
#define _PUBSCNLACE	(GCFG->PUBSCNLACE)

/* v2.04 */
	int		PUBSCNOSCAN;
#define _PUBSCNOSCAN	(GCFG->PUBSCNOSCAN)

	int		PUBSCNBEHIND;
#define _PUBSCNBEHIND	(GCFG->PUBSCNBEHIND)

/* v2.04 */
	struct Screen  *SCNPTR;			/* pointer to that screen */
#define _SCNPTR		(GCFG->SCNPTR)

	int		WIN_X;			/* zproc window X origin */
#define	_WIN_X		(GCFG->WIN_X)

	int		WIN_Y;			/* zproc window Y origin */
#define	_WIN_Y		(GCFG->WIN_Y)

	int		WIN_WIDTH;		/* zproc window width */
#define	_WIN_WIDTH	(GCFG->WIN_WIDTH)

/* new stuff ==================================================================*/
	int		PROCESSWINDOW;	/* do we want a window for PROCESS? */
#define _PROCESSWINDOW 	(GCFG->PROCESSWINDOW)

	int		PROCESS_X;		/* upper X placement */
#define _PROCESS_X	(GCFG->PROCESS_X)

	int		PROCESS_Y;		/* upper Y placement */
#define _PROCESS_Y	(GCFG->PROCESS_Y)

	int		PROCESS_WIDTH;	/* width */
#define _PROCESS_WIDTH	(GCFG->PROCESS_WIDTH)

	int		PROCESS_HEIGHT;	/* height */
#define _PROCESS_HEIGHT (GCFG->PROCESS_HEIGHT)

/* ////// REALLY NEW STUFF ///////// */

	struct areas	*ROSSAREAS; /* pointer to area structure held in memory */
#define _ROSSAREAS	(GCFG->ROSSAREAS)

	struct MyRemember *MYREMEMBER;
#define _MYREMEMBER	(GCFG->MYREMEMBER)

	unsigned long	AREACRC;
#define	_AREACRC	(GCFG->AREACRC)

	int		NOWPROCESSING;		/* a flag to the areas editor that processing is going on. this flag, combined with now editing, provides a locking system */

#define _NOWPROCESSING  (GCFG->NOWPROCESSING)

	int		NOWEDITING;		/* to hold off further processing while the areas are being edited */
#define _NOWEDITING	(GCFG->NOWEDITING)

	BPTR		AREALOCK;		/* the write lock put on the areas file while it is stored in memory */
#define _AREALOCK	(GCFG->AREALOCK)

/* end new stuff ==============================================================*/


	int		PKTROUTE;		/* enable routing of full packets */
#define	_PKTROUTE	(GCFG->PKTROUTE)

	int		NETMAILROUTE;		/* do we route netmail? */
#define	_NETMAILROUTE	(GCFG->NETMAILROUTE)

	int		LOG;			/* amount of logging */
#define	_LOG		(GCFG->LOG)
#define _VERBOSITY	(GCFG->LOG)

	int		QUICKLOG;		/* do we buffer the logging? */
#define	_QUICKLOG	(GCFG->QUICKLOG)

	int		PKTBUFFERLENGTH;	/* how big do we make the default tosser buffer? */
#define	_PKTBUFFERLENGTH (GCFG->PKTBUFFERLENGTH)

	int		LINKREPLIES;		/* link replies after import? */
#define	_LINKREPLIES	(GCFG->LINKREPLIES)

	int		CHECKDUPES;		/* enable dupechecking */
#define	_CHECKDUPES	(GCFG->CHECKDUPES)

	int		SAVEDAMAGED;		/* save damaged packets for review */
#define	_SAVEDAMAGED	(GCFG->SAVEDAMAGED)

	int		MOVEATTACHES;		/* move file attaches to odd: */
#define	_MOVEATTACHES	(GCFG->MOVEATTACHES)


	int		LOGUSERMESSAGE;		/* notify users of tossed msgs? */
#define	_LOGUSERMESSAGE	(GCFG->LOGUSERMESSAGE)

	int		NOTIFYACTIVEONLY;	/* notify users of echos in areas they are active in ONLY */
#define _NOTIFYACTIVEONLY (GCFG->NOTIFYACTIVEONLY)

	int		BORROW;			/* borrow the areas before using? */
#define	_BORROW		(GCFG->BORROW)

	int		NET2USERDIR;		/* do we stow inbound netmail in the user's directory? */
#define	_NET2USERDIR	(GCFG->NET2USERDIR)

	int		ACTIVITYLOG;		/* UNKNOWN */
#define _ACTIVITYLOG	(GCFG->ACTIVITYLOG)

	int		ENABLE_AREAFIX;		/* enable areafix if used */
#define	_ENABLE_AREAFIX	(GCFG->ENABLE_AREAFIX)

	int		ENABLE_TICK;		/* enable tick (and which tick) */
#define	_ENABLE_TICK	(GCFG->ENABLE_TICK)

	int		ENABLE_HUB;
#define _ENABLE_HUB	(GCFG->ENABLE_HUB)

	int		TDSPAWN;		/* mode for dropping to the BBS */
#define _TDSPAWN	(GCFG->TDSPAWN)

	int		NETMAILDIR;		/* the netmail directory # */
#define	_NETMAILDIR	(GCFG->NETMAILDIR)

	int		BADMSGSDIR;		/* the bad messages directory # */
#define	_BADMSGSDIR	(GCFG->BADMSGSDIR)

	int		POINTENABLE;		/* enable points */
#define _POINTENABLE    (GCFG->POINTENABLE)

	int		STRIPAKA1;		/* strip the pointnet address */
#define	_STRIPAKA1	(GCFG->STRIPAKA1)
#define	_STRIPPOINTNET	(GCFG->STRIPAKA1)

	int 		POINTNET;		/* hold int value for pointnet */
#define	_AKA1NET	(GCFG->POINTNET)
#define	_POINTNET	(GCFG->POINTNET)

	int		LEGIT;			/* is this a legit copy? */
#define	_LEGIT		(GCFG->LEGIT)

	ULONG		KEYCRC;			/* the dlg sn crc */
#define	_KEYCRC		(GCFG->KEYCRC)

	ULONG		XORCRC;			/* an XOR of the sn crc */
#define	_XORCRC		(GCFG->XORCRC)

	int		BUYER;			/* 1 if PDQMAIL prerelease ,2 if PDQMAIL release 3 if later PDQMAIL release */
#define	_BUYER		(GCFG->BUYER)

	int		RETURNCODE;		/* return codes from children */
#define _RETURNCODE	(GCFG->RETURNCODE)



/* new stuff===================================================================*/
	int		PDQ_LEGIT;
#define	_PDQ_LEGIT	(GCFG->PDQ_LEGIT)

	ULONG		PDQ_KEYCRC;
#define _PDQ_KEYCRC	(GCFG->PDQ_KEYCRC)

	ULONG		PDQ_XORCRC;
#define _PDQ_XORCRC	(GCFG->PDQ_XORCRC)
/* end new stuff===============================================================*/


	int		MyDEBUG;		/* global debug value */
#define	_MASTERDEBUG 	(GCFG->MyDEBUG)
#define	_MyDEBUG	(GCFG->MyDEBUG)


	int		ZPROC_DB;
#define	_ZPROC_DB	(GCFG->ZPROC_DB)
#define _PDQMAIL_DB	(GCFG->ZPROC_DB)

	int		ZIMP_DB;
#define	_ZIMP_DB	(GCFG->ZIMP_DB)

	int		ZEXP_DB;
#define	_ZEXP_DB	(GCFG->ZEXP_DB)

	int		ZNET_DB;
#define	_ZNET_DB	(GCFG->ZNET_DB)

	int		ZBUNDLE_DB;
#define	_ZBUNDLE_DB	(GCFG->ZBUNDLE_DB)

	int		PDQTICK_DB;
#define	_PDQTICK_DB	(GCFG->PDQTICK_DB)

	int		PDQAREAFIX_DB;
#define	_PDQAREAFIX_DB	(GCFG->PDQAREAFIX_DB)

	int 		PDQHUB_DB;
#define _PDQHUB_DB	(GCFG->PDQHUB_DB)

	int		SPR2_DB;
	int		SPR3_DB;
	int		SPR4_DB;
	int		SPR5_DB;

	int		NOLOG_ALL;		/* 1=logging, 0=no logging */
#define	_NLALL		(GCFG->NOLOG_ALL)

	int		NOLOG_PDQMAIL;
#define _NLPDQ 		(GCFG->NOLOG_PDQMAIL)

	int		NOLOG_IMP;
#define _NLIMP 		(GCFG->NOLOG_IMP)

	int		NOLOG_EXP;
#define _NLEXP 		(GCFG->NOLOG_EXP)

	int		NOLOG_NET;
#define _NLNET 		(GCFG->NOLOG_NET)

	int		NOLOG_BUNDLE;
#define _NLBUN 		(GCFG->NOLOG_BUNDLE)

	int		NOLOG_TICK;
#define _NLTIC 		(GCFG->NOLOG_TICK)

	int		NOLOG_AREAFIX;
#define _NLARE 		(GCFG->NOLOG_AREAFIX)

	int		NOLOG_HUB;
#define _NLHUB		(GCFG->NOLOG_HUB)

	int		NOLOG_SPR2;
	int		NOLOG_SPR3;
	int		NOLOG_SPR4;
	int		NOLOG_SPR5;

	/*          see DEADWINDOW for overriding stealth setting */

	int		STEALTH_PDQMAIL;		/* 1 means OK to print */
#define _SHOWPDQ	(GCFG->STEALTH_PDQMAIL)

	int		STEALTH_IMP;
#define _SHOWIMP 		(GCFG->STEALTH_IMP)

	int		STEALTH_EXP;
#define _SHOWEXP 		(GCFG->STEALTH_EXP)

	int		STEALTH_NET;
#define _SHOWNET 		(GCFG->STEALTH_NET)

	int		STEALTH_BUNDLE;
#define _SHOWBUN 		(GCFG->STEALTH_BUNDLE)

	int		STEALTH_TICK;
#define _SHOWTIC 		(GCFG->STEALTH_TICK)

	int		STEALTH_AREAFIX;
#define _SHOWARE 		(GCFG->STEALTH_AREAFIX)

	int		STEALTH_HUB;
#define _SHOWHUB		(GCFG->STEALTH_HUB)

	int		STEALTH_SPR2;
	int		STEALTH_SPR3;
	int		STEALTH_SPR4;
	int		STEALTH_SPR5;


	int		MyZone;			/* primary address zone # */
#define	_MyZone		(GCFG->MyZone)

	int		MyNet;			/* primary address net */
#define	_MyNet		(GCFG->MyNet)

	int		MyNode;			/* primary address node */
#define	_MyNode		(GCFG->MyNode)

	int		MyPoint;		/* primary address point */
#define	_MyPoint	(GCFG->MyPoint)

	int		totADDRESS;		/* total addresses in array in use */
#define	_totADDRESS	(GCFG->totADDRESS)

	char		ADDRESS[TOTALAKAS+1][20];	/* primary and up to 9 AKA's */
#define	_ADDRESS	GCFG->ADDRESS

	char __FillA[12];

	char		TDPORT[30];		/* TrapDoor's AREXX port name */
#define	_TDPORT		(GCFG->TDPORT)

	char		BBSPORT[12];		/* BBS port name that Trapdoor sits on */
#define	_BBSPORT	(GCFG->BBSPORT)

	char		ORIGIN[88];		/* our origin string if no other */
#define	_ORIGIN		(GCFG->ORIGIN)

	char		OPERATOR[52];		/* operator's name for logging things */
#define	_OPERATOR	(GCFG->OPERATOR)

	char		CALLTYPES[28];		/* 24 flags to determine default calls for each hour */
#define	_CALLTYPES	(GCFG->CALLTYPES)

	char __FillB[12];

	char		DLGSERIAL[8];
#define _DLGSERIAL	(GCFG->DLGSERIAL)

	char		TEAR[88];		/* we build this ourselves based on other info */
#define	_TEAR		(GCFG->TEAR)

	char		TWOBYTES[12];		/* the last two bytes of the DLG sn */
#define	_TWOBYTES	(GCFG->TWOBYTES)

	char		KEYFILESTRING[200];	/* the string inside the keyfile */
#define	_KEYFILESTRING	(GCFG->KEYFILESTRING)


/* new stuff====================*/


	char		PDQFILESTRING[200];
#define _PDQFILESTRING	(GCFG->PDQFILESTRING)

/* end new stuff================*/



	char		NETMAILDIRPATH[48];		/* path to netmail msg area */
#define	_NETMAILDIRPATH	(GCFG->NETMAILDIRPATH)

	char 		BADMSGSDIRPATH[48];		/* path to bad msgs area */
#define	_BADMSGSDIRPATH	(GCFG->BADMSGSDIRPATH)

	char		VERSION[24];			/* such as "1.00b" */
#define	_VERSION	(GCFG->VERSION)

	char		VERSIONDATE[48];		/* such as "05-Dec-91 3:31 AM" */
#define	_VERSIONDATE	(GCFG->VERSIONDATE)

	char		ARCHIVER[10][4][60];	/* archiver information goes here */
#define zgc_ARC 0
#define zgc_ZOO 1
#define zgc_LZH 2
#define zgc_LHA 3
#define zgc_ZIP 4
#define zgc_ARJ 5

#define zgc_ADD 0
#define zgc_EXTRACT 1
#define zgc_TEST 3

#define 	_ARCADD		(GCFG->ARCHIVER[zgc_ARC][zgc_ADD])
#define 	_ARCEXTRACT	(GCFG->ARCHIVER[zgc_ARC][zgc_EXTRACT])

#define 	_ZOOADD		(GCFG->ARCHIVER[zgc_ZOO][zgc_ADD])
#define 	_ZOOEXTRACT	(GCFG->ARCHIVER[zgc_ZOO][zgc_EXTRACT])

#define 	_LZHADD		(GCFG->ARCHIVER[zgc_LZH][zgc_ADD])
#define 	_LZHEXTRACT	(GCFG->ARCHIVER[zgc_LZH][zgc_EXTRACT])

#define 	_LHAADD		(GCFG->ARCHIVER[zgc_LHA][zgc_ADD])
#define 	_LHAEXTRACT	(GCFG->ARCHIVER[zgc_LHA][zgc_EXTRACT])

#define 	_ZIPADD		(GCFG->ARCHIVER[zgc_ZIP][zgc_ADD])
#define 	_ZIPEXTRACT	(GCFG->ARCHIVER[zgc_ZIP][zgc_EXTRACT])

#define 	_ARJADD		(GCFG->ARCHIVER[zgc_ARJ][zgc_ADD])
#define 	_ARJEXTRACT	(GCFG->ARCHIVER[zgc_ARJ][zgc_EXTRACT])

	int		SECUREIMP;
#define _SECUREIMP 		(GCFG->SECUREIMP)

	int		NOCALLBUNDLE;
#define _NOCALLBUNDLE		(GCFG->NOCALLBUNDLE)


	int		NOCALLPROCESS;
#define _NOCALLPROCESS		(GCFG->NOCALLPROCESS)

	int		FORCEINTL;
#define _FORCEINTL		(GCFG->FORCEINTL)


	int		PCFRIENDLY;
#define _PCFRIENDLY		(GCFG->PCFRIENDLY)

	char		PCADDRESS[20];
#define _PCADDRESS		(GCFG->PCADDRESS)

	char            TDLOCKNAME[20];
#define _TDLOCKNAME		(GCFG->TDLOCKNAME)

	char		TDCFGFILE[30];
#define _TDCFGFILE		(GCFG->TDCFGFILE)

	char		EXPANSION[50];

	int		MARKER2;		/* set this also to 5551212 */
};




#define IN_PKT 1<<0
#define IN_BUN 1<<1
#define IN_TIC 1<<2
#define IN_DMG 1<<3
#define IN_TMP 1<<4
#define IN_ATTACH 1<<5

#define EXPFILE "FIDO:PDQMail.EXP"

#define ACT_PDQIMP 1<<0
#define ACT_PDQEXP 1<<1
#define ACT_PDQNET 1<<2
#define ACT_PDQBUN 1<<3
#define ACT_PDQAFX 1<<4
#define ACT_PDQTIC 1<<5
#define ACT_ATTACH 1<<6
#define ACT_DMC    1<<7
#define ACT_CRASH  1<<8
#define ACT_QUIT   1<<9
#define ACT_PDQHUB 1<<10

#define QUIET 0
#define INFORMATIVE 1
#define TALKATIVE 2
#define VERBOSE 3
#define ANNOYING 4
#define OBNOXIOUS 5

