/* header containing goodies from Ross */

#define ROSSHEADER

extern int MyDEBUG;

#define MAX_STR 50        /* Maximum length of general strings. */
#define DESC_MAX 40       /* Maximum length of area description string. */
#define MAX_LONG_STR 250
#define MAXAREALEN 100
#define MAXNUMBERLEN 100
#define EXTRALENGTH 1000

#ifndef TRUE
   #define TRUE 1
#endif

#ifndef FALSE
   #define FALSE 0
#endif

struct nodenum
{
      struct nodenum *next_nodenum;
      char organization[MAX_STR];
      unsigned short zone;
      unsigned short net;
      unsigned short node;
      unsigned short point;

      long strip;

      char  from_organization[MAX_STR];
      unsigned short from_zone;
      unsigned short from_net;
      unsigned short from_node;
      unsigned short from_point;
};

struct area
{
      struct area *next_area;
      char *description;
      struct nodenum *first_nodenum;
      short passthru;	       /* 0 if not, "true" if passthru */
      short link;              /* 0 if not, "true" if link     */
      short alias;             /* 0 if not, "true" if alias    */
      long areanum;	       /* 0 if passthru is "true"      */
      char path[MAX_STR];      /* read in a number, convert to "MSG:number/" */
      char name[MAX_STR];
      short class;

      char  from_organization[MAX_STR];
      unsigned short from_zone;
      unsigned short from_net;
      unsigned short from_node;
      unsigned short from_point;
    };

struct areas
{
      struct area *first_area;
};


extern short current_strip;
extern char  current_from_organization[MAX_STR];
extern unsigned short current_from_zone;
extern unsigned short current_from_net;
extern unsigned short current_from_node;
extern unsigned short current_from_point;

extern char  default_from_organization[MAX_STR];
extern unsigned short default_from_zone;
extern unsigned short default_from_net;
extern unsigned short default_from_node;
extern unsigned short default_from_point;

extern short default_class;
extern short default_passthru;
extern short default_link;
extern short default_alias;

extern char *linetokenlist[];


#define MYADDRESS  0
#define PASSTHRU   1
#define NOPASSTHRU 2
#define CLASS	   3
#define TAG	   4
#define AREA	   5 /* Synonym for TAG */
#define LINK       6
#define NOLINK     7
#define ALIAS      8
#define NOALIAS    9

extern char *entrytokenlist[];

#define CONT	   0
#define NEWLINE    1
#define USEADDRESS 2
#define STRIP	   3
#define NOSTRIP    4
#define DESC       5

extern int global_error_code;
extern char global_error_string1[MAX_LONG_STR];
extern char global_error_string2[MAX_LONG_STR];

/* ERROR CODES */

#define NO_ERROR    0
#define STRING_LEN  1	 /* String too long to fit in fixed string size. */
#define MEM_ALLOC   2
#define FILE_ACCESS 3
#define PARSE_ERROR 4

