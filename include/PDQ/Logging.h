// Include file for PDQ Logging module

#define STARTLOG  1
#define ENDLOG    2
#define LEVELDOWN 3
#define LEVELUP   4
#define SPECIAL   5

// NewLog()
//
// Accepts 5 arguments:
// 
//    int [map] is one of the items defined above.
//    char [string] is the string to log.
//    char [logfilename] is the path and filename of the logfile.
//    char [buffer] is the current set of logstrings in memory. 
//    int [buffersize] is the max size of the logging buffer
//
// NewLog() assembles the level-dependent stuff with the string to log
// and passes it to QuickLog().

extern int NewLog(int, char *, char *, char *, int);

// QuickLog()
//
// Accepts 4 arguments:
//
//    char [string] is the string to log.
//    char [logfilename] is the path and filename of the logfile.
//    char [buffer] is the current logging buffer.
//    char [buffersize] is the max size of the logging buffer.
//
// QuickLog() actually logs the information to the log file.  It first fills a
// buffer with log strings.  If the latest log string does not overflow the buffer,
// the buffer is kept in memory.  If the buffer is filled, it gets written to disk.

extern int QuickLog(char *,char *, char *,int);

// DTG() (Date-Time Group)
//
// DTG accepts two arguments:
//
//    char [TimeString] is a string received from DLG for the time.
//    int [FidoTime] is a flag indicating whether or not to use Fidonet format.
//
// DTG() returns a formatted time date string.    

extern char *DTG(char *timestring,int FidoTime);


