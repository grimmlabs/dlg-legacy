/*
** Functions included in DLG link library msg.lib
**
 ***************************************************/

BYTE  CallMsgEditor(char *, char *, char *, struct Msg_Area *, char , char *, char , struct USER_DATA *, struct Ram_File *);
int   GetMyAddress(char *BaseAddr, char *MyAddr, struct fido *SrcFido);
BOOL  MakeBody(char *, char *, char *, char *, char **);
BYTE  SendNet(struct Msg_Header *, struct fido *, struct fido *, char *, struct USER_DATA *, int , char *, char *);

BYTE  StartThread(SHORT , SHORT , SHORT );
void  BaseThread(SHORT );
BYTE  IsBaseThread(void);
void  EndThread(void);
SHORT NextMessage(SHORT , SHORT , SHORT , SHORT , SHORT );
SHORT SkipRest(long , long , SHORT , char *);

