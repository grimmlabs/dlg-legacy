/*
** Functions included in DLG link library user.lib
**
 ***************************************************/

BOOL  AddGlobalArea   (SHORT , SHORT , long , char , char , char *);
int   AddGlobalAreas(char *, int , int , unsigned char , char *);
void  BuildPrivFlag(struct Msg_Area *, struct Msg_Log *, int );
void  BuildFPrivFlag(struct Msg_Area *, struct Msg_Log *, int );
SHORT Display_Computer_Types(void);
BYTE  New_User(char *, struct USER_DATA *, char );
LONG  Show_User(char *, struct twocoldata *, char , struct threecoldata *, char );

BYTE OpenQuest(char *);
BOOL NextQuest(struct Question *);
void CloseQuest(void);

