/*
** Functions included in DLG link library config.lib
**
 ***************************************************/

int   CheckArchiver(int number,int level,int mask);
BYTE  CheckEditor(int , int);
BOOL  CheckMenuSet(int , int );
BOOL  CheckSet(int , int );
int   ListArchivers(int level,int mask);
SHORT ListEditors(int);
SHORT ListMenuSets(int );
BOOL  ListSets(int );
int   ReadGlobals(char *, struct Global_Settings *);
BOOL  UseCharSet(UBYTE , char *);

// Color requester

void   ColorReq(UWORD *, int , int , UBYTE , UBYTE );
USHORT ColourMenu(struct Gadget *);
BOOL   OpenColorWindow(int , int , UBYTE , UBYTE );
void   CloseColorWindow(void);

