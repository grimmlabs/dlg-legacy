/*
** Functions included in DLG link library file.lib
**
 ***************************************************/

BYTE  CheckBatch(char *, char *);
BOOL  DescribeFiles(char *, struct USER_DATA *, struct Msg_Area *, struct Ram_File *, char *, char *, char *, LONG , struct Msg_Log *);
LONG  DLTime(LONG , LONG , UBYTE , char *);
BYTE  GetProtocol(char *, struct USER_DATA *, SHORT , char , char *, char *);
BOOL  KillFile(long , char *, long , char *, char , struct Msg_Area *, char , struct USER_DATA *, char *);
SHORT ListProtocols(int , char *, int , char );
SHORT LoadFiles(long , char **, char *);
LONG  LoadProtocol(struct Protocol *, char );
LONG  RawUpload(char *, char *, struct File_Header *, char *, char *, struct Msg_Area *, char *, char );
BOOL  RemoveCredit(char *, LONG , struct USER_DATA *, char *);
BOOL  ResetComment(char *);
void  ScanUp(char *, long , char *, char );
void  ScanUpBatch(char *);
BOOL  TimeString(LONG , char *);

