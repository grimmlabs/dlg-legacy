/*
** Functions included in DLG link library util.lib
**
 ***************************************************/

int   BuildArray(char **, char *, int , int , int );
int   dosort(char *, char *);
char *DTG(char *,int);
BOOL  EditFile(char *, struct USER_DATA *, struct Ram_File *, char *);
BOOL  FileToBuf(char **, char *, long , long , long );
long  GetHandFlags(long , char );
void  LToX(ULONG, char *);
void  ScreenName(UBYTE *);
BOOL  StringToDate(char *, long *);
LONG  TimeLeft(char *);

