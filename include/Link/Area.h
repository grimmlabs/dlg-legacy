/*
** Functions included in DLG link library area.lib
**
 **************************************************/

BOOL  AlignAreas(struct Msg_Area *, char );
BOOL  CheckForSig(int , SHORT *, int );
BYTE _EnterArea(long , UBYTE , UBYTE );
long  MakeSigArray(char *, SHORT **, char );
BOOL  PurgeArea(int , int , long , char );

