/***************************************************************************

 � copyright 1995-98 by Digerati Dreams
   All rights reserved

*****************************************************************************/


#ifndef EXEC_TYPES_H
#include <exec/types.h>
#endif

/*
** Global configuration - DLGCONFIG:Port/<name>.globals
**
 ****************************/

struct Global_Settings
{
  char    BBSName[36];     // name of bbs
  char    GlobalScreen;    /* determines whether local screen opens when
                              users log on */
  char    DefaultScreen;   /* determines whether "snoop" opens on screen
                              or window (no effect if GlobalScreen == 0 */
  char    TimeOut;         // idle timeout in seconds for port
  char    VerbPause;       // selects between verbose pause or silent pause
  SHORT   Session_Limit;   // not used
  SHORT   Daily_Limit;     // not used
  char    ConnectDelay;    // delay between connect and session start
  SHORT   NumUsers;        // current number of users
  char    NewUserLevel;    // not used
  SHORT   DirLimit;        // not used
  char    AutoAccess;      // selects between regular access and private port
  char    NameList[20];    // the groupname required to access private port
  char    DefMenu[18];     // menu that is selected at login
  char    CmdStack[10];    // not used
  SHORT   MinBaud;         // minimum baud rate for port
  char    Ratio;           // not used
  char    Language[22];    // default language for this port
  char    Origin[53];      // not used
  unsigned char CharSet;   // default character set translation for this port
  unsigned char unused[3]; // not used
  SHORT   Flags;           // Flags (see below)
  char    ForcedStack[10]; // command stack forced at login
  SHORT   PrivateArea;     // private message redirect area
};

#define AUTO_ANSI       256     // Do Auto-ANSI detect at login


/*
** Port config - DLGCONFIG:Port/<name>.port
**
 ****************************/

struct Port
{
  char    Device[36];       // normally serial.device
  char    Unit;             // serial device unit number
  char    GlobalFile[36];   // globals file for this port
  char    ModemFile[36];    // modem file for this port
  char    DisplayFile[36];  // display file for this port
  char    dummy;            // not used
};


/*
** Modem config - DLGCONFIG:Port/<name>.modem
**
 *****************************/

struct Modem
{
  char    Init[80];         // modem init string
  char    Hangup[10];       // modem hangup string
  char    Reset[10];        // modem reset string
  char    Lock;             // Lock baud rate?
  char    HangupMethod;     // selects between DTR and command hangup
  long    MaxBaud;          // baud rate to send command to the modem at
  char    Answer[10];       // command to answer the phone
  char    AnswerMethod;     // selects between modem answer and bbs answer
  char    Ring[10];         // ring string
  char    CommandMode[10];  // return to command mode
};


/* The below two structures together make up one Display config
**
 *******************************/

/*
** Window configuration
**
** Defines a local "snoop" or login window, as well as the size and
** position of the sysop's chat window
**
 ********************************/

struct WinStruct
{
  short x, y, width, height;  // window dimensions -- position of upper left
                              // corner followed by width and height of
                              // window. 
							  
  char fontname[41];          // font name (case sensitive)
  UBYTE fontsize;             // size of font
  UBYTE flags;                // see display flags below
};

/*
** Screen configuration
**
** Used for local login or snoop screen
**
 *******************************/

struct ScrStruct
{
  short width, height, depth; // Width and height of screen, how many bitplanes
  UBYTE hires, interlace;     // screen modes -- high res and interlace flags
  char fontname[41];          // font name (case sensitive)
  UBYTE fontsize;             // size of font
  UBYTE flags;                // see display flags below
  UWORD colortable[8];        // array defining each of the 8 colors
};

/*
** Display flags -- used with above two structs
**
 *******************************/

#define DISP_BKGRND  1  // Open window/screen in background


/*
** The two above structures as found in DLGCONFIG:Port/<name>.display
**
 *******************************/

struct Displays
{
  struct ScrStruct Screen;
  struct WinStruct Window;
};

