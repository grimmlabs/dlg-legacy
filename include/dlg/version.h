/***************************************************************************

 � copyright 1995-99 by Digerati Dreams
   All rights reserved

*****************************************************************************/

#include <private/Version.h>


#define DLGSHORTNAME "DLG v" BUILDVER
#define DLGLONGNAME "DLG BBS v" BUILDVER " - Copyright �" COPYRIGHT " by Digerati Dreams"

#define DEMO 1

// Comment the following out if compiling a demo

#ifdef DEMO
   #undef DEMO
#endif
