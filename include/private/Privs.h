/*
** Privs.h - 5 Apr 1999 -  Defines the priv codes for DLG addon modules.
**
** (c) 1999 by Digerati Dreams
**
** This information is proprietary and confidential. You may not disclose
** this information to anyone without the direct written permission of
** Digerati Dreams.
**
** The information in this include file is used by the resource manager
** to determine the clearance codes of a particular user to use add-on
** modules of DLG. By turning these on and off, we can enable and disable
** those modules.  This information is also used to determine if a package
** is in fact a demo.
 **************************************************************************/

#define  BOSS     1     // Enables ALL modules, reserved for beta testers
#define  BASIC    2     // Enable core modules
#define  FIDO     4     // Enable fido modules
#define  TELNET   8     // Enable telnet-able modules

#define  ISDEMO()    ( DLGCW == 0 )
#define  ISBOSS()    ( DLGCW & BOSS )
#define  ISBASIC()   ( DLGCW & BASIC )
#define  ISFIDO()    ( DLGCW & FIDO )
#define  ISTELNET()  ( DLGCW & TELNET )

