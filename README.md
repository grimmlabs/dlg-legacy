# README #

This is a public release of the source code for DLG Pro BB/OS 2.x. It is released without warranty
of any kind.

## What's in the box? ##

* Source code for DLG Pro and all subsystems, including DLG(PDQ)Mail and its associated bits.
* TrapDoor source is not included.

## What you need ##

* SAS/C 6.50 or thereabouts. 
* AmigaOS SDK

## Caveats ##

* No idea how you might build this today
* Feel free to contribute build instructions etc for whatever platform you are on.  I'll add them to the repo.
* Free free to send queries to grimmtooth@gmail.com about this code. Be aware that I will not be able
  to aid you in any way other than blah blah blah talking. I don't have a compiler or system to test anything
  of any sort on.
* Nothing here has anything to do with work on DLG3. Nothing.