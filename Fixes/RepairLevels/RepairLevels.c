#include <exec/types.h>
#include <stdlib.h>
#include <stdio.h>
//#include <string.h>
//#include <ctype.h>

//#include <dlg/dlg.h>
//#include <dlg/resman.h>
#include <dlg/user.h>

#include <proto/exec.h>
#include <proto/dos.h>
#include <proto/dlg.h>

#include <pragmas/dlg.h>

const UBYTE version[]="\0$VER: RepairLevels 1.0  �1999 by Digerati Dreams "__AMIGADATE__;

void main   (int, char **);
void CleanUp(int, char *);
BOOL Cycle_Through(ULONG);

struct Library *DLGBase = NULL;
struct NameStruct *ns = NULL;

/// Main
void main(int argc, char **argv)
{
   int rc = 0;

   DLGBase = OpenLibrary("dlg.library", 4L); if (!DLGBase)  CleanUp(10,"Unable to open dlg.library");

   if(Exists("DLG:FixUsers"))
   {
      printf("Running FixUsers .... \n");

      if(Execute("DLG:FixUsers",NULL,NULL))
      {
         ULONG fsize = 0;

         if(0 == FileSize("DLGConfig:Misc/Users.bbs",&fsize))
         {
            ns = calloc(1, fsize);

            if(ns != NULL)
            {
               printf("Loading users .... ");

               if(0 == GetFirstStruct("DLGConfig:Misc/Users.BBS",(char *)ns,fsize))
               {
                  ULONG l;

                  printf("done!\n\n");

                  l = fsize / sizeof(struct NameStruct);

                  if(Cycle_Through(l)) rc = 10;

               }  // end GetFirstStruct (users)
               else
               {
                  CleanUp(10, "Error loading users!");
               }

               free(ns); ns = NULL;

            }  // end calloc(ns)
            else
            {
               CleanUp(10, "Out of memory!");
            }

         }  // end FileSize(users.bbs)
         else
         {
            CleanUp(10, "Could not find Users.BBS!");
         }

      }  // end Execute(FixUsers)
      else
      {
         CleanUp(10, "FixUsers found a problem!");
      }

   }  // end Exists(Fixusers)
   else
   {
      CleanUp(10, "Could not locate FixUsers!");
   }

   CleanUp(rc,NULL);
}
//-
/// CleanUp
void CleanUp(int rc, char *s)
{
   if (ns)       free(ns); ns = NULL;
   if (DLGBase)  CloseLibrary(DLGBase);
   if (s) printf("\nError: %s\n\n",s);
   exit(rc);
}
//-
/// Cycle_Through()
BOOL  Cycle_Through(ULONG users)
{
   BOOL  rc = FALSE;
   ULONG i;

   for(i = 0; i < users; i++)
   {
      char  t[64];
      ULONG size = 0;

      printf("Reading %-30.30s ... ",ns[i].name);

      sprintf(t,"User:%s/user.data",ns[i].name);
      UnderScore(t);

      if(0 == FileSize(t,&size))
      {
         struct   USER_DATA   UDat;

         if(0 == GetFirstStruct(t,(char *)&UDat,sizeof(struct USER_DATA)))
         {
            printf("scanning ... ");

            if( (UDat.User_Level < 1) || (UDat.User_Level > 255))
            {
               BPTR  fh =  NULL;

               rc = TRUE;

               printf("repairing ... ");

               if(UDat.User_Level < 1)   UDat.User_Level = 1;
               if(UDat.User_Level > 255) UDat.User_Level = 254;

               if(fh = Open(t,MODE_NEWFILE))
               {
                  Write(fh,&UDat,sizeof(struct USER_DATA));
                  Close(fh); fh = NULL;
               }  // end Open()
               else
               {
                  printf("error writing\n");
                  continue;
               }
            }  // end level check

         }  // end GetFirstStruct(userdat)
         else
         {
            printf("error reading\n");
            continue;
         }

      }  // end FileSize(userdat)
      else
      {
         printf("error reading\n");
         continue;
      }

      printf("done!\n");
   }

   return(rc);
}
//-
