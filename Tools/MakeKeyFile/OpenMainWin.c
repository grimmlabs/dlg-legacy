#include <exec/types.h>
#include <string.h>

#include <clib/alib_protos.h>

#include <private/privs.h>

#include <libraries/bgui.h>
#include <libraries/bgui_macros.h>

#include <proto/bgui.h>
#include <proto/intuition.h>

#include "IDs.h"

Object  *MKF_Main       =  NULL;

Object  *MKF_Serial     =  NULL;
Object  *MKF_Name       =  NULL;
Object  *MKF_Go         =  NULL;
Object  *MKF_Quit       =  NULL;
Object  *MKF_Boss       =  NULL;
Object  *MKF_Basic      =  NULL;
Object  *MKF_Fido       =  NULL;
Object  *MKF_TNet       =  NULL;

char     OwnerName[256];

long     SerNo          =  0;

void RefreshChex(struct Window *win);

extern   char              DLGCW;

extern   struct   MsgPort *SharedPort;

extern   Object *myButton(char *label, LONG id, char *Tip, BOOL Enabled);

/// OpenMainWin()
struct Window *OpenMainWindow( void )
{
   struct Window     *mw = NULL;

/// Window def
if(!MKF_Main)
{
MKF_Main =
   WindowObject,
   WINDOW_Title,     "MakeKeyFile �1999 Digerati Dreams",
   WINDOW_SizeGadget,   TRUE,
   WINDOW_SmartRefresh, TRUE,   WINDOW_SharedPort,   SharedPort,
   WINDOW_AutoAspect,   TRUE,
   WINDOW_AutoKeyLabel, TRUE,
   WINDOW_CloseOnEsc,   TRUE,
   WINDOW_ScaleWidth,   25,
//   WINDOW_Bounds,(CheckIBox(&MainWinBox) ? &MainWinBox : NULL),
   WINDOW_IDCMP,IDCMP_CHANGEWINDOW,
//   WINDOW_IDCMPHook,&SizeHook,
   WINDOW_IDCMPHookBits,IDCMP_CHANGEWINDOW,
   WINDOW_ToolTicks,5,
   WINDOW_MasterGroup,
      VGroupObject, HOffset( 1 ), VOffset( 1 ), Spacing( 2 ),

         StartMember,
            MKF_Serial  =  StringObject,
                           LAB_Label,"_Serial Number ",
                           LAB_Place,PLACE_LEFT,
                           RidgeFrame,
                           STRINGA_LongVal,SerNo,
                           STRINGA_MaxChars,6,
                           GA_ID,ID_SerNo,
                           GA_TabCycle,TRUE,
                           BT_ToolTip,"Serial Number",
            EndObject,
            FixMinHeight,
         EndMember,

         StartMember,
            MKF_Name =  StringObject,
                           LAB_Label,"Owner _Name ",
                           Place(PLACE_LEFT),
                           RidgeFrame,
                           STRINGA_TextVal,OwnerName,
                           STRINGA_MaxChars,255,
                           GA_ID,ID_Name,
                           GA_TabCycle,TRUE,
                           BT_ToolTip,"Owner's Name",
            EndObject,
            FixMinHeight,
         EndMember,

         StartMember,HorizSeperator,EndMember,

         StartMember,
            HGroupObject, HOffset( 1 ), VOffset( 1 ), Spacing( 2 ),

               StartMember,
                  MKF_Boss =  CheckBoxObject,
                              LAB_Label,"_Boss ",
                              LAB_Place,PLACE_RIGHT,
                              ButtonFrame,
                              GA_Selected,ISBOSS(),
                              GA_ID,ID_Boss,
                              BT_ToolTip,"Boss System?",
                  EndObject,
                  FixMinSize,
               EndMember,

               StartMember,
                  MKF_Basic = CheckBoxObject,
                              LAB_Label,"B_asic ",
                              LAB_Place,PLACE_RIGHT,
                              ButtonFrame,
                              GA_Selected,ISBASIC(),
                              GA_ID,ID_Basic,
                              BT_ToolTip,"Basic System?",
                              GA_DISABLED,ISBOSS(),
                  EndObject,
                  FixMinSize,
               EndMember,

               StartMember,
                  MKF_Fido = CheckBoxObject,
                              LAB_Label,"_Fido ",
                              LAB_Place,PLACE_RIGHT,
                              ButtonFrame,
                              GA_Selected,ISFIDO(),
                              GA_ID,ID_Fido,
                              BT_ToolTip,"Fido System?",
                              GA_DISABLED,ISBOSS(),
                  EndObject,
                  FixMinSize,
               EndMember,

               StartMember,
                  MKF_TNet = CheckBoxObject,
                              LAB_Label,"_Telnet ",
                              LAB_Place,PLACE_RIGHT,
                              ButtonFrame,
                              GA_Selected,ISTELNET(),
                              GA_ID,ID_Telnet,
                              BT_ToolTip,"Telnet System?",
                              GA_DISABLED,ISBOSS(),
                  EndObject,
                  FixMinSize,
               EndMember,

            EndObject,
         EndMember,

         StartMember,HorizSeperator,EndMember,

         StartMember,
            HGroupObject, HOffset( 1 ), VOffset( 1 ), Spacing( 2 ),

               StartMember,
                  MKF_Go      = myButton( "_Go!", ID_Go, "Let's go!", FALSE ),
               EndMember,

               StartMember,
                  MKF_Quit    = myButton( "_Quit", ID_Quit, "Bye bye!", TRUE ),
               EndMember,

            EndObject,
         EndMember,

      EndObject,
   EndObject;
}
//-

   if ( MKF_Main )
   {
      if ( mw = WindowOpen( MKF_Main ))
      {
         strcpy(OwnerName,"Demo");
         SetGadgetAttrs((struct Gadget *)MKF_Serial,mw,NULL,STRINGA_LongVal,SerNo);
         SetGadgetAttrs((struct Gadget *)MKF_Name,mw,NULL,STRINGA_TextVal,OwnerName);
         return( mw );
      }
   }

   return(NULL);
}
//-
/// RefreshChex()
void RefreshChex(struct Window *win)
{

   SetGadgetAttrs((struct Gadget *)MKF_Boss,win,NULL,GA_Selected,ISBOSS());

   if(ISBOSS())
   {
      SetGadgetAttrs((struct Gadget *)MKF_Basic,win,NULL,GA_DISABLED,TRUE);
      SetGadgetAttrs((struct Gadget *)MKF_Fido,win,NULL,GA_DISABLED,TRUE);
      SetGadgetAttrs((struct Gadget *)MKF_TNet,win,NULL,GA_DISABLED,TRUE);
   }
   else
   {
      SetGadgetAttrs((struct Gadget *)MKF_Basic,win,NULL,GA_Selected,ISBASIC(),GA_DISABLED,FALSE);
      SetGadgetAttrs((struct Gadget *)MKF_Fido,win,NULL,GA_Selected,ISFIDO(),GA_DISABLED,FALSE);
      SetGadgetAttrs((struct Gadget *)MKF_TNet,win,NULL,GA_Selected,ISTELNET(),GA_DISABLED,FALSE);
   }

   return;
}
//-

